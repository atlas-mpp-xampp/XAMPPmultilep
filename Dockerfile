FROM atlas/athanalysis:21.2.96
ADD . /xampp/XAMPPmultilep
WORKDIR /xampp/build
RUN source ~/release_setup.sh &&  \
    sudo chown -R atlas /xampp && \
    cmake ../XAMPPmultilep && \
    make -j4
