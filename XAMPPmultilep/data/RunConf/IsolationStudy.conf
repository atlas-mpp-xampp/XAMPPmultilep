
Tree FourLepton
Analysis FourLepton
Weights GenWeight

xSecDir XAMPPplotting/xSecFiles/

## Define good quality electrons ignoring the isolation WP
New_Particle GQelecs elec
    Cut char signal = 1    
End_Particle
New_Particle GQmuons muon
    Cut char signal = 1
End_Particle
DiParticle GQLeptons GQmuons GQelecs

## Isol is the gradient WP
New_Particle SignalMuons muon
    Cut char isol = 1
    Cut char signal = 1
End_Particle

### Define foreach isol WP a muon collection
New_Particle FCLooseMuons muon
    Cut char isol_FCLoose = 1
    Cut char signal = 1
End_Particle
New_Particle FCLooseFixedRadMuons muon
    Cut char isol_FCLoose_FixedRad = 1 
    Cut char signal = 1
End_Particle
New_Particle FCTightMuons muon
    Cut char isol_FCTight = 1  
    Cut char signal = 1
End_Particle
New_Particle FCTightTrkMuons muon
    Cut char isol_FCTightTrackOnly = 1
    Cut char signal = 1
End_Particle
New_Particle FCTightTrkFixedRadMuons muon
    Cut char isol_FCTightTrackOnly_FixedRad = 1
    Cut char signal = 1
End_Particle
New_Particle FCTightFixedRadMuons muon
    Cut char isol_FCTight_FixedRad = 1
    Cut char signal = 1
End_Particle
New_Particle FCTightHighPtMuons muon
    Cut char isol_FixedCutHighPtTrackOnly = 1
    Cut char signal = 1
End_Particle

### Same game for the electrons
New_Particle SignalElectrons elec
    Cut char signal = 1
    Cut char isol = 1
End_Particle
New_Particle FCHighPtCaloOnlyElectrons elec
    Cut char isol_FCHighPtCaloOnly = 1
    Cut char signal = 1
End_Particle

New_Particle FCLooseElectrons elec
    Cut char isol_FCLoose = 1
    Cut char signal = 1
End_Particle


New_Particle FCLooseFixedRadElectrons elec
    Cut char isol_FCLoose_FixedRad = 1 
    Cut char signal = 1
End_Particle
New_Particle FCTightElectrons elec
    Cut char isol_FCTight = 1  
    Cut char signal = 1
End_Particle
New_Particle FCTightTrkFixedRadElectrons elec
    Cut char isol_FCTightTrackOnly_FixedRad = 1
    Cut char signal = 1
End_Particle
New_Particle FCTightFixedRadElectrons elec
    Cut char isol_FCTight_FixedRad = 1
    Cut char signal = 1
End_Particle

New_Particle PFlowLooseElectrons elec
    Cut char isol_FixedCutPflowLoose = 1
    Cut char signal = 1
End_Particle

New_Particle PFlowTightElectrons elec
    Cut char isol_FixedCutPflowTight = 1
    Cut char signal = 1
End_Particle

New_ClassReader MuonIsolation
    New_Class
        PseudoReader BinClass 1
        NumParCut GQmuons >  1
    End_Class
    New_Class
        PseudoReader BinClass 2
        CombCut AND
            SumNumParCut GQelecs SignalMuons >= 4        
            CombCut OR
                NumParCut GQelecs < 4
                NumParCut GQmuons >= 4
            End_CombCut
        End_CombCut
    End_Class
    New_Class
        PseudoReader BinClass 3
        CombCut AND
            SumNumParCut GQelecs FCLooseMuons >= 4
            CombCut OR
                NumParCut GQelecs < 4
                NumParCut GQmuons >= 4
            End_CombCut
        End_CombCut
    End_Class
    New_Class
        PseudoReader BinClass 4
        CombCut AND
            SumNumParCut GQelecs FCLooseFixedRadMuons >= 4
            CombCut OR
                NumParCut GQelecs < 4
                NumParCut GQmuons >= 4
            End_CombCut
        End_CombCut
    End_Class
    New_Class
        PseudoReader BinClass 5
        CombCut AND
            SumNumParCut GQelecs FCTightMuons >= 4
            CombCut OR
                NumParCut GQelecs < 4
                NumParCut GQmuons >= 4
            End_CombCut
        End_CombCut
    End_Class
    New_Class
        PseudoReader BinClass 6
        CombCut AND
            SumNumParCut GQelecs FCTightTrkMuons >= 4
            CombCut OR
                NumParCut GQelecs < 4
                NumParCut GQmuons >= 4
            End_CombCut
        End_CombCut
    End_Class
    New_Class
        PseudoReader BinClass 7
        CombCut AND
            SumNumParCut GQelecs FCTightTrkFixedRadMuons >= 4
            CombCut OR
                NumParCut GQelecs < 4
                NumParCut GQmuons >= 4
            End_CombCut
        End_CombCut
    End_Class
    New_Class
        PseudoReader BinClass 8
        CombCut AND
            SumNumParCut GQelecs FCTightFixedRadMuons >= 4
            CombCut OR
                NumParCut GQelecs < 4
                NumParCut GQmuons >= 4
            End_CombCut
        End_CombCut
    End_Class
    New_Class
        PseudoReader BinClass 9
        CombCut AND
            SumNumParCut GQelecs FCTightHighPtMuons >= 4
            CombCut OR
                NumParCut GQelecs < 4
                NumParCut GQmuons >= 4
            End_CombCut
        End_CombCut
    End_Class
End_ClassReader

###
New_ClassReader ElectronIsolation
    New_Class
        PseudoReader BinClass 1
        NumParCut GQelecs >  1
    End_Class
    New_Class
        PseudoReader BinClass 2
        CombCut AND
            SumNumParCut GQmuons SignalElectrons >= 4
            CombCut OR
                NumParCut GQelecs >= 4
                NumParCut GQmuons < 4
            End_CombCut
        End_CombCut
    End_Class
    New_Class
        PseudoReader BinClass 3
        CombCut AND
            SumNumParCut GQmuons FCLooseFixedRadElectrons >= 4
            CombCut OR
                NumParCut GQelecs >= 4
                NumParCut GQmuons < 4
            End_CombCut
        End_CombCut
    End_Class
    New_Class
        PseudoReader BinClass 4
        CombCut AND
            SumNumParCut GQmuons FCTightElectrons >= 4
            CombCut OR
                NumParCut GQelecs >= 4
                NumParCut GQmuons < 4
            End_CombCut
        End_CombCut
    End_Class
    New_Class
        PseudoReader BinClass 5
        CombCut AND
            SumNumParCut GQmuons FCTightTrkFixedRadElectrons >= 4
            CombCut OR
                NumParCut GQelecs >= 4
                NumParCut GQmuons < 4
            End_CombCut
        End_CombCut
    End_Class
    New_class
        PseudoReader BinClass 6
        CombCut AND
            SumNumParCut GQmuons FCTightFixedRadElectrons >= 4
            CombCut OR
                NumParCut GQelecs >= 4
                NumParCut GQmuons < 4
            End_CombCut
        End_CombCut
    End_Class
    New_Class
        PseudoReader BinClass 7
        CombCut AND
            SumNumParCut GQmuons PFlowLooseElectrons >= 4
            CombCut OR
                NumParCut GQelecs >= 4
                NumParCut GQmuons < 4
            End_CombCut
        End_CombCut
    End_Class
    New_Class
        PseudoReader BinClass 8
        CombCut AND
            SumNumParCut GQmuons PFlowTightElectrons >= 4
            CombCut OR
                NumParCut GQelecs >= 4
                NumParCut GQmuons < 4
            End_CombCut
        End_CombCut
    End_Class
    New_Class
        PseudoReader BinClass 9
        CombCut AND
            SumNumParCut GQmuons FCHighPtCaloOnlyElectrons >= 4
            CombCut OR
                NumParCut GQelecs >= 4
                NumParCut GQmuons < 4
            End_CombCut
        End_CombCut
    End_Class
    New_Class
        PseudoReader BinClass 10
        CombCut AND
            SumNumParCut GQmuons FCLooseElectrons >= 4
            CombCut OR
                NumParCut GQelecs >= 4
                NumParCut GQmuons < 4
            End_CombCut
        End_CombCut
    End_Class
        FCLooseElectrons
        
End_ClassReader


### Only look at events with at least four good leptons
NumParCut GQLeptons >= 4
Region 4L

Region 4LnoZ
EvCut int ZVeto = 1

Region SR0A
EvCut int ZVeto = 1
EvCut floatGeV Signal_Meff > 600 

Region SR0B
EvCut int ZVeto = 1
EvCut floatGeV Signal_Meff > 1100

Region VR0
EvCut int ZVeto = 1
EvCut floatGeV Signal_Meff < 600

Region VR0Z
EvCut floatGeV Signal_Mll_Z1 >= 81.2
EvCut floatGeV Signal_Mll_Z1 <= 101.2

EvCut floatGeV Signal_Mll_Z2 > 0.
CombCut OR
    EvCut floatGeV Signal_Mll_Z2 < 61.2
    EvCut floatGeV Signal_Mll_Z2 > 101.2
End_CombCut

Region SR0C
EvCut floatGeV Signal_Mll_Z1 >= 81.2
EvCut floatGeV Signal_Mll_Z1 <= 101.2

EvCut floatGeV Signal_Mll_Z2 >= 61.2
EvCut floatGeV Signal_Mll_Z2 <= 101.2


EvCut floatGeV MetTST_met >= 50

Region SR0D
EvCut floatGeV Signal_Mll_Z1 >= 81.2
EvCut floatGeV Signal_Mll_Z1 <= 101.2

EvCut floatGeV Signal_Mll_Z2 >= 61.2
EvCut floatGeV Signal_Mll_Z2 <= 101.2


EvCut floatGeV MetTST_met >= 100

    


