###############################################################################
#                      Tau CR1  - Loose tau                                   #
###############################################################################
Import XAMPPmultilep/RunConf/RunConfig.conf

defineMultiLine 3L1t_Req
    NumParCut SignalLightLep = 3
    NumParCut SignalInclTaus = 0
    NumParCut LooseTaus >= 1
    NumParCut LooseLightLep = 0
End_MultiLine

Region 3L1t
    @3L1t_Req

Region 3L1t_bveto
    @3L1t_Req
    NumParCut BJets = 0

Region 3L1t_bonly
    @3L1t_Req
    NumParCut BJets > 0
###############################################################################
#                      Tau CR1  - Loose tau  Z requirement                    #
###############################################################################
defineMultiLine 3L1tZ_Req
    @3L1t_Req
    EvCut floatGeV Base_Mll_Z1 >= 81.2
    EvCut floatGeV Base_Mll_Z1 <= 101.2
End_MultiLine

Region 3L1tZ
    @3L1tZ_Req

Region 3L1tZ_bveto
    @3L1tZ_Req
    NumParCut BJets = 0

Region 3L1tZ_bonly
    @3L1tZ_Req
    NumParCut BJets > 0

###############################################################################
#                      Tau CR1  - Loose tau  ZZ requirement                   #
#          --> physically not possible
###############################################################################

###############################################################################
#                      Tau CR1  - Loose tau  Z-Veto                           #
###############################################################################
defineMultiLine 3L1tnoZ_Req
    @3L1t_Req
    EvCut int Base_ZVeto = 1
End_MultiLine

Region 3L1tnoZ
    @3L1tnoZ_Req

Region 3L1tnoZ_bveto
    @3L1tnoZ_Req
    NumParCut BJets = 0

Region 3L1tnoZ_bonly
    @3L1tnoZ_Req
    NumParCut BJets > 0


###############################################################################
###############################################################################
#   1 Tight tau in combination with a loose tau
###############################################################################
defineMultiLine 2L1T1t_Req
    NumParCut SignalLightLep = 2
    NumParCut SignalInclTaus = 1
    NumParCut LooseTaus >= 1
    NumParCut LooseLightLep = 0
End_MultiLine

Region 2L1T1t
    @2L1T1t_Req


Region 2L1T1t_bveto
    @2L1T1t_Req
    NumParCut BJets = 0

Region 2L1T1t_bonly
    @2L1T1t_Req
    NumParCut BJets > 0

###############################################################################
#                      Tau CR1  - Loose   Z requirement                       #
###############################################################################
defineMultiLine 2L1T1tZ_Req
    @2L1T1t_Req    
    EvCut floatGeV Base_Mll_Z1 >= 81.2
    EvCut floatGeV Base_Mll_Z1 <= 101.2
End_MultiLine

Region 2L1T1tZ
    @2L1T1tZ_Req

Region 2L1T1tZ_bveto
    @2L1T1tZ_Req
    NumParCut BJets = 0

Region 2L1T1tZ_bonly
    @2L1T1tZ_Req
    NumParCut BJets > 0

###############################################################################
#                      Tau CR1  - Loose tau  ZZ requirement                   #
#       --> physically not possible
###############################################################################

###############################################################################
#                      Tau CR1  - Loose tau  Z-Veto                           #
###############################################################################
defineMultiLine 2L1T1tnoZ_Req
    @2L1T1t_Req
    EvCut int Base_ZVeto = 1
End_MultiLine

Region 2L1T1tnoZ
    @2L1T1tnoZ_Req

Region 2L1T1tnoZ_bveto
    @2L1T1tnoZ_Req
    NumParCut BJets = 0

Region 2L1T1tnoZ_bonly
    @2L1T1tnoZ_Req
    NumParCut BJets > 0


###############################################################################
#       1 tight tau in combination with a loose light lepton
###############################################################################
#                      Tau CR1  - Loose lepton                                #
###############################################################################
defineMultiLine 2L1T1l_Req
    NumParCut SignalLightLep = 2
    NumParCut LooseLightLep = 1
    NumParCut SignalInclTaus = 1
End_MultiLine

Region 2L1T1l
    @2L1T1l_Req


Region 2L1T1l_bveto
    @2L1T1l_Req
    NumParCut BJets = 0

Region 2L1T1l_bonly
    @2L1T1l_Req
    NumParCut BJets > 0

###############################################################################
#                      Tau CR1  - Loose lepton  Z requirement                 #
###############################################################################
defineMultiLine 2L1T1lZ_Req
    @2L1T1l_Req    
    EvCut floatGeV Base_Mll_Z1 >= 81.2
    EvCut floatGeV Base_Mll_Z1 <= 101.2
End_MultiLine

Region 2L1T1lZ
    @2L1T1lZ_Req

Region 2L1T1lZ_bveto
    @2L1T1lZ_Req
    NumParCut BJets = 0

Region 2L1T1lZ_bonly
    @2L1T1lZ_Req
    NumParCut BJets > 0

###############################################################################
#                      Tau CR1  - Loose lepton  ZZ requirement                #
#       --> physically not possible                                           #
###############################################################################

###############################################################################
#                      Tau CR1  - Loose lepton  Z-Veto                        #
###############################################################################
defineMultiLine 2L1T1lnoZ_Req
    @2L1T1l_Req
    EvCut int Base_ZVeto = 1
End_MultiLine

Region 2L1T1lnoZ
    @2L1T1lnoZ_Req

Region 2L1T1lnoZ_bveto
    @2L1T1lnoZ_Req
    NumParCut BJets = 0

Region 2L1T1lnoZ_bonly
    @2L1T1lnoZ_Req
    NumParCut BJets > 0
    
