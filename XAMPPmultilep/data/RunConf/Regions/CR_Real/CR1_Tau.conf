###############################################################################
#                      Tau CR1  - Loose tau                                   #
###############################################################################
Import XAMPPmultilep/RunConf/Regions/CR/CR1_Tau.conf

ifdef isMC
defineMultiLine Real3L1t_Req
    NumParCut RealSignalLightLep = 3
    NumParCut RealLooseInclTau >= 1
End_MultiLine

Region Real3L1t
    @Real3L1t_Req
    @3L1t_Req


Region Real3L1t_bveto
    @Real3L1t_Req
    @3L1t_Req
    NumParCut BJets = 0

Region Real3L1t_bonly
    @Real3L1t_Req
    @3L1t_Req
    NumParCut BJets > 0

###############################################################################
#                      Tau CR1  - Loose tau  Z requirement                    #
###############################################################################
defineMultiLine Real3L1tZ_Req
    @Real3L1t_Req
    @3L1tZ_Req
End_MultiLine

Region Real3L1tZ
    @Real3L1tZ_Req

Region Real3L1tZ_bveto
    @Real3L1tZ_Req
    NumParCut BJets = 0

Region Real3L1tZ_bonly
    @Real3L1tZ_Req
    NumParCut BJets > 0

###############################################################################
#                      Tau CR1  - Loose tau  ZZ requirement                   #
#          --> physically not possible
###############################################################################

###############################################################################
#                      Tau CR1  - Loose tau  Z-Veto                           #
###############################################################################
defineMultiLine Real3L1tnoZ_Req
    @Real3L1t_Req
    @3L1tnoZ_Req
End_MultiLine

Region Real3L1tnoZ
    @Real3L1tnoZ_Req

Region Real3L1tnoZ_bveto
    @Real3L1tnoZ_Req
    NumParCut BJets = 0

Region Real3L1tnoZ_bonly
    @Real3L1tnoZ_Req
    NumParCut BJets > 0

###############################################################################
###############################################################################
#   1 Tight tau in combination with a loose tau
###############################################################################
defineMultiLine Real2L1T1tReq
    NumParCut RealSignalLightLep = 2
    NumParCut RealSignalInclTau = 1
    NumParCut RealLooseInclTau >= 1
End_MultiLine

Region Real2L1T1t
    @Real2L1T1tReq
    @2L1T1t_Req


Region Real2L1T1t_bveto
    @Real2L1T1tReq
    @2L1T1t_Req
    NumParCut BJets = 0

Region Real2L1T1t_bonly
    @Real2L1T1tReq
    @2L1T1t_Req
    NumParCut BJets > 0

###############################################################################
#                      Tau CR1  - Loose   Z requirement                       #
###############################################################################
defineMultiLine Real2L1T1tZ_Req
    @Real2L1T1tReq
    @2L1T1tZ_Req    
End_MultiLine

Region Real2L1T1tZ
    @Real2L1T1tZ_Req

Region Real2L1T1tZ_bveto
    @Real2L1T1tZ_Req
    NumParCut BJets = 0

Region Real2L1T1tZ_bonly
    @Real2L1T1tZ_Req
    NumParCut BJets > 0
    
###############################################################################
#                      Tau CR1  - Loose tau  ZZ requirement                   #
#       --> physically not possible
###############################################################################

###############################################################################
#                      Tau CR1  - Loose tau  Z-Veto                           #
###############################################################################
defineMultiLine Real2L1T1tnoZ_Req
   @Real2L1T1tReq
   @2L1T1tnoZ_Req
End_MultiLine

Region Real2L1T1tnoZ
    @Real2L1T1tnoZ_Req

Region Real2L1T1tnoZ_bveto
    @Real2L1T1tnoZ_Req
    NumParCut BJets = 0

Region Real2L1T1tnoZ_bonly
    @Real2L1T1tnoZ_Req
    NumParCut BJets > 0

###############################################################################
#       1 tight tau in combination with a loose light lepton
###############################################################################
#                      Tau CR1  - Loose lepton                                #
###############################################################################

defineMultiLine Real2L1T1lReq
    NumParCut RealSignalLightLep = 2
    NumParCut RealLooseLightLep = 1
    NumParCut RealSignalInclTau = 1
End_MultiLine


Region Real2L1T1l
    @Real2L1T1lReq
    @2L1T1l_Req


Region Real2L1T1l_bveto
    @Real2L1T1lReq
    @2L1T1l_Req
    NumParCut BJets = 0

Region Real2L1T1l_bonly
    @Real2L1T1lReq
    @2L1T1l_Req
    NumParCut BJets > 0

###############################################################################
#                      Tau CR1  - Loose lepton  Z requirement                 #
###############################################################################
defineMultiLine Real2L1T1lZ_Req
    @Real2L1T1lReq
    @2L1T1lZ_Req    
End_MultiLine

Region Real2L1T1lZ
    @Real2L1T1lZ_Req

Region Real2L1T1lZ_bveto
    @Real2L1T1lZ_Req
    NumParCut BJets = 0

Region Real2L1T1lZ_bonly
    @Real2L1T1lZ_Req
    NumParCut BJets = 0

###############################################################################
#                      Tau CR1  - Loose lepton  ZZ requirement                #
#       --> physically not possible                                           #
###############################################################################

###############################################################################
#                      Tau CR1  - Loose lepton  Z-Veto                        #
###############################################################################
defineMultiLine Real2L1T1lnoZ_Req
    @Real2L1T1lReq
    @2L1T1lnoZ_Req
End_MultiLine

Region Real2L1T1lnoZ
    @Real2L1T1lnoZ_Req

Region Real2L1T1lnoZ_bveto
    @Real2L1T1lnoZ_Req
    NumParCut BJets = 0

Region Real2L1T1lnoZ_bonly
    @Real2L1T1lnoZ_Req
    NumParCut BJets = 0
    
endif
