#   Define a fat fake factor weight containing all four lepton types (1PTraus, 3PTaus, Electrons, Muons)
#   Add from the beginning the FakeRatios to the weight and freeze each of their signs. 
#   Scale-factors can be added at a second stage as well as the process fractions
#   Make sure that the scale-factors have also their frozen sign. Use the process fraction to
#   control the overall sign of the fake-factor weight in the event... Also make sure to
#   impose a cut on each process fraction component such that it only enters the fake-factor
#   if the kinematic selection of the region is satisfied...

defineVar designnumber 2
defineVar negative_ff no
defineMultiLine FFweightdefinition_tau
New_FakeFactorWeight FakeFactorWeight    
    SetLumi 138.260826904 
    # FakeFactor Tau LF ttbar 
    AddFake
        FreezeSign
        Particle Loose1PTaus 
        FakeFactorHisto @{PathToFFfile}/FakeFactors_ttbar_tau.root LF_Nominal/FakeFactor/FakeFactor_Tau_origLF_prong1P_pteta  
        xVariable ParReader Loose1PTaus pt 
        yVariable ParReader Loose1PTaus |eta| 
        SetFakeType 5 
        SetProcessBin 4 
        SetDesignNumber @{designnumber}
        SetCombinatoricGroup 1
    End_Fake  
    # FakeFactor Tau HF ttbar 
    AddFake 
        FreezeSign   
        Particle Loose1PTaus 
        FakeFactorHisto @{PathToFFfile}/FakeFactors_ttbar_tau.root HF_Nominal/FakeFactor/FakeFactor_Tau_origHF_prong1P_pteta  
        xVariable ParReader Loose1PTaus pt 
        yVariable ParReader Loose1PTaus |eta| 
        SetFakeType 6 
        SetProcessBin 4 
        SetDesignNumber @{designnumber}
        SetCombinatoricGroup 1
    End_Fake 
        #### FakeFactor Tau Gluon ttbar 
    AddFake
        FreezeSign   
        Particle Loose1PTaus 
        FakeFactorHisto @{PathToFFfile}/FakeFactors_ttbar_tau.root Gluon_Nominal/FakeFactor/FakeFactor_Tau_origGluon_prong1P_pteta  
        xVariable ParReader Loose1PTaus pt 
        yVariable ParReader Loose1PTaus |eta| 
        SetFakeType 7 
        SetProcessBin 4 
        SetDesignNumber @{designnumber}
        SetCombinatoricGroup 1
    End_Fake 
    #### FakeFactor Tau Elec ttbar 
    AddFake
        FreezeSign   
        Particle Loose1PTaus 
        FakeFactorHisto @{PathToFFfile}/FakeFactors_ttbar_tau.root Elec_Nominal/FakeFactor/FakeFactor_Tau_origElec_prong1P_pteta  
        xVariable ParReader Loose1PTaus pt 
        yVariable ParReader Loose1PTaus |eta| 
        SetFakeType 8 
        SetProcessBin 4 
        SetDesignNumber @{designnumber}
        SetCombinatoricGroup 1
    End_Fake 
  
    #### FakeFactor Tau Conv ttbar 
    AddFake
        FreezeSign   
        Particle Loose1PTaus 
        FakeFactorHisto @{PathToFFfile}/FakeFactors_ttbar_tau.root Conv_Nominal/FakeFactor/FakeFactor_Tau_origConv_prong1P_pteta  
        xVariable ParReader Loose1PTaus pt 
        yVariable ParReader Loose1PTaus |eta| 
        SetFakeType 9 
        SetProcessBin 4 
        SetDesignNumber @{designnumber}
        SetCombinatoricGroup 1
    End_Fake 

##
#       Add 3 prong taus to the queues
##
    # FakeFactor Tau LF ttbar 
    AddFake 
        FreezeSign       
        Particle Loose3PTaus 
        FakeFactorHisto @{PathToFFfile}/FakeFactors_ttbar_tau.root LF_Nominal/FakeFactor/FakeFactor_Tau_origLF_prong3P_pteta 
        xVariable ParReader Loose3PTaus pt 
        yVariable ParReader Loose3PTaus |eta| 
        SetFakeType 11 
        SetProcessBin 6 
        SetDesignNumber @{designnumber}
        SetCombinatoricGroup 1
    End_Fake 
   ## FakeFactor Tau HF ttbar 
    AddFake 
        FreezeSign
        Particle Loose3PTaus 
        FakeFactorHisto @{PathToFFfile}/FakeFactors_ttbar_tau.root HF_Nominal/FakeFactor/FakeFactor_Tau_origHF_prong3P_pteta 
        xVariable ParReader Loose3PTaus pt 
        yVariable ParReader Loose3PTaus |eta| 
        SetFakeType 12 
        SetProcessBin 6 
        SetDesignNumber @{designnumber}
        SetCombinatoricGroup 1
    End_Fake 
    # FakeFactor Tau Gluon ttbar 
    AddFake 
        FreezeSign        
        Particle Loose3PTaus 
        FakeFactorHisto @{PathToFFfile}/FakeFactors_ttbar_tau.root Gluon_Nominal/FakeFactor/FakeFactor_Tau_origGluon_prong3P_pteta 
        xVariable ParReader Loose3PTaus pt 
        yVariable ParReader Loose3PTaus |eta| 
        SetFakeType 13 
        SetProcessBin 6 
        SetDesignNumber @{designnumber}
        SetCombinatoricGroup 1
    End_Fake 
 
    #### FakeFactor Tau Elec ttbar 
    AddFake
        FreezeSign   
        Particle Loose3PTaus 
        FakeFactorHisto @{PathToFFfile}/FakeFactors_ttbar_tau.root Elec_Nominal/FakeFactor/FakeFactor_Tau_origElec_prong3P_pteta 
        xVariable ParReader Loose3PTaus pt 
        yVariable ParReader Loose3PTaus |eta|
        SetFakeType 14 
        SetProcessBin 6 
        SetDesignNumber @{designnumber}
        SetCombinatoricGroup 1
    End_Fake 
  
    #### FakeFactor Tau Conv ttbar 
    AddFake
        FreezeSign   
        Particle Loose3PTaus 
        FakeFactorHisto @{PathToFFfile}/FakeFactors_ttbar_tau.root Conv_Nominal/FakeFactor/FakeFactor_Tau_origConv_prong3P_pteta 
        xVariable ParReader Loose3PTaus pt 
        yVariable ParReader Loose3PTaus |eta|
        SetFakeType 15 
        SetProcessBin 6 
        SetDesignNumber @{designnumber}
        SetCombinatoricGroup 1
    End_Fake 
 
    #### ProcessFraction Tau LF ttbar 
    AddFake 
          FreezeSign
          Particle Loose1PTaus 
          FakeFactorHisto @{PathToFFfile}/ProcessFraction_ttbar.root @{region}/ttbar/LF/ProcessFraction_LooseTau_origLF_prong1P_pteta  
          xVariable ParReader Loose1PTaus pt 
          yVariable ParReader Loose1PTaus |eta|
          SetFakeType 5 
          SetProcessBin 4 
          SetDesignNumber @{designnumber}
          EvCut region IsRegion_@{region} = 1
          @negative_ff
          SetCombinatoricGroup 1
    End_Fake   
 
    #### ProcessFraction Tau HF ttbar     
    AddFake 
          FreezeSign
          Particle Loose1PTaus 
          FakeFactorHisto @{PathToFFfile}/ProcessFraction_ttbar.root @{region}/ttbar/HF/ProcessFraction_LooseTau_origHF_prong1P_pteta  
          xVariable ParReader Loose1PTaus pt 
          yVariable ParReader Loose1PTaus |eta|
          SetFakeType 6 
          SetProcessBin 4 
          SetDesignNumber @{designnumber}
          EvCut region IsRegion_@{region} = 1
          @negative_ff
          SetCombinatoricGroup 1
      End_Fake 
   
      #### ProcessFraction Tau Gluon ttbar
      AddFake
          FreezeSign
          Particle Loose1PTaus 
          FakeFactorHisto @{PathToFFfile}/ProcessFraction_ttbar.root @{region}/ttbar/Gluon/ProcessFraction_LooseTau_origGluon_prong1P_pteta  
          xVariable ParReader Loose1PTaus pt 
          yVariable ParReader Loose1PTaus |eta|
          SetFakeType 7 
          SetProcessBin 4 
          SetDesignNumber @{designnumber}
          EvCut region IsRegion_@{region} = 1
          @negative_ff
          SetCombinatoricGroup 1
      End_Fake 
   
      #### ProcessFraction Tau Elec ttbar
      AddFake
          FreezeSign
          Particle Loose1PTaus 
          FakeFactorHisto @{PathToFFfile}/ProcessFraction_ttbar.root @{region}/ttbar/Elec/ProcessFraction_LooseTau_origElec_prong1P_pteta  
          xVariable ParReader Loose1PTaus pt 
          yVariable ParReader Loose1PTaus |eta|
          SetFakeType 8 
          SetProcessBin 4 
          SetDesignNumber @{designnumber}
          EvCut region IsRegion_@{region} = 1
          @negative_ff
          SetCombinatoricGroup 1
      End_Fake 
  
      #### ProcessFraction Tau Conv ttbar
      AddFake
          FreezeSign
          Particle Loose1PTaus 
          FakeFactorHisto @{PathToFFfile}/ProcessFraction_ttbar.root @{region}/ttbar/Conv/ProcessFraction_LooseTau_origConv_prong1P_pteta  
          xVariable ParReader Loose1PTaus pt 
          yVariable ParReader Loose1PTaus |eta|
          SetFakeType 9 
          SetProcessBin 4 
          SetDesignNumber @{designnumber}
          EvCut region IsRegion_@{region} = 1
          @negative_ff
          SetCombinatoricGroup 1
      End_Fake  
    #### ProcessFraction Tau LF ttbar
      AddFake
          FreezeSign 
          Particle Loose3PTaus 
          FakeFactorHisto @{PathToFFfile}/ProcessFraction_ttbar.root @{region}/ttbar/LF/ProcessFraction_LooseTau_origLF_prong3P_pteta 
          xVariable ParReader Loose3PTaus pt 
          yVariable ParReader Loose3PTaus |eta|
          SetFakeType 11 
          SetProcessBin 6 
          SetDesignNumber @{designnumber}
          EvCut region IsRegion_@{region} = 1
          @negative_ff
          SetCombinatoricGroup 1
      End_Fake 
    
      #### ProcessFraction Tau HF ttbar     
      AddFake
          FreezeSign
          Particle Loose3PTaus 
          FakeFactorHisto @{PathToFFfile}/ProcessFraction_ttbar.root @{region}/ttbar/HF/ProcessFraction_LooseTau_origHF_prong3P_pteta 
          xVariable ParReader Loose3PTaus pt 
          yVariable ParReader Loose3PTaus |eta|
          SetFakeType 12 
          SetProcessBin 6 
          SetDesignNumber @{designnumber}
          EvCut region IsRegion_@{region} = 1
          @negative_ff
          SetCombinatoricGroup 1
      End_Fake 
  
      #### ProcessFraction Tau Gluon ttbar
      AddFake
          FreezeSign
          Particle Loose3PTaus 
          FakeFactorHisto @{PathToFFfile}/ProcessFraction_ttbar.root @{region}/ttbar/Gluon/ProcessFraction_LooseTau_origGluon_prong3P_pteta 
          xVariable ParReader Loose3PTaus pt 
          yVariable ParReader Loose3PTaus |eta|
          SetFakeType 13 
          SetProcessBin 6 
          SetDesignNumber @{designnumber}
          EvCut region IsRegion_@{region} = 1
          @negative_ff
          SetCombinatoricGroup 1
      End_Fake 
  
      #### ProcessFraction Tau Elec ttbar
      AddFake
          FreezeSign
          Particle Loose3PTaus 
          FakeFactorHisto @{PathToFFfile}/ProcessFraction_ttbar.root @{region}/ttbar/Elec/ProcessFraction_LooseTau_origElec_prong3P_pteta 
          xVariable ParReader Loose3PTaus pt 
          yVariable ParReader Loose3PTaus |eta|
          SetFakeType 14
          SetProcessBin 4 
          SetDesignNumber @{designnumber}
          EvCut region IsRegion_@{region} = 1
          @negative_ff
          SetCombinatoricGroup 1
      End_Fake 
  
      #### ProcessFraction Tau Conv ttbar
      AddFake
          FreezeSign
          Particle Loose3PTaus 
          FakeFactorHisto @{PathToFFfile}/ProcessFraction_ttbar.root @{region}/ttbar/Conv/ProcessFraction_LooseTau_origConv_prong3P_pteta 
          xVariable ParReader Loose3PTaus pt 
          yVariable ParReader Loose3PTaus |eta|
          SetFakeType 15 
          SetProcessBin 4 
          SetDesignNumber @{designnumber}
          EvCut region IsRegion_@{region} = 1
          @negative_ff
          SetCombinatoricGroup 1
      End_Fake 

 
End_FakeFactorWeight
End_MultiLine 

### Processfractions





  New_FakeFactorWeight FakeFactorWeight
    
  End_FakeFactorWeight


 
defineMultiLine FFweightdefinition_tau_3prong
  New_FakeFactorWeight FakeFactorWeight
      
 
    End__FakeFactorWeight
End_MultiLine 
