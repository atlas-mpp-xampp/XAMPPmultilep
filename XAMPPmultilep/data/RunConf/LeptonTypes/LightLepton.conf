#Import XAMPPplotting/RunConf/FourLepton/RunConfig.conf

defineMultiLine SignalLepton
    Cut char CORR_isol = 1  
    Cut char signal = 1 
End_MultiLine
defineMultiLine LooseLepton
    CombCut NOTAND
        @SignalLepton 
    End_CombCut
End_MultiLine

defineMultiLine StandardLepVars
    Cut char passLMR = 1
    Var float charge
    Var float z0sinTheta
    Var float d0sig    
    Var floatGeV MT
    Var char signal
    Var char CORR_isol
    Var char isol 
    ifdef isMC
        Var int truthType
        Var int truthOrigin
        Var int IFFClassType
    endif    
End_MultiLine


#########################################################
#   All reconsutrcted particles first                   #
#########################################################

### Signal
New_Particle SignalElectrons elec    
    @StandardLepVars
    @SignalLepton
End_Particle
New_Particle SignalMuons muon
    @StandardLepVars
    @SignalLepton   
End_Particle

### Loose
New_Particle LooseElectrons elec
    @LooseLepton
    @StandardLepVars
    Cut floatGeV pt > 7    
End_Particle
New_Particle LooseMuons muon
    @LooseLepton
    @StandardLepVars
    Cut floatGeV pt > 5    
End_Particle
### Baseline
New_Particle BaselineElectrons elec
    @StandardLepVars
    Cut floatGeV pt > 7   
End_Particle
New_Particle BaselineMuons muon
     @StandardLepVars
     Cut floatGeV pt > 5   
End_Particle

####

defineMultiLine SherpaFix221
    CombCut AND
        # fix for Sherpa221 VVV
        Cut int truthOrigin = 0   
        CombCut OR
            Cut int truthType = 1
            Cut int truthType = 5
        End_CombCut
    End_CombCut
End_MultiLine

defineMultiLine RealElectron
    CombCut OR
        Cut int IFFClassType = 2
        Cut int IFFClassType = 3
        Cut int IFFClassType = 7
        ### Sherpa221_VV diboson fix
        ifdef applySherpa221_MCClassFix
            @SherpaFix221
        endif
    End_CombCut
    
    
End_MultiLine

defineMultiLine Elec_Muon
    CombCut OR
        Cut int IFFClassType = 6        
    End_CombCut    
End_MultiLine


defineMultiLine Elec_Conversion
    CombCut OR
        Cut int IFFClassType = 5        
    End_CombCut    
End_MultiLine


defineMultiLine Elec_LF
    CombCut OR
        Cut int IFFClassType = 10       
    End_CombCut    
End_MultiLine


defineMultiLine Elec_HF
    CombCut OR
        Cut int IFFClassType = 8    
        Cut int IFFClassType = 9 
    End_CombCut    
End_MultiLine

defineMultiLine Elec_Unmatched    
        Cut int IFFClassType = 0 
End_MultiLine

defineMultiLine Elec_KnownUnmatched 
        Cut int IFFClassType = 1   
        ifdef applySherpa221_MCClassFix  
            CombCut NOTAND            
                @SherpaFix221            
            End_CombCut    
        endif          
End_MultiLine


defineMultiLine RealMuon    
    CombCut OR
        Cut int IFFClassType = 4    
        Cut int IFFClassType = 7 
        ### Sherpa221_VV diboson fix
        ifdef applySherpa221_MCClassFix
            @SherpaFix221
        endif 
    End_CombCut 
    
End_MultiLine

defineMultiLine Muon_HF 
        CombCut OR
        Cut int IFFClassType = 8    
        Cut int IFFClassType = 9  
    End_CombCut
End_MultiLine

defineMultiLine Muon_LF 
    CombCut OR        
        Cut int IFFClassType = 10           
    End_CombCut  
End_MultiLine

defineMultiLine Muon_Unmatched  
    CombCut OR
        Cut int IFFClassType = 0 
    End_CombCut 
      
End_MultiLine

defineMultiLine Muon_KnownUnmatched     
        Cut int IFFClassType = 1    
        ifdef applySherpa221_MCClassFix
            CombCut NOTAND            
                @SherpaFix221               
            End_CombCut   
        endif  
End_MultiLine






#####################################################
#       Real Leptons                                #
#####################################################

ifdef isMC
    #### Signal
    New_Particle RealSignalElectrons elec
        @RealElectron
        @StandardLepVars
        @SignalLepton
    End_Particle
    New_Particle RealSignalMuons muon
        @RealMuon
        @StandardLepVars
        @SignalLepton
    End_Particle
    #### Loose
    New_Particle RealLooseElectrons elec
        @RealElectron
        @StandardLepVars
        @LooseLepton
        Cut floatGeV pt > 7
    End_Particle
    New_Particle RealLooseMuons muon
        @RealMuon
        @StandardLepVars
        @LooseLepton
        Cut floatGeV pt > 5    
    End_Particle

    #### Baseline
    New_Particle RealBaseElectrons elec
        @RealElectron
        @StandardLepVars  
        Cut floatGeV pt > 7
    End_Particle
    New_Particle RealBaseMuons muon
        @RealMuon
        @StandardLepVars
        Cut floatGeV pt > 5    
    End_Particle
endif


########################################################################
#                           Fake leptons                               # 
########################################################################
ifdef isMC
    #### Signal
    New_Particle FakeSignalElectrons elec
        CombCut NOTAND
            @RealElectron
        End_CombCut
        @StandardLepVars
        @SignalLepton
    End_Particle
    New_Particle FakeSignalMuons muon
        CombCut NOTAND
            @RealMuon
        End_CombCut
        @StandardLepVars
        @SignalLepton
    End_Particle
    #### Loose
    New_Particle FakeLooseElectrons elec
        CombCut NOTAND
            @RealElectron
        End_CombCut
        @StandardLepVars
        @LooseLepton
        Cut floatGeV pt > 7
    End_Particle
    New_Particle FakeLooseMuons muon
        CombCut NOTAND
            @RealMuon
        End_CombCut
        @StandardLepVars
        @LooseLepton
        Cut floatGeV pt > 5    
    End_Particle

    #### Baseline
    New_Particle FakeBaseElectrons elec
        CombCut NOTAND
            @RealElectron
        End_CombCut
        @StandardLepVars  
        Cut floatGeV pt > 7
    End_Particle
    New_Particle FakeBaseMuons muon
        CombCut NOTAND
            @RealMuon
        End_CombCut
        @StandardLepVars
        Cut floatGeV pt > 5    
    End_Particle
endif



########################################################################
#                       LightFlavour                                   #
########################################################################

ifdef isMC
    ### Signal
    New_Particle LFSignalElectrons elec
        @Elec_LF
        @StandardLepVars
        @SignalLepton
    End_Particle
    New_Particle LFSignalMuons muon
        @Muon_LF
        @StandardLepVars
        @SignalLepton
    End_Particle
    ### Loose
    New_Particle LFLooseElectrons elec
        @Elec_LF
        @StandardLepVars
        @LooseLepton
        Cut floatGeV pt > 7
    End_Particle
    New_Particle LFLooseMuons muon
        @Muon_LF
        @StandardLepVars
        @LooseLepton
        Cut floatGeV pt > 5    
    End_Particle
    ### Baseline
    New_Particle LFBaseElectrons elec
        @Elec_LF
        @StandardLepVars  
        Cut floatGeV pt > 7     
    End_Particle
    New_Particle LFBaseMuons muon
        @Muon_LF
        @StandardLepVars    
        Cut floatGeV pt > 5       
    End_Particle
endif


########################################################################
#                       HeavyFlavour                                   #
########################################################################

ifdef isMC
    ### Signal
    New_Particle HFSignalElectrons elec
        @Elec_HF
        @StandardLepVars
        @SignalLepton
    End_Particle
    New_Particle HFSignalMuons muon
        @Muon_HF
        @StandardLepVars
        @SignalLepton
    End_Particle
    ### Loose
    New_Particle HFLooseElectrons elec
        @Elec_HF
        @StandardLepVars
        @LooseLepton
        Cut floatGeV pt > 7
    End_Particle
    New_Particle HFLooseMuons muon
        @Muon_HF
        @StandardLepVars
        @LooseLepton
        Cut floatGeV pt > 5    
    End_Particle
    ### Baseline
    New_Particle HFBaseElectrons elec
        @Elec_HF
        @StandardLepVars 
        Cut floatGeV pt > 7      
    End_Particle
    New_Particle HFBaseMuons muon
        @Muon_HF
        @StandardLepVars    
        Cut floatGeV pt > 5       
    End_Particle
endif


########################################################################
#                         Conversion                                   #
########################################################################

ifdef isMC
    ### Signal
    New_Particle ConvSignalElectrons elec
        @Elec_Conversion
        @StandardLepVars
        @SignalLepton
    End_Particle

    ### Loose
    New_Particle ConvLooseElectrons elec
        @Elec_Conversion
        @StandardLepVars
        @LooseLepton
        Cut floatGeV pt > 7
    End_Particle

    ### Baseline
    New_Particle ConvBaseElectrons elec
        @Elec_Conversion
        @StandardLepVars  
        Cut floatGeV pt > 7     
    End_Particle

    New_Particle ConvSignalMuons muon
        @Elec_Conversion
        @StandardLepVars
        @SignalLepton
    End_Particle

    ### Loose
    New_Particle ConvLooseMuons muon
        @Elec_Conversion
        @StandardLepVars
        @LooseLepton
    End_Particle

    ### Baseline
    New_Particle ConvBaseMuons muon
        @Elec_Conversion
        @StandardLepVars       
    End_Particle
endif




########################################################################
#                         Electrons from Muons                         #
########################################################################
ifdef isMC
    ### Signal
    New_Particle MuonSignalElectrons elec
        @Elec_Muon
        @StandardLepVars
        @SignalLepton
    End_Particle
    New_Particle MuonLooseElectrons elec
        @Elec_Muon
        @StandardLepVars
        @LooseLepton
        Cut floatGeV pt > 7
    End_Particle
    New_Particle MuonBaseElectrons elec
        @Elec_Muon
        @StandardLepVars   
        Cut floatGeV pt > 7     
    End_Particle

endif
########################################################################
#                         UnMatched                                    #
########################################################################
ifdef isMC
    ### Signal
    New_Particle UnmatchedSignalElectrons elec
        @Elec_Unmatched
        @StandardLepVars
        @SignalLepton
    End_Particle
    New_Particle UnmatchedSignalMuons muon
        @Muon_Unmatched
        @StandardLepVars
        @SignalLepton
    End_Particle
    
    New_Particle UnmatchedLooseElectrons elec
        @Elec_Unmatched
        @StandardLepVars
        @LooseLepton
        Cut floatGeV pt > 7
    End_Particle
    New_Particle UnmatchedLooseMuons muon
        @Muon_Unmatched
        @StandardLepVars
        @LooseLepton
        Cut floatGeV pt > 5    
    End_Particle
    
    New_Particle UnmatchedBaseElectrons elec
        @Elec_Unmatched
        @StandardLepVars   
        Cut floatGeV pt > 7     
    End_Particle
    New_Particle UnmatchedBaseMuons muon
        @Muon_Unmatched
        @StandardLepVars    
        Cut floatGeV pt > 5        
    End_Particle
endif


########################################################################
#                       Known  UnMatched                               #
########################################################################
ifdef isMC
    ### Signal
    New_Particle KnownUnmatchedSignalElectrons elec
        @Elec_KnownUnmatched
        @StandardLepVars
        @SignalLepton
    End_Particle
    New_Particle KnownUnmatchedSignalMuons muon
        @Muon_KnownUnmatched
        @StandardLepVars
        @SignalLepton
    End_Particle
    
    New_Particle KnownUnmatchedLooseElectrons elec
        @Elec_KnownUnmatched
        @StandardLepVars
        @LooseLepton
        Cut floatGeV pt > 7
    End_Particle
    New_Particle KnownUnmatchedLooseMuons muon
        @Muon_KnownUnmatched
        @StandardLepVars
        @LooseLepton
        Cut floatGeV pt > 5    
    End_Particle
    
    New_Particle KnownUnmatchedBaseElectrons elec
        @Elec_KnownUnmatched
        @StandardLepVars       
        Cut floatGeV pt > 7 
    End_Particle
    New_Particle KnownUnmatchedBaseMuons muon
        @Muon_KnownUnmatched
        @StandardLepVars    
        Cut floatGeV pt > 5    
    End_Particle
endif
################################################
#       Di particles                           #
################################################

DiParticle LooseLightLep LooseElectrons LooseMuons
DiParticle SignalLightLep SignalElectrons SignalMuons
DiParticle BaselineLightLep BaselineElectrons BaselineMuons

ifdef isMC
    DiParticle RealSignalLightLep RealSignalElectrons RealSignalMuons
    DiParticle RealLooseLightLep RealLooseElectrons RealLooseMuons
    DiParticle RealBaseLightLep RealBaseElectrons RealBaseMuons
endif

