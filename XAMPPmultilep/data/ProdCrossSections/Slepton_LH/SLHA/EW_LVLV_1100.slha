##******************************************************************
##                      MadGraph/MadEvent                          *
##******************************************************************
##                                                                 *
##                 Default MadEvent param_card                     *
##       corresponding the SPS point 1a (by SoftSusy 2.0.5)        *
##                                                                 *
##******************************************************************
## Les Houches friendly file for the (MS)SM parameters of MadGraph *
##      SM parameter set and decay widths produced by MSSMCalc     *
##******************************************************************
##*Please note the following IMPORTANT issues:                     *
##                                                                 *
##0. REFRAIN from editing this file by hand! Some of the parame-   *
##   ters are not independent. Always use a calculator.            *
##                                                                 *
##1. alpha_S(MZ) has been used in the calculation of the parameters*
##   This value is KEPT by madgraph when no pdf are used lpp(i)=0, *
##   but, for consistency, it will be reset by madgraph to the     *
##   value expected IF the pdfs for collisions with hadrons are    *
##   used.                                                         *
##                                                                 *
##2. Values of the charm and bottom kinematic (pole) masses are    *
##   those used in the matrix elements and phase space UNLESS they *
##   are set to ZERO from the start in the model (particles.dat)   *
##   This happens, for example,  when using 5-flavor QCD where     *
##   charm and bottom are treated as partons in the initial state  *
##   and a zero mass might be hardwired in the model definition.   *
##                                                                 *
##       The SUSY decays have calculated using SDECAY 1.1a         *
##                                                                 *
##******************************************************************
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY      # decay calculator
     2   1.1a        # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SOFTSUSY    # spectrum calculator                 
     2   2.0.5         # version number                    
#
BLOCK MODSEL  # Model selection
     1     1   sugra                                             
#
BLOCK SMINPUTS  # Standard Model inputs
     1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
     2     1.16637000E-05   # G_F [GeV^-2]
     3     1.18000000E-01   # alpha_S(M_Z)^MSbar
     4     9.11876000E+01   # M_Z pole mass
     5     4.25000000E+00   # mb(mb)^MSbar
     6     1.72500000E+02   # mt pole mass
     7     1.77682000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
     1     1.00000000E+02   # m0                  
     2     2.50000000E+02   # m12                 
     3     1.00000000E+01   # tanb                
     4     1.00000000E+00   # sign(mu)            
     5    -1.00000000E+02   # A0                  
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
         5     4.80000000E+00   # b-quark pole mass calculated from mb(mb)_Msbar
         6     1.72500000E+02   # t-quark pole mass (not read by ME)
        15     1.77682000E+00   # tau pole mass (not read by ME)
        23     9.11876000E+01   # Z pole mass (not read by ME)
        24     8.03990000E+01   # W+
        25     1.25000000E+02   # h
        35     3.99960116E+05   # H
        36     3.99583917E+05   # A
        37     4.07879012E+05   # H+
   1000001    8665.8  # 
   2000001    8665.8  # 
   1000002    8865.5  # 
   2000002    8665.8  # 
   1000003    8665.8  # 
   2000003    8665.8  # 
   1000004    8665.8  # 
   2000004    8665.8  # 
   1000005    8665.8  # 
   2000005    8665.8  # 
   1000006    8665.8  # 
   2000006    8665.8  # 
   1000011    1100  # 
   2000011    8665.8  # 
   1000012    1100  # 
   1000013    1100  # 
   2000013    8665.8  # 
   1000014    1100  # 
   1000015    1100  # 
   2000015    8665.8  # 
   1000016    1100  # 
   1000021    8665.8  # 
   1000022    500  # 
   1000023    8665.8  # 
   1000025    8665.8  # 
   1000035    8665.8  # 
   1000024    8665.8  # 
   1000037    8665.8  # 
#
BLOCK NMIX  # Neutralino Mixing Matrix
	1	1	1.000000E+00	#N_11
	1	2	0.000000E+00	#N_12
	1	3	0.000000E+00	#N_13
	1	4	0.000000E+00	#N_14
	2	1	0.000000E+00	#N_21
	2	2	1.000000E+00	#N_22
	2	3	0.000000E+00	#N_23
	2	4	0.000000E+00	#N_24
	3	1	0.000000E+00	#N_31
	3	2	0.000000E+00	#N_32
	3	3	1.000000E+00	#N_33
	3	4	0.000000E+00	#N_34
	4	1	0.000000E+00	#N_41
	4	2	0.000000E+00	#N_42
	4	3	0.000000E+00	#N_43
	4	4	1.000000E+00	#N_44

BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.16834859E-01   # U_11
  1  2    -3.99266629E-01   # U_12
  2  1     3.99266629E-01   # U_21
  2  2     9.16834859E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.72557835E-01   # V_11
  1  2    -2.32661249E-01   # V_12
  2  1     2.32661249E-01   # V_21
  2  2     9.72557835E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     5.53644960E-01   # O_{11}
  1  2     8.32752820E-01   # O_{12}
  2  1     8.32752820E-01   # O_{21}
  2  2    -5.53644960E-01   # O_{22}
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.38737896E-01   # O_{11}
  1  2     3.44631925E-01   # O_{12}
  2  1    -3.44631925E-01   # O_{21}
  2  2     9.38737896E-01   # O_{22}
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     1.000000E+00   # O_{11}
  1  2     0.000000E+00   # O_{12}
  2  1     0.000000E+00   # O_{21}
  2  2     1.000000E+00   # O_{22}
#
BLOCK ALPHA  # Higgs mixing
          -1.13825210E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  4.67034192E+02  # DRbar Higgs Parameters
     1     3.57680977E+02   # mu(Q)MSSM DRbar     
     2     9.74862403E+00   # tan beta(Q)MSSM DRba
     3     2.44894549E+02   # higgs vev(Q)MSSM DRb
     4     1.66439065E+05   # mA^2(Q)MSSM DRbar   
#
BLOCK GAUGE Q=  4.67034192E+02  # The gauge couplings
     3     1.10178679E+00   # g3(Q) MSbar
#
BLOCK AU Q=  4.67034192E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3    -4.98129778E+02   # A_t(Q) DRbar
#
BLOCK AD Q=  4.67034192E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3    -7.97274397E+02   # A_b(Q) DRbar
#
BLOCK AE Q=  4.67034192E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3    -2.51776873E+02   # A_tau(Q) DRbar
#
BLOCK YU Q=  4.67034192E+02  # The Yukawa couplings
  3  3     8.92844550E-01   # y_t(Q) DRbar
#
BLOCK YD Q=  4.67034192E+02  # The Yukawa couplings
  3  3     1.38840206E-01   # y_b(Q) DRbar
#
BLOCK YE Q=  4.67034192E+02  # The Yukawa couplings
  3  3     1.00890810E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  4.67034192E+02  # The soft SUSY breaking masses at the scale Q
     1     1.01396534E+02   # M_1(Q)              
     2     1.91504241E+02   # M_2(Q)              
     3     5.88263031E+02   # M_3(Q)              
    21     3.23374943E+04   # mH1^2(Q)            
    22    -1.28800134E+05   # mH2^2(Q)            
    31     1.95334764E+02   # meL(Q)              
    32     1.95334764E+02   # mmuL(Q)             
    33     1.94495956E+02   # mtauL(Q)            
    34     1.36494061E+02   # meR(Q)              
    35     1.36494061E+02   # mmuR(Q)             
    36     1.34043428E+02   # mtauR(Q)            
    41     5.47573466E+02   # mqL1(Q)             
    42     5.47573466E+02   # mqL2(Q)             
    43     4.98763839E+02   # mqL3(Q)             
    44     5.29511195E+02   # muR(Q)              
    45     5.29511195E+02   # mcR(Q)              
    46     4.23245877E+02   # mtR(Q)              
    47     5.23148807E+02   # mdR(Q)              
    48     5.23148807E+02   # msR(Q)              
    49     5.19867261E+02   # mbR(Q)              
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY        23     2.49520000E+00   # Z width (SM calculation) BR taken  from Pythia8
DECAY        24     2.08500000E+00   # W width (SM calculation) BR taken from Pythia8
	
#         PDG            Width
DECAY   1000012     1.49881634E-01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
#         PDG            Width
DECAY   1000014     1.49881634E-01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
#
#         PDG            Width
DECAY   1000016     1.47518977E-01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.0000000E+00    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)

#         PDG            Width
DECAY   1000022     1.000000E-07   # neutralino1 decays
#          BR         NDA	ID1	ID2	ID3
	1.666667E-01	3	12	-13	11
	1.666667E-01	3	-12	13	-11
	3.333333E-01	3	14	-11	11
	3.333333E-01	3	12	-13	13


#         PDG            Width
DECAY   1000011     2.69906096E-01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
#         PDG            Width
DECAY   2000011     2.69906096E-01   # selectron_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        11   # BR(~e_R -> ~chi_10 e-)

#         PDG            Width
DECAY   1000013     2.69906096E-01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
#
#         PDG            Width
DECAY   2000013     2.69906096E-01   # smuon_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)

#         PDG            Width
DECAY   1000015     2.69906096E-01   # stau_1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
#
#         PDG            Width
DECAY   2000015     2.69906096E-01   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
