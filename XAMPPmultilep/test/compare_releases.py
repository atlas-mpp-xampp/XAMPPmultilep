from ClusterSubmission.Utils import RecursiveLS, ResolvePath, ReadListFromFile, CreateDirectory, id_generator
from XAMPPplotting.Utils import setupBaseParser
from XAMPPplotting.PlotUtils import PlotUtils
from XAMPPplotting.FileStructureHandler import GetStructure
import os, ROOT, math, logging, argparse


def setup_parser():
    # do this here, since before it destroys the argparse
    ROOT.gROOT.Macro("rootlogon.C")
    ROOT.gROOT.SetStyle("ATLAS")
    parser = argparse.ArgumentParser(description="Script to compare two MCP TPPostProcessing files histogram by histogram",
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('--referenceFile', help='Path to the reference file', default="CI_Reference.root")
    parser.add_argument('--testFile', help='Path to the file from the CI', default="CI_Test.root")
    parser.add_argument("--outDir", help="Output directory to store all plots", default="ReleaseComparisons")
    return parser


### Loop over all analyses det regions etc.
### To count how many histograms are part of the test
def count_obj_in_file(file_struct):
    n = 0
    for a in file_struct.get_analyses_names():
        for r in file_struct.get_regions(a):
            for v in file_struct.get_variables(a, r):
                n += file_struct.get_variables(a, r).size()
    return n


def compare_files(ref_file, test_file, out_dir=""):
    file_struct_test = ROOT.XAMPP.FileHandler.getInstance().GetFileStructure(test_file)
    file_struct_reference = ROOT.XAMPP.FileHandler.getInstance().GetFileStructure(ref_file)

    n_checked_plots = 0
    n_mismatches = 0

    for a in file_struct_reference.get_analyses_names():
        ref_regions = file_struct_reference.get_regions(a)
        test_regions = file_struct_test.get_regions(a)
        n_reg = min(len(ref_regions), len(test_regions))
        if len(ref_regions) != len(test_regions):
            logging.warning("Different number of regions found for reference and test file. %d vs %s" %
                            (len(ref_regions), len(test_regions)))
            logging.warning("The following regions are missing in the testing file")
            logging.warning("     *** %s" % (", ".join([x for x in ref_regions if x not in test_regions])))
            logging.warning("The following regions are missing in the reference file")
            logging.warning("     *** %s" % (", ".join([x for x in test_regions if x not in ref_regions])))

        for r in range(n_reg):
            if test_regions[r] != ref_regions[r]:
                logging.warning("Different regions in test and reference file found %s vs. %s.  Although the number matches " %
                                (test_regions[r], ref_regions[r]))

        for r in file_struct_reference.get_regions(a):
            ref_vars = file_struct_reference.get_variables(a, r)
            test_vars = file_struct_test.get_variables(a, r)
            if len(ref_vars) != len(test_vars):
                logging.warning("Different number of variables found for reference and test file in region %s. %d vs %s" %
                                (r, len(ref_vars), len(test_vars)))
                logging.warning("The following variables are missing in the testing file")
                logging.warning("     *** %s" % (", ".join([x for x in ref_vars if x not in test_vars])))
                logging.warning("The following variables are missing in the reference file")
                logging.warning("     *** %s" % (", ".join([x for x in test_vars if x not in ref_vars])))

            for v in ref_vars:
                reference_histo = ROOT.XAMPP.SampleHisto("reference", v, a, r, ref_file)
                test_histo = ROOT.XAMPP.SampleHisto("test", v, a, r, test_file)
                n_checked_plots += 1
                if not reference_histo.loadIntoMemory() or not test_histo.loadIntoMemory():
                    n_mismatches += 1
                    continue

                reference_histo.GetHistogram().Add(test_histo.GetHistogram().get(), -1)
                if math.fabs(reference_histo.GetMaximum()) < 1.e-8 and math.fabs(reference_histo.GetMinimum()) < 1.e-8:
                    continue

                n_mismatches += 1
                logging.warning("Releases do not match for %s distribution in %s min delta: %f, max delta: %f" %
                                (reference_histo.GetVariableName(), reference_histo.GetRegion(), reference_histo.GetMinimum(),
                                 reference_histo.GetMaximum()))
                if reference_histo.GetDimension() != 1: continue
                ### Recache the histogram again for the plot
                reference_histo = ROOT.XAMPP.SampleHisto("reference", v, a, r, ref_file)
                reference_histo.loadIntoMemory()
                reference_histo.SetTitle("Reference")
                reference_histo.SetLineColor(ROOT.kBlack)

                test_histo.SetTitle("Test")
                test_histo.SetLineColor(ROOT.kRed)

                max_val = max(reference_histo.GetMaximum(), test_histo.GetMaximum()) * 1.3
                styling_histo = reference_histo.GetHistogram().Clone(id_generator(24))

                pu = PlotUtils()
                plot_name = "%s_%s_%s" % (a, r, v)
                pu.Prepare2PadCanvas(plot_name, 800, 600)
                pu.GetTopPad().cd()
                pu.drawStyling(styling_histo, ymin=0, ymax=max_val, TopPad=True, RemoveLabel=False)

                reference_histo.Draw("histSAME")
                test_histo.Draw("histSAME")

                pu.CreateLegend(0.6, 0.5, 1 - ROOT.gPad.GetRightMargin(), 0.72)
                pu.AddToLegend([reference_histo, test_histo], "PL")
                pu.DrawLegend(5)
                #######################################
                #       Draw the labels               #
                #######################################
                pu.DrawPlotLabels(0.19, 0.52, r)
                ################################################################
                #         Now let's see how everything deviates from the truth #
                ################################################################
                pu.GetBottomPad().cd()
                pu.GetBottomPad().SetGridy()

                ratio = test_histo.GetHistogram().Clone(id_generator(25))
                ratio.Divide(reference_histo.GetHistogram().get())
                pu.AdaptLabelsBottomPad([ratio])
                ratio.Draw("hist")
                pu.saveHisto("%s/%s" % (out_dir, plot_name), ["pdf"])
                pu.saveHisto("%s/AllReleaseChecks" % (out_dir), ["pdf"])

    n_reference = count_obj_in_file(file_struct_reference)
    n_test = count_obj_in_file(file_struct_test)
    ### make sure to reset the cache to prepare the next round of files
    ROOT.XAMPP.FileHandler.getInstance().closeAll()
    logging.info("Checked in total %d distributions. %d showed differences w.r.t. the reference file" % (n_checked_plots, n_mismatches))
    if n_reference != n_test:
        logging.error("Apparently distributions were added/removed to the test. Will return failure")
        return False
    return n_mismatches == 0 and n_checked_plots > 0


if __name__ == '__main__':
    options = setup_parser().parse_args()
    CreateDirectory(options.outDir, False)

    dummycanvas = ROOT.TCanvas("dummy", "dummy", 800, 600)
    dummycanvas.SaveAs("%s/AllReleaseChecks.pdf[" % (options.outDir))
    good = compare_files(ref_file=options.referenceFile, test_file=options.testFile, out_dir=options.outDir)
    dummycanvas.SaveAs("%s/AllReleaseChecks.pdf]" % (options.outDir))
    if not good: exit(1)
