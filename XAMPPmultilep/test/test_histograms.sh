#!/bin/bash

# get kerberos token
if [ -z ${SERVICE_PASS} ]; then
  echo "You did not set the environment variable SERVICE_PASS.\n\
Please define in the gitlab project settings/CI the secret variables SERVICE_PASS and CERN_USER."
  exit 1
else
  echo "${SERVICE_PASS}" | kinit ${CERN_USER}@CERN.CH
fi

REF_TAG=`cat XAMPPmultilep/test/reference_commit.txt`
# Download reference file
EOS_PATH="root://eoshome.cern.ch//eos/user/x/xampp/ci/4lepton/CI_ref/histograms/${REF_TAG}/"
xrdcp "${EOS_PATH}/${PROCESS}.root" ./${PROCESS}-ref.root
if [ $? -ne 0 ]; then 
    echo "A problem occurred in download the reference histograms from EOS"
    exit 1
else
    echo "Downloaded reference histograms from EOS"
fi

# make reference plots
echo "Use test file ${TEST_FILE}"
ls -lh ${TEST_FILE}
echo "python /xampp/XAMPPmultilep/XAMPPmultilep/test/compare_releases.py --referenceFile ${PROCESS}-ref.root --testFile  ${TEST_FILE} --outDir ${CI_PROJECT_DIR}/CI_Comparison"

python /xampp/XAMPPmultilep/XAMPPmultilep/test/compare_releases.py --referenceFile ${PROCESS}-ref.root --testFile  ${TEST_FILE} --outDir ${CI_PROJECT_DIR}/CI_Comparison
