Extracting MetaData information...
Having found CutFlow for MC, printing...
Printing raw events
CutFlowHisto                               DSID_410218_CutFlow
Systematic variation                       Nominal   
##############################################################
Cut                                        Yields    
##############################################################
Initial                                    122523 ± 350.03
PassGRL                                    122523 ± 350.03
passLArTile                                122523 ± 350.03
Trigger                                    122334 ± 349.76
HasVtx                                     122334 ± 349.76
BadJet                                     121922 ± 349.17
CosmicMuon                                 119876 ± 346.23
BadMuon                                    119874 ± 346.23
N_{e/#mu}>=1                               117250 ± 342.42
N_{e/#mu}>=2                               92977  ± 304.92
(N_{e/#mu}^{Baseline}>=3 && N_{e/#mu}>=2)  39793  ± 199.48
N_{e/#mu}>=3                               27794  ± 166.72
N_{e/#mu}^{Baseline} >=4                   5895   ± 76.78
(N_{e/#mu}>=4 || isNominal)                5895   ± 76.78
N_{e/#mu}>=4                               2832   ± 53.22
#slash{Z}                                  394    ± 19.85
m_{eff}>600                                187    ± 13.67
m_{eff}>900                                75     ± 8.66
##############################################################
Printing raw events
CutFlowHisto                               DSID_410276_CutFlow
Systematic variation                       Nominal   
##############################################################
Cut                                        Yields    
##############################################################
Initial                                    31311 ± 176.95
PassGRL                                    31311 ± 176.95
passLArTile                                31311 ± 176.95
Trigger                                    30884 ± 175.74
HasVtx                                     30884 ± 175.74
BadJet                                     30771 ± 175.42
CosmicMuon                                 30177 ± 173.72
BadMuon                                    30177 ± 173.72
N_{e/#mu}>=1                               25795 ± 160.61
N_{e/#mu}>=2                               10747 ± 103.67
(N_{e/#mu}^{Baseline}>=3 && N_{e/#mu}>=2)  2904  ± 53.89
N_{e/#mu}>=3                               1193  ± 34.54
N_{e/#mu}^{Baseline} >=4                   184   ± 13.56
(N_{e/#mu}>=4 || isNominal)                184   ± 13.56
N_{e/#mu}>=4                               40    ± 6.32
#slash{Z}                                  22    ± 4.69
m_{eff}>600                                9     ± 3.00
m_{eff}>900                                4     ± 2.00
##############################################################
