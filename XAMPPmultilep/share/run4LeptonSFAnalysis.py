#include the common library instead of importing it. So we have full access to the globals
# Python you're a strange guy sometimes. Sometimes? Alsmost the entire day
include("XAMPPmultilep/FourLeptonToolSetup.py")
AssembleIO()
SetupSystematicsTool(noJets=False, noBtag=False, noElectrons=False, noMuons=False, noTaus=False, noDiTaus=True, noPhotons=True)

Setup4LeptonSFHelper()
#SetupSUSYElectronSelector().OutputLevel = DEBUG
#SetupSUSYMuonSelector().OutputLevel = DEBUG
#SetupSUSYTauSelector().OutputLevel = DEBUG

ParseCommonFourLeptonOptions(STFile="XAMPPmultilep/SUSYTools_SUSY4L.conf")

SetupSUSYElectronSelector().StoreTruthClassifier = True
SetupSUSYMuonSelector().StoreTruthClassifier = True
SetupSUSYTauSelector().StoreTruthClassifier = True
Setup4LeptonSFHelper().fillLHEWeights = False
SetupAlgorithm()
#ServiceMgr.MessageSvc.OutputLevel = DEBUG
