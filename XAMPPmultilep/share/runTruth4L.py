from XAMPPmultilep.SubmitToGrid import getLogicalDataSets
#include the common library instead of importing it. So we have full access to the globals
# Python you're a strange guy sometimes. Sometimes? Alsmost the entire day
include("XAMPPmultilep/FourLeptonToolSetup.py")
AssembleIO(1)
SetupFourLeptonTruthAnaHelper().fillLHEWeights = True
SetupFourLeptonTruthAnaHelper().AnalysisConfig.ApplySkimming = True
#ParseTruthOptionsToHelper()
if isData():
    print "You're a son of the sickness. That is a TRUTH AnalysisHelper!!! No TRUTH in data. Truth + Data = baeeh"
    exit(1)

SetupSystematicsTool().doNoPhotons = True
for sample, DSIDS in getLogicalDataSets().iteritems():
    if sample.find("Sherpa") != -1 and (getMCChannelNumber() in DSIDS or getRunNumbersMC() in DSIDS):
        print "We're running over a Sherpa sample (DSID: %d). Limit the GenWeight to be within 100." % (
            getRunNumbersMC() if getMCChannelNumber() == 0 else getMCChannelNumber())
        SetupFourLeptonTruthAnaHelper().OutlierWeightStrategy = 1
        break
    if sample in ["MG5Py8_ttZ", "aMCatNLOPy8_ttZ", "aMCAtNLO_ttY", "MG5Py8_ttY_incl", "MG5Py8_ttY_4080", "MG5Py8_ttY_80"
                  ] and (getMCChannelNumber() in DSIDS or getRunNumbersMC() in DSIDS):
        SetupSystematicsTool().doNoPhotons = False

SetupTruthSelector().ElectronContainer = "TruthElectrons"
SetupTruthSelector().MuonContainer = "TruthMuons"
SetupTruthSelector().TauContainer = "TruthTaus"
SetupTruthSelector().PhotonContainer = "TruthPhotons"
SetupTruthSelector().NeutrinoContainer = "TruthNeutrinos"
SetupTruthSelector().BSMContainer = "TruthBSM"
SetupTruthSelector().JetContainer = "AntiKt4TruthDressedWZJets"

SetupTruthSelector().isTRUTH3 = getFlags().isTRUTH3()
SetupTruthSelector().doTruthParticles = not getFlags().isTRUTH3()
SetupTruthSelector().doJets = True
SetupTruthSelector().rejectUnknownOrigin = True
SetupTruthSelector().OnlySelectHadronicTau = False
SetupTruthSelector().OnlySelectHadronicBaseTau = True
SetupTruthSelector().BaselineDecorator = "passOR"
SetupTruthSelector().SignalHardProcess = True

SetupTruthSelector().TruthBaselineMuonPtCut = 3000
SetupTruthSelector().TruthBaselineMuonEtaCut = 2.7
SetupTruthSelector().TruthSignalMuonPtCut = 5000

SetupTruthSelector().TruthBaselineElectronPtCut = 4500
SetupTruthSelector().TruthBaselineElectronEtaCut = 2.47
SetupTruthSelector().TruthSignalElectronPtCut = 7000

#SetupTruthSelector().ExcludeBaselineElectronInEta = ["-1.52;-1.37", "1.37;1.52"]
SetupTruthSelector().TruthBaselinePhotonPtCut = 15000
SetupTruthSelector().TruthBaselinePhotonEtaCut = 2.35

SetupTruthSelector().TruthBaselineTauPtCut = 20000
SetupTruthSelector().TruthBaselineTauEtaCut = 2.47
SetupTruthSelector().ExcludeBaselineTauInEta = ["-1.52;-1.37", "1.37;1.52"]

SetupTruthSelector().TruthBaselineJetPtCut = 20000
SetupTruthSelector().TruthBaselineJetEtaCut = 4.5
SetupTruthSelector().TruthSignalJetEtaCut = 2.8
SetupTruthSelector().TruthBJetEtaCut = 2.5

SetupTruthSelector().TruthBaselineNeutrinoPtCut = 5000

#SetupFourLeptonTruthAnaHelper().AnalysisModules += [setupRJbuilder()]
#setupRJbuilder().ElectronContainer = "elec"
#setupRJbuilder().MuonContainer = "muon"
#setupRJbuilder().JetContainer = "jet"
#setupRJbuilder().MissingET = "TruthMET"

SetupAlgorithm().printInterval = 10000
