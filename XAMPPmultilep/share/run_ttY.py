from XAMPPmultilep.SubmitToGrid import getLogicalDataSets
#include the common library instead of importing it. So we have full access to the globals
# Python you're a strange guy sometimes. Sometimes? Alsmost the entire day
include("XAMPPmultilep/FourLeptonToolSetup.py")
AssembleIO()
####################################################################################
# Optionally:
# Call the SetupSystematicTool() method in order to switch on/off specific objects
####################################################################################
SetupSystematicsTool(noJets=False, noBtag=False, noElectrons=False, noMuons=False, noTaus=False, noDiTaus=True, noPhotons=False)

SetupTruthSelector()
setupTTgammaHelper().doTruth = False

### ttGamma sample needs a ME photon....
setupTTgammaHelper().MEPhotDSID = getLogicalDataSets()["aMCAtNLO_ttY"] + getLogicalDataSets()["MG5Py8_ttY_incl"] + getLogicalDataSets(
)["MG5Py8_ttY_4080"] + getLogicalDataSets()["MG5Py8_ttY_80"] + getLogicalDataSets()["Sherpa221_ZYee"] + getLogicalDataSets(
)["Sherpa221_ZYmumu"] + getLogicalDataSets()["Sherpa221_ZYtautau"]
### Veto ME photons for the ttbar and ordinary Z+ jets
setupTTgammaHelper().FSRPhotDSID = getLogicalDataSets()["PowHegPy8_Zee"] + getLogicalDataSets()["PowHegPy8_Zmumu"] + getLogicalDataSets(
)["PowHegPy8_Ztautau"] + getLogicalDataSets()["Sherpa221_Zmumu"] + getLogicalDataSets()["Sherpa221_Zee"] + getLogicalDataSets(
)["Sherpa221_Ztautau"] + getLogicalDataSets()["PowhegPy_top"] + getLogicalDataSets()["PowHegPy8_ttbar_incl"] + getLogicalDataSets(
)["PowHegPy8_ttbar_diL"]
ParseCommonFourLeptonOptions(STFile="XAMPPmultilep/SUSYTools_SUSYttY.conf", )

SetupSUSYTriggerTool().Triggers += ["HLT_g120_loose"]

SetupTruthSelector().doTruthParticles = True
#Setup4LeptonAnaConfig().ApplyTriggerSkimming = False

SetupSUSYElectronSelector().StoreTruthClassifier = True
SetupSUSYMuonSelector().StoreTruthClassifier = True
Setup4LTauSelector().StoreTruthClassifier = True

SetupSUSYElectronSelector().RequireIsoSignal = False
SetupSUSYMuonSelector().RequireIsoSignal = False

#SetupFourLeptonAnaHelper().AnalysisModules += [setupRJbuilder()]
#setupRJbuilder().ElectronContainer = "elec"
##setupRJbuilder().MuonContainer = "muon"
#setupRJbuilder().JetContainer = "jet"
#SetupFourLeptonAnaHelper().doJigSawReconstruction = True

SetupAlgorithm()
