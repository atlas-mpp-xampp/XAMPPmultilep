#include the common library instead of importing it. So we have full access to the globals
# Python you're a strange guy sometimes. Sometimes? Alsmost the entire day
include("XAMPPmultilep/FourLeptonToolSetup.py")
AssembleIO()
####################################################################################
# Optionally:
# Call the SetupSystematicTool() method in order to switch on/off specific objects
####################################################################################
SetupSystematicsTool(noJets=False, noBtag=False, noElectrons=False, noMuons=False, noTaus=False, noDiTaus=True, noPhotons=True)

SetupFourLeptonAnaHelper()
ParseCommonFourLeptonOptions(STFile="XAMPPmultilep/SUSYTools_SUSY4L.conf")
Setup4LTruthSelector().isTRUTH3 = False
ServiceMgr.MessageSvc.debugLimit = 100000
svcMgr.MessageSvc.Format = "% F%50W%S%7W%R%T %0W%M"

if not isData():
    #### Switch off the event skimming for any signal sample
    from XAMPPmultilep.SubmitToGrid import getLogicalDataSets
    dsid = getMCChannelNumber()
    model_patterns = ["GGM", "_LLE", "BiLepton", "C1N2", "C1C1", "SlepSlep", "Wino"]
    for sample, dsid_range in getLogicalDataSets().iteritems():
        if dsid not in dsid_range: continue
        if len([x for x in model_patterns if sample.find(x) != -1]) == 0: continue
        Setup4LeptonAnaConfig().ApplyTriggerSkimming = False
        Setup4LeptonAnaConfig().ApplySkimming = False
        SetupFourLeptonAnaHelper().SkimEvents = False
        break

if len(SetupSUSYTools().PRWConfigFiles) == 0:
    ConfigFiles, LumiCalcFiles = configurePRWtool(package_path="XAMPPmultilep/PRWFile/")
    SetupSUSYTools().PRWConfigFiles = ConfigFiles
SetupFourLeptonAnaHelper().createCommonTree = True

SetupSUSYElectronSelector().StoreTruthClassifier = True
SetupSUSYMuonSelector().StoreTruthClassifier = True
Setup4LTauSelector().StoreTruthClassifier = True

from AthenaCommon.AppMgr import ToolSvc
from AthenaCommon import CfgMgr
from XAMPPmultilep.XAMPPmultilepConf import XAMPP__SUSY4LepMTModule
MT2Module = CfgMgr.XAMPP__SUSY4LepMTModule("SignalMT2")
MT2Module.ElectronContainer = "baseline_elec"
MT2Module.MuonContainer = "baseline_muon"
MT2Module.TauContainer = "baseline_tau"
MT2Module.GroupName = "Signal"
MT2Module.SignalOnly = True

ToolSvc += MT2Module

SetupFourLeptonAnaHelper().AnalysisModules += [MT2Module]

#SetupFourLeptonAnaHelper().AnalysisModules += [setupRJbuilder()]
#SetupFourLeptonAnaHelper().AnalysisModules += [setupRJbuilder()]
#setupRJbuilder().ElectronContainer = "elec"
##setupRJbuilder().MuonContainer = "muon"
#setupRJbuilder().JetContainer = "jet"
#SetupFourLeptonAnaHelper().doJigSawReconstruction = True

Extra_Iso = ["FCTight", "FCTight_FixedRad", "FCLoose_FixedRad"]
Extra_Iso = []
for Iso in Extra_Iso:
    SetupFourLeptonAnaHelper().IsolationWPTools += [AddIsolationSelectionTool(ElectronWP=Iso, MuonWP=Iso)]

SetupAlgorithm()
