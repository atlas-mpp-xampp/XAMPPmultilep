include("XAMPPbase/BaseToolSetup.py")


def Setup4LElectronSelector():
    from AthenaCommon.AppMgr import ToolSvc
    from AthenaCommon import CfgMgr
    if not hasattr(ToolSvc, "SUSYElectronSelector"):
        from XAMPPmultilep.XAMPPmultilepConf import XAMPP__SUSY4LeptonElectronSelector
        EleSelector = CfgMgr.XAMPP__SUSY4LeptonElectronSelector("SUSYElectronSelector")
        EleSelector.ApplyQFlipSF = False
        EleSelector.TagQFlip = False
        #default map defined in SUSYTools/Root/SUSYObjDef_xAOD.cxx
        #comment out next line if map2 becomes the default
        #EleSelector.EfficiencyMapFilePath = "ElectronEfficiencyCorrection/2015_2017/rel21.2/Consolidation_September2018_v1/map2.txt"
        ToolSvc += EleSelector

    return getattr(ToolSvc, "SUSYElectronSelector")


def Setup4LMuonSelector():
    from AthenaCommon.AppMgr import ToolSvc
    from AthenaCommon import CfgMgr
    if not hasattr(ToolSvc, "SUSYMuonSelector"):
        from XAMPPmultilep.XAMPPmultilepConf import XAMPP__SUSY4LeptonMuonSelector
        MuoSelector = CfgMgr.XAMPP__SUSY4LeptonMuonSelector("SUSYMuonSelector")
        ToolSvc += MuoSelector
    return getattr(ToolSvc, "SUSYMuonSelector")


def SetupLLQQJetSelector():
    from AthenaCommon.AppMgr import ToolSvc
    from AthenaCommon import CfgMgr
    if not hasattr(ToolSvc, "SUSYJetSelector"):
        from XAMPPmultilep.XAMPPmultilepConf import XAMPP__SUSYllqqJetSelector
        JetSelector = CfgMgr.XAMPP__SUSYllqqJetSelector("SUSYJetSelector")
        JetSelector.SeparateSF = True
        JetSelector.BTagSelectionTool_77WP = GetBtagSelectionTool(
            JetCollection="AntiKt4EMTopoJets_BTagging201810",
            WorkingPoint="FixedCutBEff_77",
            Tagger="MV2c10",
            CalibPath="xAODBTaggingEfficiency/13TeV/2019-21-13TeV-MC16-CDI-2019-10-07_v1.root")
        JetSelector.BTagEfficiencyTool_77WP = SetupBtaggingEfficiencyTool(
            JetCollection="AntiKt4EMTopoJets_BTagging201810",
            WorkingPoint="FixedCutBEff_77",
            Tagger="MV2c10",
            CalibPath="xAODBTaggingEfficiency/13TeV/2019-21-13TeV-MC16-CDI-2019-10-07_v1.root")
        ToolSvc += JetSelector
    return getattr(ToolSvc, "SUSYJetSelector")


def Setup4LDiTauSelector():
    from AthenaCommon.AppMgr import ToolSvc
    from AthenaCommon import CfgMgr
    if not hasattr(ToolSvc, "SUSYTauSelector"):
        from XAMPPmultilep.XAMPPmultilepConf import XAMPP__SUSY4LeptonDiTauSelector
        TauSelector = CfgMgr.XAMPP__SUSY4LeptonDiTauSelector("SUSYTauSelector")
        TauSelector.JetSelectionTool = SetupSUSYJetSelector()
        ToolSvc += TauSelector
    return getattr(ToolSvc, "SUSYTauSelector")


def Setup4LTauSelector():
    from AthenaCommon.AppMgr import ToolSvc
    from AthenaCommon import CfgMgr
    if not hasattr(ToolSvc, "SUSYTauSelector"):
        from XAMPPmultilep.XAMPPmultilepConf import XAMPP__SUSY4LeptonTauSelector
        from TauAnalysisTools.TauAnalysisToolsConf import TauAnalysisTools__TauSelectionTool
        from ClusterSubmission.Utils import ResolvePath
        TauSelector = CfgMgr.XAMPP__SUSY4LeptonTauSelector("SUSYTauSelector")
        TauSelector.JetSelectionTool = SetupSUSYJetSelector()  #SetupLLQQJetSelector()
        ### Setup the RNN tau selection tool
        ToolSvc += TauSelector

    return getattr(ToolSvc, "SUSYTauSelector")


def Setup4LTruthSelector():
    from AthenaCommon.AppMgr import ToolSvc
    from AthenaCommon import CfgMgr
    if not hasattr(ToolSvc, "SUSYTruthSelector"):
        from XAMPPmultilep.XAMPPmultilepConf import XAMPP__SUSY4LeptonTruthSelector
        from XAMPPmultilep.SubmitToGrid import getLogicalDataSets
        ggm_dsids = []
        for ds_name, dsids in getLogicalDataSets().iteritems():
            if ds_name.find("GGM") != -1:
                ggm_dsids += dsids
        TruthSelector = CfgMgr.XAMPP__SUSY4LeptonTruthSelector("SUSYTruthSelector")
        TruthSelector.GGMDSIDs = ggm_dsids
        ToolSvc += TruthSelector

    return getattr(ToolSvc, "SUSYTruthSelector")


def Setup4LeptonTruthAnaConfig(name="TruthFourLepton"):
    from AthenaCommon.AppMgr import ToolSvc
    from AthenaCommon import CfgMgr
    if not hasattr(ToolSvc, "AnalysisConfig"):
        from XAMPPmultilep.XAMPPmultilepConf import XAMPP__SUSY4LepTruthAnalysisConfig
        AnaConfig = CfgMgr.XAMPP__SUSY4LepTruthAnalysisConfig(name="AnalysisConfig", TreeName=name)
        ToolSvc += AnaConfig

    return getattr(ToolSvc, "AnalysisConfig")


def SetupFourLeptonTruthAnaHelper(TreeName="TruthFourLepton"):
    from AthenaCommon.AppMgr import ToolSvc
    from AthenaCommon import CfgMgr
    if not hasattr(ToolSvc, "AnalysisHelper"):
        from XAMPPmultilep.XAMPPmultilepConf import XAMPP__SUSY4LeptonTruthAnalysisHelper
        BaseHelper = CfgMgr.XAMPP__SUSY4LeptonTruthAnalysisHelper("AnalysisHelper")
        BaseHelper.AnalysisConfig = Setup4LeptonTruthAnaConfig(TreeName)
        BaseHelper.SystematicsTool = SetupSystematicsTool(noJets=True,
                                                          noBtag=True,
                                                          noElectrons=True,
                                                          noMuons=True,
                                                          noTaus=True,
                                                          noDiTaus=True,
                                                          noPhotons=True)
        BaseHelper.TruthSelector = Setup4LTruthSelector()

        ToolSvc += BaseHelper

    return getattr(ToolSvc, "AnalysisHelper")


def Setup4LeptonAnaConfig(name="FourLepton"):
    from AthenaCommon.AppMgr import ToolSvc
    from AthenaCommon import CfgMgr
    if not hasattr(ToolSvc, "AnalysisConfig"):
        from XAMPPmultilep.XAMPPmultilepConf import XAMPP__SUSY4LeptonAnalysisConfig
        AnaConfig = CfgMgr.XAMPP__SUSY4LeptonAnalysisConfig(name="AnalysisConfig", TreeName=name)
        ToolSvc += AnaConfig

    return getattr(ToolSvc, "AnalysisConfig")


def SetupFourLeptonAnaHelper(TreeName="FourLepton"):
    from AthenaCommon.AppMgr import ToolSvc
    from AthenaCommon import CfgMgr
    if not hasattr(ToolSvc, "AnalysisHelper"):
        from XAMPPmultilep.XAMPPmultilepConf import XAMPP__SUSY4LeptonAnalysisHelper
        BaseHelper = CfgMgr.XAMPP__SUSY4LeptonAnalysisHelper("AnalysisHelper")
        BaseHelper.AnalysisConfig = Setup4LeptonAnaConfig(TreeName)
        BaseHelper.SystematicsTool = SetupSystematicsTool()
        BaseHelper.ElectronSelector = Setup4LElectronSelector()
        BaseHelper.MuonSelector = Setup4LMuonSelector()
        BaseHelper.TauSelector = Setup4LTauSelector()
        BaseHelper.TruthSelector = Setup4LTruthSelector()

        ToolSvc += BaseHelper

    return getattr(ToolSvc, "AnalysisHelper")


def Setup4LeptonSFConfig(name="SUSY4LeptonScaleFactor"):
    from AthenaCommon.AppMgr import ToolSvc
    from AthenaCommon import CfgMgr
    if not hasattr(ToolSvc, "AnalysisConfig"):
        from XAMPPmultilep.XAMPPmultilepConf import XAMPP__SUSY4LeptonScaleFactorAnalysisConfig
        AnaConfig = CfgMgr.XAMPP__SUSY4LeptonScaleFactorAnalysisConfig(name="AnalysisConfig", TreeName=name)
        ToolSvc += AnaConfig
    return getattr(ToolSvc, "AnalysisConfig")


def Setup4LeptonSFHelper(TreeName="4LepScaleFactor"):
    from AthenaCommon.AppMgr import ToolSvc
    from AthenaCommon import CfgMgr
    if not hasattr(ToolSvc, "AnalysisHelper"):
        from XAMPPmultilep.XAMPPmultilepConf import XAMPP__SUSY4LeptonScaleFactorAnalysisHelper
        BaseHelper = CfgMgr.XAMPP__SUSY4LeptonScaleFactorAnalysisHelper("AnalysisHelper")
        BaseHelper.AnalysisConfig = Setup4LeptonSFConfig(TreeName)
        BaseHelper.SystematicsTool = SetupSystematicsTool(noJets=False,
                                                          noBtag=False,
                                                          noElectrons=False,
                                                          noMuons=False,
                                                          noTaus=False,
                                                          noDiTaus=True,
                                                          noPhotons=False)
        BaseHelper.ElectronSelector = Setup4LElectronSelector()
        BaseHelper.MuonSelector = Setup4LMuonSelector()
        BaseHelper.TauSelector = Setup4LTauSelector()
        BaseHelper.TruthSelector = Setup4LTruthSelector()
        ToolSvc += BaseHelper
    return getattr(ToolSvc, "AnalysisHelper")


def SetupTTgammaPhotonSelector():
    from AthenaCommon.AppMgr import ToolSvc
    from AthenaCommon import CfgMgr
    if not hasattr(ToolSvc, "SUSYPhotonSelector"):
        from XAMPPmultilep.XAMPPmultilepConf import XAMPP__SUSYttYPhotonSelector
        PhotonSelector = CfgMgr.XAMPP__SUSYttYPhotonSelector("SUSYPhotonSelector")
        ToolSvc += PhotonSelector
    return getattr(ToolSvc, "SUSYPhotonSelector")


def setupTTgammaConfig(name="ttGamma"):
    from AthenaCommon.AppMgr import ToolSvc
    from AthenaCommon import CfgMgr
    if not hasattr(ToolSvc, "AnalysisConfig"):
        from XAMPPmultilep.XAMPPmultilepConf import XAMPP__SUSYttYAnalysisConfig
        AnaConfig = CfgMgr.XAMPP__SUSYttYAnalysisConfig(name="AnalysisConfig", TreeName=name)
        ToolSvc += AnaConfig
    return getattr(ToolSvc, "AnalysisConfig")


def setupTTgammaHelper(TreeName="ttGamma"):
    from AthenaCommon.AppMgr import ToolSvc
    from AthenaCommon import CfgMgr
    if not hasattr(ToolSvc, "AnalysisHelper"):
        from XAMPPmultilep.XAMPPmultilepConf import XAMPP__SUSYttYAnalysisHelper
        BaseHelper = CfgMgr.XAMPP__SUSYttYAnalysisHelper("AnalysisHelper")
        BaseHelper.AnalysisConfig = setupTTgammaConfig(TreeName)
        BaseHelper.SystematicsTool = SetupSystematicsTool(noJets=False,
                                                          noBtag=False,
                                                          noElectrons=False,
                                                          noMuons=False,
                                                          noTaus=False,
                                                          noDiTaus=True,
                                                          noPhotons=False)
        BaseHelper.ElectronSelector = Setup4LElectronSelector()
        BaseHelper.MuonSelector = Setup4LMuonSelector()
        BaseHelper.TauSelector = Setup4LTauSelector()
        BaseHelper.PhotonSelector = SetupTTgammaPhotonSelector()
        BaseHelper.TruthSelector = Setup4LTruthSelector()
        ToolSvc += BaseHelper
    return getattr(ToolSvc, "AnalysisHelper")


def ParseCommonFourLeptonOptions(STFile="XAMPPmultilep/SUSYTools_SUSY4L.conf", TriggerSkimming=False):

    from AthenaCommon.Logging import logging
    from XAMPPbase.Utils import GetPropertyFromConfFile
    from ClusterSubmission.Utils import ResolvePath
    recoLog = logging.getLogger('XAMPP 4LeptonToolSetup')

    ################# electron triggers ####################
    SingleElectronTriggers2015 = ["HLT_e24_lhmedium_L1EM20VH", "HLT_e60_lhmedium", "HLT_e120_lhloose"]

    SingleElectronTriggers2016 = ["HLT_e26_lhtight_nod0_ivarloose", "HLT_e60_lhmedium_nod0", "HLT_e140_lhloose_nod0"]

    DiElectronTriggers2015 = ["HLT_2e12_lhloose_L12EM10VH"]

    DiElectronTriggers2016 = ["HLT_2e17_lhvloose_nod0"]

    DiElectronTriggers2017 = ["HLT_2e17_lhvloose_nod0_L12EM15VHI", "HLT_2e24_lhvloose_nod0"]

    #Electron Trig SF
    #Note: last update: 21.2.59, map2,
    EleTrigSFConfig = [  # other possibility: "SF-name;isolation-name"
        #single electrons
        "SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0",
        #di-electrons
        "DI_E_2015_e12_lhloose_L1EM10VH_2016_e17_lhvloose_nod0_2017_2018_e24_lhvloose_nod0_L1EM20VH",  # 2e24 only for 2017 B5-B8
        "2017_e17_lhvloose_nod0_L1EM15VHI",  #deployed in 21.2.59 in map2 of consolidated rec's, aimed for all 2017 but B4-B8 -->NOTE: not written in the trees by xampp
        #combined ele-muo
        "MULTI_L_2015_e17_lhloose_2016_2018_e17_lhloose_nod0",
        "MULTI_L_2015_e7_lhmedium_2016_2018_e7_lhmedium_nod0",
        "MULTI_L_2015_e12_lhloose_2016_2018_e12_lhloose_nod0",
        "MULTI_L_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_e26_lhmedium_nod0_L1EM22VHI_2017_2018_e26_lhmedium_nod0",  #
    ]

    #Electron Trig Matching for SF
    ElectronTriggerSF = [  # OR between different trig items of a given SF string
        "e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_OR_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0",
        "2e12_lhloose_L1EM10VH_OR_2e17_lhvloose_nod0_OR_2e24_lhvloose_nod0_L1EM20VH",  #2e24 only for 2017 B5-B8 
        "2e17_lhvloose_nod0_L1EM15VHI",  #aimed for 2017 but B4-B8, present only in 2017-2018 -->NOTE: not recongnised by xampp
        "e17_lhloose_OR_e17_lhloose_nod0",
        "e7_lhmedium_OR_e7_lhmedium_nod0",
        "e12_lhloose_OR_e12_lhloose_nod0",
        "e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_OR_e26_lhmedium_nod0_L1EM22VHI_OR_e26_lhmedium_nod0",
    ]

    SingleElectronTriggers = SingleElectronTriggers2015 + SingleElectronTriggers2016

    ############  Muon triggers #######################
    SingleMuonTriggers2015 = [
        "HLT_mu20_iloose_L1MU15",
        "HLT_mu20_iloose",
        "HLT_mu40",
        "HLT_mu50",
        "HLT_mu24_imedium",
    ]

    DiMuonTriggers2015 = ["HLT_2mu10", "HLT_mu18_mu8noL1"]

    SingleMuonTriggers2016 = ["HLT_mu24_ivarmedium", "HLT_mu26_ivarmedium", "HLT_mu40", "HLT_mu50"]

    DiMuonTriggers2016 = [
        "HLT_2mu10",
        "HLT_2mu14",
        "HLT_mu20_mu8noL1",
        "HLT_mu22_mu8noL1",
    ]

    # muon trig SF
    MuonTriggerSF2015 = ["HLT_mu20_iloose_L1MU15_OR_HLT_mu40", "HLT_2mu10", "HLT_mu18_mu8noL1"]

    MuonTriggerSF2016 = [
        "HLT_mu24_ivarmedium_OR_HLT_mu40", "HLT_mu24_ivarmedium_OR_HLT_mu50", "HLT_mu26_ivarmedium_OR_HLT_mu50", "HLT_2mu10", "HLT_2mu14",
        "HLT_mu20_mu8noL1", "HLT_mu22_mu8noL1"
    ]

    MuonTriggerSF2017 = ["HLT_mu26_ivarmedium_OR_HLT_mu50", "HLT_2mu14", "HLT_mu22_mu8noL1"]

    SingleMuonTriggers = SingleMuonTriggers2015 + SingleMuonTriggers2016

    ################## electron-muon triggers   #######################
    DiLeptonElectronMuon = [
        #2015
        "HLT_e17_lhloose_mu14",
        #2017/16
        "HLT_e17_lhloose_nod0_mu14",
        #2015
        "HLT_e24_lhmedium_L1EM20VH_mu8noL1",  # ZZ 2/5/2018: to be checked
        #2016
        "HLT_e26_lhmedium_nod0_L1EM22VHI_mu8noL1",
        #2017
        "HLT_e26_lhmedium_nod0_mu8noL1",
        #2015
        "HLT_e7_lhmedium_mu24",
        #2016/17
        "HLT_e7_lhmedium_nod0_mu24"
    ]

    TriLeptonElectronMuon = ["HLT_e12_lhloose_nod0_2mu10", "HLT_2e12_lhloose_nod0_mu10"]

    #electron legs of multi-lep triggers meant for trigger matching
    #driven by the SF menu above. Update the single triggers menu.
    SingleElectronTriggers += [
        "HLT_e7_lhmedium", "HLT_e7_lhmedium_nod0", "HLT_e17_lhloose", "HLT_e17_lhloose_nod0", "HLT_e12_lhloose_nod0",
        "HLT_2e12_lhloose_nod0"
    ]

    # electron-muon trigger SFs: updating the previous lists
    MuonTriggerSF2015 += [
        "HLT_mu14",
        "HLT_mu8noL1",  # ZZ 2/5/2018: remove it if HLT_e24_lhmedium_L1EM20VH_mu8noL1 is not used in 2015
        "HLT_mu24",
        "HLT_mu10"
    ]

    MuonTriggerSF2016 += [
        "HLT_mu8noL1",
        "HLT_mu14",
        "HLT_mu24",
        "HLT_mu10"  # Note: not available as of rel. 21.2.27 (30 April '18)
    ]

    MuonTriggerSF2017 += [
        "HLT_mu8noL1",  # Note: not available as of rel. 21.2.27 (30 April '18)
        "HLT_mu14",  # Note: not available as of rel. 21.2.27 (30 April '18)
        "HLT_mu24",  # Note: not available as of rel. 21.2.27 (30 April '18)
        "HLT_mu10"  # Note: not available as of rel. 21.2.27 (30 April '18)
    ]

    ################ Lepton Triggers ##############################################
    SingleLepTriggers = list(set(SingleMuonTriggers + SingleElectronTriggers))

    DiLepTriggers = list(
        set(DiMuonTriggers2015 + DiMuonTriggers2016 + DiElectronTriggers2015 + DiElectronTriggers2016 + DiElectronTriggers2017 +
            DiLeptonElectronMuon))

    TriLepTriggers = list(set(TriLeptonElectronMuon))

    SetupSUSYTriggerTool().Triggers = SingleLepTriggers + DiLepTriggers + TriLepTriggers
    SetupSUSYTriggerTool().DisableSkimming = not TriggerSkimming
    SetupSUSYTriggerTool().SignalElecForMatching = False
    SetupSUSYTriggerTool().SignalMuonForMatching = False

    Setup4LElectronSelector().SFTrigger = ElectronTriggerSF
    Setup4LElectronSelector().TriggerSFconfig = EleTrigSFConfig

    Setup4LMuonSelector().SFTrigger2015 = MuonTriggerSF2015
    Setup4LMuonSelector().SFTrigger2016 = MuonTriggerSF2016
    Setup4LMuonSelector().SFTrigger2017 = MuonTriggerSF2017
    Setup4LMuonSelector().SFTrigger2018 = MuonTriggerSF2017

    SetupSUSYMetSelector().WriteMetSignificance = True

    ParseBasicConfigsToHelper(STFile, SeparateSF=False)

    #
    #    Account for the m_LL removal after the first step of OR
    Setup4LMuonSelector().BaselineDecorator = "passLMR"
    #   Apply the cosmic veto w.r.t overlap removal step
    Setup4LMuonSelector().CosmicSelectionDecorator = "passOR"
    Setup4LElectronSelector().BaselineDecorator = "passLMR"

    Setup4LElectronSelector().WriteBaselineSF = True
    Setup4LMuonSelector().WriteBaselineSF = True
    Setup4LTauSelector().WriteBaselineSF = True

    #Setup4LElectronSelector().PreSelectionIP_Z0Cut = 0.5
    #Setup4LMuonSelector().PreSelectionIP_Z0Cut = 0.5

    #
    #
    #    Account for the isolation correction
    Setup4LMuonSelector().IsolationDecorator = "CORR_isol"
    Setup4LElectronSelector().IsolationDecorator = "CORR_isol"

    Setup4LTauSelector().SeparateSF = False
    Setup4LMuonSelector().RequireIsoSignal = True
    Setup4LElectronSelector().RequireIsoSignal = True

    Setup4LElectronSelector().StoreTruthClassifier = False

    SetupFourLeptonAnaHelper().fillLHEWeights = True

    SetupSUSYTools().JetLargeRcollection = ""

    if not isData():
        ### Get rid of the stupdily large event weights in the case we're running over sherpa
        generator = getFlags().generators()
        if generator.lower().find("sherpa") != -1:
            logging.info("We're running over a Sherpa sample (DSID: %d). Limit the GenWeight to be within 50." %
                         (getRunNumbersMC() if getMCChannelNumber() == 0 else getMCChannelNumber()))
            ### remove the crazy events from the meta data
            SetupFourLeptonAnaHelper().OutlierWeightStrategy = 1
            SetupFourLeptonAnaHelper().OutlierWeightThreshold = 50.

    ##### We do not use the charge flip tagger
    ##### And no High pt working point...
    SetupSystematicsTool().pruneSystematics = [
        "EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1down",
        "EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1up",
        "MUON_EFF_BADMUON_STAT__1down",
        "MUON_EFF_BADMUON_STAT__1up",
        "MUON_EFF_BADMUON_SYS__1down",
        "MUON_EFF_BADMUON_SYS__1up",
    ]

    ###############################################################################################
    ##      The BTagging people broke the naming schme of their containers
    ##  https://twiki.cern.ch/twiki/bin/view/AtlasProtected/BTagTaggerRecommendationsRelease21
    ##  https://indico.cern.ch/event/836130/contributions/3504999/attachments/1884841/3106548/2019-07-23-FTAGPlenary.pdf
    ###############################################################################################
    try:
        p_tag = int(getFlags().p_tag()[1:])
    except:
        recoLog.warning("Could not derive the p-tag")
        p_tag = -1

    if p_tag >= 3954:
        SetupSUSYJetSelector().AntiKt4BTagContainer = "AntiKt4EMTopoJets_BTagging201810"

        return
        tagger = GetPropertyFromConfFile(STFile, "Btag.Tagger")
        b_tag_wp = GetPropertyFromConfFile(STFile, "Btag.WP")
        jet_input = int(GetPropertyFromConfFile(STFile, "Jet.InputType"))

        b_tag_container = ""
        ### 1 is EM Topo
        if jet_input == 1:
            ### The ancient relict from PowHeg
            b_tag_container = "AntiKt4EMTopoJets_BTagging201810"
        ### 9 is PFlow
        elif jet_input == 9:
            ### Actually we need to check which one is the recommended one for PFlow
            b_tag_container = "AntiKt4EMPFlowJets_BTagging201810"
            #b_tag_container = "AntiKt4EMPFlowJets_BTagging201903"
        else:
            recoLog.error("I've no idea what JetCollection you're aiming for. But it will not work")
            exit(1)

        #####################################################################################################################################
        #####################################################################################################################################
        #  Cf. SUSYTools for more information about the numbering scheme
        shower_id = GetBTagShowerDSID()
        cdi_file = "xAODBTaggingEfficiency/13TeV/2019-21-13TeV-MC16-CDI-2019-10-07_v1.root"
        recoLog.info("The standard BTagging container scheme is broken. Will setup the Tool")
        recoLog.info("    --- Container:       %s" % (b_tag_container))
        recoLog.info("    --- Tagger:          %s" % (tagger))
        recoLog.info("    --- WorkingPoint:    %s" % (b_tag_wp))
        recoLog.info("    --- CDIFile:         %s" % (ResolvePath(cdi_file)))
        recoLog.info("    --- ShowerID:        %d" % (shower_id))

        SetupSUSYTools().BTaggingSelectionTool = GetBtagSelectionTool(JetCollection=b_tag_container,
                                                                      WorkingPoint=b_tag_wp,
                                                                      Tagger=tagger,
                                                                      CalibPath=cdi_file)
        SetupSUSYTools().BTaggingSelectionTool_OR = GetBtagSelectionTool(JetCollection=b_tag_container,
                                                                         WorkingPoint=b_tag_wp,
                                                                         Tagger=tagger,
                                                                         CalibPath=cdi_file)

        SetupSUSYTools().BTaggingEfficiencyTool = SetupBtaggingEfficiencyTool(JetCollection=b_tag_container,
                                                                              WorkingPoint=b_tag_wp,
                                                                              Tagger=tagger,
                                                                              CalibPath=cdi_file,
                                                                              SystStrategy="Envelope",
                                                                              EigenvecRedB="Loose",
                                                                              EigenvecRedC="Loose",
                                                                              EigenvecRedLight="Loose",
                                                                              EffBCalib=str(shower_id),
                                                                              EffCCalib=str(shower_id),
                                                                              EffTCalib=str(shower_id),
                                                                              EffLightCalib=str(shower_id))
