#include the common library instead of importing it. So we have full access to the globals
# Python you're a strange guy sometimes. Sometimes? Alsmost the entire day
include("XAMPPmultilep/share/FourLeptonToolSetup.py")
AssembleIO()

SingleLepTriggers = [
    "HLT_e24_lhmedium_iloose_L1EM20VH", "HLT_e24_lhmedium_iloose_L1EM18VH", "HLT_e60_lhmedium", "HLT_e26_lhtight_iloose",
    "HLT_e120_lhloose", "HLT_mu20_iloose_L1MU15", "HLT_mu24_iloose_L1MU15", "HLT_mu26_imedium", "HLT_mu40", "HLT_mu50"
]
DiLepTriggers = [
    "HLT_2e12_lhloose_L12EM10VH", "HLT_2e15_lhloose_L12EM13VH", "HLT_2mu10", "HLT_2mu14", "HLT_e7_lhmedium_mu24", "HLT_e17_lhloose_mu14"
]
TriLepTriggers = ["HLT_e17_lhmedium_2e9_lhmedium", "HLT_2e12_lhloose_mu10", "HLT_e12_lhloose_2mu10", "HLT_3mu6"]

#Setup4LDiTauSelector()
#SetupAnalysisHelper()
SetupFourLeptonAnaHelper()

ParseBasicConfigsToHelper(STConfigFile="XAMPPmultilep/SUSYTools_SUSY4L.conf")

SystTool = SetupSystematicsTool()
SystTool.doNoDiTaus = False

SetupSUSYTriggerTool().SingleLeptonTriggers = SingleLepTriggers
SetupSUSYTriggerTool().DiLeptonTriggers = DiLepTriggers
SetupSUSYTriggerTool().TriLeptonTriggers = TriLepTriggers
SetupAlgorithm()
