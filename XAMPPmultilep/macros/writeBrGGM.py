import os, argparse, logging
from ClusterSubmission.Utils import ResolvePath, CreateDirectory, WriteList, MakePathResolvable
from XAMPPplotting.FileUtils import ReadInputConfig
if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='This script makes input configs for the fake-background composition from a given directory',
        prog='writeFakeCompositionConfigs')
    parser.add_argument("--in_cfg_dir", help="Relative path of the input config files", required=True)
    parser.add_argument("--out_dir", help="output_directory for the fake composition config files", default='GGM_BranchingRatios')
    parser.add_argument("--reweight_to",
                        help="Keywords to be inserted into the input config such that reweighting takes place",
                        nargs="+",
                        default=[
                            "ReweightGGM0",
                            "ReweightGGM25",
                            "ReweightGGM75",
                            "ReweightGGM100",
                            "ReweightGGM50",
                        ])

    options = parser.parse_args()

    in_cfg_dir = ResolvePath(options.in_cfg_dir)
    if not in_cfg_dir: exit(1)

    GGM_samples = []
    for smp in os.listdir(in_cfg_dir):
        if not smp.startswith("GGM"): continue
        GGM_samples += [smp]
    if len(GGM_samples) == 0:
        logging.error("No GGM config files found at %s" % (options._in_cfg_dir))
        exit(1)

    if len(options.reweight_to) == 0:
        logging.error("Please give at least one valid define statement to which GGM is going to be reweighted")
        exit(1)
    for smp in GGM_samples:
        for br in options.reweight_to:
            cfg_name = "GGM_%s_%s" % (smp[smp.rfind("_") + 1:smp.rfind(".")], br.replace("ReweightGGM", ""))
            WriteList(["Import %s/%s" % (MakePathResolvable(options.in_cfg_dir), smp),
                       "define %s" % (br)], "%s/%s.conf" % (options.out_dir, cfg_name))
