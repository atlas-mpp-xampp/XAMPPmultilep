import os, sys, argparse, ROOT, math
from ClusterSubmission.Utils import CreateDirectory, IsROOTFile
from XAMPPplotting import Utils
from XAMPPplotting import PlotUtils
from XAMPPplotting import PlottingHistos

from collections import defaultdict
TObjects = []


def FlatEff(region):  # approximate reco+id lepton eff at 20 GeV
    if "4L" in region or "Incl" in region:
        return 0.9**4
    elif "3L" in region and "1T" in region:
        return 0.6 * 0.9**3
    elif "2L" in region and "2T" in region:
        return (0.6**2) * (0.9**3)
    else:
        return 0.


def ReadInFiles(InDir="./", Model="C1C1", Couplings="LLE12k"):
    OutList = {}
    for File in os.listdir(InDir):
        if not IsROOTFile(File): continue
        # The pattern of the files should be  TRUTH_<model>_NLSP_LSP_<coupling>.root
        a = 0
        if "TRUTH" in File.split("_"):
            a = 1
        if ".root" not in File or Model not in File.split("_")[a] or Couplings not in File.split("_")[-1]:
            continue
        try:
            NLSP = int(File.split("_")[a + 1])
            LSP = int(File.split("_")[a + 2])
        except:
            NLSP = int(File.split("_")[a + 1])
            LSP = -1
        OutList[(NLSP, LSP)] = InDir + "/" + File
    return OutList


def ReadInGrid(InFiles, Options, Region="Inclusive", Var="meff_met"):
    Grid = {}
    for N, L in sorted(InFiles.iterkeys()):
        Path = InFiles[(N, L)]
        Histo = ROOT.XAMPP.SampleHisto("%d_%d" % (N, L), Var, Options.analysis, Region, Path)
        Histo.SetLumi(Options.lumi)
        Grid[(N, L)] = Histo
    return Grid


def GetGridYields(Options, Grid, MeffCut=0, MetCut=0, integrate=False):
    Yields = {}
    for N, L in Grid.iterkeys():
        if L < 0:
            continue
        Histo = Grid[(N, L)]
        BinX = Histo.GetHistogram().GetXaxis().FindBin(MeffCut)
        BinY = Histo.GetHistogram().GetYaxis().FindBin(MetCut)
        Nevt = Utils.IntegrateTH2(Histo.GetHistogram(), StartX=BinX, StartY=BinY) if not integrate else Histo.GetBinContent(
            Histo.GetBin(BinX, BinY))
        if Nevt <= 0:
            print "WARNING: Skip point %d %d as no contribution is expected" % (N, L)
            if not Options.include_low_yield_bins:
                continue
        Yields[(N, L)] = Nevt
    return Yields


def GetDirectYields(Grid, MeffCut=0, MetCut=0):
    Yields = {}
    for N, L in Grid.iterkeys():
        if L > 0:
            continue
        Histo = Grid[(N, L)]
        BinX = Histo.GetHistogram().GetXaxis().FindBin(MeffCut)
        BinY = Histo.GetHistogram().GetYaxis().FindBin(MetCut)
        Yields[N] = Utils.IntegrateTH2(Histo.GetHistogram(), StartX=BinX, StartY=BinY)
    return Yields


def GetDummyEffGrid(Grid, Eff=0.65):
    EffGrid = {}
    try:
        for N, L in Grid.iterkeys():
            EffGrid[(N, L)] = Eff
    except:
        for N in Grid.iterkeys():
            EffGrid[N] = Eff
    return EffGrid


def GetRecoEfficiency(TruthYields, RecoYields):
    Eff = {}
    for N, L in RecoYields.iterkeys():
        if (N, L) not in TruthYields.iterkeys():
            continue
        if L < 0:
            continue
        E = RecoYields[(N, L)] / TruthYields[(N, L)]
        Eff[(N, L)] = E
    return Eff


def GetEventsPer5K(Acceptance, RecoEff, ReWeight=1.):
    EvPer5k = {}
    EffNorm = Eff50 = Eff10 = 0.
    NNorm = N50 = N10 = 0
    for N, L in RecoEff.iterkeys():
        if L >= 100:
            EffNorm += RecoEff[(N, L)]
            NNorm += 1
        elif L == 50:
            Eff50 += RecoEff[(N, L)]
            N50 += 1
        elif L == 10:
            Eff10 += RecoEff[(N, L)]
            N10 += 1
    EffNorm = EffNorm / NNorm
    Eff50 = Eff50 / N50
    Eff10 = Eff10 / N10
    print "Found average reconstruction efficiencies of %f for M_{LSP}>100, %f for M_{LSP}=50 and %f for M_{LSP}=10" % (EffNorm, Eff50,
                                                                                                                        Eff10)
    for N, L in Acceptance.iterkeys():
        Events = 5000 * ReWeight * Acceptance[(N, L)]
        if (N, L) in RecoEff.iterkeys():
            Events *= RecoEff[(N, L)]
        elif L >= 100:
            Events *= EffNorm
        elif L == 50:
            Events *= Eff50
        elif L == 10:
            Events *= Eff10
        EvPer5k[(N, L)] = Events
    return EvPer5k


def GetEventsPer5kDirect(Acceptance, RecoEff, FlatEff=0.8, ReWeight=1.):
    EvPer5k = {}
    for N in Acceptance.iterkeys():
        E = 5000 * Acceptance[N] * ReWeight
        if N in RecoEff.iterkeys():
            E *= RecoEff[N]
        else:
            E *= FlatEff
        EvPer5k[N] = E
    return EvPer5k


def CalcNeed(AcceptedEv, UpperBound):
    if AcceptedEv < 1:
        return -1
    U = 1
    while (1. / math.sqrt(AcceptedEv * U) > UpperBound):
        U = U + 1
    return U * 5000


def GetNeededEvents(EvPer5k, UpperBound):
    NeedEv = {}
    #print EvPer5k
    try:
        for N, L in EvPer5k.iterkeys():
            E = CalcNeed(EvPer5k[(N, L)], UpperBound)
            if E == -1:
                continue
            NeedEv[(N, L)] = E
    except:
        for N in EvPer5k.iterkeys():
            E = CalcNeed(EvPer5k[N], UpperBound)
            if E == -1:
                continue
            NeedEv[N] = E
    #print NeedEv
    return NeedEv


def GetGridPoints(GridValues):
    NLSPmasses = []
    LSPmasses = {}
    if len(GridValues) > 0:
        [NLSPmasses.append(m[0]) for m in GridValues.iterkeys() if not m[0] in NLSPmasses]
        NLSPmasses = sorted(NLSPmasses)
        for mN in NLSPmasses:
            LSPmasses[mN] = sorted([k[1] for k in GridValues.iterkeys() if k[0] == mN])
    return NLSPmasses, LSPmasses


def GetGridHistogram(GridValues, xAxisTitle, zAxisTitle):
    NLSPmasses, LSPmasses = GetGridPoints(GridValues)
    Graph = ROOT.TGraph2D()
    TObjects.append(Graph)
    minLSP = 1e8
    minNLSP = 1e8
    maxLSP = 0
    maxNLSP = 0
    for mN in NLSPmasses:
        for mL in LSPmasses[mN]:
            Graph.SetPoint(Graph.GetN(), mN, mL, GridValues[(mN, mL)])
            minLSP = mL if mL < minLSP else minLSP
            minNLSP = mN if mN < minNLSP else minNLSP
            maxLSP = mL if mL > maxLSP else maxLSP
            maxNLSP = mN if mN > maxNLSP else maxNLSP
    GrdHisto = Graph.GetHistogram()

    GrdHisto.GetXaxis().SetRangeUser(minNLSP, maxNLSP * 1.05)
    GrdHisto.GetYaxis().SetRangeUser(minLSP, maxLSP * 1.05)
    GrdHisto.GetXaxis().SetTitle("m_{" + xAxisTitle + "} [GeV]")
    TickX = 8
    TickY = 9
    GrdHisto.GetXaxis().SetNdivisions(TickX)
    GrdHisto.GetYaxis().SetNdivisions(TickY)
    GrdHisto.GetYaxis().SetTitle("m_{#tilde{#chi}^{0}_{1}} [GeV]")
    GrdHisto.GetZaxis().SetLabelOffset(1.4 * GrdHisto.GetZaxis().GetLabelOffset())
    GrdHisto.GetZaxis().SetTitle(zAxisTitle)

    GrdHisto.SetContour(2000)
    return GrdHisto


def DrawHistogram(EffectiveGrid, Grid, Options, Region, MeffCut, MetCut, bonusstr=""):
    if len(Grid) == 0:
        return False

    pu = PlotUtils.PlotUtils(status=Options.label)
    pu.Size = 24
    pu.Lumi = Options.lumi
    PlotName = ""
    if len(bonusstr) == 0:
        PlotName = "%s_%s_%s_%d_%d_%dfb" % (Options.Model, Options.Couplings, Region, MeffCut, MetCut, Options.lumi)
    else:
        PlotName = "%s_%s_%s_%d_%d_%dfb_%s" % (Options.Model, Options.Couplings, Region, MeffCut, MetCut, Options.lumi, bonusstr)

    pu.Prepare1PadCanvas(PlotName, 800, 600)
    can = pu.GetCanvas()

    H1 = GetGridHistogram(Grid, Options.NLSPname, Options.zAxis)
    if Utils.IntegrateTH2(H1) <= 0.:
        return False

    # rescale the yields if requested
    if Options.apply_flat_eff and not "Acc" in Region:
        H1.Scale(FlatEff(Region))

    #reset ranges to make the differences more visible
    zMin = H1.GetBinContent(H1.GetMinimumBin())
    zmax = H1.GetBinContent(H1.GetMaximumBin())
    if not "Acc" in Region:
        H1.GetZaxis().SetRangeUser(3, zmax)  # do not paint areas with < 3 events
    elif "Acc" in Region:
        H1.GetZaxis().SetRangeUser(zMin, zmax)  # set lower contour level to minimum efficiency
    if Options.SetMax > 0.:
        H1.SetMaximum(Options.SetMax)

    H1.Draw("COLZ")

    can.SetTopMargin(0.15)
    can.SetRightMargin(0.18)
    if not Options.noATLAS:
        pu.DrawAtlas(can.GetLeftMargin(), 0.92)
    pu.DrawLumiSqrtS(can.GetLeftMargin(), 0.87)

    x0 = 0.22
    y0 = 0.72

    CutValues = ""
    if int(MeffCut) > 0:
        CutValues += "m_{eff}>%i GeV" % (int(MeffCut))
        if int(MetCut) > 0: CutValues += ", "
    if int(MetCut) > 0:
        CutValues += "E_{T}^{miss}>%i GeV" % (int(MetCut))
    pu.DrawTLatex(x0, y0, CutValues, 26)
    pu.DrawTLatex(x0, y0 - 0.07, region, 26)
    if Options.display_grid:

        for key, val in sorted(Grid.iterkeys()):
            MA = ROOT.TMarker(key, val, 2)
            TObjects.append(MA)
            MA.SetMarkerSize(1)
            MA.SetMarkerColor(13)
            MA.Draw()

            if str(key) in EffectiveGrid and str(val) in EffectiveGrid[str(key)]:
                print "Marking %i %i as effective grid point for official request" % (key, val)
                MAeff = ROOT.TMarker(key, val, 4)
                TObjects.append(MAeff)
                MAeff.SetMarkerSize(2)
                MAeff.SetMarkerColor(41)
                MAeff.Draw()

    pu.saveHisto("%s/%s" % (Options.outputDir, PlotName), Options.OutFileType)
    pu.saveHisto(
        Utils.RemoveSpecialChars("%s/Acceptance_%s_%s_%s_%d" % (Options.outputDir, Options.Model, Options.Couplings, region, Options.lumi)),
        Options.OutFileType)
    return True


def DrawHistogram1D(Column, Options, Region, MeffCut, MetCut, bonusstr=""):
    if len(Column) == 0:
        return False

    if len(bonusstr) == 0:
        PlotName = "%s_direct%s_%s_%d_%d_%dfb" % (Options.Model, Options.Couplings, Region, MeffCut, MetCut, Options.lumi)
    else:
        PlotName = "%s_direct%s_%s_%d_%d_%dfb_%s" % (Options.Model, Options.Couplings, Region, MeffCut, MetCut, Options.lumi, bonusstr)

    H1 = ROOT.TH1D("TH1" + PlotName, "TH1", len(Column), 0, len(Column))
    H1.GetXaxis().SetTitle("m_{" + Options.NLSPname + "} [GeV]")
    H1.GetYaxis().SetTitle(Options.zAxis)
    TObjects.append(H1)
    Bin = 1
    for N in sorted(Column.iterkeys()):
        H1.GetXaxis().SetBinLabel(Bin, str(N))
        H1.SetBinContent(Bin, Column[N])
        Bin += 1
    if Utils.IntegrateTH1(H1) <= 0.:
        return False
    pu = PlotUtils.PlotUtils(status=Options.label)
    pu.Size = 24
    pu.Lumi = Options.lumi
    pu.Prepare1PadCanvas(PlotName, 800, 600)

    ymin, ymax = pu.GetFancyAxisRanges([H1], Options)
    pu.drawStyling(H1, ymin, ymax, RemoveLabel=False, TopPad=False)

    H1.Draw("HISTsameTEXT")

    xCoordForDrawing = 0.195
    yCoordForDrawing = 0.83
    if not Options.noATLAS:
        pu.DrawAtlas(xCoordForDrawing, yCoordForDrawing)
        yCoordForDrawing -= 0.06
        pu.DrawLumiSqrtS(xCoordForDrawing, yCoordForDrawing)
        yCoordForDrawing -= 0.06
    else:
        pu.DrawLumiSqrtS(xCoordForDrawing, yCoordForDrawing)
        yCoordForDrawing -= 0.06

    CutValues = ""
    if int(MeffCut) > 0:
        CutValues += "m_{eff}>%i GeV" % (int(MeffCut))
        if int(MetCut) > 0: CutValues += ", "
    if int(MetCut) > 0:
        CutValues += "E_{T}^{miss}>%i GeV" % (int(MetCut))
    pu.DrawTLatex(xCoordForDrawing, yCoordForDrawing, CutValues, 26)
    yCoordForDrawing -= 0.06
    pu.DrawRegionLabel(Options.analysis, region, xCoordForDrawing, yCoordForDrawing)
    pu.saveHisto("%s/%s" % (Options.outputDir, PlotName), Options.OutFileType)
    pu.saveHisto(
        Utils.RemoveSpecialChars("%s/Acceptance_%s_%s_%s_%d" % (Options.outputDir, Options.Model, Options.Couplings, region, Options.lumi)),
        Options.OutFileType)
    return True


def WriteNeedEvToFile(NeedEv, Acceptance, OutPath, ModelName, MeffCut, MetCut):

    if len(NeedEv) == 0:
        return
    Path = "%s/%s_%d_%d.txt" % (OutPath, ModelName, MeffCut, MetCut)
    OutFile = open(Path, "w")
    OutFile.write("M(NLSP)\tM(LSP)\tAcceptance\tMinEvts\tN(x 5k evts)\tSum Evts\n")
    try:
        NLSPmasses, LSPmasses = GetGridPoints(NeedEv)
        totalEvents = 0
        for mN in NLSPmasses:
            for mL in LSPmasses[mN]:
                nEvts = NeedEv[(mN, mL)]
                totalEvents += nEvts
                nFiles = math.ceil(nEvts / 5e3)
                Line = "%d\t%d\t%f\t%d\t%d\t\t%d\n" % (mN, mL, Acceptance[(mN, mL)], nEvts, nFiles, totalEvents)
                OutFile.write(Line)
    except:
        for N in sorted(NeedEv.iterkeys()):
            Line = "%d   %f    %d\n" % (N, Acceptance[N], NeedEv[N])
            OutFile.write(Line)
    print OutFile.name, " has been created..."
    OutFile.close()


if __name__ == '__main__':

    ROOT.gROOT.Macro("rootlogon.C")
    ROOT.gStyle.SetPalette(ROOT.kViridis)
    ROOT.gROOT.SetBatch(1)

    parser = argparse.ArgumentParser(
        description='This script produces MC only histograms from tree files. For more help type \"python MCPlots.py -h\"',
        prog='MCPlots',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser = Utils.setupBaseParser(parser)
    parser.set_defaults(lumi=139)
    parser.set_defaults(outputDir="GridDesign")
    parser.add_argument("--InputPath", help='Define the path where all the input histograms are stored', required=True)
    parser.add_argument("--Model", help='Which model should be considered', required=True)
    parser.add_argument("--Couplings", help='Specify the couplings.', required=True)

    parser.add_argument("--MeffCut", help="Specify the cut on Meff", type=int, nargs="+", default=[0])
    parser.add_argument("--MetCut", help="Specify the cut on Met", type=int, nargs="+", default=[0])

    parser.add_argument("--NLSPname", help="Name of the x-axis", default="#tilde{#chi}^{#pm}_{1}")
    parser.add_argument("--display_grid", help='Display grid points', default=False, action="store_true")
    parser.add_argument("--apply_flat_eff",
                        help='Apply an approximate flat efficiency based on the objects Reco+ID',
                        default=False,
                        action="store_true")

    parser.add_argument("--zAxis", help="z-Axis title", default="Expected events")

    parser.add_argument("--SetMax", help="Specify the maximum", type=float, default=-1)
    parser.add_argument("--SetMin", help="Specify the minimum", type=float, default=0)

    parser.add_argument("--GetNeedEv",
                        help="Asks whether the script should calculate the needed events",
                        action="store_true",
                        default=False)
    parser.add_argument("--MaxStatUncert", help="You can specify which relative statistical uncertainty you want to achieve", default=0.05)

    parser.add_argument("--accepted-grid-points",
                        help="Give a file with the grid points which are effectively going into production.",
                        type=str,
                        default="")

    parser.add_argument("--include-low-yield-bins", help='Include grid points with very low yield', default=False, action="store_true")

    Options = parser.parse_args()

    CreateDirectory(Options.outputDir, False)

    # read the effective grid if any
    effective_grid_points = defaultdict(list)
    if Options.accepted_grid_points:
        with open(Options.accepted_grid_points, "r") as ins:
            for line in ins:
                point = line.rstrip('\n').split("\t")
                if len(point) < 2:
                    continue
                nlsp = point[0]
                lsp = point[1]
                if nlsp in effective_grid_points:
                    effective_grid_points[nlsp].append(lsp)
                else:
                    effective_grid_points[nlsp] = list(lsp)

    if effective_grid_points:
        print "Effective grid points: ", effective_grid_points

    # Read in of the Filestructure to obtain all the regions in the files
    In = ReadInFiles(Options.InputPath, Options.Model, Options.Couplings)
    if not len(In):
        sys.exit("Cannot obtain files from structure...")
    FileStructure = ROOT.XAMPP.FileHandler.getInstance().GetFileStructure(In.itervalues().next())

    dummy = ROOT.TCanvas("dummy", "dummy", 800, 600)
    ana = FileStructure.get_analyses_names()[0]
    Options.analysis = ana
    region_names = [
        r.name() for r in FileStructure.get_analysis(ana).get_regions() if len(Options.regions) == 0 or r.name() in Options.regions
    ]
    for region in region_names:
        if "Acc" in region and Options.lumi != 1.:
            print "INFO: skipping acceptance region %s because lumi is %s" % (region, str(Options.lumi))
            continue
        PlotDrawn = False
        bonusstr = Utils.RemoveSpecialChars("Acceptance_%s_%s_%s_%d" % (Options.Model, Options.Couplings, region, Options.lumi))
        Histos = ReadInGrid(In, Options, region, "Cul_meff")
        Reco_Efficiency = GetDummyEffGrid(In, Eff=0.8)
        dummy.SaveAs("%s/%s.pdf[" % (Options.outputDir, bonusstr))
        #
        for M in Options.MeffCut:
            for E in Options.MetCut:
                Yields = GetGridYields(Options, Histos, M, E)
                if len(Yields) == 0:
                    continue
                PlotDrawn = DrawHistogram(effective_grid_points, Yields, Options, region, M, E) or PlotDrawn
                if Options.GetNeedEv and Options.lumi == 1. and "Acc" in region:
                    EvPer5k = GetEventsPer5K(Yields, Reco_Efficiency)
                    NeedEv = GetNeededEvents(EvPer5k, Options.MaxStatUncert)
                    zAxis = Options.zAxis
                    Options.zAxis = "Needed events"
                    PlotDrawn = DrawHistogram(effective_grid_points, NeedEv, Options, region, M, E, "NeedEv") or PlotDrawn
                    Options.zAxis = zAxis
                    WriteNeedEvToFile(NeedEv, Yields, Options.outputDir,
                                      Utils.RemoveSpecialChars(Options.Model + "_" + Options.Couplings + "_" + region), M, E)
        #
        for M in Options.MeffCut:
            for E in Options.MetCut:
                Yields = GetDirectYields(Histos, M, E)
                PlotDrawn = DrawHistogram1D(Yields, Options, region, M, E) or PlotDrawn
                if Options.GetNeedEv and Options.lumi == 1. and "Acc" in region:
                    EvPer5k = GetEventsPer5kDirect(Yields, Reco_Efficiency)
                    NeedEv = GetNeededEvents(EvPer5k, Options.MaxStatUncert)
                    zAxis = Options.zAxis
                    Options.zAxis = "Needed events"
                    PlotDrawn = DrawHistogram1D(NeedEv, Options, region, M, E, "NeedEv") or PlotDrawn
                    Options.zAxis = zAxis
                    WriteNeedEvToFile(NeedEv, Yields, Options.outputDir,
                                      Utils.RemoveSpecialChars(Options.Model + "_direct" + Options.Couplings + "_" + region), M, E)
        #
        dummy.SaveAs("%s/%s.pdf]" % (Options.outputDir, bonusstr))
        if not PlotDrawn:
            os.system("rm %s/%s.pdf" % (Options.outputDir, bonusstr))
