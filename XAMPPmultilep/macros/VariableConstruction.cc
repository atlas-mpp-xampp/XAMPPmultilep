/**************************************************

  Abhishek Sharma
  a.sharma@cern.ch

  Paul Jackson
  paul.douglas.jackson@cern.ch

  POWERED BY RESTFRAMES
  restframes.com

A package for calculating different kinematic variables for 4-lepton final states
 ***************************************************/

#include "VariableConstruction.hh"

#include <algorithm>

using namespace RestFrames;

VariableConstruction::VariableConstruction(TTree* tree) : NtupleBase<MultiLeptonBase>(tree) {
    DEBUG = false;
    ///////////////////////////////////////////////////////////////////////
    /// 1. Create RJigsaw for N2N2 -> ZZN1N1 -> 4L+MET High mass region
    ///////////////////////////////////////////////////////////////////////

    LAB_4L = new LabRecoFrame("LAB_4L", "lab4L");
    N2N2_4L = new DecayRecoFrame("N2N2_4L", "#tilde{#chi}^{0}_{2} #tilde{#chi}^{ 0}_{2}");
    N2a_4L = new DecayRecoFrame("N2a_4L", "#tilde{#chi}^{0}_{2}");
    N2b_4L = new DecayRecoFrame("N2b_4L", "#tilde{#chi}^{ 0}_{2}");

    Za_4L = new DecayRecoFrame("Za_4L", "Z_{a}");
    Zb_4L = new DecayRecoFrame("Zb_4L", "Z_{b}");

    L1a_4L = new VisibleRecoFrame("L1a_4L", "#it{l}_{1a}");
    L2a_4L = new VisibleRecoFrame("L2a_4L", "#it{l}_{2a}");
    L1b_4L = new VisibleRecoFrame("L1b_4L", "#it{l}_{1b}");
    L2b_4L = new VisibleRecoFrame("L2b_4L", "#it{l}_{2b}");

    X1a_4L = new InvisibleRecoFrame("X1a_4L", "#tilde{#chi}^{ 0}_{1 a}");
    X1b_4L = new InvisibleRecoFrame("X1b_4L", "#tilde{#chi}^{ 0}_{1 b}");

    LAB_4L->SetChildFrame(*N2N2_4L);

    N2N2_4L->AddChildFrame(*N2a_4L);
    N2N2_4L->AddChildFrame(*N2b_4L);

    N2a_4L->AddChildFrame(*Za_4L);
    N2a_4L->AddChildFrame(*X1a_4L);

    N2b_4L->AddChildFrame(*Zb_4L);
    N2b_4L->AddChildFrame(*X1b_4L);

    Za_4L->AddChildFrame(*L1a_4L);
    Za_4L->AddChildFrame(*L2a_4L);

    Zb_4L->AddChildFrame(*L1b_4L);
    Zb_4L->AddChildFrame(*L2b_4L);

    if (LAB_4L->InitializeTree())
        std::cout << "...4L Successfully initialized reconstruction trees" << std::endl;
    else
        std::cout << "...4L Failed initializing reconstruction trees" << std::endl;

    //////////////////////////////
    // Setting the invisible
    //////////////////////////////
    INV_4L = new InvisibleGroup("INV_4L", "#tilde{#chi}_{1}^{ 0} Jigsaws");
    INV_4L->AddFrame(*X1a_4L);
    INV_4L->AddFrame(*X1b_4L);

    // Set di-LSP mass to minimum Lorentz-invariant expression
    X1_mass_4L = new SetMassInvJigsaw("X1_mass_4L", "Set M_{#tilde{#chi}_{1}^{ 0} #tilde{#chi}_{1}^{ 0}} to minimum");
    INV_4L->AddJigsaw(*X1_mass_4L);

    // Set di-LSP rapidity to that of visible particles
    X1_eta_4L = new SetRapidityInvJigsaw("X1_eta_4L", "#eta_{#tilde{#chi}_{1}^{ 0} #tilde{#chi}_{1}^{ 0}} = #eta_{2jet+2#it{l}}");
    INV_4L->AddJigsaw(*X1_eta_4L);
    X1_eta_4L->AddVisibleFrames(N2N2_4L->GetListVisibleFrames());

    X1X1_contra_4L = new ContraBoostInvJigsaw("X1X1_contra_4L", "Contraboost invariant Jigsaw");
    INV_4L->AddJigsaw(*X1X1_contra_4L);
    X1X1_contra_4L->AddVisibleFrames(N2a_4L->GetListVisibleFrames(), 0);
    X1X1_contra_4L->AddVisibleFrames(N2b_4L->GetListVisibleFrames(), 1);
    X1X1_contra_4L->AddInvisibleFrame(*X1a_4L, 0);
    X1X1_contra_4L->AddInvisibleFrame(*X1b_4L, 1);

    // Lepton combinatoric groups
    LepComb = new CombinatoricGroup("LepComb", "combinatoric assignment of leptons");
    LepComb->AddFrame(*Za_4L);
    LepComb->AddFrame(*Zb_4L);
    LepComb->SetNElementsForFrame(*Za_4L, 2);
    LepComb->SetNElementsForFrame(*Zb_4L, 2);

    // mass minimisation of the lep pairs
    MinLeps = new MinMassesCombJigsaw("LepMassMin", "minimising di-di-leptons");
    LepComb->AddJigsaw(*MinLeps);
    MinLeps->AddFrames(N2a_4L->GetListVisibleFrames(), 0);
    MinLeps->AddFrames(N2b_4L->GetListVisibleFrames(), 1);
    LAB_4L->InitializeAnalysis();

    //////////////////////////////////// END OF 4L ////////////////////////////////////////

    /////////////////////////// INTERMEDIATE ///////////////////////////////////
    // RestFrames stuff

    // combinatoric (transverse) tree
    // for jet assignment
    LAB_comb = new LabRecoFrame("LAB_comb", "LAB");
    CM_comb = new DecayRecoFrame("CM_comb", "CM");
    S_comb = new DecayRecoFrame("S_comb", "S");
    ISR_comb = new VisibleRecoFrame("ISR_comb", "ISR");
    J_comb = new VisibleRecoFrame("J_comb", "Jets");
    L_comb = new VisibleRecoFrame("L_comb", "#it{l}'s");
    I_comb = new InvisibleRecoFrame("I_comb", "Inv");
    // cout << "test 5" << endl;
    LAB_comb->SetChildFrame(*CM_comb);
    CM_comb->AddChildFrame(*ISR_comb);
    CM_comb->AddChildFrame(*S_comb);
    S_comb->AddChildFrame(*L_comb);
    S_comb->AddChildFrame(*J_comb);
    S_comb->AddChildFrame(*I_comb);
    // cout << "test 6" << endl;
    LAB_comb->InitializeTree();
    // cout << "test 7" << endl;
    // 2L+NJ tree (Z->ll + W/Z->qq)
    LAB_4LNJ = new LabRecoFrame("LAB_4LNJ", "LAB");
    CM_4LNJ = new DecayRecoFrame("CM_4LNJ", "CM");
    S_4LNJ = new DecayRecoFrame("S_4LNJ", "S");
    ISR_4LNJ = new VisibleRecoFrame("ISR_4LNJ", "ISR");
    Na_4LNJ = new DecayRecoFrame("Na_4LNJ", "N_{a}");
    Za_4LNJ = new DecayRecoFrame("Za_4LNJ", "Za");
    L1a_4LNJ = new VisibleRecoFrame("L1a_4LNJ", "#it{l}_{1a}");
    L2a_4LNJ = new VisibleRecoFrame("L2a_4LNJ", "#it{l}_{2a}");
    Nb_4LNJ = new DecayRecoFrame("Nb_4LNJ", "N_{b}");
    Zb_4LNJ = new DecayRecoFrame("Zb_4LNJ", "Zb");
    L1b_4LNJ = new VisibleRecoFrame("L1b_4LNJ", "#it{l}_{1b}");
    L2b_4LNJ = new VisibleRecoFrame("L2b_4LNJ", "#it{l}_{2b}");
    Ia_4LNJ = new InvisibleRecoFrame("Ia_4LNJ", "I_{a}");
    Ib_4LNJ = new InvisibleRecoFrame("Ib_4LNJ", "I_{b}");

    LAB_4LNJ->SetChildFrame(*CM_4LNJ);
    CM_4LNJ->AddChildFrame(*ISR_4LNJ);
    CM_4LNJ->AddChildFrame(*S_4LNJ);
    S_4LNJ->AddChildFrame(*Na_4LNJ);
    S_4LNJ->AddChildFrame(*Nb_4LNJ);
    Na_4LNJ->AddChildFrame(*Za_4LNJ);
    Na_4LNJ->AddChildFrame(*Ia_4LNJ);
    Nb_4LNJ->AddChildFrame(*Zb_4LNJ);
    Nb_4LNJ->AddChildFrame(*Ib_4LNJ);
    Za_4LNJ->AddChildFrame(*L1a_4LNJ);
    Za_4LNJ->AddChildFrame(*L2a_4LNJ);
    Zb_4LNJ->AddChildFrame(*L1b_4LNJ);
    Zb_4LNJ->AddChildFrame(*L2b_4LNJ);

    LAB_4LNJ->InitializeTree();

    ////////////// Jigsaw rules set-up /////////////////

    // combinatoric (transverse) tree
    // for jet assignment
    INV_comb = new InvisibleGroup("INV_comb", "Invisible System");
    INV_comb->AddFrame(*I_comb);

    InvMass_comb = new SetMassInvJigsaw("InvMass_comb", "Invisible system mass Jigsaw");
    INV_comb->AddJigsaw(*InvMass_comb);

    JETS_comb = new CombinatoricGroup("JETS_comb", "Jets System");
    JETS_comb->AddFrame(*ISR_comb);
    JETS_comb->SetNElementsForFrame(*ISR_comb, 1);
    JETS_comb->AddFrame(*J_comb);
    JETS_comb->SetNElementsForFrame(*J_comb, 0);

    SplitJETS_comb = new MinMassesCombJigsaw("SplitJETS_comb", "Minimize M_{ISR} and M_{S} Jigsaw");
    JETS_comb->AddJigsaw(*SplitJETS_comb);
    SplitJETS_comb->AddCombFrame(*ISR_comb, 0);
    SplitJETS_comb->AddCombFrame(*J_comb, 1);
    SplitJETS_comb->AddObjectFrame(*ISR_comb, 0);
    SplitJETS_comb->AddObjectFrame(*S_comb, 1);

    if (!LAB_comb->InitializeAnalysis()) { cout << "Problem initializing \"comb\" analysis" << endl; }

    // 2L+NJ tree (Z->ll + W/Z->qq)
    INV_4LNJ = new InvisibleGroup("INV_4LNJ", "Invisible System");
    INV_4LNJ->AddFrame(*Ia_4LNJ);
    INV_4LNJ->AddFrame(*Ib_4LNJ);

    InvMass_4LNJ = new SetMassInvJigsaw("InvMass_4LNJ", "Invisible system mass Jigsaw");
    INV_4LNJ->AddJigsaw(*InvMass_4LNJ);
    InvRapidity_4LNJ = new SetRapidityInvJigsaw("InvRapidity_4LNJ", "Set inv. system rapidity");
    INV_4LNJ->AddJigsaw(*InvRapidity_4LNJ);
    InvRapidity_4LNJ->AddVisibleFrames(S_4LNJ->GetListVisibleFrames());
    SplitINV_4LNJ = new ContraBoostInvJigsaw("SplitINV_4LNJ", "INV -> I_{a}+ I_{b} jigsaw");
    INV_4LNJ->AddJigsaw(*SplitINV_4LNJ);
    SplitINV_4LNJ->AddVisibleFrames(Na_4LNJ->GetListVisibleFrames(), 0);
    SplitINV_4LNJ->AddVisibleFrames(Nb_4LNJ->GetListVisibleFrames(), 1);
    SplitINV_4LNJ->AddInvisibleFrame(*Ia_4LNJ, 0);
    SplitINV_4LNJ->AddInvisibleFrame(*Ib_4LNJ, 1);

    if (!LAB_4LNJ->InitializeAnalysis()) { cout << "Problem initializing \"4LNJ\" analysis" << endl; }
    ////////////// Jigsaw rules set-up /////////////////
}
VariableConstruction::~VariableConstruction() {
    // deletion of RJ 2-lepton+2-jet pointers
    delete LAB_4L;
    delete N2N2_4L;
    delete N2a_4L;
    delete N2b_4L;
    delete Za_4L;
    delete Zb_4L;
    delete L1a_4L;
    delete L2a_4L;
    delete L1b_4L;
    delete L2b_4L;
    delete X1a_4L;
    delete X1b_4L;
    delete INV_4L;
    delete X1_mass_4L;
    delete X1_eta_4L;
    delete X1X1_contra_4L;
    delete LepComb;
    delete MinLeps;

    // combinatoric (transverse) tree
    // for jet assignment
    delete LAB_comb;
    delete CM_comb;
    delete S_comb;
    delete ISR_comb;
    delete J_comb;
    delete L_comb;
    delete I_comb;
    delete INV_comb;
    delete InvMass_comb;
    delete JETS_comb;
    delete SplitJETS_comb;

    // 2L+NJ tree (Z->ll + W/Z->qq)
    delete LAB_4LNJ;
    delete CM_4LNJ;
    delete S_4LNJ;
    delete ISR_4LNJ;
    delete Na_4LNJ;
    delete Za_4LNJ;
    delete L1a_4LNJ;
    delete L2a_4LNJ;
    delete Nb_4LNJ;
    delete Zb_4LNJ;
    delete L1b_4LNJ;
    delete L2b_4LNJ;
    delete Ia_4LNJ;
    delete Ib_4LNJ;
    delete INV_4LNJ;
    delete InvMass_4LNJ;
    delete InvRapidity_4LNJ;
    delete SplitINV_4LNJ;
}

// This function initialises the Tree Branches for all of the Variables we have defined
void VariableConstruction::InitOutputTree() {
    if (m_Tree) delete m_Tree;

    string name = string(fChain->GetName());
    cout << "My tree name is " << name << std::endl;
    m_Tree = (TTree*)new TTree(name.c_str(), name.c_str());

    m_Tree->Branch("RunNumber", &m_RunNumber);
    m_Tree->Branch("EventNumber", &m_EventNumber);
    m_Tree->Branch("LumiBlock", &m_LumiBlock);
    m_Tree->Branch("mu", &m_mu);
    m_Tree->Branch("weight", &m_weight);
    m_Tree->Branch("pileUp_weight", &m_pileUp_weight);

    // m_Tree->Branch("nBaselineLeptons",&m_nBaselineLeptons);
    // m_Tree->Branch("nSignalLeptons"  ,&m_nSignalLeptons);
    m_Tree->Branch("is4Lep", &m_is4Lep);
    m_Tree->Branch("is4LInt", &m_is4LInt);

    m_Tree->Branch("lept1Pt", &m_lept1Pt);
    m_Tree->Branch("lept1Eta", &m_lept1Eta);
    m_Tree->Branch("lept1Phi", &m_lept1Phi);
    m_Tree->Branch("lept1sign", &m_lept1sign);

    m_Tree->Branch("lept2Pt", &m_lept2Pt);
    m_Tree->Branch("lept2Eta", &m_lept2Eta);
    m_Tree->Branch("lept2Phi", &m_lept2Phi);
    m_Tree->Branch("lept2sign", &m_lept2sign);

    m_Tree->Branch("lept3Pt", &m_lept3Pt);
    m_Tree->Branch("lept3Eta", &m_lept3Eta);
    m_Tree->Branch("lept3Phi", &m_lept3Phi);
    m_Tree->Branch("lept3sign", &m_lept3sign);

    m_Tree->Branch("lept3Pt", &m_lept3Pt);
    m_Tree->Branch("lept3Eta", &m_lept3Eta);
    m_Tree->Branch("lept3Phi", &m_lept3Phi);
    m_Tree->Branch("lept3sign", &m_lept3sign);

    m_Tree->Branch("lept4Pt", &m_lept4Pt);
    m_Tree->Branch("lept4Eta", &m_lept4Eta);
    m_Tree->Branch("lept4Phi", &m_lept4Phi);
    m_Tree->Branch("lept4sign", &m_lept4sign);

    m_Tree->Branch("nJets", &m_nJets);

    m_Tree->Branch("nBtagJets", &m_nBtagJets);

    m_Tree->Branch("jet1Pt", &m_jet1Pt);
    m_Tree->Branch("jet1Eta", &m_jet1Eta);
    m_Tree->Branch("jet1Phi", &m_jet1Phi);
    m_Tree->Branch("jet1M", &m_jet1M);

    m_Tree->Branch("jet2Pt", &m_jet2Pt);
    m_Tree->Branch("jet2Eta", &m_jet2Eta);
    m_Tree->Branch("jet2Phi", &m_jet2Phi);
    m_Tree->Branch("jet2M", &m_jet2M);

    m_Tree->Branch("jet3Pt", &m_jet3Pt);
    m_Tree->Branch("jet3Eta", &m_jet3Eta);
    m_Tree->Branch("jet3Phi", &m_jet3Phi);
    m_Tree->Branch("jet3M", &m_jet3M);

    m_Tree->Branch("jet4Pt", &m_jet4Pt);
    m_Tree->Branch("jet4Eta", &m_jet4Eta);
    m_Tree->Branch("jet4Phi", &m_jet4Phi);
    m_Tree->Branch("jet4M", &m_jet4M);

    m_Tree->Branch("L1aPt", &m_L1a_pt);
    m_Tree->Branch("L1aEta", &m_L1a_eta);
    m_Tree->Branch("L1aPhi", &m_L1a_phi);
    m_Tree->Branch("L1asign", &m_L1a_sign);

    m_Tree->Branch("L2aPt", &m_L2a_pt);
    m_Tree->Branch("L2aEta", &m_L2a_eta);
    m_Tree->Branch("L2aPhi", &m_L2a_phi);
    m_Tree->Branch("L2asign", &m_L2a_sign);

    m_Tree->Branch("L1bPt", &m_L1b_pt);
    m_Tree->Branch("L1bEta", &m_L1b_eta);
    m_Tree->Branch("L1bPhi", &m_L1b_phi);
    m_Tree->Branch("L1bsign", &m_L1b_sign);

    m_Tree->Branch("L2bPt", &m_L2b_pt);
    m_Tree->Branch("L2bEta", &m_L2b_eta);
    m_Tree->Branch("L2bPhi", &m_L2b_phi);
    m_Tree->Branch("L2bsign", &m_L2b_sign);

    m_Tree->Branch("mZa", &m_mZa);
    m_Tree->Branch("mZb", &m_mZb);
    m_Tree->Branch("minDphi", &m_minDphi);

    m_Tree->Branch("met", &m_MET);
    m_Tree->Branch("met_phi", &m_MET_phi);
    // RJ variables
    // m_Tree->Branch("MDR"             ,&m_MDR);
    // m_Tree->Branch("PP_VisShape"     ,&m_PP_VisShape);
    // m_Tree->Branch("gaminvPP"        ,&m_gaminvPP);
    // m_Tree->Branch("MP"              ,&m_MP);

    // m_Tree->Branch("mC1"              ,&m_mC1);
    // m_Tree->Branch("mN2"              ,&m_mN2);
    m_Tree->Branch("mTW_Pa", &m_mTW_Pa);
    m_Tree->Branch("mTW_PP", &m_mTW_PP);
    // m_Tree->Branch("mTZ_Pb"             ,&m_mTZ_Pb);
    // m_Tree->Branch("mTZ_PP"             ,&m_mTZ_PP);

    m_Tree->Branch("cosPP", &m_cosPP);
    m_Tree->Branch("cosPa", &m_cosPa);
    m_Tree->Branch("cosPb", &m_cosPb);
    m_Tree->Branch("dphiVP", &m_dphiVP);

    m_Tree->Branch("dphiPPV", &m_dphiPPV);
    m_Tree->Branch("dphiPC1", &m_dphiPC1);
    m_Tree->Branch("dphiPN2", &m_dphiPN2);
    // m_Tree->Branch("sangle"          ,&m_sangle);
    // m_Tree->Branch("dangle"          ,&m_dangle);

    m_Tree->Branch("H2PP", &m_H2PP);

    m_Tree->Branch("HT2PP", &m_HT2PP);
    m_Tree->Branch("H4PP", &m_H4PP);
    m_Tree->Branch("HT4PP", &m_HT4PP);
    m_Tree->Branch("H5PP", &m_H5PP);

    m_Tree->Branch("HT5PP", &m_HT5PP);

    m_Tree->Branch("H6PP", &m_H6PP);
    m_Tree->Branch("HT6PP", &m_HT6PP);

    m_Tree->Branch("H2Pa", &m_H2Pa);
    m_Tree->Branch("H2Pb", &m_H2Pb);
    m_Tree->Branch("H3Pa", &m_H3Pa);
    m_Tree->Branch("H3Pb", &m_H3Pb);
    m_Tree->Branch("minH2P", &m_minH2P);
    m_Tree->Branch("minH3P", &m_minH3P);
    m_Tree->Branch("R_H3Pa_H3Pb", &m_R_H3Pa_H3Pb);
    m_Tree->Branch("R_H2Pa_H2Pb", &m_R_H2Pa_H2Pb);
    m_Tree->Branch("R_minH2P_minH3P", &m_R_minH2P_minH3P);

    // m_Tree->Branch("minR_pT2i_HT3Pi" ,&m_minR_pT2i_HT3Pi);
    // m_Tree->Branch("maxR_H1PPi_H2PPi",&m_maxR_H1PPi_H2PPi);

    m_Tree->Branch("RPZ_HT4PP", &m_RPZ_HT4PP);
    m_Tree->Branch("RPT_HT4PP", &m_RPT_HT4PP);
    m_Tree->Branch("R_HT4PP_H4PP", &m_R_HT4PP_H4PP);
    m_Tree->Branch("RPZ_HT5PP", &m_RPZ_HT5PP);
    m_Tree->Branch("RPT_HT5PP", &m_RPT_HT5PP);

    m_Tree->Branch("R_HT5PP_H5PP", &m_R_HT5PP_H5PP);
    // m_Tree->Branch("mTl3",  &m_mTl3);
    // m_Tree->Branch("mj2j3", &m_mj2j3);
    // m_Tree->Branch("is_Z",&m_Is_Z);
    // m_Tree->Branch("is_OS",&m_Is_OS);
    m_Tree->Branch("PTCM", &m_PTCM);
    m_Tree->Branch("PTISR", &m_PTISR);
    m_Tree->Branch("PTI", &m_PTI);
    m_Tree->Branch("RISR", &m_RISR);
    m_Tree->Branch("cosCM", &m_cosCM);
    m_Tree->Branch("cosS", &m_cosS);
    m_Tree->Branch("MISR", &m_MISR);
    m_Tree->Branch("dphiCMI", &m_dphiCMI);
    m_Tree->Branch("dphiSI", &m_dphiSI);
    m_Tree->Branch("dphiISRI", &m_dphiISRI);
    m_Tree->Branch("HN2S", &m_HN2S);
    //    m_Tree->Branch("H11S", &m_H11S);
    //    m_Tree->Branch("HN1Ca", &m_HN1Ca);
    //    m_Tree->Branch("HN1Cb", &m_HN1Cb);
    //    m_Tree->Branch("H11Ca", &m_H11Ca);
    //    m_Tree->Branch("H11Cb", &m_H11Cb);
    //    m_Tree->Branch("cosC", &m_cosC);

    m_Tree->Branch("MZa", &m_MZa);
    m_Tree->Branch("MZb", &m_MZb);
    // m_Tree->Branch("mTWComp", &m_mTWComp);

    // m_Tree->Branch("cosZ", &m_cosZ);
    // m_Tree->Branch("cosJ", &m_cosJ);
    m_Tree->Branch("NjISR", &m_NjISR);
    m_Tree->Branch("NjS", &m_NjS);
    m_Tree->Branch("NbISR", &m_NbISR);
    m_Tree->Branch("NbS", &m_NbS);
    m_Tree->Branch("IaPP", &m_IaPP);
    m_Tree->Branch("IbPP", &m_IbPP);
    m_Tree->Branch("IaPa", &m_IaPa);
    m_Tree->Branch("IbPb", &m_IbPb);
    m_Tree->Branch("IaLAB", &m_IaLAB);
    m_Tree->Branch("IbLAB", &m_IbLAB);
}

// This function assigns values to all of the Tree Branches we initialised above
void VariableConstruction::FillOutputTree() {
    m_H2PP_visible = -999.;
    m_H2PP_invisible = -999.;
    m_IaPP = -999.;
    m_IbPP = -999.;
    m_IaPa = -999.;
    m_IbPb = -999.;
    m_IaLAB = -999;
    m_IbLAB = -999;
    m_H4PP_Lept1A = -999.;
    m_H4PP_Lept1B = -999.;
    m_H4PP_Lept2B = -999.;
    m_mu = -999;
    m_pileUp_weight = -999;

    //////Initialize variables
    m_nBaselineLeptons = -999;
    m_nSignalLeptons = -999;

    m_lept1Pt = -999;
    m_lept1Eta = -999;
    m_lept1Phi = -999;
    m_lept1sign = -999;
    m_lept1origin = -999;
    m_lept1type = -999;

    m_lept2Pt = -999;
    m_lept2Eta = -999;
    m_lept2Phi = -999;
    m_lept2sign = -999;
    m_lept2origin = -999;
    m_lept2type = -999;

    m_lept3Pt = -999;
    m_lept3Eta = -999;
    m_lept3Phi = -999;
    m_lept3sign = -999;
    m_lept3origin = -999;
    m_lept3type = -999;

    m_lept4Pt = -999;
    m_lept4Eta = -999;
    m_lept4Phi = -999;
    m_lept4sign = -999;
    m_lept4origin = -999;
    m_lept4type = -999;
    m_Zlep1Pt = -999;
    m_Zlep1Phi = -999;
    m_Zlep1Eta = -999;
    m_Zlep1No = -999;
    m_Zlep1sign = -999;

    m_Zlep2Pt = -999;
    m_Zlep2sign = -999;
    m_Zlep2Phi = -999;
    m_Zlep2Eta = -999;
    m_Zlep2No = -999;

    m_WlepPt = -999;
    m_WlepPhi = -999;
    m_WlepEta = -999;
    m_WlepNo = -999;
    m_Wlepsign = -999;

    m_L1a_sign = -999;
    m_L2a_sign = -999;
    m_L1b_sign = -999;
    m_L2b_sign = -999;
    // Jet Variables
    m_nJets = 0;
    m_nBtagJets = 0;

    m_jet1Pt = -999;
    m_jet1Eta = -999;
    m_jet1Phi = -999;
    m_jet1M = -999;
    m_jet1origin = -999;
    m_jet1type = -999;

    m_jet2Pt = -999;
    m_jet2Eta = -999;
    m_jet2Phi = -999;
    m_jet2M = -999;
    m_jet2origin = -999;
    m_jet2type = -999;

    m_jet3Pt = -999;
    m_jet3Eta = -999;
    m_jet3Phi = -999;
    m_jet3M = -999;
    m_jet3origin = -999;
    m_jet3type = -999;

    m_jet4Pt = -999;
    m_jet4Eta = -999;
    m_jet4Phi = -999;
    m_jet4M = -999;
    m_jet4origin = -999;
    m_jet4type = -999;

    // Di-Lepton System: Calculated for OS Pairs
    m_mZa = -999;
    m_mZb = -999;
    m_mt2 = -999;
    m_dRll = -999;
    m_ptll = -999;
    m_Zeta = -999;
    // Cleaning Variable: If MET is in the same direction as the Jet
    m_minDphi = -999;
    // Some lab frame angles and stuff
    m_MET = -999;
    m_MET_phi = -999;
    m_METTST = -999;
    m_METTST_phi = -999;
    m_Meff = -999;
    m_LT = -999;

    m_MDR = -999;
    m_PP_VisShape = -999;
    m_gaminvPP = -999;
    m_MP = -999;

    m_mC1 = -999;
    m_mN2 = -999;

    m_mTW_Pa = -999;
    m_mTW_PP = -999;

    m_mTZ_Pb = -999;
    m_mTZ_PP = -999;

    // 3L CA
    m_min_mt = -999;
    m_pt_lll = -999;
    m_mTl3 = -999;
    //##############################//
    //# Recursive Jigsaw Variables #//
    //##############################//

    // Scale Variables
    m_H2PP = -999;
    m_HT2PP = -999;
    m_H3PP = -999;
    m_HT3PP = -999;
    m_H4PP = -999;
    m_HT4PP = -999;
    m_H5PP = -999;
    m_HT5PP = -999;
    m_H6PP = -999;
    m_HT6PP = -999;

    m_H2Pa = -999;
    m_H2Pb = -999;
    m_minH2P = -999;
    m_R_H2Pa_H2Pb = -999;
    m_H3Pa = -999;
    m_H3Pb = -999;
    m_minH3P = -999;
    m_R_H3Pa_H3Pb = -999;
    m_R_minH2P_minH3P = -999;
    m_minR_pT2i_HT3Pi = -999;
    m_maxR_H1PPi_H2PPi = -999;

    // Anglular Variables
    m_cosPP = -999;
    m_cosPa = -999;
    m_cosPb = -999;
    m_dphiVP = -999;
    m_dphiPPV = -999;
    m_dphiPC1 = -999;
    m_dphiPN2 = -999;

    m_sangle = -999;
    m_dangle = -999;

    // Ratio Variables
    m_RPZ_HT4PP = -999;
    m_RPT_HT4PP = -999;
    m_R_HT4PP_H4PP = -999;

    m_RPZ_HT5PP = -999;
    m_RPT_HT5PP = -999;
    m_R_HT5PP_H5PP = -999;
    m_W_PP = -999;
    m_WZ_PP = -999;
    /// Variables for the compressed/Intermediate tree
    m_is4LInt = -999;

    m_PTCM = -999;
    m_PTISR = -999;
    m_PTI = -999;
    m_RISR = -999;
    m_cosCM = -999;
    m_cosS = -999;
    m_MISR = -999;
    m_dphiCMI = -999;
    m_dphiSI = -999;
    m_dphiISRI = -999;
    m_HN2S = -999;
    m_R_Ib_Ia = -999;
    m_H11S = -999.;
    m_HN1Ca = -999.;
    m_HN1Cb = -999.;
    m_H11Ca = -999.;
    m_H11Cb = -999.;

    m_Is_Z = -999.;
    m_Is_OS = -999;
    m_MZa = -999.;
    m_MZb = -999.;
    m_mTWComp = -999.;
    m_cosZ = -999.;
    m_cosJ = -999.;
    m_NjS = 0;
    m_NjISR = 0;
    m_NbS = 0;
    m_NbISR = 0;
    //////End initialization

    if (DEBUG) std::cout << "EVENT::FillOutputTree()..." << std::endl;

    // Converting from MeV to GeV
    // const double GeV = 1000.00;
    // NB: This is older code, used to change the output of KinVarConstruction from MeV to GeV, by dividing by 1000.
    //    However, since it isn't obvious whether some of the recursive jigsaw variables are in MeV or GeV, this
    //    has been discontinued, but may be used in the future.

    // Trigger Selection
    bool passTrigger = true;
    if (isMCBkg) {
        if (PRWHash == 2015) {
            if (!HLT_2e12_lhloose_L12EM10VH && !HLT_e17_lhloose_mu14 && !HLT_mu18_mu8noL1) { passTrigger = false; }
        }
        if (PRWHash == 2016) {
            if (!HLT_e17_lhloose_nod0_mu14 && !HLT_2e17_lhloose_nod0 && !HLT_mu22_mu8noL1) { passTrigger = false; }
        }
    } else if (isData) {
        if (RunNumber <= 284484) {
            if (!HLT_2e12_lhloose_L12EM10VH && !HLT_e17_lhloose_mu14 && !HLT_mu18_mu8noL1) {
                passTrigger = false;
                //      cout << "old run number failed!" << endl;
            }

            // else if(!HLT_e17_lhloose_nod0_mu14 && !HLT_2e17_lhloose_nod0 && !HLT_mu22_mu8noL1)
            // {
            //     passTrigger=false;
            //        cout << "new run number failed!" << endl;
            //  }
        }

    } else if (isFakes) {
        if (!DiElec_TrigMatched && !DiMuon_TrigMatched && !ElecMuon_TrigMatched) passTrigger = false;
    }
    if (!passTrigger) return;

    if (DEBUG) std::cout << "EVENT::Trigger passed..." << std::endl;

    // Calculating the Weights
    double xsec2 = -999;
    normWeight = 1.0;
    if (isFakes) {
        weight *= normWeight * wgt_fake;
        m_weight = weight;

    } else if (isMCBkg) {
        weight = elSF_weight * muSF_weight * bTagSF_weight * jvfSF_weight * gen_weight * pileUp_weight * sherpaVjets_weight * trigSF_weight;
        // weight=elSF_weight*muSF_weight*bTagSF_weight*gen_weight*pileUp_weight;
        // N=L x sigma
        // Sample lumi:
        // cout << "TotalEvents = " << TotalEvents << endl;
        double lumiSample = TotalEvents / xsec;
        normWeight = luminosity / lumiSample;
        // cout << " normWeight -----------------------------> " << normWeight << endl;
    } else if (isSignal) {
        weight = elSF_weight * muSF_weight * bTagSF_weight * jvfSF_weight * gen_weight * pileUp_weight * sherpaVjets_weight * trigSF_weight;

        float lumisample = 1. / xsec;
        normWeight = luminosity / lumisample;

        if (DataSetID == 392260) {
            double lumiSample = TotalEvents / ((0.021644 + 0.00852674) * 0.03266518922 * 0.7832);
            normWeight = luminosity / lumiSample;
        } else if (DataSetID == 392278) {
            double lumiSample = TotalEvents / ((0.00699008 + 0.0025208) * 0.03266518922 * 0.792);
            normWeight = luminosity / lumiSample;
        } else if (DataSetID == 392200) {
            double lumiSample = TotalEvents / ((0.143126 + 0.0663319) * 0.03266518922 * 0.7517);
            normWeight = luminosity / lumiSample;
        }
    } else {
        weight = 1.0;
        normWeight = 1.0;
    }

    weight *= normWeight;
    m_weight = weight;

    // Boolean for deciding whether to write the event or not
    bool writetheEvent = false;

    // Event variables
    m_RunNumber = RunNumber;
    m_EventNumber = EventNumber;
    m_LumiBlock = LumiBlockNumber;
    m_mu = AveIntPerBC;
    m_pileUp_weight = pileUp_weight;

    //# Defining Variables: #//
    // Leptons
    vector<int> lep_signal_index;
    // Find Signal Leptons
    GetLeptons(lep_signal_index, 10000.00, 2.5);  // pt and eta cuts for leptons,Check AnalysisBase.C: eta cuts are applied in the function
                                                  // by hand.. different cut values for electrons and muons
    // Number of Signal Leptons
    m_nSignalLeptons = (int)lep_signal_index.size();
    // Putting the Leptons in a more useful form
    vector<pair<TLorentzVector, int> > myLeptons;
    // vector<lep> myLeptons;
    for (unsigned int ilep = 0; ilep < lep_signal_index.size(); ilep++) {
        pair<TLorentzVector, int> temp;
        TLorentzVector tlv_temp;

        tlv_temp.SetPtEtaPhiM(lepton_pt->at(lep_signal_index[ilep]), lepton_eta->at(lep_signal_index[ilep]),
                              lepton_phi->at(lep_signal_index[ilep]), 0.0);
        temp.first = tlv_temp;
        temp.second = lepton_charge->at(lep_signal_index[ilep]);
        // temp.third = lepton_origin->at(lep_signal_index[ilep]);
        // temp.fourth = lepton_type->at(lep_signal_index[ilep]);
        // temp =
        // make_tuple(tlv_temp,lepton_charge->at(lep_signal_index[ilep]),lepton_origin->at(lep_signal_index_[ilep]),lepton_type->at(lepton_signal_index[ilep]));
        myLeptons.push_back(temp);
    }
    /*
        if(lep_signal_index.size()>0) {
            m_lept1origin = lepton_origin->at(0);
            m_lept1type = lepton_type->at(0);
        }
        if(lep_signal_index.size()>1) {
            m_lept2origin = lepton_origin->at(1);
            m_lept2type = lepton_type->at(1);
        }
        if(lep_signal_index.size()>2){
            m_lept3origin = lepton_origin->at(2);
            m_lept3type = lepton_type->at(2);
        }
        if(lep_signal_index.size()>3) {
            m_lept4origin = lepton_origin->at(3);
            m_lept4type = lepton_type->at(3);
        } */
    // Jets
    // Find signal jets (still contains B-Tagged jets)
    vector<int> jet_signal_index;
    GetJets(jet_signal_index, 20000., 2.4);
    // Create a vector of jets keeping the w/ TLorentzVectors
    // Putting the Jets in a more useful form
    vector<TLorentzVector> myJets;
    for (unsigned int ijet = 0; ijet < jet_signal_index.size(); ijet++) {
        TLorentzVector tmp;
        tmp.SetPtEtaPhiM(jet_pt->at(jet_signal_index[ijet]), jet_eta->at(jet_signal_index[ijet]), jet_phi->at(jet_signal_index[ijet]),
                         jet_m->at(jet_signal_index[ijet]));
        myJets.push_back(tmp);
    }
    // Number of Jets
    m_nJets = (int)jet_signal_index.size();
    // Find the B-Tagged Jets
    vector<int> jet_btag_index;
    for (unsigned int ijet = 0; ijet < jet_pt->size(); ijet++) {
        if (jet_pt->at(ijet) > 20000.00 && fabs(jet_eta->at(ijet) < 2.4)) {  // used to be 30
            if (jet_isBtag->at(ijet)) jet_btag_index.push_back(ijet);
        }
    }
    m_nBtagJets = (int)jet_btag_index.size();
    // if(m_nBtagJets>0) return;

    if (DEBUG) std::cout << "EVENT::Find b-tagged jets/jets " << std::endl;

    // Get the MET of the event
    TVector3 ETMiss = GetMET();
    m_MET = sqrt(ETMiss.X() * ETMiss.X() + ETMiss.Y() * ETMiss.Y());
    m_MET_phi = EtMiss_Phi;
    m_METTST = EtMissTST;
    m_METTST_phi = EtMissTST_Phi;

    // Classify the Events
    // Analysis will be performed only for the 2L or 3L final states
    m_is4Lep = false;
    m_is4LInt = false;

    if (m_nSignalLeptons == 4)
        m_is4Lep = true;
    else
        return;

    if (m_is4Lep && m_nJets > 0) m_is4LInt = true;

    if (DEBUG) std::cout << "EVENT::Categorization of the events..." << std::endl;

    // Sorting the Lepton and Jet Vectors
    sort(myLeptons.begin(), myLeptons.end(), SortLeptons);
    sort(myJets.begin(), myJets.end(), SortJets);

    TLorentzVector metLV;
    // TLorentzVector bigFatJet;
    metLV.SetPxPyPzE(ETMiss.X(), ETMiss.Y(), 0., sqrt(ETMiss.X() * ETMiss.X() + ETMiss.Y() * ETMiss.Y()));

    if (m_is4Lep) {
        if (DEBUG) std::cout << "It is a two-lepton + 2-jet event" << std::endl;

        // Creating the Lab Frame
        LAB_4L->ClearEvent();

        if (myLeptons[0].first.Pt() < 25000.0 || myLeptons[1].first.Pt() < 20000.0) return;

        // Setting the Standard Variables
        // Di-Lepton System:
        m_lept1Pt = myLeptons[0].first.Pt();
        m_lept1Eta = myLeptons[0].first.Eta();
        m_lept1Phi = myLeptons[0].first.Phi();
        m_lept1sign = myLeptons[0].second;

        m_lept2Pt = myLeptons[1].first.Pt();
        m_lept2Eta = myLeptons[1].first.Eta();
        m_lept2Phi = myLeptons[1].first.Phi();
        m_lept2sign = myLeptons[1].second;

        m_lept3Pt = myLeptons[2].first.Pt();
        m_lept3Eta = myLeptons[2].first.Eta();
        m_lept3Phi = myLeptons[2].first.Phi();
        m_lept3sign = myLeptons[2].second;

        m_lept4Pt = myLeptons[3].first.Pt();
        m_lept4Eta = myLeptons[3].first.Eta();
        m_lept4Phi = myLeptons[3].first.Phi();
        m_lept4sign = myLeptons[3].second;

        L1a_4L->SetLabFrameFourVector(myLeptons[0].first);  // Set lepton 4-vectors
        L1a_4L->SetCharge(myLeptons[0].second);
        L2a_4L->SetLabFrameFourVector(myLeptons[1].first);
        L2a_4L->SetCharge(myLeptons[1].second);
        L1b_4L->SetLabFrameFourVector(myLeptons[2].first);  // Set lepton 4-vectors
        L1b_4L->SetCharge(myLeptons[2].second);
        L2b_4L->SetLabFrameFourVector(myLeptons[3].first);
        L2b_4L->SetCharge(myLeptons[3].second);
        TVector3 MET = ETMiss;  // Get the MET
        MET.SetZ(0.);
        INV_4L->SetLabFrameThreeVector(MET);  // Set the MET in reco tree
        //////////////////////////////////////////////////
        // Lotentz vectors have been set, now do the boosts
        //////////////////////////////////////////////////
        LAB_4L->AnalyzeEvent();  // analyze the event

        //... then by setting the Variables
        TLorentzVector vP_V1aPP = L1a_4L->GetFourVector(*N2N2_4L);
        TLorentzVector vP_V2aPP = L2a_4L->GetFourVector(*N2N2_4L);
        TLorentzVector vP_V1bPP = L1b_4L->GetFourVector(*N2N2_4L);
        TLorentzVector vP_V2bPP = L2b_4L->GetFourVector(*N2N2_4L);
        TLorentzVector vP_IaPP = X1a_4L->GetFourVector(*N2N2_4L);
        TLorentzVector vP_IbPP = X1b_4L->GetFourVector(*N2N2_4L);

        TLorentzVector vP_V1aPa = L1a_4L->GetFourVector(*N2a_4L);
        TLorentzVector vP_V2aPa = L2a_4L->GetFourVector(*N2a_4L);
        TLorentzVector vP_IaPa = X1a_4L->GetFourVector(*N2a_4L);
        TLorentzVector vP_V1bPb = L1b_4L->GetFourVector(*N2b_4L);
        TLorentzVector vP_V2bPb = L2b_4L->GetFourVector(*N2b_4L);
        TLorentzVector vP_IbPb = X1b_4L->GetFourVector(*N2b_4L);

        TLorentzVector vP_V1aLAB = L1a_4L->GetFourVector(*LAB_4L);
        TLorentzVector vP_V2aLAB = L2a_4L->GetFourVector(*LAB_4L);
        TLorentzVector vP_IaLAB = X1a_4L->GetFourVector(*LAB_4L);
        TLorentzVector vP_V1bLAB = L1b_4L->GetFourVector(*LAB_4L);
        TLorentzVector vP_V2bLAB = L2b_4L->GetFourVector(*LAB_4L);
        TLorentzVector vP_IbLAB = X1b_4L->GetFourVector(*LAB_4L);

        m_mZa = Za_4L->GetMass();
        m_mZb = Zb_4L->GetMass();

        m_L1a_pt = vP_V1aLAB.Pt();
        m_L1a_eta = vP_V1aLAB.Eta();
        m_L1a_phi = vP_V1aLAB.Phi();
        m_L1a_sign = L1a_4L->GetCharge();

        m_L2a_pt = vP_V2aLAB.Pt();
        m_L2a_eta = vP_V2aLAB.Eta();
        m_L2a_phi = vP_V2aLAB.Phi();
        m_L2a_sign = L2a_4L->GetCharge();

        m_L1b_pt = vP_V1bLAB.Pt();
        m_L1b_eta = vP_V1bLAB.Eta();
        m_L1b_phi = vP_V1bLAB.Phi();
        m_L1b_sign = L1b_4L->GetCharge();

        m_L2b_pt = vP_V2bLAB.Pt();
        m_L2b_eta = vP_V2bLAB.Eta();
        m_L2b_phi = vP_V2bLAB.Phi();
        m_L2b_sign = L2b_4L->GetCharge();
        // if(!m_L1a_sign*m_L2a_sign<0 && abs(m_L1a_sign)!=abs(m_L2a_sign)) return;
        // cout << "Za leptons: " << L1a_4L->GetCharge() << " " << L2a_4L->GetCharge() << endl;
        // if(!m_L1b_sign*m_L2b_sign<0 && abs(m_L1b_sign)!=abs(m_L2b_sign)) return;
        // cout << "Zb leptons: " << L1b_4L->GetCharge() << " " << L2b_4L->GetCharge() << endl;

        // Variables w/ 4 objects
        // Four vector sum of all visible objets + four vector sum of inv objects
        m_H2PP = (vP_V1aPP + vP_V2aPP + vP_V1bPP + vP_V2bPP).P() + (vP_IaPP + vP_IbPP).P();     // H(1,1)PP
        m_HT2PP = (vP_V1aPP + vP_V2aPP + vP_V1bPP + vP_V2bPP).Pt() + (vP_IaPP + vP_IbPP).Pt();  // HT(1,1)PP
        // Scalar sum of all visible objects + vector sum of invisible momenta
        m_H5PP = vP_V1aPP.P() + vP_V2aPP.P() + vP_V1bPP.P() + vP_V2bPP.P() + (vP_IaPP + vP_IbPP).P();        // H(4,1)PP
        m_HT5PP = vP_V1aPP.Pt() + vP_V2aPP.Pt() + vP_V1bPP.Pt() + vP_V2bPP.Pt() + (vP_IaPP + vP_IbPP).Pt();  // HT(4,1)PP
        // scalar sum of all objects
        m_H6PP = vP_V1aPP.P() + vP_V2aPP.P() + vP_V1bPP.P() + vP_V2bPP.P() + vP_IaPP.P() + vP_IbPP.P();  // H(4,2)PP
        m_HT6PP = vP_V1aPP.Pt() + vP_V2aPP.Pt() + vP_V1bPP.Pt() + vP_V2bPP.Pt() + vP_IaPP.Pt() + vP_IbPP.Pt();

        m_H2Pa = (vP_V1aPa + vP_V2aPa).P() + vP_IaPa.P();
        m_H2Pb = (vP_V1bPb + vP_V2bPb).P() + vP_IbPb.P();
        m_H3Pa = vP_V1aPa.P() + vP_V2aPa.P() + vP_IaPa.P();
        m_H3Pb = vP_V1bPb.P() + vP_V2bPb.P() + vP_IbPb.P();
        m_minH2P = std::min(m_H2Pa, m_H2Pb);
        m_minH3P = std::min(m_H3Pa, m_H3Pb);
        m_R_H2Pa_H2Pb = m_H2Pa / m_H2Pb;
        m_R_H3Pa_H3Pb = m_H3Pa / m_H3Pb;
        m_R_minH2P_minH3P = m_minH2P / m_minH3P;
        double H3PTa = vP_V1aPa.Pt() + vP_V2aPa.Pt() + vP_IaPa.Pt();

        m_minR_pT2i_HT3Pi = std::min(vP_V1aPa.Pt() / H3PTa, vP_V2aPa.Pt() / H3PTa);

        m_R_HT5PP_H5PP = m_HT5PP / m_H5PP;

        // Invisible in the PP frame, Px frames and lab frame
        //        TLorentzVector vP_IaLAB = X1a_4L->GetFourVector(*LAB_4L);
        //      TLorentzVector vP_IbLAB = X1b_4L->GetFourVector(*LAB_4L);
        m_IaLAB = vP_IaLAB.P();
        m_IbLAB = vP_IbLAB.P();
        m_IaPP = vP_IaPP.P();
        m_IbPP = vP_IbPP.P();
        m_IaPa = vP_IaPa.P();
        m_IbPb = vP_IbPb.P();

        double jetMetphiP = (vP_V1aPa + vP_V2aPa).DeltaPhi(vP_IaPa);
        m_mTW_Pa = sqrt(2 * (vP_V1aPa + vP_V2aPa).Pt() * vP_IaPa.Pt() * (1 - cos(jetMetphiP)));

        double jetMetphiPP = (vP_V1aPP + vP_V2aPP).DeltaPhi(vP_IaPP + vP_IbPP);
        m_mTW_PP = sqrt(2 * (vP_V1aPP + vP_V2aPP).Pt() * (vP_IaPP + vP_IbPP).Pt() * (1 - cos(jetMetphiPP)));

        double dilepMetphiP = (vP_V1bPb + vP_V2bPb).DeltaPhi(vP_IbPb);
        m_mTZ_Pb = sqrt(2 * (vP_V1bPb + vP_V2bPb).Pt() * vP_IbPb.Pt() * (1 - cos(dilepMetphiP)));

        double dilepMetphiPP = (vP_V1bPP + vP_V2bPP).DeltaPhi(vP_IaPP + vP_IbPP);
        m_mTZ_PP = sqrt(2 * (vP_V1bPP + vP_V2bPP).Pt() * (vP_IaPP + vP_IbPP).Pt() * (1 - cos(dilepMetphiPP)));

        double H1PPa = (vP_V1aPP + vP_V2aPP).P();
        double H1PPb = (vP_V1bPP + vP_V2bPP).P();
        double H2PPa = vP_V1aPP.P() + vP_V2aPP.P();
        double H2PPb = vP_V1bPP.P() + vP_V2bPP.P();
        m_maxR_H1PPi_H2PPi = std::max(H1PPa / H2PPa, H1PPb / H2PPb);

        // signal variables
        TLorentzVector vP_Va = N2a_4L->GetVisibleFourVector(*N2a_4L);
        TLorentzVector vP_Vb = N2b_4L->GetVisibleFourVector(*N2b_4L);
        m_MP = (vP_Va.M2() - vP_Vb.M2()) / (2. * (vP_Va.E() - vP_Vb.E()));

        double P_P = N2a_4L->GetMomentum(*N2N2_4L);

        double MPP = 2. * sqrt(P_P * P_P + m_MP * m_MP);
        TVector3 vP_PP = N2N2_4L->GetFourVector(*LAB_4L).Vect();
        double Pt_PP = vP_PP.Pt();
        double Pz_PP = fabs(vP_PP.Pz());
        m_RPT_HT5PP = Pt_PP / (Pt_PP + m_HT5PP);
        m_RPZ_HT5PP = Pz_PP / (Pz_PP + m_HT5PP);

        m_PP_VisShape = N2N2_4L->GetVisibleShape();

        m_gaminvPP = 2. * m_MP / MPP;
        m_MDR = m_PP_VisShape * N2N2_4L->GetMass();

        m_mC1 = N2a_4L->GetMass();
        m_mN2 = N2b_4L->GetMass();

        // Angular properties of the sparticles system
        m_cosPP = N2N2_4L->GetCosDecayAngle();        // decay angle of the PP system
        m_cosPa = N2a_4L->GetCosDecayAngle(*X1a_4L);  // decay angle of the C1a system
        m_cosPb = N2b_4L->GetCosDecayAngle(*X1b_4L);  // decay angle of the N2b system

        // difference in azimuthal angle between the total sum of visible ojects in the N2N2 frame
        m_dphiPPV = N2N2_4L->GetDeltaPhiBoostVisible();
        m_dphiVP = N2N2_4L->GetDeltaPhiDecayVisible();

        // hemisphere variables
        m_dphiPC1 = N2a_4L->GetDeltaPhiDecayPlanes(*Za_4L);
        m_dphiPN2 = N2b_4L->GetDeltaPhiDecayPlanes(*Zb_4L);

        m_sangle = (m_cosPa + (m_dphiVP - acos(-1.) / 2.) / (acos(-1.) / 2.)) / 2.;
        m_dangle = (m_cosPa - (m_dphiVP - acos(-1.) / 2.) / (acos(-1.) / 2.)) / 2.;

        writetheEvent = true;

    }  // end is 4L event

    // bool doit = true;
    if (m_is4LInt) {
        // min{d#phi}
        double mindphi = 100000;
        double dphi = 0;
        TLorentzVector tempjet;
        for (unsigned int ijet = 0; ijet < jet_signal_index.size(); ijet++) {
            tempjet.SetPtEtaPhiM(jet_pt->at(jet_signal_index[ijet]), jet_eta->at(jet_signal_index[ijet]),
                                 jet_phi->at(jet_signal_index[ijet]), jet_m->at(jet_signal_index[ijet]));

            dphi = fabs(metLV.DeltaPhi(tempjet));

            if (dphi < mindphi) mindphi = dphi;
        }

        m_minDphi = mindphi;  // cleaning variable for missmeasured jets;
        // if( fabs(mindphi)<0.4) return;

        LAB_comb->ClearEvent();

        vector<RFKey> jetID;
        for (int i = 0; i < int(myJets.size()); i++) {
            TLorentzVector jet = myJets[i];
            // transverse view of jet 4-vectors
            jet.SetPtEtaPhiM(jet.Pt(), 0.0, jet.Phi(), jet.M());
            jetID.push_back(JETS_comb->AddLabFrameFourVector(jet));
        }

        TLorentzVector lepSys(0., 0., 0., 0.);
        for (int i = 0; i < int(myLeptons.size()); i++) {
            TLorentzVector lep1;
            // transverse view of mu 4-vectors
            lep1.SetPtEtaPhiM(myLeptons[i].first.Pt(), 0.0, myLeptons[i].first.Phi(), myLeptons[i].first.M());
            lepSys = lepSys + lep1;
        }
        L_comb->SetLabFrameFourVector(lepSys);

        INV_comb->SetLabFrameThreeVector(ETMiss);
        if (!LAB_comb->AnalyzeEvent()) cout << "Something went wrong with \"INTERMEDIATE\" tree event analysis" << endl;

        for (int i = 0; i < int(jetID.size()); i++) {
            if (JETS_comb->GetFrame(jetID[i]) == *J_comb) {
                m_NjS++;
                if (jet_isBtag->at(jet_signal_index[i])) m_NbS++;
            } else {
                m_NjISR++;
                if (jet_isBtag->at(jet_signal_index[i])) m_NbISR++;
            }
        }

        LAB_4LNJ->ClearEvent();

        // put jets in their place
        int NJ = jetID.size();
        TLorentzVector vISR(0., 0., 0., 0.);
        for (int i = 0; i < NJ; i++) {
            if (JETS_comb->GetFrame(jetID[i]) == *J_comb) {
                //                JETS_4LNJ->AddLabFrameFourVector(myJets[i]);

            } else {
                vISR += myJets[i];
            }
        }

        ISR_4LNJ->SetLabFrameFourVector(vISR);

        // put leptons in their place
        L1a_4LNJ->SetLabFrameFourVector(myLeptons[0].first);  // Set lepton 4-vectors
        L1a_4LNJ->SetCharge(myLeptons[0].second);
        L2a_4LNJ->SetLabFrameFourVector(myLeptons[1].first);
        L2a_4LNJ->SetCharge(myLeptons[1].second);
        L1b_4LNJ->SetLabFrameFourVector(myLeptons[2].first);  // Set lepton 4-vectors
        L1b_4LNJ->SetCharge(myLeptons[2].second);
        L2b_4LNJ->SetLabFrameFourVector(myLeptons[3].first);
        L2b_4LNJ->SetCharge(myLeptons[3].second);

        INV_4LNJ->SetLabFrameThreeVector(ETMiss);

        if (!LAB_4LNJ->AnalyzeEvent()) cout << "Something went wrong with \"4LNJ\" tree event analysis" << endl;

        /*m_L1a_sign = L1a_4LNJ->GetCharge();
        m_L2a_sign = L2a_4LNJ->GetCharge();
        m_L1b_sign = L1b_4LNJ->GetCharge();
        m_L2b_sign = L2b_4LNJ->GetCharge();
        */
        TLorentzVector vP_V1aLABCOMP = L1a_4LNJ->GetFourVector(*LAB_4LNJ);
        TLorentzVector vP_V2aLABCOMP = L2a_4LNJ->GetFourVector(*LAB_4LNJ);
        TLorentzVector vP_IaLABCOMP = Ia_4LNJ->GetFourVector(*LAB_4LNJ);
        TLorentzVector vP_V1bLABCOMP = L1b_4LNJ->GetFourVector(*LAB_4LNJ);
        TLorentzVector vP_V2bLABCOMP = L2b_4LNJ->GetFourVector(*LAB_4LNJ);
        TLorentzVector vP_IbLABCOMP = Ib_4LNJ->GetFourVector(*LAB_4LNJ);

        // m_L1a_pt = vP_V1aLABCOMP.Pt();
        // m_L2a_pt = vP_V2aLABCOMP.Pt();
        // m_L1b_pt = vP_V1bLABCOMP.Pt();
        // m_L2b_pt = vP_V2bLABCOMP.Pt();

        m_MZa = Za_4LNJ->GetMass();
        m_MZb = Zb_4LNJ->GetMass();

        TLorentzVector vP_CM;
        TLorentzVector vP_ISR;
        TLorentzVector vP_I;

        vP_CM = CM_4LNJ->GetFourVector();
        vP_ISR = ISR_4LNJ->GetFourVector();
        vP_I = (*Ia_4LNJ + *Ib_4LNJ).GetFourVector();

        m_cosCM = CM_4LNJ->GetCosDecayAngle();
        m_cosS = S_4LNJ->GetCosDecayAngle();
        m_MISR = ISR_4LNJ->GetMass();
        m_dphiCMI = acos(-1.) - fabs(CM_4LNJ->GetDeltaPhiBoostVisible());
        m_dphiSI = acos(-1.) - fabs(S_4LNJ->GetDeltaPhiBoostVisible());

        m_HN2S =  // Z_4LNJ->GetFourVector(*S_4LNJ).E() +
            L1a_4LNJ->GetFourVector(*S_4LNJ).E() + L2a_4LNJ->GetFourVector(*S_4LNJ).E() + L1b_4LNJ->GetFourVector(*S_4LNJ).E() +
            L1b_4LNJ->GetFourVector(*S_4LNJ).E() + Ia_4LNJ->GetFourVector(*S_4LNJ).P() + Ib_4LNJ->GetFourVector(*S_4LNJ).P();

        //    m_MZ = Z_4LNJ->GetMass();
        //   m_MJ = J_4LNJ->GetMass();
        //  m_cosZ = Z_4LNJ->GetCosDecayAngle();
        // if(m_NjS > 1)

        m_PTCM = vP_CM.Pt();

        TVector3 boostZ = vP_CM.BoostVector();
        boostZ.SetX(0.);
        boostZ.SetY(0.);

        vP_CM.Boost(-boostZ);
        vP_ISR.Boost(-boostZ);
        vP_I.Boost(-boostZ);

        TVector3 boostT = vP_CM.BoostVector();
        vP_ISR.Boost(-boostT);
        vP_I.Boost(-boostT);

        TVector3 vPt_ISR = vP_ISR.Vect();
        TVector3 vPt_I = vP_I.Vect();
        vPt_ISR -= vPt_ISR.Dot(boostZ.Unit()) * boostZ.Unit();
        vPt_I -= vPt_I.Dot(boostZ.Unit()) * boostZ.Unit();

        m_PTISR = vPt_ISR.Mag();
        m_RISR = -vPt_I.Dot(vPt_ISR.Unit()) / m_PTISR;
        m_PTI = vPt_I.Mag();
        m_dphiISRI = fabs(vPt_ISR.Angle(vPt_I));

        writetheEvent = true;

    }  // end INTERMEDIATE

    if (m_Tree && writetheEvent) m_Tree->Fill();
}
bool VariableConstruction::SortLeptons(const pair<TLorentzVector, int> lv1, const pair<TLorentzVector, int> lv2)
// bool VariableConstruction::SortLeptons(const lep lv1, const lep lv2)
{
    return lv1.first.Pt() > lv2.first.Pt();
}
bool VariableConstruction::SortJets(const TLorentzVector jv1, const TLorentzVector jv2) { return jv1.Pt() > jv2.Pt(); }

/*struct lep {
  TLorentzVector first; int second; int third; int fourth;
  };*/
