import os, commands, math, sys, re, random, subprocess, ROOT, argparse, logging
from XAMPPplotting.PlotUtils import PlotUtils
from XAMPPplotting.PlottingHistos import CreateHistoSets
from XAMPPplotting.PlotLabels import DrawXaxisMassLabel, DrawYaxisMassLabel
from ClusterSubmission.Utils import CreateDirectory
from XAMPPplotting.FileStructureHandler import GetStructure, GetFileHandler
from XAMPPplotting.StatisticalFunctions import sigmaZn
from XAMPPplotting.Utils import ExtractMasses, setupBaseParser

from array import array
from pprint import pprint

#from XAMPPplotting.Utils import
USER = os.getenv("USER")
TObjects = []


def ScaleAxisLabels(H, s=0.25):
    H.SetMinimum(0.001)
    H.GetYaxis().SetLabelFont(43)
    H.GetXaxis().SetLabelFont(43)
    H.GetYaxis().SetLabelSize(24.)
    H.GetXaxis().SetTitleSize(24.)
    H.GetXaxis().SetLabelSize(24.)
    H.GetXaxis().SetTitleSize(s * H.GetXaxis().GetTitleSize())
    H.GetYaxis().SetTitleSize(s * H.GetYaxis().GetTitleSize())
    H.GetYaxis().SetTitleOffset(0.29)


def ReadInGrid(InDir="./", Model="C1C1", Couplings="LLE12k"):
    OutList = {}
    for File in os.listdir(InDir):
        if not File.split("_")[0] == Model:
            continue
        if not File.split("_")[-1].startswith(Couplings):
            continue
        NLSP, LSP = ExtractMasses(File)
        OutList[(NLSP, LSP)] = InDir + File
    return OutList


def GetYields(DataSets, variable, analysis, region, MeffCut, MetCut, Lumi=1.):
    Yields = {}
    for N, L in DataSets.iterkeys():
        file_name = DataSets[(N, L)]
        #SampleHisto(const std::string& sample_name, const std::string& variable, const std::string& analysis, const std::string& region,
        #            const std::string& root_file);
        H = ROOT.XAMPP.SampleHisto(file_name[file_name.rfind("/") + 1:file_name.rfind(".")], variable, analysis, region, file_name)
        if not H.loadIntoMemory(): continue
        Y = H.IntegrateWithError(H.GetXaxis().FindBin(MeffCut), -1, H.GetYaxis().FindBin(MetCut), -1)[0]
        Yields[(N, L)] = Y * Lumi
    return Yields


def GetGridPoints(GridValues):
    NLSPmasses = []
    [NLSPmasses.append(m[0]) for m in GridValues.iterkeys() if not m[0] in NLSPmasses]
    NLSPmasses = sorted(NLSPmasses)
    LSPmasses = {}
    for mN in NLSPmasses:
        LSPmasses[mN] = sorted([k[1] for k in GridValues.iterkeys() if k[0] == mN and k[1] > -1])
    return NLSPmasses, LSPmasses


def GetGridRow(GridValues, LSP=-1):
    Row = {}
    for mN, mL in GridValues.iterkeys():
        if mL == LSP:
            Row[mN] = GridValues[(mN, mL)]
    return Row


def CalculateZnGrid(GridYields, BgYield, RelUncert=0.2):
    Zn = {}
    for N, L in GridYields:
        Signal = GridYields[(N, L)]
        Background = BgYield
        PseudoSig = Background + Signal
        # ZnValue = sigmaZn(PseudoSig, Background, RelUncert * Background)
        ZnValue = ROOT.RooStats.NumberCountingUtils.BinomialExpZ(Signal, Background, RelUncert)

        Zn[(N, L)] = ZnValue
    return Zn


def GetGridHistogram(GridValues):
    NLSPmasses, LSPmasses = GetGridPoints(GridValues)
    Graph = ROOT.TGraph2D()
    global TObjects
    TObjects += [Graph]
    for mN in NLSPmasses:
        for mL in LSPmasses[mN]:
            Graph.SetPoint(Graph.GetN(), mN, mL, GridValues[(mN, mL)])
    GrdHisto = Graph.GetHistogram()
    GrdHisto.SetContour(99)
    GrdHisto.SetMinimum(0.)
    GrdHisto.GetZaxis().SetLabelOffset(1.4 * GrdHisto.GetZaxis().GetLabelOffset())
    GrdHisto.GetXaxis().SetNdivisions(7)
    GrdHisto.GetYaxis().SetNdivisions(9)
    return GrdHisto


def DrawHistogram(Options, Grid, MeffCut, MetCut, region, Zaxis, DrawCont=True, PlotPre="Zn", AddInfo="", MaxZ=-1):
    if len(Grid) == 0:
        return
    pu = PlotUtils(status=Options.label, size=24, lumi=Options.lumi)
    HistoName = "%s_%s_%s_%s_%i_%i_%.f" % (PlotPre, Options.Model, Options.Couplings, region, MeffCut, MetCut, Options.lumi)
    pu.Prepare1PadCanvas(HistoName, 800, 600, Options.quadCanvas)
    pu.GetCanvas().SetTopMargin(0.12)
    pu.GetCanvas().SetRightMargin(0.18)
    pu.GetCanvas().SetLeftMargin(0.14)

    if Options.doLogY:
        pu.GetCanvas().SetLogz()
        HistoName += "_LogZ"

    H1 = GetGridHistogram(Grid)
    DrawXaxisMassLabel(H1, 1, ("%s_%s" % (Options.Model, Options.Couplings)))
    DrawYaxisMassLabel(H1, 2, ("%s_%s" % (Options.Model, Options.Couplings)))
    H1.GetZaxis().SetTitle(Zaxis)
    if MaxZ > 0:
        H1.SetMaximum(MaxZ)

    H1.Draw("COLZ")
    if DrawCont == True:
        H2 = H1.Clone("3Sigma")
        H2.SetContour(1)
        H2.SetContourLevel(0, 1.65)
        H2.Draw("CONT3 same")
        H3 = H1.Clone("5Sigma")
        H3.SetContour(1)
        H3.SetContourLevel(0, 3.)
        H3.SetLineStyle(ROOT.kDashed)
        H3.Draw("CONT3 same")

    CutValues = ""
    if MeffCut > 0:
        CutValues += "m_{eff}>%.0f GeV " % (MeffCut)
    if MetCut > 0:
        CutValues += "E_{T}^{miss}>%i GeV" % (MetCut)

    x = 0.18
    y = 0.85
    if len(CutValues) > 0:
        pu.DrawTLatex(x, y, CutValues, 26)
        y -= 0.05

    if len(AddInfo) > 0:
        pu.DrawTLatex(x, y, AddInfo)

    pu.DrawAtlas(pu.GetCanvas().GetLeftMargin(), 0.95)
    pu.DrawLumiSqrtS(pu.GetCanvas().GetLeftMargin(), 0.92)
    pu.DrawTLatex(0.6, 0.92, region)

    pu.saveHisto("%s/%s" % (Options.outputDir, HistoName), Options.OutFileType)


def DrawHistogram1D(Options, Column, MeffCut, MetCut, region, Yaxis, PlotPre="Zn", AddInfo="", MaxY=-1):
    if len(Column) == 0:
        return

    pu = PlotUtils(status=Options.label, size=24, lumi=Options.lumi)
    HistoName = "%s_%s_%s_%s_%i_%i_%.f" % (PlotPre, Options.Model, Options.Couplings, region, MeffCut, MetCut, Options.lumi)
    pu.Prepare1PadCanvas(HistoName, 800, 600, Options.quadCanvas)

    H1 = ROOT.TH1D("TH1" + HistoName, "TH1", len(Column), 0, len(Column))
    DrawXaxisMassLabel(H1, 1, ("%s_%s" % (Options.Model, Options.Couplings)))

    H1.GetYaxis().SetTitle(Yaxis)
    H1.SetMinimum(0.)
    if MaxY > -1:
        H1.SetMaximum(MaxY)
    H1.SetMaximum(H1.GetMaximum() * 1.4)
    Bin = 1
    for N in sorted(Column):
        H1.GetXaxis().SetBinLabel(Bin, str(N))
        H1.SetBinContent(Bin, Column[N])
        Bin += 1

    H1.Draw("HIST")

    pu.DrawPlotLabels(0.195, 0.83, Options.regionLabel if len(Options.regionLabel) > 0 else region, "Blub", Options.noATLAS)

    #     DrawLabels(Can , MeffCut , MetCut, AddInfo=AddInfo + " #tilde{#chi}^{#pm}_{1}#rightarrowlll/#nu#nul", RightMargin=0.1)

    pu.saveHisto("%s/%s" % (Options.outputDir, HistoName), Options.OutFileType)


def getGridParser():
    ROOT.gROOT.Macro("rootlogon.C")
    ROOT.gROOT.SetStyle("ATLAS")
    ROOT.gStyle.SetPalette(ROOT.kViridis)
    ROOT.gROOT.SetBatch(1)

    parser = argparse.ArgumentParser(
        description='This script produces Zn histograms for your entire signal grid. For more help type \"python ZN_C1C1.py -h\"',
        prog='MCPlots')
    parser = setupBaseParser(parser)
    parser.add_argument('--MeffCuts',
                        help='Specify the cuts on meff on which the Zn Calculation should run',
                        nargs='+',
                        default=[0],
                        type=int)
    parser.add_argument('--MetCuts',
                        help='Specify the cuts on meff on which the Zn Calculation should run',
                        nargs='+',
                        default=[0],
                        type=int)
    parser.add_argument("--ModelPath", help="Path of the Signal datasets")
    parser.add_argument("--Model", help="For which model you want to do the calculation", default="Wino")
    parser.add_argument("--Couplings", help="For which RPV couplings", default="LLE12k")
    parser.add_argument("--CutSignificance", help="Scales the signal prediction", default=5., type=float)
    parser.add_argument("--CutYields", help="Scales the signal prediction", default=-1, type=float)
    parser.add_argument("--BkgUncertainty", help="Assumed uncertainty on the background", default=0.3, type=float)
    parser.add_argument("--SignalScale", help="Scales the signal prediction", default=1., type=float)
    parser.set_defaults(var=["meff_met"])
    parser.set_defaults(lumi=140.)
    parser.set_defaults(outputDir="signalGridTests/")
    return parser


if __name__ == '__main__':
    PlottingOptions = getGridParser().parse_args()

    CreateDirectory(PlottingOptions.outputDir, False)

    # Analyze the samples and adjust the PlottingOptions
    FileStructure = GetStructure(PlottingOptions)

    SignalFiles = ReadInGrid(InDir=PlottingOptions.ModelPath, Model=PlottingOptions.Model, Couplings=PlottingOptions.Couplings)
    for ana in FileStructure.GetConfigSet().GetAnalyses():
        for region in FileStructure.GetConfigSet().GetAnalysisRegions(ana):
            var = "meff_met"
            Histo = ana + "_Nominal/" + region + "/" + var
            FullHistoSet = CreateHistoSets(PlottingOptions, ana, region, var, UseData=False)
            if not FullHistoSet or not FullHistoSet[0].CheckHistoSet():
                logging.warning('Cannot draw stacked background MC histogram without background samples, skipping...')
                continue

            h_SumBg = FullHistoSet[0].GetSummedBackground()
            for M in PlottingOptions.MeffCuts:
                for E in PlottingOptions.MetCuts:
                    print "#########################################################################################"
                    print "Region: %s Meff: %d Met %d" % (region, M, E)
                    print "#########################################################################################"
                    SignalYields = GetYields(DataSets=SignalFiles,
                                             variable=var,
                                             analysis=ana,
                                             region=region,
                                             MeffCut=M,
                                             MetCut=E,
                                             Lumi=PlottingOptions.lumi)
                    BgYield = h_SumBg.IntegrateWithError(h_SumBg.GetXaxis().FindBin(M), -1, h_SumBg.GetYaxis().FindBin(E), -1)[0]

                    Sig_Over_Bkg = {}
                    for points, yields in SignalYields.iteritems():
                        Sig_Over_Bkg[points] = yields / max(1.e-8, BgYield)

                    Zn = CalculateZnGrid(SignalYields, BgYield, RelUncert=PlottingOptions.BkgUncertainty)
                    ### Check if the grid is actually nothing else than a single line
                    ZnRow = GetGridRow(Zn)
                    YieldRow = GetGridRow(SignalYields)

                    if len(ZnRow) > 0:
                        DrawHistogram1D(PlottingOptions, ZnRow, M, E, region, "Significance", MaxY=PlottingOptions.CutSignificance)
                        AddInfo = "Bkg: %.2f" % (BgYield)

                        DrawHistogram1D(PlottingOptions,
                                        YieldRow,
                                        M,
                                        E,
                                        region,
                                        "Expected events",
                                        AddInfo="Bkg: %.2f" % (BgYield),
                                        PlotPre="Yields")

                        continue
                        DrawHistogram1D(PlottingOptions, YieldRow, M, E, region, "Efficiency", AddInfo=AddInfo, PlotPre="Eff")

                    else:
                        DrawHistogram(PlottingOptions,
                                      Zn,
                                      M,
                                      E,
                                      region,
                                      Zaxis="Significance",
                                      DrawCont=True,
                                      MaxZ=PlottingOptions.CutSignificance)

                        DrawHistogram(PlottingOptions,
                                      SignalYields,
                                      M,
                                      E,
                                      region,
                                      Zaxis="Expected events",
                                      AddInfo="Bkg: %.2f" % (BgYield),
                                      DrawCont=False,
                                      PlotPre="Yields")

                        DrawHistogram(PlottingOptions,
                                      Sig_Over_Bkg,
                                      M,
                                      E,
                                      region,
                                      Zaxis="signal / background",
                                      AddInfo="",
                                      DrawCont=False,
                                      PlotPre="SigOverBkg")

                        continue
                        DrawHistogram(PlottingOptions, SignalYields, M, E, region, "Efficiency", AddInfo="", DrawCont=False, PlotPre="Eff")

                    print "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
