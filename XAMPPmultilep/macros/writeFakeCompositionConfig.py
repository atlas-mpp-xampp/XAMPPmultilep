import os, argparse, logging
from ClusterSubmission.Utils import ResolvePath, CreateDirectory, WriteList
from XAMPPplotting.FileUtils import ReadInputConfig
if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='This script makes input configs for the fake-background composition from a given directory',
        prog='writeFakeCompositionConfigs')
    parser.add_argument("--in_cfg_dir", help="Relative path of the input config files", required=True)

    parser.add_argument("--lightlep_fake_types",
                        help="Which fake types exist for light leptons",
                        default=["Real", "Fake", "LF", "HF", "Conv", "Unmatched"],
                        nargs='+')
    parser.add_argument("--tau_fake_types",
                        help="Which fake types exist for taus",
                        default=["Conv", "Elec", "Fake", "Gluon", "HF", "LF", "Muon", "Real", "Unmatched"],
                        nargs='+')
    parser.add_argument(
        "--mc_samples",
        help="Required MC samples to run the fake composition",
        default=[
            "Sherpa221_VV",
            "aMCatNLOPy8_ttW",
            "aMCatNLOPy8_ttZ",
            "aMcAtNlo_tWZ",
            "MG5Py8_4t",
            "MG5Py8_ttWW",
            "MG5Py8_ttWZ",
            "Sherpa221_VVV",
            "PowHegPy8_ttbar_incl",
            "PowHegPy8_ZH",
            #"PowHegPy8_ggH",
            #"PowHegPy8_VBFH",
            "PowHegPy8_ttH",
            "Sherpa222_VV",
            "Sherpa222_ggZZ",
            "PowHegPy8_Zee",
            "PowHegPy8_Zmumu",
            "PowHegPy8_Ztautau",
            "PowHegPy8_Wenu",
            "PowHegPy8_Wmunu",
            "PowHegPy8_Wtaunu"
        ],
        nargs='+')
    parser.add_argument("--out_dir", help="output_directory for the fake composition config files", default='fake_cfgs')

    options = parser.parse_args()

    in_cfg_dir = ResolvePath(options.in_cfg_dir)
    if not in_cfg_dir: exit(1)

    if len(options.mc_samples) == 0:
        logging.error("Please give at least one sample to run on")
        exit(1)

    logging.info("Look at %s. To check whether all %d samples are valid" % (options.in_cfg_dir, len(options.mc_samples)))
    sample_cfgs = []
    for smp in options.mc_samples:
        cfg = "%s/%s.conf" % (in_cfg_dir, smp)
        if not os.path.isfile(cfg):
            logging.error("Sample %s is not in the input configs. Please check carefully your backgrounds" % (smp))
            exit(1)
        sample_cfgs += ["Input %s" % (r_file) for r_file in ReadInputConfig(cfg)]
    ### Setup the configuration foreach fake type.
    ### Real, Fake LF, HF and Unmatched are commonly defined for leptons and taus
    ### make sure to process them in one go
    fake_types = []
    for light_lep in options.lightlep_fake_types:
        if light_lep in options.tau_fake_types:
            fake_types += [(light_lep, light_lep)]
        else:
            fake_types += [(light_lep, None)]
    for tau in options.tau_fake_types:
        if len([x for x in fake_types if x[1] == tau]) > 0: continue
        fake_types += [(None, tau)]
    ### Write the config files
    for light_lep, tau in fake_types:
        cfg_content = [s for s in sample_cfgs] + ["define applySherpa221_MCClassFix"]
        if light_lep:
            cfg_content += ["define processLightLep", "defineVar fakeTypeLightLep %s" % (light_lep)]
        if tau:
            cfg_content += ["define processTau", "defineVar fakeTypeTau %s" % (tau)]
        cfg_name = light_lep if light_lep else tau
        WriteList(cfg_content, "%s/%s.conf" % (options.out_dir, cfg_name))
