#!/env/python
import ROOT, operator
from XAMPPmultilep.SubmitToGrid import getLogicalDataSets
from ClusterSubmission.Utils import FillWhiteSpaces, ResolvePath, ReadListFromFile
from XAMPPmultilep.SubmitToGrid import GetDSID
DSIDS = []
for DS in getLogicalDataSets().itervalues():
    DSIDS += DS

MyxSecDB = ROOT.SUSY.CrossSectionDB()

for sample, DSIDs in sorted(getLogicalDataSets().iteritems()):
    break
    DSDIDs = sorted([ds for ds in DSIDs if len(MyxSecDB.name(ds))])
    if len(DSIDs) == 0: continue
    maxchar = max([len(MyxSecDB.name(ds)) for ds in DSIDs])
    print FillWhiteSpaces(80, "#")
    print "%s:" % (sample)
    for ds in sorted(DSIDs):
        print "     --- %s%s (%d)" % (MyxSecDB.name(ds), FillWhiteSpaces(maxchar - len((MyxSecDB.name(ds)))), ds)
    print FillWhiteSpaces(80, "#")

#exit(1)
Smp_Dir = ResolvePath("XAMPPmultilep/SampleLists/mc16_13TeV/")
Lists = [
    #"backgrounds.txt",
    "signals.txt",
]
DSIDS = []
### Get the lits of unordered samples
for l in Lists:
    datasets = ReadListFromFile("%s/%s" % (Smp_Dir, l))
    for smp in sorted(datasets):
        if GetDSID(smp) in DSIDS: continue
        DSIDS += [GetDSID(smp)]
        if len([x for x in getLogicalDataSets().itervalues() if GetDSID(smp) in x]) > 0: continue
        print "\"%s\": [%d]," % (smp[smp.find(".", smp.find(str(GetDSID(smp)))) + 1:smp.rfind(".deriv")], GetDSID(smp))

sorted_datasets = sorted(getLogicalDataSets().items(), key=lambda x: x[1][0])
for smp, ds in sorted_datasets:
    break
    if len([d for d in ds if d in DSIDS]) == 0: continue
    print "\"%s\": [%s]," % (smp, ",".join([str(d) for d in ds]))
