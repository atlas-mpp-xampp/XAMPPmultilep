from ClusterSubmission.Utils import WriteList, ResolvePath
import os

User = os.getenv("USER")
RootCoreBin = os.getenv("TestArea")

NTUP_DIR = "/ptmp/mpp/%s/MC17_Simulation/TRUTH/" % (User)

List_DIR = os.getcwd() + "/XAMPPbase/XAMPPbase/data/GroupDiskLists/MPPMU_LOCALGROUPDISK/"

os.system("mkdir -p " + List_DIR)
print "NTUP_DIR: " + NTUP_DIR
print "List_DIR: " + List_DIR

os.chdir(NTUP_DIR)
DatasetList = sorted(os.listdir(NTUP_DIR))
for Datasets in DatasetList:
    print Datasets
    os.chdir("%s/%s" % (NTUP_DIR, Datasets))
    FileList = [
        "%s/%s/%s" % (NTUP_DIR, Datasets, F) for F in os.listdir("%s/%s" % (NTUP_DIR, Datasets))
        if F.endswith(".root") and F.find("TRUTH1") != -1
    ]
    if len(FileList) == 0: continue
    ListName = "TRUTH_" + Datasets
    WriteList(FileList, "%s/%s.txt" % (List_DIR, ListName))
