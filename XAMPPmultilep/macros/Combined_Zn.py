import os, math, ROOT, argparse
from XAMPPplotting.Defs import *
from XAMPPplotting.PlotUtils import *
from XAMPPplotting.PlottingHistos import CreateHistoSets
from XAMPPplotting.FileStructureHandler import GetStructure, ClearServices
from XAMPPplotting.Utils import ExtractMasses
from XAMPPplotting.PrintYields import CutFlowYield, CutFlowSample
from XAMPPplotting.PlotSignificances_Grid import styleHisto, PreparePlotUtils, setupSignificanceParser, DrawLabels
from ClusterSubmission.Utils import CreateDirectory, id_generator

if __name__ == '__main__':
    ROOT.gROOT.Macro("rootlogon.C")
    ROOT.gStyle.SetPalette(ROOT.kViridis)
    ROOT.gROOT.SetBatch(1)

    parser = setupSignificanceParser()
    ### Simple script that picks up the different signal regions to calculate the combined significances
    ### Works with a XAMPPplotitng DSConfig file providing signal and background files
    parser.add_argument("--combine_regions",
                        help="Regions to be combined in a single plot.",
                        nargs="+",
                        default=["SR0B", "SR0G", "SR1B", "SR1C", "SR2B", "SR2C"])
    parser.set_defaults(outputDir="combinedSignificances/")
    parser.set_defaults(noSyst=True)
    parser.set_defaults(lumi=139.)
    PlottingOptions = parser.parse_args()

    CreateDirectory(PlottingOptions.outputDir, False)

    ### Analyze the samples and adjust the PlottingOptions
    FileStructure = GetStructure(PlottingOptions)

    #### Dummy TCanvas ro make the summary plot
    for ana in FileStructure.GetConfigSet().GetAnalyses():
        regions = [r for r in PlottingOptions.combine_regions if r in FileStructure.GetConfigSet().GetAnalysisRegions(ana)]
        bkg_files = [
            f for cfg in FileStructure.GetConfigSet().DSConfigs() for f in cfg.Filepaths
            if cfg.SampleType == SampleTypes.Irreducible or cfg.SampleType == SampleTypes.Reducible
        ]

        bkg_sample = CutFlowSample(smp_name="Sum_BG", smp_label="Total MC")
        signal_cfgs = [cfg for cfg in FileStructure.GetConfigSet().DSConfigs() if cfg.SampleType == SampleTypes.Signal]
        signal_samples = [CutFlowSample(smp_name=cfg.Name, smp_label=cfg.Label) for cfg in signal_cfgs]
        for r in regions:
            bkg_sample.add_region(cf_yield=CutFlowYield(
                smp_name="Sum_BG",
                analysis=ana,
                region=r,
                root_files=bkg_files,
                use_weighted=True,
                lumi=PlottingOptions.lumi,
            ))
            for i, sig_smp in enumerate(signal_samples):
                sig_smp.add_region(cf_yield=CutFlowYield(
                    smp_name=signal_cfgs[i].Name,
                    analysis=ana,
                    region=r,
                    root_files=signal_cfgs[i].Filepaths,
                    use_weighted=True,
                    lumi=PlottingOptions.lumi,
                ))
        ### Now the fun begins
        my_graph = ROOT.TGraph2D()
        for sig in signal_samples:
            nlsp, lsp = ExtractMasses(sig.name())
            my_graph.SetPoint(my_graph.GetN(), nlsp, lsp, sig.calculate_significance(sum_bg=bkg_sample))

        my_graph.Print()
        grid_histo = my_graph.GetHistogram()
        style_histo = styleHisto(grid_histo, PlottingOptions, 1, 2, signal_samples[0].name())
        style_histo.GetZaxis().SetTitle("Significance")
        grid_histo.GetZaxis().SetTitle("Significance")
        pu = PreparePlotUtils(PlottingOptions, id_generator(30))
        pu.GetCanvas()
        style_histo.Draw("AXIS")
        grid_histo.SetContour(2000)
        grid_histo.Draw("same colz l")

        H167 = grid_histo.Clone("167SIGMA")
        H167.SetContour(1)
        H167.SetContourLevel(0, 1.67)
        H167.SetLineColor(ROOT.kRed)
        H167.Draw("CONT3 same")
        H2 = grid_histo.Clone("3SIGMA")
        H2.SetContour(1)
        H2.SetContourLevel(0, 3.)
        H2.Draw("CONT3 same")
        H3 = grid_histo.Clone("5Sigma")
        H3.SetContour(1)
        H3.SetContourLevel(0, 5.)
        H3.SetLineStyle(ROOT.kDashed)
        H3.Draw("CONT3 same")

        DrawLabels(pu, 1, 2, signal_samples[0].name(), PlottingOptions, "", ",".join(regions), 0)
        pu.saveHisto("%s/CombinedZn_%s" % (PlottingOptions.outputDir, "_".join(regions)), ["pdf"])

    ClearServices()
