#!/env/python
import ROOT, logging, math
from ClusterSubmission.Utils import FillWhiteSpaces, ResolvePath, ReadListFromFile, ClearFromDuplicates, WriteList
from XAMPPmultilep.SubmitToGrid import GetDSID

MyxSecDB = ROOT.SUSY.CrossSectionDB()
xs_db = ROOT.SUSY.CrossSectionDB("dev/PMGTools/PMGxsecDB_mc16.txt", False, False, False)
#xs_db.loadFile(ResolvePath("XAMPPmultilep/data/susy_crosssections_13TeV.txt"))

Smp_Dir = ResolvePath("XAMPPmultilep/SampleLists/mc16_13TeV/")
Lists = [
    #"backgrounds.txt",
    #"signals.txt",
    "../mc16_TRUTH3.txt",
]
DSIDs = []
### Get the lits of unordered samples
for l in Lists:
    DSIDs += ClearFromDuplicates([GetDSID(smp) for smp in ReadListFromFile("%s/%s" % (Smp_Dir, l))])

for dsid in DSIDs:
    break
    if xs_db.xsectTimesEff(dsid, 0) < 0:
        #logging.info("%d (%s) has not been known one year ago"%(dsid,MyxSecDB.name(dsid)))
        continue
    elif math.fabs(xs_db.xsectTimesEff(dsid, 0) - MyxSecDB.xsectTimesEff(dsid, 0)) / xs_db.xsectTimesEff(dsid, 0) > 1.e-2:
        logging.warning("%d (%s) paaaarty. The cross-section has changed from %3f to %f" %
                        (dsid, MyxSecDB.name(dsid), xs_db.xsectTimesEff(dsid, 0), MyxSecDB.xsectTimesEff(dsid, 0)))

new_file = []
for line in ReadListFromFile(ResolvePath("dev/PMGTools/PMGxsecDB_mc16.txt")):
    try:
        dsid = int(line.split()[0])
    except:
        continue
    if dsid not in DSIDs: continue
    new_file += [line]

WriteList(["#Mad of the official PMG file... Does not make much sense at all"] + sorted(new_file), "backgrounds.txt")
