import os, commands, math, sys, re, random, subprocess, ROOT, argparse, logging

from XAMPPplotting.PlotLabels import DrawXaxisMassLabel, DrawYaxisMassLabel
from ClusterSubmission.Utils import CreateDirectory, id_generator, ResolvePath, ReadListFromFile, WriteList

from pprint import pprint
In_Path = "/ptmp/mpp/junggjo9/Cluster/OUTPUT/2020-04-13/4L_Histos/"

signal_dict = {}
for x in sorted(os.listdir(In_Path)):
    if x.find("LLE") == -1 and x.find("GGM_") == -1: continue
    grid_point = x[:x.rfind(".")]
    #print grid_point
    model_name = grid_point.split("_")[0] + "_" + grid_point.split("_")[-1] if "GGM" not in grid_point else "GGM"
    try:
        signal_dict[model_name] += [grid_point]
    except:
        signal_dict[model_name] = [grid_point]

Template_Cfg = []

with open("XAMPPplotting/python/DSConfigs/FourLepton_Bkg.py") as f:
    for l in f:
        Template_Cfg += [l[:-1]]

#pprint (Template_Cfg)
for model, points in signal_dict.iteritems():
    model_cfg = [x for x in Template_Cfg] + ["SignalPath='%s'" % (In_Path)]
    for grid_point in points:
        model_cfg += [
            "%s = DSconfig(sampletype = SampleTypes.Signal," % (grid_point),
            "name = '%s'," % (grid_point),
            "label='%s'," % (grid_point),
            "filepath = SignalPath+'/%s.root'" % (grid_point), ")"
        ]
    #pprint(model_cfg)
    WriteList(model_cfg, "XAMPPplotting/python/DSConfigs/FourLepton_Rel21/FourLepton_%s.py" % (model))

#/ptmp/mpp/junggjo9/Cluster/OUTPUT/2020-03-04/Thesis20p7/
