import os, argparse, logging
from pprint import pprint
from math import fabs, sqrt
from XAMPPplotting.Utils import setupBaseParser
from ClusterSubmission.Utils import ResolvePath
from XAMPPplotting.FileStructureHandler import GetSystPairer, GetStructure, ClearServices
from XAMPPplotting.PlottingHistos import CreateHistoSets
from XAMPPplotting.PrintYields import CutFlowYield
from XAMPPplotting.CheckMetaData import prettyPrint, max_width, writeTableHeader, formatItem, writeTableFooter
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Simple script which calculates the theory uncertainties from a given DSConfig -h\"',
                                     prog='MCPlots',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser = setupBaseParser(parser)
    parser.add_argument("--out_tex_file", help="Out tex file", default="syst_tables.tex")
    parser.set_defaults(lumi=139.)
    parser.set_defaults(config=[ResolvePath("XAMPPplotting/python/DSConfigs/FourLepton_Truth.py")])
    parser.set_defaults(regions=[
        #'CR_ZZ',
        #'CR_ttZ',
        #'SR0A',
        #'SR0B',
        #'SR0G',
        #'SR0C',
        #'SR0D',
        #'SR0E',
        #'SR0F',
        #'SR1A',
        #'SR1B',
        #'SR1C',
        #'SR2A',
        #'SR2B',
        #'SR2C',
        #'VR0',
        #'VR0Z_bveto',
        #'VR0_ttZ',
        #'VR1',
        #'VR1Z',
        #'VR2',
        #'VR2Z',
        #"VR0Z",
        '5L'
    ])
    parser.add_argument(
        "--exclude_from_total",
        help="List of uncertainty sources to be shown in the table but to be excluded from the total uncertainty calculation",
        nargs="+",
        default=[
            "otherPDF",
            "PDFset=260",
            "PDFset=261",
            "PDF4LHC15_nlo_30",
            "Generator",
        ])
    options = parser.parse_args()
    FileStructure = GetStructure(options)

    syst_list = {}
    global_syst_names = []
    for ana in FileStructure.GetConfigSet().GetAnalyses():
        for region in FileStructure.GetConfigSet().GetAnalysisRegions(ana):

            ### Use the cutflow histogram to extract the yield
            FullHistoSet = CreateHistoSets(options, ana, region, "InfoHistograms/CutFlow_weighted", UseData=False)

            if not FullHistoSet or not FullHistoSet[0].CheckHistoSet():
                logging.warning("No cutflow has been given")
                continue

            DefaultSet = FullHistoSet[0]

            #sum_bg = DefaultSet.GetSummedBackground()

            for sample in DefaultSet.GetBackgrounds():
                cf_yield = CutFlowYield(smp_histo=sample)
                if not cf_yield.label() in syst_list.iterkeys():
                    syst_list[cf_yield.label()] = {}
                if "Nominal" not in syst_list[cf_yield.label()].iterkeys():
                    syst_list[cf_yield.label()]["Nominal"] = {}
                    syst_list[cf_yield.label()]["Total"] = {}

                syst_list[cf_yield.label()]["Nominal"][region] = (cf_yield.get_yield(), cf_yield.get_stat_error())

                if cf_yield.get_yield() == 0: continue
                cf_bin = cf_yield.get_n_bins()
                for syst in sample.GetSystComponents():
                    if syst.name() not in global_syst_names:
                        global_syst_names += [syst.name()]
                    if syst.name() not in syst_list[cf_yield.label()].iterkeys():
                        syst_list[cf_yield.label()][syst.name()] = {}

                    up_var = syst.up_histo().GetBinContent(cf_bin) / cf_yield.get_yield()
                    dn_var = syst.dn_histo().GetBinContent(cf_bin) / cf_yield.get_yield()

                    syst_list[cf_yield.label()][syst.name()][region] = (100. * up_var, 100. * dn_var)
                    if syst.name() in options.exclude_from_total: continue
                    if region not in syst_list[cf_yield.label()]["Total"].iterkeys():
                        syst_list[cf_yield.label()]["Total"][region] = [0., 0.]
                    syst_list[cf_yield.label()]["Total"][region][0] = sqrt(syst_list[cf_yield.label()]["Total"][region][0]**2 + up_var**2)
                    syst_list[cf_yield.label()]["Total"][region][1] = sqrt(syst_list[cf_yield.label()]["Total"][region][1]**2 + dn_var**2)

        ### Multiply the uncertainties by 100.
        for label, syst_dict in syst_list.iteritems():
            for region in syst_dict["Total"].iterkeys():
                syst_dict["Total"][region][0] *= 100.
                syst_dict["Total"][region][1] *= 100.

        global_syst_names.sort()
        global_syst_names += ["Total"]
        with open(options.out_tex_file, "w") as out_file:
            #out_file.write("%%%%\n ")
            for sample, syst_yield in syst_list.iteritems():

                tabular = []
                for region in [r for r in options.regions if r in FileStructure.GetConfigSet().GetAnalysisRegions(ana)]:
                    tab_line = [region] + ["%.1f#pm%.1f" % (syst_yield["Nominal"][region][0], syst_yield["Nominal"][region][1])]
                    for syst in [g for g in global_syst_names if g in syst_yield.iterkeys()]:
                        if region not in syst_yield[syst].iterkeys():
                            tab_line += ["---"]
                        elif "%.1f" % (syst_yield[syst][region][0]) != "%.1f" % (syst_yield[syst][region][1]):
                            tab_line += ["\\ensuremath{%.1f\\%% | %.1f\\%%}" % (syst_yield[syst][region][0], syst_yield[syst][region][1])]
                        else:
                            tab_line += ["%.1f" % (syst_yield[syst][region][0])]
                    tabular += [tab_line]
                header = ['Region', 'Nominal yield'] + [
                    formatItem(GetSystPairer().get_syst_title(g), True) for g in global_syst_names if g in syst_yield.iterkeys()
                ]
                max_widths = max_width(tabular, doLaTeX=True)

                out_file.write(writeTableHeader(header, vertical=False))
                for the_line in tabular:
                    out_file.write(prettyPrint(itemsToPrint=the_line, doLaTeX=True, row_widths=max_widths))
                out_file.write(
                    writeTableFooter("Theoretical uncertainties for the %s background based on generator level events." %
                                     (formatItem("$" + sample + "$", True)),
                                     vertical=False))
