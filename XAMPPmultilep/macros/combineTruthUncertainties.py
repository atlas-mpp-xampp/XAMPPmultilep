import os, argparse, logging, ROOT
from ClusterSubmission.Utils import ResolvePath, CreateDirectory, WriteList
from XAMPPplotting.FileUtils import ReadInputConfig
from XAMPPplotting.FileStructureHandler import GetFileHandler
from XAMPPplotting.QuickPlots import GetHistoPaths
from XAMPPplotting.Utils import mkdir
#### The truth uncertainties are calculated from the
#### internal weights of the generated samples in general
#### using the XAMPPplotting framework. Nevertheless,
#### there are few extra samples
####   --- Sherpa222_VV_CKKW15
####   --- Sherpa222_VV_CKKW30
####   --- Sherpa222_VV_CSSKIN
####   --- Sherpa222_VV_QSF025
####   --- Sherpa222_VV_QSF4
####
####   --- aMCatNLOPy8_ttZ_A14VarDn
####   --- aMCatNLOPy8_ttZ_A14VarUp

####  requiring some manipulation of the final output file structures.
####  Also there are the generator comparison samples which need to be
####  merged into the final file such that the truth uncertainties can be
####  properly evaluated.
####  --- Sherpa221_ttZ_multileg
####  --- PowHegPy8_VV
####  --- aMCatNLOttH
####    For the 36.1 ifb the ttZ samples need some manipulation as well
####    --- MG5_ttZ_LO
####    --- MG5_ttZ_ScaleDn
####    --- MG5_ttZ_ScaleUp
####    --- SherpaLO_ttZ

### Reads the ROOTFile and renames the nominal systematic


def rename_nominal(out_dir, samples=[]):
    CreateDirectory(out_dir, True)
    for smp in samples:
        smp_file = GetFileHandler().LoadFile(smp)
        nominal_key = [Key for Key in smp_file.GetListOfKeys() if Key.GetName().endswith("Nominal")][0]
        histos_in_file = GetHistoPaths(TFile=smp_file, Directory=nominal_key.GetName())
        new_file = ROOT.TFile("%s/%s" % (out_dir, smp[smp.rfind("/") + 1:]), "RECREATE")
        for h in histos_in_file:
            new_path = h[:h.find("_")] + "_" + smp[smp.rfind("_") + 1:smp.rfind(".")] + "/" + h[h.find("/") + 1:h.rfind("/") + 1]
            h_name = h[h.rfind("/") + 1:]
            obj_dir = mkdir(root_file=new_file, path=new_path)
            obj_dir.WriteObject(smp_file.Get(h), h_name)
        new_file.Close()


def merge_files(out_dir, sample_name, nominal_sample, systematic_samples=[]):
    rename_nominal(out_dir=out_dir + "/TMP/", samples=systematic_samples)
    hadd_cmd = "hadd -f %s/%s.root %s %s" % (out_dir, sample_name, nominal_sample, " ".join(
        [out_dir + "/TMP/" + x for x in os.listdir(out_dir + "/TMP/")]))
    os.system(hadd_cmd)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='This script makes the final input files for the truth uncertainty calculations',
                                     prog='writeFakeCompositionConfigs')
    parser.add_argument("--in_file_dir", help="Path to the directory where the ROOTFiles are listed", required=True)
    parser.add_argument("--out_file_dir", help="New files are going to be stored where?", default="MergedTruth/")
    parser.add_argument("--nominal_zz_sample", help="Name of the nominal ZZ sample", default="Sherpa222_VV")
    parser.add_argument("--nominal_ttz_sample", help="Name of the nominal ttZ sample", default="aMCatNLOPy8_ttZ")
    parser.add_argument("--nominal_tth_sample", help="Name of the nominal ttH sample", default="PowHegPy8_ttH")
    parser.add_argument("--alternative_zz_sample", help="Name of the alternative zz sample", default="PowHegPy8_VV")
    parser.add_argument("--alternative_ttz_sample", help="Name of the alternative ttZ sample", default="Sherpa221_ttZ_multileg")
    parser.add_argument("--alternative_tth_sample", help="Name of the alternative ttH sample", default="aMcAtNloPy8_ttH")
    options = parser.parse_args()

    in_file_dir = ResolvePath(options.in_file_dir)
    if not in_file_dir: exit(1)

    zz_samples = [
        in_file_dir + "/" + x for x in os.listdir(in_file_dir)
        if (x[:x.rfind("_")] == options.nominal_zz_sample or x[:x.rfind(".")] == options.alternative_zz_sample)
        and x[:x.rfind(".")] != options.nominal_zz_sample
    ]
    ttz_samples = [
        in_file_dir + "/" + x for x in os.listdir(in_file_dir)
        if (x[:x.rfind("_")] == options.nominal_ttz_sample or x[:x.rfind(".")] == options.alternative_ttz_sample)
        and x[:x.rfind(".")] != options.nominal_ttz_sample
    ]
    tth_samples = [
        in_file_dir + "/" + x for x in os.listdir(in_file_dir)
        if (x[:x.rfind("_")] == options.nominal_tth_sample or x[:x.rfind(".")] == options.alternative_tth_sample)
        and x[:x.rfind(".")] != options.nominal_tth_sample
    ]

    #### Open the files and rename the nominal systematic in them into a copied file
    merge_files(out_dir=options.out_file_dir,
                sample_name=options.nominal_zz_sample,
                nominal_sample="%s/%s.root" % (options.in_file_dir, options.nominal_zz_sample),
                systematic_samples=zz_samples)
    merge_files(out_dir=options.out_file_dir,
                sample_name=options.nominal_ttz_sample,
                nominal_sample="%s/%s.root" % (options.in_file_dir, options.nominal_ttz_sample),
                systematic_samples=ttz_samples)
    merge_files(out_dir=options.out_file_dir,
                sample_name=options.nominal_tth_sample,
                nominal_sample="%s/%s.root" % (options.in_file_dir, options.nominal_tth_sample),
                systematic_samples=tth_samples)
