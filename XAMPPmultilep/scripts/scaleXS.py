import ROOT
import os, sys, re, logging
from enum import Enum

logger = logging.getLogger('scaleXS')
logger.setLevel(logging.DEBUG)

fh = logging.FileHandler('scaleXS.log')
fh.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(levelname)s - %(message)s')
fh.setFormatter(formatter)
logger.addHandler(fh)

ch = logging.StreamHandler()
ch.setLevel(logging.INFO)
formatter = logging.Formatter('%(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)


class RPV(Enum):
    LLE12k = 1
    LLEi33 = 2

    def str_to_RPV(string):
        if "12k" in string:
            return RPV.LLE12k
        elif "i33" in string:
            return RPV.LLEi33
        else:
            return None

    def __str__(self):
        if self == RPV.LLE12k:
            return "12k"
        elif self == RPV.LLEi33:
            return "i33"
        else:
            return "unknown"


class Sample:
    def __init__(self, DSID):
        self.__DSID = DSID
        self.__totalXS = 0.
        self.__procXS = {}

    def addProc(self, procAndXS):
        # Key: Process ID, Value: XS, eff, k-factor, uncertainty
        self.__procXS[int(procAndXS[0])] = [float(n) for n in procAndXS[1:]]
        return True

    def calcTotalXS(self):
        totalXS = 0.
        for k in self.__procXS.keys():
            XStuple = tuple(self.__procXS[k])
            totalXS += XStuple[0]
        self.__totalXS = totalXS
        return True

    def scaleToXSlimit(self, XS_limit):
        for proc, XStuple in self.__procXS.items():
            if XS_limit == 0:
                logger.info("Calculated XS limit for sample {:d} is 0. Not scaling.".format(self.__DSID))
            elif XS_limit < self.__totalXS:
                self.__procXS[proc] = [XStuple[0] * (XS_limit / self.__totalXS)] + XStuple[1:]
        return True

    def getDSID(self):
        return self.__DSID

    def getProcXS(self):
        return self.__procXS.copy()

    def getTotalXS(self):
        return self.__totalXS

    def __str__(self):
        return "\n".join([
            "{dsid:6d}   {proc:d}   {XStuple[0]:f}   {XStuple[1]:f}   {XStuple[2]:f}   {XStuple[3]:f}".format(
                dsid=self.__DSID, proc=proc, XStuple=XStuple) for proc, XStuple in self.__procXS.items()
        ]) + "\n"


def read_DSIDtoMass_from_txt(lines):
    DSIDtoMass = {}
    for line in lines:
        if not line.startswith("#") or "direct" in line:
            continue
        dsid = int(line.split('.')[0].split()[1])
        mass_tuple = line.split('_')[-3:]

        # The HEPdata files have x:m(LSP), y:m(NLSP)
        # But the sample names are m(NLSP)_m(LSP)
        # We swap them here
        masses = (int(mass_tuple[1]), int(mass_tuple[0]), RPV.str_to_RPV(mass_tuple[2].strip()))
        DSIDtoMass[dsid] = masses
    return DSIDtoMass


def read_xs_from_txt(lines):
    sample_dict = {}
    for line in lines:
        if line.startswith("#") or "direct" in line:
            continue
        info = line.split()
        if len(info) == 0:
            continue
        dsid = int(info[0])
        if sample_dict.get(dsid, None):
            sample_dict.get(dsid).addProc(info[1:])
        else:
            sample = Sample(dsid)
            sample.addProc(info[1:])
            sample_dict[dsid] = sample
    return sample_dict


def read_xs_from_root(f_xs_limit, table_no):
    histos = []

    for n in table_no:
        name_dir = "Table {:d}".format(n)
        name_graph = ""
        for k in f_xs_limit.Get(name_dir).GetListOfKeys():
            if isinstance(f_xs_limit.Get(os.path.join(name_dir, k.GetName())), ROOT.TGraph2DErrors):
                name_graph = k.GetName()
        if name_graph == "":
            logger.error("No graph!")
            return None

        graph = f_xs_limit.Get(os.path.join(name_dir, name_graph))
        histos.append(graph.GetHistogram().Clone())

    return histos


def main():
    ROOT.gROOT.SetBatch(True)

    baseDir_4L = "/Users/ggallard/Documents/Docs4L"
    baseDir_XAMPP = os.path.join(baseDir_4L, "src/XAMPPmultilep")

    baseDir_XS = os.path.join(baseDir_XAMPP, "XAMPPplotting/data/xSecFiles/")

    table_no = {"Wino": ((23, 25), (24, 26)), "LV": ((31, 33), (32, 34)), "GG": ((35, 37), (36, 38))}

    # table_no_root_12k, table_no_root_i33 = table_no["GG"]
    # filename_PMG = "LHCSUSY_GG_LLE.txt"

    # table_no_root_12k, table_no_root_i33 = table_no["Wino"]
    # filename_PMG = "MGPy8EG_Wino.txt"

    table_no_root_12k, table_no_root_i33 = table_no["LV"]
    filename_PMG = "MG5Py8EG_LV_LLE.txt"

    filename_limit = "HEPdata.root"

    filename_out = "new_" + filename_PMG
    dict_dsid = {}
    dict_mass = {}

    logger.info("Starting program")

    logger.info("Reading from xs file {}".format(os.path.join(baseDir_XS, filename_PMG)))
    DSIDtoMass = {}
    with open(os.path.join(baseDir_XS, filename_PMG)) as f_xs_PMG:
        lines = f_xs_PMG.readlines()
        DSIDtoMass = read_DSIDtoMass_from_txt(lines)
        sample_dict = read_xs_from_txt(lines)
    logger.info("Read DSIDtoMass dictionary and sample dictionary")

    for dsid, sample in sample_dict.items():
        logger.debug(dsid)
        logger.debug("Total cross section (pre-sum): {}".format(sample.getTotalXS()))
        logger.debug(sample.getProcXS())
        sample.calcTotalXS()
        logger.debug("Total cross section (post-sum): {}".format(sample.getTotalXS()))
        logger.debug("")

    logger.info("Summed sample cross sections from text files")

    can = ROOT.TCanvas()
    logger.info("Reading histogram from {}".format(os.path.join(baseDir_4L, filename_limit)))
    f_limit = ROOT.TFile(os.path.join(baseDir_4L, filename_limit))
    hist_12k, hist_12k_B = read_xs_from_root(f_limit, table_no_root_12k)
    hist_i33, hist_i33_B = read_xs_from_root(f_limit, table_no_root_i33)
    bins_max_12k = hist_12k.GetNbinsX() * hist_12k.GetNbinsY()
    bins_max_i33 = hist_i33.GetNbinsX() * hist_i33.GetNbinsY()

    logger.info("Read histograms from ROOT files")

    updated_sample_dict = sample_dict.copy()

    for dsid, sample in updated_sample_dict.items():
        masses = DSIDtoMass.get(dsid, None)
        if not masses:
            continue
        if masses[2] == RPV.LLE12k:
            h = hist_12k
            h_B = hist_12k_B
            bins_max = bins_max_12k
        elif masses[2] == RPV.LLEi33:
            h = hist_i33
            h_B = hist_i33_B
            bins_max = bins_max_i33
        else:
            logger.error("DSID {} is neither 12k nor i33?".format(dsid))
            continue

        logger.debug("DSID {:6d}: m(N1)={:4d}, m(NLSP)={:4d}, {}".format(dsid, masses[0], masses[1], masses[2]))
        bin_n = h.FindBin(masses[0], masses[1])
        if bin_n <= 0:
            logger.debug("-- Underflow. Skipping...")
            logger.debug("")
            continue
        if bin_n > bins_max:
            logger.debug("-- Overflow. Skipping...")
            logger.debug("")
            continue

        xs_limit = min(h.GetBinContent(bin_n), h_B.GetBinContent(bin_n)) / 1000.
        pre_XS = sample.getTotalXS()
        sample.scaleToXSlimit(xs_limit)
        sample.calcTotalXS()
        post_XS = sample.getTotalXS()
        logger.debug("-- Pre: {:.5f} XS limit: {:.5f} Post: {:.5f} ".format(pre_XS, xs_limit, post_XS))

        logger.debug("")

    logger.info("Updated cross sections")

    with open(os.path.join(baseDir_4L, filename_out), "w") as outFileTxt:
        for sample in updated_sample_dict.values():
            outFileTxt.write("{}".format(sample))
    logger.info("Wrote out new cross sections to {:s}".format(os.path.join(baseDir_4L, filename_out)))


if __name__ == "__main__":
    main()
