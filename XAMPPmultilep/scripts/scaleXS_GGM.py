import ROOT
import os, sys, re, logging, math
from enum import Enum
from scaleXS import read_xs_from_root, Sample, logger


def read_DSIDtoMass_from_txt(lines):
    DSIDtoMass = {}
    nLinePairs = math.floor(len(lines) / 2)
    for nPair in range(nLinePairs):
        sample_name = lines[nPair * 2 - 1]
        if not line.startswith("#") or "direct" in line:
            continue
        dsid = int(line.split('.')[0].split()[1])
        mass_tuple = line.split('_')[-3:]

        # The HEPdata files have x:m(LSP), y:m(NLSP)
        # But the sample names are m(NLSP)_m(LSP)
        # We swap them here
        masses = (int(mass_tuple[1]), int(mass_tuple[0]), RPV.str_to_RPV(mass_tuple[2].strip()))
        DSIDtoMass[dsid] = masses
    return DSIDtoMass


def read_xs_from_txt(lines):
    DSIDtoMass = {}
    sample_dict = {}

    nLinePairs = math.floor(len(lines) / 2)
    for pair_n in range(nLinePairs):
        line_sampleName = lines[pair_n * 2]
        line_sampleData = lines[pair_n * 2 + 1]

        # Extract mass
        mass = int(line_sampleName.split('#')[1].split('_')[4])

        # Extract DSID and XS info
        info = line_sampleData.split()
        dsid = int(info[0])
        if sample_dict.get(dsid, None):
            logger.error("Sample {:d} already in sample_dict!?".format(dsid))
        else:
            sample = Sample(dsid)
            sample.addProc([0] + info[1:])
            sample_dict[dsid] = sample

        DSIDtoMass[dsid] = mass

    return DSIDtoMass, sample_dict


def main():

    ROOT.gROOT.SetBatch(True)

    baseDir_4L = "/Users/ggallard/Documents/Docs4L"
    baseDir_XAMPP = os.path.join(baseDir_4L, "src/XAMPPmultilep")

    baseDir_XS = os.path.join(baseDir_XAMPP, "XAMPPplotting/data/xSecFiles/")

    table_no_root = (39, 40)
    filename_PMG = "GGMHino.txt"

    filename_limit = "HEPdata.root"

    filename_out = "new_" + filename_PMG
    dict_dsid = {}
    dict_mass = {}

    logger.info("Starting program")

    logger.info("Reading from xs file {}".format(os.path.join(baseDir_XS, filename_PMG)))
    DSIDtoMass = {}
    with open(os.path.join(baseDir_XS, filename_PMG)) as f_xs_PMG:
        lines = f_xs_PMG.readlines()
        DSIDtoMass, sample_dict = read_xs_from_txt(lines)
    logger.info("Read DSIDtoMass dictionary and sample dictionary")

    for dsid, sample in sample_dict.items():
        logger.debug(dsid)
        logger.debug("Total cross section (pre-sum): {}".format(sample.getTotalXS()))
        logger.debug(sample.getProcXS())
        sample.calcTotalXS()
        logger.debug("Total cross section (post-sum): {}".format(sample.getTotalXS()))
        logger.debug("")

    logger.info("Summed sample cross sections from text files")

    can = ROOT.TCanvas()
    logger.info("Reading histogram from {}".format(os.path.join(baseDir_4L, filename_limit)))
    f_limit = ROOT.TFile(os.path.join(baseDir_4L, filename_limit))
    hist = read_xs_from_root(f_limit, table_no_root)
    bins_max = hist[0].GetNbinsX() * hist[0].GetNbinsY()

    logger.info("Read histograms from ROOT files")

    updated_sample_dict = sample_dict.copy()

    for dsid, sample in updated_sample_dict.items():
        mass = DSIDtoMass.get(dsid, None)
        if not mass:
            logger.warning("Corresponding mass not found for sample {:d}".format(dsid))
            continue

        logger.debug("DSID {:6d}: m(h)={:d}".format(dsid, mass))

        bin_n = hist[0].FindBin(50, mass)  # (x, y) = (BR, mass); BR = 50
        if bin_n <= 0:
            logger.debug("-- Underflow. Skipping...")
            logger.debug("")
            continue
        if bin_n > bins_max:
            logger.debug("-- Overflow. Skipping...")
            logger.debug("")
            continue

        xs_limit = min(hist[0].GetBinContent(bin_n), hist[1].GetBinContent(bin_n)) / 1000.
        pre_XS = sample.getTotalXS()
        sample.scaleToXSlimit(xs_limit)
        sample.calcTotalXS()
        post_XS = sample.getTotalXS()
        logger.debug("-- Pre: {:.5f} XS limit: {:.5f} Post: {:.5f} ".format(pre_XS, xs_limit, post_XS))

        logger.debug("")

    logger.info("Updated cross sections")

    with open(os.path.join(baseDir_4L, filename_out), "w") as outFileTxt:
        for sample in updated_sample_dict.values():
            outFileTxt.write("{}".format(sample))
    logger.info("Wrote out new cross sections to {:s}".format(os.path.join(baseDir_4L, filename_out)))


if __name__ == "__main__":
    main()
