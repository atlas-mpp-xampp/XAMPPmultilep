#!/env/python
import os, sys, argparse, time
from ClusterSubmission.Utils import ResolvePath, ReadListFromFile, id_generator, WriteList
from XAMPPmultilep.SubmitToGrid import getLogicalDataSets

Smp_Dir = ResolvePath("XAMPPmultilep/SampleLists/mc16_13TeV/")
Update_Cmd = "python %s --ListDir %s --derivation SUSY2 --min_ptag 3700" % (ResolvePath("XAMPPbase/python/UpdateSampleLists.py"), Smp_Dir)
#os.system(Update_Cmd)
#exit(1)
Lists = [
    "backgrounds.txt",
    # "data.txt",
    # "signals.txt",
    #"combinations.txt",
]

Samples = [
    # "Sherpa221_VVV",
    # "MG5Py8_ttXX",
    # "PowHegPy8_ZHtautau",
    # "PowHegPy8_Zee",
    #"PowHegPy8_Zmumu",
    #"PowHegPy8_ttbar_incl",
    #"PowHegPy8_ttbar_diL",
    "aMcAtNloPy8_tHjb_4fl",
    "aMcAtNloPy8_tWH"
]
# Samples = [x for x in getLogicalDataSets().iterkeys() if x.find("Sherpa") != -1]
# Samples = ["PowHegPy8_ttH"]
#######################################################################################################
# SubmitCmd = "python XAMPPmultilep/python/SubmitToGrid.py --sampleList %s --stream SUSY4L --production Rel21_v029 --logicalSamplesOnly  --productionRole phys-susy %s --noMergeJob" % (
#     " ".join(["%s/%s" % (Smp_Dir, L) for L in Lists]), "" if len(Samples) == 0 else "--physicalSamples " + " ".join(Samples))
# os.system(SubmitCmd)
#exit(1)
#  --destRSE UKI-SOUTHGRID-RALPP_LOCALGROUPDISK
#  UKI-SOUTHGRID-OX-HEP_LOCALGROUPDISK
Monitor_Cmd = "python XAMPPmultilep/python/checkProduction.py --stream SUSY4L --production Rel21_v029_ETrigSF --destRSE UKI-SOUTHGRID-RALPP_LOCALGROUPDISK --replicate_ds --productionRole phys-susy  --log_file panda_progess_SUSY4L.log "  # --resubmit_broken"
os.system(Monitor_Cmd)
# SubmitCmd = "python XAMPPmultilep/python/SubmitToGrid.py --sampleList %s --stream 4LepSF --production Rel21_v028_ConstrainedSherpa --logicalSamplesOnly --noSyst --productionRole det-muon %s" % (
#     " ".join("%s/%s" % (Smp_Dir, L) for L in Lists if L.find("signal") == -1 and L.find("combinations") == -1),
#     "" if len(Samples) == 0 else "--physicalSamples " + " ".join(Samples))
# #os.system(SubmitCmd)

# Monitor_Cmd = "python XAMPPmultilep/python/checkProduction.py --stream 4LepSF --production Rel21_v028_ConstrainedSherpa --destRSE MPPMU_LOCALGROUPDISK --replicate_ds --productionRole det-muon "  # --resubmit_broken"
# os.system(Monitor_Cmd)

# SubmitCmd = "python XAMPPmultilep/python/SubmitToGrid.py --sampleList XAMPPmultilep/SampleLists/mc16_TRUTH3.txt --stream Truth4L --production Rel21_v028 --logicalSamplesOnly --noSyst "
# #os.system(SubmitCmd)

# SubmitCmd = "python XAMPPmultilep/python/SubmitToGrid.py --sampleList %s --stream ttGamma --production Rel21_v023 --productionRole det-muon" % (
#     " ".join("%s/%s" % (Smp_Dir, L) for L in Lists if L.find("signal") == -1), )

# #os.system(SubmitCmd)

# # ~ aMCatNLOPy8_ttZ_A14VarUp

# Monitor_Cmd = "python XAMPPmultilep/python/checkProduction.py --stream Truth4L --production Rel21_v028 --replicate_ds --destRSE MPPMU_LOCALGROUPDISK --rucio jojungge --in_samples %s/../mc16_TRUTH3.txt " % (
#     Smp_Dir)
# os.system(Monitor_Cmd)
