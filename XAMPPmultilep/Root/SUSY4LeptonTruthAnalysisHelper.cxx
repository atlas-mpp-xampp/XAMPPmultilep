#include <XAMPPbase/AnalysisUtils.h>
#include <XAMPPbase/EventInfo.h>
#include <XAMPPbase/ReconstructedParticles.h>
#include <XAMPPbase/ToolHandleSystematics.h>
#include <XAMPPmultilep/AnalysisUtils.h>
#include <XAMPPmultilep/SUSY4LeptonTruthAnalysisHelper.h>

#include <fstream>
#include <iostream>
#include <sstream>

namespace XAMPP {
    SUSY4LeptonTruthAnalysisHelper::SUSY4LeptonTruthAnalysisHelper(const std::string& myname) :
        SUSYTruthAnalysisHelper(myname),
        m_applyDFOSVeto(true),
        m_lowMassDiLepCut(12.e3),
        m_JetHt_minPt(40.e3),
        m_ZMassWindow(10.e3),
        m_useNuForInvP4(false) {
        declareProperty("ApplyDFOS", m_applyDFOSVeto);
        declareProperty("DiMllCut", m_lowMassDiLepCut);
        declareProperty("HtMinJetPt", m_JetHt_minPt);
        declareProperty("ZmassWindow", m_ZMassWindow);
        declareProperty("NeutrinosInInvariantP4", m_useNuForInvP4);
    }

    SUSY4LeptonTruthAnalysisHelper::~SUSY4LeptonTruthAnalysisHelper() {}
    StatusCode SUSY4LeptonTruthAnalysisHelper::initializeObjectTools() {
        ATH_CHECK(SUSYTruthAnalysisHelper::initializeObjectTools());
        return StatusCode::SUCCESS;
    }

    StatusCode SUSY4LeptonTruthAnalysisHelper::RemoveOverlap() {
        // Last argument to disable the usage of the rapidity
        ATH_CHECK(XAMPP::RemoveOverlap(m_truth_selection->GetTruthPreTaus(), m_truth_selection->GetTruthPreElectrons(), 0.2, false));
        ATH_CHECK(XAMPP::RemoveOverlap(
            m_truth_selection->GetTruthPreTaus(), m_truth_selection->GetTruthPreMuons(),
            [](const xAOD::IParticle* Tau, const xAOD::IParticle* Mu) {
                /// Only assume that the muons cannot be combined for high eta
                if (Tau->pt() > 50.e3 && fabs(Mu->eta()) > 2.5) return 0.;
                return 0.2;
            },
            false));

        // Simple analysis never sets CaloTagged Muons. Keep this line for completeness commented in
        // ATH_CHECK(XAMPP::RemoveOverlap(m_truth_selection->GetTruthPreMuons(), m_truth_selection->GetTruthPreElectrons(), 0.01,false));
        ATH_CHECK(XAMPP::RemoveOverlap(m_truth_selection->GetTruthPreElectrons(), m_truth_selection->GetTruthPreMuons(), 0.01, false));

        ATH_CHECK(XAMPP::RemoveOverlap(m_truth_selection->GetTruthPreJets(), m_truth_selection->GetTruthPreElectrons(), 0.2, false));
        ATH_CHECK(XAMPP::RemoveOverlap(m_truth_selection->GetTruthPreElectrons(), m_truth_selection->GetTruthPreJets(), 0.4, false));
        // Also this line is only for completeness
        // ATH_CHECK(XAMPP::RemoveOverlap(m_truth_selection->GetTruthPreJets(), m_truth_selection->GetTruthPreMuons(), 0.4,false));
        ATH_CHECK(XAMPP::RemoveOverlap(m_truth_selection->GetTruthPreMuons(), m_truth_selection->GetTruthPreJets(), 0.4, false));

        // Be aware that SimpleAnalysis takes SignalTaus. Since we do not have any change in kinematics going from
        // baseline -> signal we can have this line here
        ATH_CHECK(XAMPP::RemoveOverlap(m_truth_selection->GetTruthPreJets(), m_truth_selection->GetTruthPreTaus(), 0.4, false));
        // Let's do the photons at the very last if they are needed at all
        if (m_systematics->ProcessObject(XAMPP::SelectionObject::Photon)) {
            ATH_CHECK(XAMPP::RemoveOverlap(m_truth_selection->GetTruthPrePhotons(), m_truth_selection->GetTruthPreElectrons(), 0.4, false));
            ATH_CHECK(XAMPP::RemoveOverlap(m_truth_selection->GetTruthPrePhotons(), m_truth_selection->GetTruthPreMuons(), 0.4, false));
            ATH_CHECK(XAMPP::RemoveOverlap(m_truth_selection->GetTruthPreJets(), m_truth_selection->GetTruthPrePhotons(), 0.4, false));
        }
        static CharDecorator dec_passLMR("passLMR");
        static CharAccessor acc_passOR("passOR");

        xAOD::IParticleContainer Lep(SG::VIEW_ELEMENTS);
        for (const auto& E : *m_truth_selection->GetTruthPreElectrons()) {
            dec_passLMR(*E) = acc_passOR(*E);
            if (acc_passOR(*E)) Lep.push_back(E);
        }
        for (const auto& M : *m_truth_selection->GetTruthPreMuons()) {
            dec_passLMR(*M) = acc_passOR(*M);
            if (acc_passOR(*M)) Lep.push_back(M);
        }
        ATH_CHECK(RemoveLowMassLeptons(&Lep, OppositeSign, 4.e3));
        ATH_CHECK(RemoveLowMassLeptons(m_truth_selection->GetTruthPreElectrons(), IsSFOS, 10400, 8400));
        ATH_CHECK(RemoveLowMassLeptons(m_truth_selection->GetTruthPreMuons(), IsSFOS, 10400, 8400));
        return StatusCode::SUCCESS;
    }
    StatusCode SUSY4LeptonTruthAnalysisHelper::initializeEventVariables() {
        ATH_CHECK(initializeParticleStores());
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("LLE_LSP1"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("LLE_LSP2"));

        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("ZVeto"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("Base_ZVeto"));

        ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("Ht_Jet"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("Ht_Lep"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("Meff"));

        // Calculate the Mt (min/max) considering light leptons only
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("Mt_ElMuMin"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("Mt_ElMuMax"));
        // Including taus. Might be different due to the exta neutrino from the tau decay
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("Mt_LepMin"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("Mt_LepMax"));

        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("n_PreElectrons"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("n_PreMuons"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("n_PreJets"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("n_PreLeptons"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("n_PreTaus"));

        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("n_BaselineElectrons"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("n_BaselineMuons"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("n_BaselineJets"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("n_BaselineLeptons"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("n_BaselineTaus"));

        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("n_SignalElectrons"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("n_SignalMuons"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("n_SignalJets"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("n_SignalLeptons"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("n_SignalTaus"));

        ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("Signal_Mll_Z1"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("Signal_Mll_Z2"));

        ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("Base_Mll_Z1"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("Base_Mll_Z2"));

        return StatusCode::SUCCESS;
    }
    StatusCode SUSY4LeptonTruthAnalysisHelper::ComputeEventVariables() {
        static XAMPP::Storage<float>* dec_HtLep = m_XAMPPInfo->GetVariableStorage<float>("Ht_Lep");
        static XAMPP::Storage<float>* dec_HtJet = m_XAMPPInfo->GetVariableStorage<float>("Ht_Jet");
        static XAMPP::Storage<float>* dec_Meff = m_XAMPPInfo->GetVariableStorage<float>("Meff");
        static XAMPP::Storage<xAOD::MissingET*>* dec_MET = m_XAMPPInfo->GetVariableStorage<xAOD::MissingET*>("TruthMET");
        static XAMPP::Storage<float>* dec_MtLepMin = m_XAMPPInfo->GetVariableStorage<float>("Mt_LepMin");
        static XAMPP::Storage<float>* dec_MtLepMax = m_XAMPPInfo->GetVariableStorage<float>("Mt_LepMax");
        static XAMPP::Storage<float>* dec_MtLightLepMin = m_XAMPPInfo->GetVariableStorage<float>("Mt_ElMuMin");
        static XAMPP::Storage<float>* dec_MtLightLepMax = m_XAMPPInfo->GetVariableStorage<float>("Mt_ElMuMax");
        static XAMPP::Storage<int>* dec_LLE1 = m_XAMPPInfo->GetVariableStorage<int>("LLE_LSP1");
        static XAMPP::Storage<int>* dec_LLE2 = m_XAMPPInfo->GetVariableStorage<int>("LLE_LSP2");

        static XAMPP::Storage<int>* dec_NPreLeptons = m_XAMPPInfo->GetVariableStorage<int>("n_PreLeptons");
        static XAMPP::Storage<int>* dec_NPreElectrons = m_XAMPPInfo->GetVariableStorage<int>("n_PreElectrons");
        static XAMPP::Storage<int>* dec_NPreMuons = m_XAMPPInfo->GetVariableStorage<int>("n_PreMuons");
        static XAMPP::Storage<int>* dec_NPreJets = m_XAMPPInfo->GetVariableStorage<int>("n_PreJets");
        static XAMPP::Storage<int>* dec_NPreTaus = m_XAMPPInfo->GetVariableStorage<int>("n_PreTaus");

        static XAMPP::Storage<int>* dec_NBaselineLeptons = m_XAMPPInfo->GetVariableStorage<int>("n_BaselineLeptons");
        static XAMPP::Storage<int>* dec_NBaselineElectrons = m_XAMPPInfo->GetVariableStorage<int>("n_BaselineElectrons");
        static XAMPP::Storage<int>* dec_NBaselineMuons = m_XAMPPInfo->GetVariableStorage<int>("n_BaselineMuons");
        static XAMPP::Storage<int>* dec_NBaselineJets = m_XAMPPInfo->GetVariableStorage<int>("n_BaselineJets");
        static XAMPP::Storage<int>* dec_NBaselineTaus = m_XAMPPInfo->GetVariableStorage<int>("n_BaselineTaus");

        static XAMPP::Storage<int>* dec_NSignalLeptons = m_XAMPPInfo->GetVariableStorage<int>("n_SignalLeptons");
        static XAMPP::Storage<int>* dec_NSignalElectrons = m_XAMPPInfo->GetVariableStorage<int>("n_SignalElectrons");
        static XAMPP::Storage<int>* dec_NSignalMuons = m_XAMPPInfo->GetVariableStorage<int>("n_SignalMuons");
        static XAMPP::Storage<int>* dec_NSignalJets = m_XAMPPInfo->GetVariableStorage<int>("n_SignalJets");
        static XAMPP::Storage<int>* dec_NSignalTaus = m_XAMPPInfo->GetVariableStorage<int>("n_SignalTaus");

        float Ht_Lep = XAMPP::CalculateHt(m_truth_selection->GetTruthSignalElectrons()) +
                       XAMPP::CalculateHt(m_truth_selection->GetTruthSignalMuons()) +
                       XAMPP::CalculateHt(m_truth_selection->GetTruthSignalTaus());
        float Ht_Jet = XAMPP::CalculateHt(m_truth_selection->GetTruthSignalJets(), m_JetHt_minPt);
        float Meff = Ht_Lep + Ht_Jet + dec_MET->GetValue()->met();

        int N_PreElectrons = m_truth_selection->GetTruthPreElectrons()->size();
        int N_PreMuons = m_truth_selection->GetTruthPreMuons()->size();
        int N_PreLeptons = N_PreElectrons + N_PreMuons;
        int N_PreJets = m_truth_selection->GetTruthPreJets()->size();
        int N_PreTaus = m_truth_selection->GetTruthPreTaus()->size();

        ATH_CHECK(dec_NPreLeptons->Store(N_PreLeptons));
        ATH_CHECK(dec_NPreElectrons->Store(N_PreElectrons));
        ATH_CHECK(dec_NPreMuons->Store(N_PreMuons));
        ATH_CHECK(dec_NPreJets->Store(N_PreJets));
        ATH_CHECK(dec_NPreTaus->Store(N_PreTaus));

        int N_BaselineElectrons = m_truth_selection->GetTruthBaselineElectrons()->size();
        int N_BaselineMuons = m_truth_selection->GetTruthBaselineMuons()->size();
        int N_BaselineLeptons = N_BaselineElectrons + N_BaselineMuons;
        int N_BaselineJets = m_truth_selection->GetTruthBaselineJets()->size();
        int N_BaselineTaus = m_truth_selection->GetTruthBaselineTaus()->size();

        ATH_CHECK(dec_NBaselineLeptons->Store(N_BaselineLeptons));
        ATH_CHECK(dec_NBaselineElectrons->Store(N_BaselineElectrons));
        ATH_CHECK(dec_NBaselineMuons->Store(N_BaselineMuons));
        ATH_CHECK(dec_NBaselineJets->Store(N_BaselineJets));
        ATH_CHECK(dec_NBaselineTaus->Store(N_BaselineTaus));

        int N_SignalElectrons = m_truth_selection->GetTruthSignalElectrons()->size();
        int N_SignalMuons = m_truth_selection->GetTruthSignalMuons()->size();
        int N_SignalLeptons = N_SignalElectrons + N_SignalMuons;
        int N_SignalJets = m_truth_selection->GetTruthSignalJets()->size();
        int N_SignalTaus = m_truth_selection->GetTruthSignalTaus()->size();

        ATH_CHECK(dec_NSignalLeptons->Store(N_SignalLeptons));
        ATH_CHECK(dec_NSignalElectrons->Store(N_SignalElectrons));
        ATH_CHECK(dec_NSignalMuons->Store(N_SignalMuons));
        ATH_CHECK(dec_NSignalJets->Store(N_SignalJets));
        ATH_CHECK(dec_NSignalTaus->Store(N_SignalTaus));

        int LLE1(-1), LLE2(-1);

        static SG::AuxElement::Decorator<IntVector> dec_children("children");
        static FloatDecorator Cos("CosTheta");
        static FloatDecorator dec_Darlitz12("Darlitz_12");
        static FloatDecorator dec_Darlitz13("Darlitz_13");
        static FloatDecorator dec_Darlitz23("Darlitz_23");
        static FloatDecorator dec_DecayTime("TauDecay");
        static FloatDecorator dec_DecayLen("DistDecay");
        for (const auto& Part : *m_truth_selection->GetTruthPrimaryParticles()) {
            const xAOD::TruthParticle* LastChain = GetLastChainLink(Part);
            if (!IsSame(LastChain, Part)) continue;

            float CosTheta(-2.), DarlitzMass12(-1.e12), DarlitzMass13(-1.e12), DarlitzMass23(-1.e12);
            float DecayTime(-1.e3), DecayLength(-1.e3);
            if (Part->absPdgId() == 1000022) {
                if (LLE1 == -1) {
                    LLE1 = XAMPP::GetLLE_RPVCoupling(Part);
                } else if (LLE2 == -1) {
                    LLE2 = XAMPP::GetLLE_RPVCoupling(Part);
                } else {
                    ATH_MSG_WARNING("There are more than two LSPs in the event");
                }
                const xAOD::TruthParticle* Neutrino = nullptr;
                const xAOD::TruthParticle* PosLep = nullptr;
                const xAOD::TruthParticle* NegLep = nullptr;
                for (size_t c = 0; c < LastChain->nChildren(); ++c) {
                    if (LastChain->child(c)->isNeutrino())
                        Neutrino = Part->child(c);
                    else if (LastChain->child(c)->charge() > 0.)
                        PosLep = Part->child(c);
                    else
                        NegLep = LastChain->child(c);
                }
                if (Neutrino) {
                    TLorentzVector Neut = Neutrino->p4();
                    Neut.Boost(-GetRestBoost(Part));
                    CosTheta = TMath::Cos(Neut.Angle(GetRestBoost(Part)));
                }
                if (Neutrino && PosLep) DarlitzMass12 = TMath::Power(InvariantMass(Neutrino, PosLep), 2);
                if (Neutrino && NegLep) DarlitzMass13 = TMath::Power(InvariantMass(Neutrino, NegLep), 2);
                if (PosLep && NegLep) DarlitzMass23 = TMath::Power(InvariantMass(PosLep, NegLep), 2);
            } else /*if (isSparticle(Part))*/ {
                // Get cos theta Star between the NLSP and the LSP
                for (size_t c = 0; c < LastChain->nChildren(); ++c) {
                    const xAOD::TruthParticle* Child = LastChain->child(c);
                    if (isSparticle(Child)) {
                        TLorentzVector Neut = Child->p4();
                        Neut.Boost(-GetRestBoost(Part));
                        CosTheta = TMath::Cos(Neut.Angle(GetRestBoost(Part)));
                    }
                }
                // Fill the Darlitz plots
                if (Part->nChildren() >= 2) DarlitzMass12 = TMath::Power(InvariantMass(Part->child(0), Part->child(1)), 2);
                if (Part->nChildren() >= 3) {
                    DarlitzMass13 = TMath::Power(InvariantMass(Part->child(0), Part->child(2)), 2);
                    DarlitzMass23 = TMath::Power(InvariantMass(Part->child(1), Part->child(2)), 2);
                }
            }
            dec_children(*Part) = IntVector();
            for (size_t c = 0; c < LastChain->nChildren(); ++c) {
                int Id = LastChain->child(c)->pdgId();
                dec_children(*Part).push_back(Id);
            }
            if (Part->hasProdVtx() && Part->hasDecayVtx()) {
                TLorentzVector V = Part->prodVtx()->v4() - Part->decayVtx()->v4();
                DecayTime = TMath::Abs(V.T());
                DecayLength = TMath::Sqrt(V.X() * V.X() + V.Y() * V.Y() + V.Z() * V.Z());
            }

            Cos(*Part) = CosTheta;
            dec_Darlitz12(*Part) = DarlitzMass12;
            dec_Darlitz13(*Part) = DarlitzMass13;
            dec_Darlitz23(*Part) = DarlitzMass23;
            dec_DecayTime(*Part) = DecayTime;
            dec_DecayLen(*Part) = DecayLength;
        }

        xAOD::IParticleContainer Lep(SG::VIEW_ELEMENTS);
        for (const auto E : *m_truth_selection->GetTruthBaselineElectrons()) Lep.push_back(E);
        for (const auto M : *m_truth_selection->GetTruthBaselineMuons()) Lep.push_back(M);
        if (m_useNuForInvP4) {
            for (const auto N : *m_truth_selection->GetTruthNeutrinos()) {
                if (isParticleFromHardProcess(N)) Lep.push_back(N);
            }
        }
        ATH_MSG_DEBUG("Calculate the Mt separately for each object");
        CalculateMt(m_truth_selection->GetTruthBaselineElectrons(), dec_MET);
        CalculateMt(m_truth_selection->GetTruthBaselineMuons(), dec_MET);
        CalculateMt(m_truth_selection->GetTruthPreTaus(), dec_MET);

        float Light_MtMin(1.e16), Lep_MtMin(1.e16), Light_MtMax(-1.), Lep_MtMax(-1.);
        static FloatAccessor acc_MT("MT");

        for (const auto& L : Lep) {
            if (!m_truth_selection->GetTruthDecorations()->passSignal(*L)) continue;
            if (L->type() != xAOD::Type::ObjectType::Tau) {
                if (Light_MtMin > acc_MT(*L)) Light_MtMin = acc_MT(*L);
                if (Light_MtMax < acc_MT(*L)) Light_MtMax = acc_MT(*L);
            }
            if (Lep_MtMin > acc_MT(*L)) Lep_MtMin = acc_MT(*L);
            if (Lep_MtMax < acc_MT(*L)) Lep_MtMax = acc_MT(*L);
        }
        ATH_CHECK(dec_MtLightLepMin->Store(Light_MtMin));
        ATH_CHECK(dec_MtLightLepMax->Store(Light_MtMax));
        ATH_CHECK(dec_MtLepMin->Store(Lep_MtMin));
        ATH_CHECK(dec_MtLepMax->Store(Lep_MtMax));
        ATH_CHECK(PassZVeto(Lep));
        ATH_CHECK(dec_LLE1->Store(LLE1));
        ATH_CHECK(dec_LLE2->Store(LLE2));
        ATH_CHECK(dec_HtLep->Store(Ht_Lep));
        ATH_CHECK(dec_HtJet->Store(Ht_Jet));
        ATH_CHECK(dec_Meff->Store(Meff));

        static XAMPP::ParticleStorage* EleStore = m_XAMPPInfo->GetParticleStorage("elec");
        static XAMPP::ParticleStorage* MuoStore = m_XAMPPInfo->GetParticleStorage("muon");
        static XAMPP::ParticleStorage* TauStore = m_XAMPPInfo->GetParticleStorage("taus");
        static XAMPP::ParticleStorage* JetStore = m_XAMPPInfo->GetParticleStorage("jet");
        static XAMPP::ParticleStorage* InitStore = m_XAMPPInfo->GetParticleStorage("InitialPart");
        static XAMPP::ParticleStorage* RecoCandStore = m_XAMPPInfo->GetParticleStorage("RecoCandidates");

        ATH_CHECK(EleStore->Fill(m_truth_selection->GetTruthPreElectrons()));
        ATH_CHECK(MuoStore->Fill(m_truth_selection->GetTruthPreMuons()));
        ATH_CHECK(TauStore->Fill(m_truth_selection->GetTruthBaselineTaus()));
        ATH_CHECK(JetStore->Fill(m_truth_selection->GetTruthSignalJets()));
        ATH_CHECK(InitStore->Fill(m_truth_selection->GetTruthPrimaryParticles()));
        ATH_CHECK(RecoCandStore->Fill(m_ParticleConstructor->GetSubContainer("InvariantMomenta")));

        if (m_systematics->ProcessObject(XAMPP::SelectionObject::Photon)) {
            static XAMPP::ParticleStorage* PhotonStore = m_XAMPPInfo->GetParticleStorage("phot");
            ATH_CHECK(PhotonStore->Fill(m_truth_selection->GetTruthBaselinePhotons()));
        }
        return StatusCode::SUCCESS;
    }
    StatusCode SUSY4LeptonTruthAnalysisHelper::initializeParticleStores() {
        ATH_CHECK(m_XAMPPInfo->BookParticleStorage("elec", true));
        ATH_CHECK(m_XAMPPInfo->BookParticleStorage("muon", true));
        ATH_CHECK(m_XAMPPInfo->BookParticleStorage("jet", true));
        ATH_CHECK(m_XAMPPInfo->BookParticleStorage("taus", true));
        ATH_CHECK(m_XAMPPInfo->BookParticleStorage("InitialPart", true));
        ATH_CHECK(m_XAMPPInfo->BookParticleStorage("RecoCandidates", true));

        {
            StringVector FloatVars{"charge"};
            StringVector CharVars{"passLMR", "signal"};
            StringVector IntVars{"truthType", "truthOrigin"};

            XAMPP::ParticleStorage* EleStore = m_XAMPPInfo->GetParticleStorage("elec");
            XAMPP::ParticleStorage* MuoStore = m_XAMPPInfo->GetParticleStorage("muon");

            ATH_CHECK(EleStore->SaveVariable<float>(FloatVars));
            ATH_CHECK(MuoStore->SaveVariable<float>(FloatVars));

            ATH_CHECK(EleStore->SaveVariable<char>(CharVars));
            ATH_CHECK(MuoStore->SaveVariable<char>(CharVars));

            ATH_CHECK(EleStore->SaveVariable<int>(IntVars));
            ATH_CHECK(MuoStore->SaveVariable<int>(IntVars));
        }
        {
            XAMPP::ParticleStorage* JetStore = m_XAMPPInfo->GetParticleStorage("jet");
            ATH_CHECK(JetStore->SaveInteger("PartonTruthLabelID"));
        }
        {
            XAMPP::ParticleStorage* TauStore = m_XAMPPInfo->GetParticleStorage("taus");
            StringVector FloatVars{"charge", "eta_orig", "phi_orig", "pt_orig", "m_orig"};
            StringVector IntVars{"truthType", "truthOrigin"};
            StringVector SizeVars{"numCharged", "numNeutral"};
            StringVector CharVars{"signal"};
            ATH_CHECK(TauStore->SaveVariable<int>(IntVars));
            ATH_CHECK(TauStore->SaveVariable<size_t>(SizeVars));
            ATH_CHECK(TauStore->SaveVariable<float>(FloatVars));
            ATH_CHECK(TauStore->SaveVariable<char>(CharVars));
        }
        {
            XAMPP::ParticleStorage* RecoCandStore = m_XAMPPInfo->GetParticleStorage("RecoCandidates");
            StringVector IntVars{"pdgId", "nConstituents" /* , "Index"*/};
            StringVector CharVars{"SameFlavour", "signal"};
            StringVector FloatVars{"charge"};
            ATH_CHECK(RecoCandStore->SaveVariable<int>(IntVars));
            ATH_CHECK(RecoCandStore->SaveVariable<char>(CharVars));
            ATH_CHECK(RecoCandStore->SaveVariable<float>(FloatVars));
        }
        {
            XAMPP::ParticleStorage* InitStore = m_XAMPPInfo->GetParticleStorage("InitialPart");
            StringVector FloatVars{"CosTheta", "charge", "Darlitz_12", "Darlitz_23", "Darlitz_13", "TauDecay", "DistDecay"};
            StringVector IntVars{"pdgId"};
            ATH_CHECK(InitStore->SaveVariable<int>(IntVars));
            ATH_CHECK(InitStore->SaveVariable<float>(FloatVars));
            ATH_CHECK(InitStore->SaveVariable<IntVector>("children"));
        }
        if (m_systematics->ProcessObject(XAMPP::SelectionObject::Photon)) {
            ATH_CHECK(m_XAMPPInfo->BookParticleStorage("phot"));
            XAMPP::ParticleStorage* photons = m_XAMPPInfo->GetParticleStorage("phot");
            StringVector IntVars{"truthType", "truthOrigin"};

            ATH_CHECK(photons->SaveVariable<int>(IntVars));
        }

        return StatusCode::SUCCESS;
    }
    StatusCode SUSY4LeptonTruthAnalysisHelper::PassZVeto(xAOD::IParticleContainer& Leptons) {
        static XAMPP::Storage<int>* dec_ZVeto = m_XAMPPInfo->GetVariableStorage<int>("ZVeto");
        static XAMPP::Storage<int>* dec_BaseZVeto = m_XAMPPInfo->GetVariableStorage<int>("Base_ZVeto");

        static XAMPP::Storage<float>* dec_Mll_Z1 = m_XAMPPInfo->GetVariableStorage<float>("Signal_Mll_Z1");
        static XAMPP::Storage<float>* dec_Mll_Z2 = m_XAMPPInfo->GetVariableStorage<float>("Signal_Mll_Z2");

        static XAMPP::Storage<float>* dec_Base_Mll_Z1 = m_XAMPPInfo->GetVariableStorage<float>("Base_Mll_Z1");
        static XAMPP::Storage<float>* dec_Base_Mll_Z2 = m_XAMPPInfo->GetVariableStorage<float>("Base_Mll_Z2");

        int ZVeto(XAMPP::ZVeto::Pass), Base_ZVeto(XAMPP::ZVeto::Pass);
        const xAOD::Particle* Mll_Z1 = nullptr;
        const xAOD::Particle* Mll_Z2 = nullptr;
        const xAOD::Particle* Base_Mll_Z1 = nullptr;
        const xAOD::Particle* Base_Mll_Z2 = nullptr;

        // Obtain the two and four lepton invariant masses, if there any
        ATH_MSG_DEBUG("Calculate invariant lepton momenta out of " << Leptons.size() << " particles");

        xAOD::IParticleContainer Signals(SG::VIEW_ELEMENTS);
        for (const auto L : Leptons) {
            if (m_truth_selection->GetTruthDecorations()->passSignal(*L)) { Signals.push_back(L); }
        }
        int NBaseLep = Leptons.size();
        int NSignalLep = Signals.size();
        Base_ZVeto = GetZVeto(Leptons, m_ZMassWindow);
        // If nothing has changed between baseline and signal why evaluate the thing again
        ZVeto = NBaseLep == NSignalLep ? Base_ZVeto : GetZVeto(Signals, m_ZMassWindow);

        ATH_CHECK(m_ParticleConstructor->CreateSubContainer("InvariantMomenta"));
        ConstructInvariantMomenta(Leptons, m_ParticleConstructor.get(), [this](const xAOD::IParticle* P) -> bool {
            return m_truth_selection->GetTruthDecorations()->passSignal(*P);
        });
        m_ParticleConstructor->DetachSubContainer();

        float BestDiZmass(1.e16), BestDiZmassBase(1.e16);
        xAOD::ParticleContainer* InvLepMom = m_ParticleConstructor->GetSubContainer("InvariantMomenta");
        ATH_MSG_DEBUG("Constructed in total " << InvLepMom->size() << " particles");

        xAOD::ParticleContainer::const_iterator Itr_Begin = InvLepMom->begin();
        xAOD::ParticleContainer::const_iterator Itr_End = InvLepMom->end();
        for (xAOD::ParticleContainer::const_iterator Itr = Itr_Begin; Itr != Itr_End; ++Itr) {
            const xAOD::Particle* ZCand = (*Itr);
            if (!isZCandidate(ZCand)) { continue; }
            float DeltaM_Z = fabs(ZCand->m() - XAMPP::Z_MASS);
            // Impossible to build  up two Z candidates
            if (NBaseLep < 4) {
                if (!Base_Mll_Z1 || fabs(Base_Mll_Z1->m() - Z_MASS) > DeltaM_Z) { Base_Mll_Z1 = ZCand; }
                if (IsSignalLepton(ZCand) && (!Mll_Z1 || fabs(Mll_Z1->m() - XAMPP::Z_MASS) > DeltaM_Z)) { Mll_Z1 = ZCand; }
                continue;
            }
            // Check it for the signal leptons as well
            else if (NSignalLep < 4 && IsSignalLepton(ZCand) && (!Mll_Z1 || fabs(Mll_Z1->m() - XAMPP::Z_MASS) > DeltaM_Z)) {
                Mll_Z1 = ZCand;
            }
            // Check if we can find another pair
            bool FoundSecondPair = false;
            // ZVeto is determined -> Go to the ZZ seleciton
            for (xAOD::ParticleContainer::const_iterator Itr1 = Itr_Begin; Itr1 != Itr; ++Itr1) {
                const xAOD::Particle* ZCand1 = (*Itr1);
                // Reject everything uneeded
                if (!isZCandidate(ZCand1) || HasCommonConstructingParticles(ZCand, ZCand1)) { continue; }
                FoundSecondPair = true;
                float DeltaM_Z2 = fabs(ZCand1->m() - XAMPP::Z_MASS);
                if (DeltaM_Z + DeltaM_Z2 < BestDiZmassBase) {
                    Base_Mll_Z1 = DeltaM_Z <= DeltaM_Z2 ? ZCand : ZCand1;
                    Base_Mll_Z2 = DeltaM_Z > DeltaM_Z2 ? ZCand : ZCand1;
                    BestDiZmassBase = DeltaM_Z + DeltaM_Z2;
                }
                if (IsSignalLepton(ZCand) && IsSignalLepton(ZCand1) && (DeltaM_Z + DeltaM_Z2) < BestDiZmass) {
                    Mll_Z1 = DeltaM_Z <= DeltaM_Z2 ? ZCand : ZCand1;
                    Mll_Z2 = DeltaM_Z > DeltaM_Z2 ? ZCand : ZCand1;
                    BestDiZmass = DeltaM_Z + DeltaM_Z2;
                }
            }
            if (!FoundSecondPair) {
                if (!Base_Mll_Z1 || DeltaM_Z < std::abs(Base_Mll_Z1->m() - XAMPP::Z_MASS)) { Base_Mll_Z1 = ZCand; }
                if (IsSignalLepton(ZCand) && (!Mll_Z1 || DeltaM_Z < std::abs(Mll_Z1->m() - XAMPP::Z_MASS))) { Mll_Z1 = ZCand; }
            }
        }
        ATH_CHECK(dec_ZVeto->Store(ZVeto));
        ATH_CHECK(dec_BaseZVeto->Store(Base_ZVeto));

        ATH_CHECK(dec_Mll_Z1->Store(Mll_Z1 ? Mll_Z1->m() : -1.e3));
        ATH_CHECK(dec_Mll_Z2->Store(Mll_Z2 ? Mll_Z2->m() : -1.e3));

        ATH_CHECK(dec_Base_Mll_Z1->Store(Base_Mll_Z1 ? Base_Mll_Z1->m() : -1.e3));
        ATH_CHECK(dec_Base_Mll_Z2->Store(Base_Mll_Z2 ? Base_Mll_Z2->m() : -1.e3));

        return StatusCode::SUCCESS;
    }

    bool SUSY4LeptonTruthAnalysisHelper::IsSignalLepton(const xAOD::IParticle* P) const {
        if (P->type() == xAOD::Type::ObjectType::TruthParticle || P->type() == xAOD::Type::ObjectType::Jet) {
            return m_truth_selection->GetTruthDecorations()->passSignal(*P);
        }
        static CharAccessor acc_signal("signal");
        if (acc_signal.isAvailable(*P)) return acc_signal(*P);
        return false;
    }

}  // namespace XAMPP
