#ifndef XAMPPmultilep_LINKDEF_H
#define XAMPPmultilep_LINKDEF_H

#include <XAMPPmultilep/SUSY4LeptonAnalysisHelper.h>
#include <XAMPPmultilep/SUSY4LeptonTruthAnalysisHelper.h>
#include <XAMPPmultilep/SUSY4LeptonAnalysisConfig.h>

#include <XAMPPmultilep/SUSYllqqAnalysisHelper.h>
#include <XAMPPmultilep/SUSYllqqAnalysisConfig.h>

#include <XAMPPmultilep/SUSY4LeptonScaleFactorAnalysisHelper.h>
#include <XAMPPmultilep/SUSY4LeptonScaleFactorAnalysisConfig.h>

#include <XAMPPmultilep/SUSYttYAnalysisHelper.h>
#include <XAMPPmultilep/SUSYttYAnalysisConfig.h>


#include <XAMPPmultilep/SUSY4LeptonElectronSelector.h>
#include <XAMPPmultilep/SUSY4LeptonMuonSelector.h>
#include <XAMPPmultilep/SUSY4LeptonTauSelector.h>
#include <XAMPPmultilep/SUSY4LeptonDiTauSelector.h>
#include <XAMPPmultilep/SUSY4LeptonTruthSelector.h>


#include <XAMPPmultilep/SUSYttYPhotonSelector.h>

#include <XAMPPmultilep/SUSYllqqJetSelector.h>
#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

#endif

#ifdef __CINT__
#pragma link C++ class XAMPP::SUSY4LeptonAnalysisHelper;
#pragma link C++ class XAMPP::SUSY4LeptonAnalysisConfig;

#pragma link C++ class XAMPP::SUSY4LepTruthAnalysisConfig;
#pragma link C++ class XAMPP::SUSY4LeptonTruthAnalysisHelper;

#pragma link C++ class XAMPP::SUSYllqqAnalysisConfig;
#pragma link C++ class XAMPP::SUSYllqqAnalysisHelper;

#pragma link C++ class XAMPP::SUSY4LeptonScaleFactorAnalysisConfig;
#pragma link C++ class XAMPP::SUSY4LeptonScaleFactorAnalysisHelper;

#pragma link C++ class XAMPP::SUSYttYAnalysisConfig;
#pragma link C++ class XAMPP::SUSYttYAnalysisHelper;

#pragma link C++ class XAMPP::SUSY4LeptonElectronSelector;
#pragma link C++ class XAMPP::SUSY4LeptonMuonSelector;
#pragma link C++ class XAMPP::SUSY4LeptonTauSelector;
#pragma link C++ class XAMPP::SUSY4LeptonDiTauSelector;
#pragma link C++ class XAMPP::SUSY4LeptonTruthSelector;
#pragma link C++ class XAMPP::SUSYttYPhotonSelector;
#pragma link C++ class XAMPP::SUSYllqqJetSelector;

#endif

#endif
