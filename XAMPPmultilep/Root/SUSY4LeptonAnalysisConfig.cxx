#include <XAMPPbase/AnalysisUtils.h>
#include <XAMPPmultilep/SUSY4LeptonAnalysisConfig.h>

namespace XAMPP {
    SUSY4LeptonAnalysisConfig::SUSY4LeptonAnalysisConfig(const std::string& Analysis) :
        AnalysisConfig(Analysis), m_SkimEvents(true), m_TriggerSkimming(true) {
        declareProperty("ApplySkimming", m_SkimEvents);
        declareProperty("ApplyTriggerSkimming", m_TriggerSkimming);
    }

    StatusCode SUSY4LeptonAnalysisConfig::initializeCustomCuts() {
        if (!m_TriggerSkimming) ATH_MSG_INFO("Disable the skimming of events failing the trigger");
        if (!m_SkimEvents) ATH_MSG_INFO("Disable event skimming entirely");
        CutFlow FourLepCutFlow("4L0T");  // Lets give our cutflow a proper name.. Be creative

        Cut* TrigCut = NewCut("TrigMatching", Cut::CutType::CutChar, m_SkimEvents && m_TriggerSkimming);
        if (!TrigCut->initialize("TrigMatching", "=1")) return StatusCode::FAILURE;
        m_XAMPPInfo->GetVariableStorage<char>("TrigMatching")->SetSaveTrees(!(m_SkimEvents && m_TriggerSkimming));
        // FourLepCutFlow.push_back(TrigCut);

        Cut* OneLep = NewCut("N_{e/#mu}>=1", Cut::CutType::CutInt, m_SkimEvents);
        if (!OneLep->initialize("n_SignalLep", ">=1")) return StatusCode::FAILURE;
        FourLepCutFlow.push_back(OneLep);

        Cut* TwoLep = NewCut("N_{e/#mu}>=2", Cut::CutType::CutInt, m_SkimEvents);
        if (!TwoLep->initialize("n_SignalLep", ">=2")) return StatusCode::FAILURE;
        FourLepCutFlow.push_back(TwoLep);

        // This should  reduce our ntuple writing
        Cut* DumpCut = NewCut("N_{e/#mu}^{Baseline}>=3", Cut::CutType::CutInt, m_SkimEvents);
        if (!DumpCut->initialize("n_BaseLep", ">=3")) return StatusCode::FAILURE;
        FourLepCutFlow.push_back(DumpCut->combine(TwoLep, Cut::Combine::AND));

        Cut* ThreeLep = NewCut("N_{e/#mu}>=3", Cut::CutType::CutInt, false);
        if (!ThreeLep->initialize("n_SignalLep", ">=3")) return StatusCode::FAILURE;
        FourLepCutFlow.push_back(ThreeLep);

        // A majority of the events in the n-tuples contains 3 baseline leptons. Making the n-tuples too large
        Cut* TightDump = NewCut("N_{e/#mu}^{Baseline} >=4", Cut::CutType::CutInt, m_SkimEvents);
        if (!TightDump->initialize("n_BaseLep", ">=4")) return StatusCode::FAILURE;
        FourLepCutFlow.push_back(TightDump);

        // Skim systematic trees with 4 signal leptons only
        Cut* SystSkim = NewCut("isNominal", Cut::CutType::CutChar, m_SkimEvents);
        if (!SystSkim->initialize("isNominal", "=1")) return StatusCode::FAILURE;

        Cut* FourLep = NewCut("N_{e/#mu}>=4", Cut::CutType::CutInt, m_SkimEvents);
        if (!FourLep->initialize("n_SignalLep", ">=4")) return StatusCode::FAILURE;
        FourLepCutFlow.push_back(FourLep->combine(SystSkim, Cut::Combine::OR));

        FourLep = NewCut("N_{e/#mu}>=4", Cut::CutType::CutInt, false);
        if (!FourLep->initialize("n_SignalLep", ">=4")) return StatusCode::FAILURE;
        FourLepCutFlow.push_back(FourLep);

        Cut* ZCut = NewCut("#slash{Z}", Cut::CutType::CutInt, false);
        if (!ZCut->initialize(m_XAMPPInfo->GetVariableStorage<int>("ZVeto"), ZVeto::Pass, Cut::Relation::Equal)) return StatusCode::FAILURE;
        FourLepCutFlow.push_back(ZCut);
        Cut* Meff600 = NewCut("m_{eff}>600", Cut::CutType::CutFloat, false);
        if (!Meff600->initialize("Signal_Meff", ">=600000.")) return StatusCode::FAILURE;
        FourLepCutFlow.push_back(Meff600);

        Cut* Meff900 = NewCut("m_{eff}>900", Cut::CutType::CutFloat, false);
        if (!Meff900->initialize("Signal_Meff", ">=900000.")) return StatusCode::FAILURE;
        FourLepCutFlow.push_back(Meff900);

        Cut* Meff12000 = NewCut("m_{eff}>12000", Cut::CutType::CutFloat, false);
        if (!Meff12000->initialize("Signal_Meff", ">=12000000.")) return StatusCode::FAILURE;
        FourLepCutFlow.push_back(Meff12000);
        ATH_CHECK(AddToCutFlows(FourLepCutFlow));

        CutFlow ThreeLepOneTau("3L1T");
        // Take care of the fake estimate in the 3L1T regions
        // CR2: LLlt LTll
        // CR1: LLLt LLTl
        //  ==> N-tuple dump
        //   (N_{e/mu}^{signal} >= 1 && N_{tau}^{signal} >= 1) && N_{e/mu}^{baseline} == 3
        //   (N_{e/mu}^{signal} >= 2 && N_{tau}^{baseline} >= 1) && N_{e/mu}^{baseline} == 3
        //
        ThreeLepOneTau.push_back(TrigCut);
        ThreeLepOneTau.push_back(OneLep);

        Cut* TwoLepNoSkim = NewCut("N_{e/#mu}>=2", Cut::CutType::CutInt, false);
        if (!TwoLepNoSkim->initialize("n_SignalLep", ">=2")) return StatusCode::FAILURE;
        ThreeLepOneTau.push_back(TwoLepNoSkim);

        Cut* Exact3BaseLep = NewCut("N_{e/#mu}^{baseline}=3", Cut::CutType::CutInt, m_SkimEvents);
        if (!Exact3BaseLep->initialize("n_BaseLep", "=3")) return StatusCode::FAILURE;
        ThreeLepOneTau.push_back(Exact3BaseLep);

        Cut* ThreeLepEq = NewCut("N_{e/#mu}=3", Cut::CutType::CutInt, m_SkimEvents);
        if (!ThreeLepEq->initialize("n_SignalLep", "=3")) return StatusCode::FAILURE;
        ThreeLepOneTau.push_back(ThreeLepEq->combine(SystSkim, Cut::Combine::OR));

        ThreeLepEq = NewCut("N_{e/#mu}=3", Cut::CutType::CutInt, false);
        if (!ThreeLepEq->initialize("n_SignalLep", "=3")) return StatusCode::FAILURE;
        ThreeLepOneTau.push_back(ThreeLepEq);

        // 1 baseline tau and two signal leptons
        Cut* DumpCutTau = NewCut("N^{baseline}_{#tau}>=1", Cut::CutType::CutInt, m_SkimEvents);
        if (!DumpCutTau->initialize("n_BaseTau", ">=1")) return StatusCode::FAILURE;
        DumpCutTau = DumpCutTau->combine(TwoLep, Cut::Combine::AND);
        // 1 signal tau & 1 signal lepton + 2 baseline leptons
        Cut* DumpCutTau1 = NewCut("N^{signal}_{#tau}>=1", Cut::CutType::CutInt, m_SkimEvents);
        if (!DumpCutTau1->initialize("n_SignalTau", ">=1")) return StatusCode::FAILURE;
        ThreeLepOneTau.push_back(DumpCutTau->combine(DumpCutTau1, Cut::Combine::OR));

        Cut* SigTau = NewCut("N_{#tau}>=1", Cut::CutType::CutInt, false);
        if (!SigTau->initialize("n_SignalTau", ">=1")) return StatusCode::FAILURE;
        ThreeLepOneTau.push_back(SigTau);
        ThreeLepOneTau.push_back(ZCut);
        Cut* Meff500 = NewCut("m_{eff}>500", Cut::CutType::CutFloat, false);
        if (!Meff500->initialize("Signal_Meff", ">=500000.")) return StatusCode::FAILURE;
        ThreeLepOneTau.push_back(Meff500);

        ATH_CHECK(AddToCutFlows(ThreeLepOneTau));

        CutFlow TwoLepTwoTau("2L2T");
        TwoLepTwoTau.push_back(TrigCut);
        TwoLepTwoTau.push_back(OneLep);

        Cut* TwoLepEq = NewCut("N_{e/#mu}=2", Cut::CutType::CutInt, m_SkimEvents);
        if (!TwoLepEq->initialize("n_SignalLep", "=2")) return StatusCode::FAILURE;
        TwoLepTwoTau.push_back(TwoLepEq);

        TwoLepTwoTau.push_back(DumpCutTau);
        TwoLepTwoTau.push_back(SigTau);
        Cut* DumpCutTau2 = NewCut("N^{baseline}_{#tau}>=2", Cut::CutType::CutInt, m_SkimEvents);
        if (!DumpCutTau2->initialize("n_BaseTau", ">=2")) return StatusCode::FAILURE;
        TwoLepTwoTau.push_back(DumpCutTau2);

        Cut* TwoSigTau = NewCut("N_{#tau}>=2", Cut::CutType::CutInt, m_SkimEvents);
        if (!TwoSigTau->initialize("n_SignalTau", ">=2")) return StatusCode::FAILURE;
        TwoLepTwoTau.push_back(TwoSigTau->combine(SystSkim, Cut::Combine::OR));

        TwoSigTau = NewCut("N_{#tau}>=2", Cut::CutType::CutInt, false);
        if (!TwoSigTau->initialize("n_SignalTau", ">=2")) return StatusCode::FAILURE;
        TwoLepTwoTau.push_back(TwoSigTau);

        TwoLepTwoTau.push_back(ZCut);
        TwoLepTwoTau.push_back(Meff500);
        ATH_CHECK(AddToCutFlows(TwoLepTwoTau));

        // Special cutflow to run on data dumping all events with 4 baseline
        // leptons. This
        if (isData()) {
            CutFlow DataFakeNeglected_LLLL("FFNeglected_LLLL");
            // Require one signal tau or one signal lepton
            DataFakeNeglected_LLLL.push_back(TightDump);
            // Cut* Base_Meff600 = NewSkimmingCut("m_{eff}>=600", Cut::CutType::CutFloat);
            // if (!Base_Meff600->initialize("Base_Meff", ">=600000.")) return StatusCode::FAILURE;
            // DataFakeNeglected_LLLL.push_back(Base_Meff600);
            ATH_CHECK(AddToCutFlows(DataFakeNeglected_LLLL));

            CutFlow DataFakeNeglected_TLLL("FFNeglected_TLLL");
            DataFakeNeglected_TLLL.push_back(DumpCut);
            DataFakeNeglected_TLLL.push_back(DumpCutTau);

            // Cut* Base_Meff500 = NewSkimmingCut("m_{eff}>=500", Cut::CutType::CutFloat);
            // if (!Base_Meff500->initialize("Base_Meff", ">=500000.")) return StatusCode::FAILURE;
            // DataFakeNeglected_TLLL.push_back(Base_Meff500);
            ATH_CHECK(AddToCutFlows(DataFakeNeglected_TLLL));

            CutFlow DataFakeNeglected_TTLL("FFNeglected_TTLL");
            Cut* TwoBaseline = NewSkimmingCut("N_{e/#mu}^{base}>=2", Cut::CutType::CutInt);
            if (!TwoBaseline->initialize("n_BaseLep", ">=2")) return StatusCode::FAILURE;
            DataFakeNeglected_TTLL.push_back(TwoBaseline);
            DataFakeNeglected_TTLL.push_back(DumpCutTau2);
            // DataFakeNeglected_TTLL.push_back(Base_Meff500);
            ATH_CHECK(AddToCutFlows(DataFakeNeglected_TTLL));
        }
        return StatusCode::SUCCESS;
    }
    SUSY4LepTruthAnalysisConfig::SUSY4LepTruthAnalysisConfig(const std::string& Analysis) : TruthAnalysisConfig(Analysis), m_skim(true) {
        declareProperty("ApplySkimming", m_skim);
    }

    StatusCode SUSY4LepTruthAnalysisConfig::initializeCustomCuts() {
        // 4L - 0T
        CutFlow FourLepCutFlow0("4L0T");

        Cut* OneLep = NewCut("N_{e/#mu}>=1", Cut::CutType::CutInt, m_skim);
        if (!OneLep->initialize("n_SignalLeptons", ">=1")) return StatusCode::FAILURE;
        FourLepCutFlow0.push_back(OneLep);

        Cut* TwoLep = NewCut("N_{e/#mu}>=2", Cut::CutType::CutInt, m_skim);
        if (!TwoLep->initialize("n_SignalLeptons", ">=2")) return StatusCode::FAILURE;
        FourLepCutFlow0.push_back(TwoLep);

        Cut* ThreeLep = NewCut("N_{e/#mu}>=3", Cut::CutType::CutInt, false);
        if (!ThreeLep->initialize("n_SignalLeptons", ">=3")) return StatusCode::FAILURE;
        FourLepCutFlow0.push_back(ThreeLep);

        Cut* FourLep = NewCut("N_{e/#mu}>=4", Cut::CutType::CutInt, false);
        if (!FourLep->initialize("n_SignalLeptons", ">=4")) return StatusCode::FAILURE;

        FourLepCutFlow0.push_back(FourLep);
        Cut* TauVeto = NewCut("N_{#tau}=0", Cut::CutType::CutInt, false);
        if (!TauVeto->initialize("n_SignalTaus", "==0")) return StatusCode::FAILURE;
        FourLepCutFlow0.push_back(TauVeto);

        Cut* ZCut = NewCut("#slash{Z}", Cut::CutType::CutInt, false);
        if (!ZCut->initialize(m_XAMPPInfo->GetVariableStorage<int>("ZVeto"), ZVeto::Pass, Cut::Relation::Equal)) return StatusCode::FAILURE;
        FourLepCutFlow0.push_back(ZCut);

        Cut* Meff600 = NewCut("m_{eff}>600", Cut::CutType::CutFloat, false);
        if (!Meff600->initialize("Meff", ">=600000.")) return StatusCode::FAILURE;
        FourLepCutFlow0.push_back(Meff600);

        Cut* Meff900 = NewCut("m_{eff}>900", Cut::CutType::CutFloat, false);
        if (!Meff900->initialize("Meff", ">=900000.")) return StatusCode::FAILURE;
        FourLepCutFlow0.push_back(Meff900);

        ATH_CHECK(AddToCutFlows(FourLepCutFlow0));

        // 4L - 1T
        CutFlow FourLepCutFlow1("3L1T");

        FourLepCutFlow1.push_back(OneLep);

        FourLepCutFlow1.push_back(TwoLep);

        FourLepCutFlow1.push_back(ThreeLep);

        Cut* OneTau = NewCut("N_{#tau}>=1", Cut::CutType::CutInt, false);
        if (!OneTau->initialize("n_SignalTaus", ">=1")) return StatusCode::FAILURE;
        FourLepCutFlow1.push_back(OneTau);

        FourLepCutFlow1.push_back(ZCut);

        FourLepCutFlow1.push_back(Meff600);

        FourLepCutFlow1.push_back(Meff900);

        ATH_CHECK(AddToCutFlows(FourLepCutFlow1));

        // 4L - 2T
        CutFlow FourLepCutFlow2("2L2T");  // Lets give our cutflow a proper name.. Be creative

        FourLepCutFlow2.push_back(OneLep);

        FourLepCutFlow2.push_back(TwoLep);

        FourLepCutFlow2.push_back(OneTau);

        Cut* TwoTaus = NewCut("N_{#tau}>=2", Cut::CutType::CutInt, false);
        if (!TwoTaus->initialize("n_SignalTaus", ">=2")) return StatusCode::FAILURE;
        FourLepCutFlow2.push_back(TwoTaus);

        FourLepCutFlow2.push_back(ZCut);

        FourLepCutFlow2.push_back(Meff600);

        FourLepCutFlow2.push_back(Meff900);

        ATH_CHECK(AddToCutFlows(FourLepCutFlow2));
        return StatusCode::SUCCESS;
    }

}  // namespace XAMPP
