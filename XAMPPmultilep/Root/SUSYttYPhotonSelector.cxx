#include <XAMPPmultilep/SUSYttYPhotonSelector.h>

#include <fstream>
#include <iostream>
#include <sstream>
namespace XAMPP {
    SUSYttYPhotonSelector::SUSYttYPhotonSelector(std::string myname) : SUSYPhotonSelector(myname) {}

    SUSYttYPhotonSelector::~SUSYttYPhotonSelector() {}

    StatusCode SUSYttYPhotonSelector::FillPhotons(const CP::SystematicSet& systset) {
        ATH_CHECK(SUSYPhotonSelector::FillPhotons(systset));
        for (const auto& iphot : *GetBaselinePhotons()) { IsMEPhoton(iphot); }
        return StatusCode::SUCCESS;
    }
    void SUSYttYPhotonSelector::IsMEPhoton(xAOD::Photon* Ph) {
        static CharDecorator dec_ME("ME");
        static FloatDecorator dec_MEpt("ME_pt");
        float ME_PT = -1;
        bool IsME = true;
        if (m_XAMPPInfo->isMC()) {
            const xAOD::TruthParticle* TruPh = XAMPP::getTruthMatchedParticle(*Ph);
            if (TruPh) {
                IsME = (TruPh && TruPh->isPhoton() && XAMPP::isParticleFromHardProcess(TruPh));
                ME_PT = TruPh->pt();
            }
        }
        dec_MEpt(*Ph) = ME_PT;
        dec_ME(*Ph) = IsME;
    }
}  // namespace XAMPP
