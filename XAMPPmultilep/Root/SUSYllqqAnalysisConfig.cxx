#include <XAMPPbase/AnalysisUtils.h>
#include <XAMPPmultilep/SUSYllqqAnalysisConfig.h>

namespace XAMPP {
    SUSYllqqAnalysisConfig::SUSYllqqAnalysisConfig(std::string Analysis) : AnalysisConfig(Analysis) {}

    StatusCode SUSYllqqAnalysisConfig::initializeCustomCuts() {
        CutFlow LLqqSelection("LLqq");
        Cut* TrigCut = NewCut("TrigMatching", Cut::CutType::CutChar, true);
        if (!TrigCut->initialize("TrigMatching", "=1")) return StatusCode::FAILURE;
        LLqqSelection.push_back(TrigCut);
        // Select events with 1 or 2  leptons
        Cut* OneLep = NewCut("N_{l}>0", Cut::CutType::CutInt, true);
        if (!OneLep->initialize("n_SignalLep", ">0")) return StatusCode::FAILURE;
        LLqqSelection.push_back(OneLep);

        Cut* TwoBaseLep = NewCut("N_{l}^{baseline}=2", Cut::CutType::CutInt, true);
        if (!TwoBaseLep->initialize("n_BaseLep", "=2")) return StatusCode::FAILURE;
        LLqqSelection.push_back(TwoBaseLep);

        Cut* TwoSigLep = NewCut("N_{l}=2", Cut::CutType::CutInt, true);
        if (!TwoSigLep->initialize("n_SignalLep", "=2")) return StatusCode::FAILURE;
        LLqqSelection.push_back(TwoSigLep);

        Cut* OsCut = NewCut("l^{+}l^{-}", Cut::CutType::CutChar, true);
        if (!OsCut->initialize("OS_Lep", "=1")) return StatusCode::FAILURE;
        LLqqSelection.push_back(OsCut);
        Cut* TwoElecCut = NewCut("N_{e}=2", Cut::CutType::CutInt, true);
        if (!TwoElecCut->initialize("n_SignalElec", "=2")) return StatusCode::FAILURE;
        Cut* TwoMuoCut = NewCut("N_{#mu}=2", Cut::CutType::CutInt, true);
        if (!TwoMuoCut->initialize("n_SignalMuon", "=2")) return StatusCode::FAILURE;
        Cut* SFCut = TwoElecCut->combine(TwoMuoCut, Cut::Combine::OR);
        LLqqSelection.push_back(SFCut);
        Cut* ZCut = NewCut("Z-requirement", Cut::CutType::CutInt, false);
        if (!ZCut->initialize(m_XAMPPInfo->GetVariableStorage<int>("ZVeto"), ZVeto::Pass, Cut::Relation::Greater))
            return StatusCode::FAILURE;
        LLqqSelection.push_back(ZCut);

        ATH_CHECK(AddToCutFlows(LLqqSelection));

        CutFlow LLLnuSelection("LLLnu");
        LLLnuSelection.push_back(TrigCut);
        LLLnuSelection.push_back(OneLep);
        Cut* MinTwoSigLep = NewCut("N_{l}>=2", Cut::CutType::CutInt, true);
        if (!MinTwoSigLep->initialize("n_SignalLep", ">=2")) return StatusCode::FAILURE;

        LLLnuSelection.push_back(MinTwoSigLep);

        Cut* ThreeBaseLep = NewCut("N_{l}^{baseline}=3", Cut::CutType::CutInt, true);
        if (!ThreeBaseLep->initialize("n_BaseLep", "=3")) return StatusCode::FAILURE;
        LLLnuSelection.push_back(ThreeBaseLep);

        Cut* ThreeSigLep = NewCut("N_{l}=3", Cut::CutType::CutInt, true);
        if (!ThreeSigLep->initialize("n_SignalLep", "=3")) return StatusCode::FAILURE;
        LLLnuSelection.push_back(ThreeSigLep);
        LLLnuSelection.push_back(ZCut);
        ATH_CHECK(AddToCutFlows(LLLnuSelection));

        CutFlow LLLLSelection("LLLL");
        LLLLSelection.push_back(TrigCut);
        LLLLSelection.push_back(OneLep);
        LLLLSelection.push_back(MinTwoSigLep);
        Cut* MinThreeSigLep = NewCut("N_{l}>=3", Cut::CutType::CutInt, true);
        if (!MinThreeSigLep->initialize("n_SignalLep", ">=3")) return StatusCode::FAILURE;
        LLLLSelection.push_back(MinThreeSigLep);

        Cut* FourBaseLep = NewCut("N_{l}^{baseline}=4", Cut::CutType::CutInt, true);
        if (!FourBaseLep->initialize("n_BaseLep", "=4")) return StatusCode::FAILURE;
        LLLLSelection.push_back(FourBaseLep);

        Cut* FourSigLep = NewCut("N_{l}=4", Cut::CutType::CutInt, true);
        if (!FourSigLep->initialize("n_SignalLep", "=4")) return StatusCode::FAILURE;
        LLLLSelection.push_back(FourSigLep);
        ATH_CHECK(AddToCutFlows(LLLLSelection));
        return StatusCode::SUCCESS;
    }
}  // namespace XAMPP
