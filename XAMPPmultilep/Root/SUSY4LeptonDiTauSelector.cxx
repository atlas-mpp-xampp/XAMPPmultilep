#include <FourMomUtils/xAODP4Helpers.h>
#include <PathResolver/PathResolver.h>
#include <XAMPPbase/IJetSelector.h>
#include <XAMPPbase/ReconstructedParticles.h>
#include <XAMPPmultilep/SUSY4LeptonDiTauSelector.h>
#include <XAMPPmultilep/SUSY4LeptonTruthSelector.h>
#include <tauRecTools/DiTauDiscriminantTool.h>
#include <tauRecTools/DiTauIDVarCalculator.h>
#include <xAODJet/JetAuxContainer.h>
#include <xAODJet/JetContainer.h>

#include <fstream>
#include <iostream>
#include <sstream>

#ifdef SUSY4LeptonDiTauSelector_H
namespace XAMPP {

    static FloatDecorator dec_BDT("BDT");
    static FloatAccessor acc_BDT("BDT");

    SUSY4LeptonDiTauSelector::SUSY4LeptonDiTauSelector(const std::string& myname) :
        SUSY4LeptonTauSelector(myname),
        m_truth_selection("SUSYTruthSelector"),
        //    m_DiscrTool("DiTauBDTCalculator"),
        //    m_IDVarCalculator("DiTauVarCalc"),
        m_ParticleConstructor("ParticleConstructor") {
        declareProperty("TruthSelector", m_truth_selection);
        m_ParticleConstructor.declarePropertyFor(this, "ParticleConstructor", "The XAMPP particle constuctor");

        //       m_DiscrTool.declarePropertyFor(this, "DiTauBDTCalculator", "The DiTau discriminant tool");
        //       m_IDVarCalculator.declarePropertyFor(this, "DiTauVarCalc", "IdVar calculation tool");
    }

    SUSY4LeptonDiTauSelector::~SUSY4LeptonDiTauSelector() {}

    StatusCode SUSY4LeptonDiTauSelector::initialize() {
        if (isInitialized()) { return StatusCode::SUCCESS; }
        ATH_CHECK(SUSY4LeptonTauSelector::initialize());
        ATH_CHECK(m_ParticleConstructor.retrieve());

        //        if (ProcessObject(XAMPP::SelectionObject::DiTau)) {
        //            if (!m_DiscrTool.isUserConfigured()) {
        //                ATH_MSG_INFO("Create instance of the DiTauDiscriminantTool");
        //                m_DiscrTool.setTypeAndName("tauRecTools::DiTauDiscriminantTool/DiTauBDTCalculator");
        //                std::string sWeightsFile = PathResolverFindCalibFile("tauRecTools/DiTau_JetBDT_prelim.weights.xml");
        //                ATH_CHECK(m_DiscrTool.setProperty("WeightsFile", sWeightsFile));
        //                ATH_CHECK(m_DiscrTool.retrieve());
        //            }
        //            if (!m_IDVarCalculator.isUserConfigured()) {
        //                ATH_MSG_INFO("Create instance of the DiTauIDVarCalculator");
        //                m_IDVarCalculator.setTypeAndName("tauRecTools::DiTauIDVarCalculator/DiTauVarCalc");
        //                ATH_CHECK(m_IDVarCalculator.retrieve());
        //            }
        //        }
        return StatusCode::SUCCESS;
    }

    StatusCode SUSY4LeptonDiTauSelector::FillTaus(const CP::SystematicSet& systset) {
        ATH_CHECK(SUSY4LeptonTauSelector::FillTaus(systset));
        ATH_CHECK(ViewElementsContainer("DiTauBaseline", m_BaselineDiTaus));
        ATH_CHECK(ViewElementsContainer("DiTauSignal", m_SignalDiTaus));
        for (const auto itau : *m_PreDiTaus) {
            if (PassBaseline(*itau)) {
                m_BaselineDiTaus->push_back(itau);
                ATH_CHECK(PerformTruthMatching(itau));
            }
            static SG::AuxElement::Decorator<char> dec_signal("signal");
            if (PassSignal(*itau)) {
                dec_signal(*itau) = true;
                m_SignalDiTaus->push_back(itau);
            } else {
                dec_signal(*itau) = false;
            }
        }
        return StatusCode::SUCCESS;
    }

    StatusCode SUSY4LeptonDiTauSelector::InitialFill(const CP::SystematicSet& systset) {
        ATH_CHECK(SUSY4LeptonTauSelector::InitialFill(systset));
        LinkStatus Link = CreateContainerLinks("DiTauJets", m_DiTaus);
        if (Link == LinkStatus::Failed) {
            return StatusCode::FAILURE;
        } else if (Link == LinkStatus::Created) {
            // Calibration and fancy shit
            ATH_CHECK(ViewElementsContainer("DiTauPreSel", m_PreDiTaus));

            for (const auto itau : *m_DiTaus) {
                //                ATH_CHECK(m_IDVarCalculator->calculateIDVariables(*itau));
                //                dec_BDT(*itau) = m_DiscrTool->getJetBDTScore(*itau);

                if (PassPreSelection(*itau)) { m_PreDiTaus->push_back(itau); }
                //              static SG::AuxElement::Accessor<std::vector< ElementLink < xAOD::TrackParticleContainer > >>
                //              trackAcc("trackLinks"); static SG::AuxElement::Decorator<std::vector<float>> dec_track_pt("track_pt");
                //                if (trackAcc.isAvailable(*itau)) {
                //                    for (size_t t = 0; t < trackAcc(*itau).size(); ++t) {
                //                        if (trackAcc(*itau).at(t).isValid()) {
                //                            const xAOD::TrackParticle* track = itau->track(t);
                //                            dec_track_pt(*itau).push_back(track->pt());
                //                        }
                //                    }
                //                }
                //                static SG::AuxElement::Accessor<std::vector< ElementLink < xAOD::TrackParticleContainer > >>
                //                isotrackAcc("isoTrackLinks"); static SG::AuxElement::Decorator<std::vector<float>>
                //                dec_isotrack_pt("isotrack_pt"); if (isotrackAcc.isAvailable(*itau)) {
                //                    for (size_t i = 0; i < isotrackAcc(*itau).size(); ++i) {
                //                        if (isotrackAcc(*itau).at(i).isValid()) {
                //                            const xAOD::TrackParticle* isotrack = itau->isoTrack(i);
                //                            dec_isotrack_pt(*itau).push_back(isotrack->pt());
                //                        }
                //                    }
                //                }
            }

            m_PreDiTaus->sort(XAMPP::ptsorter);
        } else if (Link == LinkStatus::Loaded) {
            // Do loading stuff
            ATH_CHECK(LoadViewElementsContainer("DiTauPreSel", m_PreDiTaus, true));
        }
        return StatusCode::SUCCESS;
    }

    bool SUSY4LeptonDiTauSelector::PassPreSelection(const xAOD::IParticle& P) const {
        static SG::AuxElement::Decorator<char> dec_selected("selected");

        if (P.type() == xAOD::Type::ObjectType::Tau) { return SUSY4LeptonTauSelector::PassPreSelection(P); }
        bool Pass = false;
        if (P.pt() > 20000) {
            dec_selected(P) = 2;
            Pass = true;
        }
        SetBaselineDecorator(P, Pass);
        return Pass;
    }
    bool SUSY4LeptonDiTauSelector::PassSignal(const xAOD::IParticle& P) const {
        if (P.type() == xAOD::Type::ObjectType::Tau) { return SUSY4LeptonTauSelector::PassSignal(P); }
        bool Pass = false;
        if (acc_BDT(P) > 0.7 && PassBaseline(P)) { Pass = true; }

        return Pass;
    }

    StatusCode SUSY4LeptonDiTauSelector::PerformTruthMatching(const xAOD::DiTauJet* itau) {
        static SG::AuxElement::Decorator<char> dec_TruthMatchedToDiTau("TruthMatched");
        static SG::AuxElement::Decorator<char> dec_TruthMatchedToHadronicDiTau("TruthMatchedToHadronic");

        dec_TruthMatchedToDiTau(*itau) = false;
        dec_TruthMatchedToHadronicDiTau(*itau) = false;

        static SG::AuxElement::ConstAccessor<const xAOD::IParticle*> acc_Ptr1("BuildPtr1");
        static SG::AuxElement::ConstAccessor<const xAOD::IParticle*> acc_Ptr2("BuildPtr2");
        const xAOD::IParticle* ditau = dynamic_cast<const xAOD::IParticle*>(itau);
        for (auto TruthDiTau : *m_ParticleConstructor->GetSubContainer("TruthDiTaus")) {
            static SG::AuxElement::Decorator<char> dec_MatchedToRecoDiTau("MatchedToRecoDiTau");
            static SG::AuxElement::Decorator<char> dec_MatchedToHighBDTRecoDiTau("MatchedToHighBDTRecoDiTau");
            static SG::AuxElement::ConstAccessor<int> acc_N_HadronicDecays("N_HadronicDecays");
            const xAOD::IParticle* Particle1 = acc_Ptr1(*TruthDiTau);
            const xAOD::IParticle* Particle2 = acc_Ptr2(*TruthDiTau);

            if (Overlaps(Particle1, ditau, 0.8) && Overlaps(Particle2, ditau, 0.8)) {
                dec_TruthMatchedToDiTau(*itau) = true;
                dec_MatchedToRecoDiTau(*TruthDiTau) = true;
                if (acc_BDT(*itau) > 0.7) { dec_MatchedToHighBDTRecoDiTau(*TruthDiTau) = true; }
                if (acc_N_HadronicDecays(*TruthDiTau) == 2) { dec_TruthMatchedToHadronicDiTau(*itau) = true; }
            }
        }

        static SG::AuxElement::Decorator<int> dec_MatchesToTruthElectron("MatchesToTruthElectron");
        static SG::AuxElement::Decorator<int> dec_MatchesToTruthMuon("MatchesToTruthMuon");
        static SG::AuxElement::Decorator<int> dec_MatchesToTruthPhoton("MatchesToTruthPhoton");
        static SG::AuxElement::Decorator<int> dec_MatchesToTruthTau("MatchesToTruthTau");

        static SG::AuxElement::Decorator<int> dec_SubJetMatchesToTruthElectron("SubJetMatchesToTruthElectron");
        static SG::AuxElement::Decorator<int> dec_SubJetMatchesToTruthMuon("SubJetMatchesToTruthMuon");
        static SG::AuxElement::Decorator<int> dec_SubJetMatchesToTruthPhoton("SubJetMatchesToTruthPhoton");
        static SG::AuxElement::Decorator<int> dec_SubJetMatchesToTruthTau("SubJetMatchesToTruthTau");

        dec_MatchesToTruthElectron(*itau) = GetNMatchesToFatJet(itau, m_truth_selection->GetTruthBaselineElectrons(), 0.8);
        dec_MatchesToTruthMuon(*itau) = GetNMatchesToFatJet(itau, m_truth_selection->GetTruthBaselineMuons(), 0.8);
        dec_MatchesToTruthPhoton(*itau) = GetNMatchesToFatJet(itau, m_truth_selection->GetTruthBaselinePhotons(), 0.8);
        dec_MatchesToTruthTau(*itau) = GetNMatchesToFatJet(itau, m_truth_selection->GetTruthBaselineTaus(), 0.8);

        xAOD::JetContainer* SubJets = nullptr;
        ATH_CHECK(PipeSubJetsIntoContainer(itau, SubJets));

        dec_SubJetMatchesToTruthElectron(*itau) = GetNMatches(SubJets, m_truth_selection->GetTruthBaselineElectrons(), 0.2);
        dec_SubJetMatchesToTruthMuon(*itau) = GetNMatches(SubJets, m_truth_selection->GetTruthBaselineMuons(), 0.2);
        dec_SubJetMatchesToTruthPhoton(*itau) = GetNMatches(SubJets, m_truth_selection->GetTruthBaselinePhotons(), 0.2);
        dec_SubJetMatchesToTruthTau(*itau) = GetNMatches(SubJets, m_truth_selection->GetTruthBaselineTaus(), 0.2);

        return StatusCode::SUCCESS;
    }

    StatusCode SUSY4LeptonDiTauSelector::PipeSubJetsIntoContainer(const xAOD::DiTauJet* DiTau, xAOD::JetContainer*& SubJetContainer) {
        std::string ContainerName = StoreName() + "SubJetDiTau" + std::to_string(DiTau->index());
        if (evtStore()->contains<xAOD::JetContainer>(ContainerName)) {
            ATH_CHECK(LoadContainer(ContainerName, SubJetContainer));
        } else {
            SubJetContainer = new xAOD::JetContainer();
            xAOD::JetAuxContainer* SubJetsAux = new xAOD::JetAuxContainer();
            SubJetContainer->setStore(SubJetsAux);
            ATH_CHECK(evtStore()->record(SubJetContainer, ContainerName));
            ATH_CHECK(evtStore()->record(SubJetsAux, ContainerName + "Aux."));
            for (unsigned int S = 0; S < NSubJets(DiTau); ++S) {
                xAOD::Jet* myJet = new xAOD::Jet();
                SubJetContainer->push_back(myJet);
                xAOD::JetFourMom_t FourVec;
                TLorentzVector CrapJet;
                CrapJet.SetPtEtaPhiE(DiTau->subjetPt(S), DiTau->subjetEta(S), DiTau->subjetPhi(S), DiTau->subjetE(S));
                ROOT::Math::LorentzVector<ROOT::Math::PxPyPzM4D<double> >::Scalar E = CrapJet.E();
                ROOT::Math::LorentzVector<ROOT::Math::PxPyPzM4D<double> >::Scalar Px = CrapJet.Px();
                ROOT::Math::LorentzVector<ROOT::Math::PxPyPzM4D<double> >::Scalar Py = CrapJet.Py();
                ROOT::Math::LorentzVector<ROOT::Math::PxPyPzM4D<double> >::Scalar Pz = CrapJet.Pz();
                FourVec.SetPxPyPzE(Px, Py, Pz, E);
                myJet->setJetP4(FourVec);
            }
        }

        return StatusCode::SUCCESS;
    }

    unsigned int SUSY4LeptonDiTauSelector::NSubJets(const xAOD::DiTauJet* DiTau) const {
        static SG::AuxElement::ConstAccessor<std::vector<float> > subjetPtAcc("subjet_pt");
        if (subjetPtAcc.isAvailable(*DiTau)) { return subjetPtAcc(*DiTau).size(); }
        ATH_MSG_WARNING("No subjet accessor is available....");
        return 0;
    }

    unsigned int SUSY4LeptonDiTauSelector::GetNMatches(xAOD::IParticleContainer* DiTauJetContainer,
                                                       xAOD::IParticleContainer* TruthContainer, float dR) const {
        unsigned int N = 0;
        for (const auto& Truth : *TruthContainer) {
            const xAOD::IParticle* CloseDiTauJet = GetClosestParticle(DiTauJetContainer, Truth, false, TruthContainer);
            if (CloseDiTauJet && Overlaps(CloseDiTauJet, Truth, dR)) { ++N; }
        }
        return N;
    }

    unsigned int SUSY4LeptonDiTauSelector::GetNMatchesToFatJet(const xAOD::DiTauJet* DiTau, xAOD::IParticleContainer* TruthContainer,
                                                               float dR) const {
        unsigned int N = 0;
        for (const auto& Truth : *TruthContainer) {
            if (Overlaps(DiTau, Truth, dR)) { ++N; }
        }
        return N;
    }

}  // namespace XAMPP
#endif
