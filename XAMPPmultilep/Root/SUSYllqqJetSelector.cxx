#include <FTagAnalysisInterfaces/IBTaggingEfficiencyTool.h>
#include <FTagAnalysisInterfaces/IBTaggingSelectionTool.h>
#include <SUSYTools/SUSYObjDef_xAOD.h>
#include <XAMPPbase/ReconstructedParticles.h>
#include <XAMPPmultilep/SUSYllqqJetSelector.h>
#include <math.h>

#include <fstream>
#include <iostream>
#include <sstream>
namespace XAMPP {
    SUSYllqqJetSelector::SUSYllqqJetSelector(const std::string& myname) :
        SUSYJetSelector(myname),
        m_PreTrkJets02(nullptr),
        m_TrkJet02Key("AntiKt2PV0TrackJets"),
        m_PreFatJets10(nullptr),
        m_FatJetKey10("AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets"),
        m_JetChargeToUse(0),
        m_TrkSelTool("TrackParticleSelectionTool"),
        m_ParticleConstructor("ParticleConstructor"),
        m_BtagSFTool_WP77(""),
        m_BtagSelTool_WP77(""),
        m_BtagSFs(),
        m_BTagWP77_decoration("btag77WP"),
        m_jet_wp77_decors(nullptr) {
        m_TrkSelTool.declarePropertyFor(this, "TrackSelectionTool", "Loose track selection tool");
        m_ParticleConstructor.declarePropertyFor(this, "ParticleConstructor", "The XAMPP particle constuctor");

        declareProperty("BTagSelectionTool_77WP", m_BtagSelTool_WP77);
        declareProperty("BTagEfficiencyTool_77WP", m_BtagSFTool_WP77);
        declareProperty("BTagWP77_decoration", m_BTagWP77_decoration);
    }
    StatusCode SUSYllqqJetSelector::LoadContainers() {
        for (auto& sf : m_BtagSFs) ATH_CHECK(sf.second->initEvent());
        return SUSYJetSelector::LoadContainers();
    }
    SUSYllqqJetSelector::~SUSYllqqJetSelector() {}
    StatusCode SUSYllqqJetSelector::initialize() {
        if (isInitialized()) { return StatusCode::SUCCESS; }
        ATH_CHECK(SUSYJetSelector::initialize());

        ATH_CHECK(m_ParticleConstructor.retrieve());
        ATH_CHECK(m_TrkSelTool.retrieve());
        ATH_CHECK(m_BtagSelTool_WP77.retrieve());

        m_jet_wp77_decors = std::make_shared<JetDecorations>(*GetJetDecorations());
        m_jet_wp77_decors->isBJet.setDecorationString(m_BTagWP77_decoration);

        if (isData()) return StatusCode::SUCCESS;
        ATH_CHECK(m_BtagSFTool_WP77.retrieve());

        ATH_CHECK(DeclareAsWeightSyst(m_BtagSFTool_WP77, XAMPP::SelectionObject::BTag));

        for (auto& syst_set : m_systematics->GetWeightSystematics(XAMPP::SelectionObject::BTag)) {
            BTagJetWeight_Ptr BTag = std::make_shared<BTagJetWeight>(m_BtagSFTool_WP77);
            BTag->SetBJetEtaCut(2.5);
            BTag->setupDecorations(m_jet_wp77_decors);
            ATH_CHECK(initIParticleWeight(*BTag, "BTag77P", syst_set, ScaleFactorMapContains::SignalSf, true, "Jet"));
            if (m_BtagSFs.find(syst_set) != m_BtagSFs.end()) {
                ATH_MSG_FATAL("The BTag SF's already exist for systematic " << syst_set->name());
                return StatusCode::FAILURE;
            }
            m_BtagSFs.insert(std::pair<const CP::SystematicSet*, std::shared_ptr<JetWeightDecorator>>(syst_set, BTag));
        }

        return StatusCode::SUCCESS;
    }
    StatusCode SUSYllqqJetSelector::InitialFill(const CP::SystematicSet& systset) {
        ATH_CHECK(SUSYJetSelector::InitialFill(systset));
        //        ATH_CHECK(CalibrateJets(m_FatJetKey10, m_PreFatJets10, "FatJet10", true));
        return StatusCode::SUCCESS;
    }
    void SUSYllqqJetSelector::SaveTrackFraction(xAOD::Jet* J, xAOD::TrackParticleContainer& Container, unsigned int Trk) {
        J->auxdata<float>(Form("Frac_%iTrk", Trk)) = Container.size() > Trk ? Container.at(Trk)->pt() / J->pt() : -1.;
    }
    StatusCode SUSYllqqJetSelector::SaveScaleFactor() {
        ATH_CHECK(SUSYJetSelector::SaveScaleFactor());
        for (auto& JetSF : m_BtagSFs) {
            if (m_XAMPPInfo->GetSystematic() != m_systematics->GetNominal() && JetSF.first != m_systematics->GetNominal()) continue;
            ATH_CHECK(m_systematics->setSystematic(JetSF.first));
            for (auto jet : *GetSignalJets()) { ATH_CHECK(JetSF.second->saveSF(*jet)); }
            ATH_CHECK(JetSF.second->applySF());
        }
        return StatusCode::SUCCESS;
    }
    StatusCode SUSYllqqJetSelector::FillJets(const CP::SystematicSet& systset) {
        ATH_CHECK(SUSYJetSelector::FillJets(systset));
        for (auto ijet : *GetSignalNoORJets()) { m_jet_wp77_decors->isBJet.set(*ijet, m_BtagSelTool_WP77->accept(*ijet)); }
        return StatusCode::SUCCESS;
        xAOD::JetContainer* FatAfterOR = nullptr;
        ATH_CHECK(ViewElementsContainer("BaseFatJet10", FatAfterOR));
        //        for (const auto& Jet : *m_PreFatJets10)
        //            if (PassBaseline(*Jet)) FatAfterOR->push_back(Jet);

        // Basic idea ZZ->llqq  the qq should form jets with invariant masses around the Z-boson peak.
        // ATH_CHECK(ReclusterJets(GetSignalJets(), 1.2));

        // For ZZ->llqq the q's should have a positive and negative charge. Let's see whether we can use that or not
        static FloatDecorator dec_charge("charge");
        static IntDecorator dec_chargeTrks("NChTrks");
        static SG::AuxElement::Decorator<std::vector<float>> dec_TrackFrac("TrackFracs");
        //        static SG::AuxElement::Decorator<std::vector<float>> dec_ClusterFrac("ClusterFracs");
        static SG::AuxElement::Decorator<std::vector<int>> dec_TrackQ("TrackQs");
        for (auto J : *GetBaselineJets()) {
            // Therefore we use the GhostTracks of a jet and check how the charge evolves
            std::vector<const xAOD::TrackParticle*> Tracks;
            xAOD::TrackParticleContainer Jet_Tracks(SG::VIEW_ELEMENTS);
            if (!J->getAssociatedObjects<xAOD::TrackParticle>("GhostTrack", Tracks)) return StatusCode::FAILURE;

            // Quality criteria on tracks should ensure that we pick tracks close to the interaction point
            for (const auto T : Tracks) {
                if (!T || !m_TrkSelTool->accept(*T, m_XAMPPInfo->GetPrimaryVertex())) { continue; }
                Jet_Tracks.push_back(const_cast<xAOD::TrackParticle*>(T));
            }
            Jet_Tracks.sort(XAMPP::ptsorter);

            // Calculate the different charges of the jet
            dec_charge(*J) = CalculateCharge(J, 1.);
            for (int K = 2; K < 10; ++K) { J->auxdata<float>(Form("Q0%i", K)) = CalculateCharge(J, 1. / K); }

            dec_chargeTrks(*J) = Jet_Tracks.size();

            // Save the track fractions of the 10 leading jet
            //            std::vector<float> TrackPt;
            std::vector<float> TrackFrac;
            std::vector<float> ClusterFrac;

            std::vector<int> TrackQ;
            for (const auto T : Jet_Tracks) {
                //                TrackPt.push_back(T->pt());
                TrackFrac.push_back(T->pt() / J->pt());
                TrackQ.push_back(T->charge());
            }
            dec_TrackFrac(*J) = TrackFrac;
            dec_TrackQ(*J) = TrackQ;
        }
        return StatusCode::SUCCESS;
    }
    float SUSYllqqJetSelector::CalculateCharge(const xAOD::Jet* Jet, float Kappa) const {
        if (m_JetChargeToUse == 0) return CalculatePtWeightedCharge(Jet, Kappa);
        if (m_JetChargeToUse == 1) return CalculateAngularWeightedCharge(Jet, Kappa);
        if (m_JetChargeToUse == 2) return CalculateEnergyWeightedCharge(Jet, Kappa);
        ATH_MSG_WARNING("Unknown jet charge index " << m_JetChargeToUse);
        return -1.e3;
    }
    float SUSYllqqJetSelector::CalculateAngularWeightedCharge(const xAOD::Jet* Jet, float Kappa, unsigned int MaxTrk) const {
        double Q = 0.;
        double TotMoment = 0;
        unsigned int N = 0;
        std::vector<const xAOD::TrackParticle*> Tracks;
        if (!Jet->getAssociatedObjects<xAOD::TrackParticle>("GhostTrack", Tracks)) return -1.e9;
        for (const auto& T : Tracks) {
            if (!T || !m_TrkSelTool->accept(*T, m_XAMPPInfo->GetPrimaryVertex())) { continue; }
            if (N >= MaxTrk) break;
            double DotProd = TMath::Power(Jet->p4().Vect().Dot(T->p4().Vect()), Kappa);
            Q += T->charge() * DotProd;
            TotMoment += DotProd;
            ++N;
        }
        if (Q != 0.) Q = Q / TotMoment;
        return Q;
    }
    float SUSYllqqJetSelector::CalculateEnergyWeightedCharge(const xAOD::Jet* Jet, float Kappa, unsigned int MaxTrk) const {
        double Q = 0.;
        unsigned int N = 0;
        std::vector<const xAOD::TrackParticle*> Tracks;
        if (!Jet->getAssociatedObjects<xAOD::TrackParticle>("GhostTrack", Tracks)) return -1.e9;
        for (const auto& T : Tracks) {
            if (!T || !m_TrkSelTool->accept(*T, m_XAMPPInfo->GetPrimaryVertex())) { continue; }
            if (N >= MaxTrk) break;
            double EnFrac = TMath::Power(T->e() / Jet->e(), Kappa);
            Q += T->charge() * EnFrac;
            ++N;
        }
        return Q;
    }

    float SUSYllqqJetSelector::CalculatePtWeightedCharge(const xAOD::Jet* Jet, float Kappa, unsigned int MaxTrk) const {
        float Q = 0.;
        unsigned int N = 0;
        std::vector<const xAOD::TrackParticle*> Tracks;
        if (!Jet->getAssociatedObjects<xAOD::TrackParticle>("GhostTrack", Tracks)) return -1.e9;
        for (const auto& T : Tracks) {
            if (!T || !m_TrkSelTool->accept(*T, m_XAMPPInfo->GetPrimaryVertex())) { continue; }
            if (N >= MaxTrk) break;
            // Q = 1/ p_{T} ^{kappa} sum Q_{i} * p_{Ti}
            Q += T->charge() * TMath::Power(T->pt(), Kappa);
            ++N;
        }
        Q = Q / TMath::Power(Jet->pt(), Kappa);
        return Q;
    }
}  // namespace XAMPP
