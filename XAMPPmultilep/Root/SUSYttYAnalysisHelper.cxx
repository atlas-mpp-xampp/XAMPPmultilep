#include <MCTruthClassifier/MCTruthClassifier.h>
#include <MCTruthClassifier/MCTruthClassifierDefs.h>
#include <XAMPPbase/AnalysisUtils.h>
#include <XAMPPbase/ReconstructedParticles.h>
#include <XAMPPmultilep/AnalysisUtils.h>
#include <XAMPPmultilep/SUSYttYAnalysisHelper.h>

namespace XAMPP {
    SUSYttYAnalysisHelper::SUSYttYAnalysisHelper(const std::string& myname) :
        SUSY4LeptonAnalysisHelper(myname), m_MEphotonPtCut(-1), m_MEPhotonSamples(), m_FSRPhotonSamples(), m_SkimMECut(false) {
        declareProperty("MEptCut", m_MEphotonPtCut);
        declareProperty("MEPhotDSID", m_MEPhotonSamples);
        declareProperty("FSRPhotDSID", m_FSRPhotonSamples);
        declareProperty("SkimMECut", m_SkimMECut);
    }
    SUSYttYAnalysisHelper::~SUSYttYAnalysisHelper() {}
    StatusCode SUSYttYAnalysisHelper::initialize() {
        ATH_MSG_INFO("Photon ME pt cut: " << m_MEphotonPtCut);
        ATH_CHECK(SUSY4LeptonAnalysisHelper::initialize());
        return StatusCode::SUCCESS;
    }
    unsigned int SUSYttYAnalysisHelper::finalState() { return 0; }

    bool SUSYttYAnalysisHelper::AcceptEvent() {
        static XAMPP::Storage<char>* dec_GRL = m_XAMPPInfo->GetVariableStorage<char>("PassY_Sel");
        static XAMPP::Storage<float>* dec_LeadPhPt = m_XAMPPInfo->GetVariableStorage<float>("PassY_Pt");
        bool Accept = true;
        float LeadPt = -1;
        if (m_XAMPPInfo->isMC()) {
            int SampleNr = m_XAMPPInfo->mcChannelNumber();
            if (IsInVector(SampleNr, m_MEPhotonSamples) || IsInVector(SampleNr, m_FSRPhotonSamples)) {
                std::vector<const xAOD::TruthParticle*> Photons;
                for (const auto& particle : *m_truth_selection->GetTruthInContainer()) {
                    // barcode selection
                    if (particle->isGenSpecific() || particle->barcode() >= 200000 || particle->status() != 1 || !particle->isPhoton())
                        continue;
                    Photons.push_back(particle);
                }
                std::sort(Photons.begin(), Photons.end(), XAMPP::ptsorter);
                const xAOD::TruthParticle* LeadPh = !Photons.empty() ? Photons.at(0) : nullptr;
                if (LeadPh) LeadPt = LeadPh->pt();
                if (IsInVector(SampleNr, m_FSRPhotonSamples)) {
                    Accept = !IsMEPhoton(LeadPh, m_MEphotonPtCut);
                } else {
                    Accept = IsMEPhoton(LeadPh, m_MEphotonPtCut);
                }
            }
        }
        if (!dec_GRL->ConstStore(Accept).isSuccess()) return false;
        if (!dec_LeadPhPt->ConstStore(LeadPt).isSuccess()) return false;
        return true;
    }
    StatusCode SUSYttYAnalysisHelper::initializeEventVariables() {
        // These two variables are used to determine whether the event is written to  the Ntuple
        ATH_CHECK(initLightLeptonTrees());
        ATH_CHECK(initRecoParticleTree());
        ATH_CHECK(initJetTree());
        ATH_CHECK(initPhotonTree());
        if (doTruth()) ATH_CHECK(initTruthTree());

        ATH_CHECK(InitializeMultiplicities());
        ATH_CHECK(InitializeMeffVariable());

        ATH_CHECK(m_XAMPPInfo->NewEventVariable<char>("OS_Lep"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<char>("PassY_Sel", !isData()));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("PassY_Pt", !isData()));

        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("PassZVeto"));
        return StatusCode::SUCCESS;
    }
    StatusCode SUSYttYAnalysisHelper::ComputeEventVariables() {
        ATH_CHECK(FillMultiplicities());
        ATH_CHECK(CalculateMeffVariable());

        static XAMPP::ParticleStorage* ElecStore = m_XAMPPInfo->GetParticleStorage("elec");
        static XAMPP::ParticleStorage* MuonStore = m_XAMPPInfo->GetParticleStorage("muon");
        static XAMPP::ParticleStorage* PhotonStore = m_XAMPPInfo->GetParticleStorage("pho");
        static XAMPP::ParticleStorage* JetStore = m_XAMPPInfo->GetParticleStorage("jet");
        static XAMPP::ParticleStorage* RecoCandStore = m_XAMPPInfo->GetParticleStorage("RecoCandidates");

        ATH_MSG_DEBUG("Save the particles");
        ATH_CHECK(RecoCandStore->Fill(m_ParticleConstructor->GetContainer()));
        ATH_CHECK(PhotonStore->Fill(m_photon_selection->GetBaselinePhotons()));
        ATH_CHECK(ElecStore->Fill(m_electron_selection->GetSignalNoORElectrons()));
        ATH_CHECK(MuonStore->Fill(m_muon_selection->GetSignalNoORMuons()));
        ATH_CHECK(JetStore->Fill(m_jet_selection->GetSignalNoORJets()));

        static XAMPP::Storage<char>* dec_OpLep = m_XAMPPInfo->GetVariableStorage<char>("OS_Lep");

        static XAMPP::Storage<int>* dec_ZVeto = m_XAMPPInfo->GetVariableStorage<int>("PassZVeto");

        static XAMPP::Storage<xAOD::MissingET*>* dec_MET = m_XAMPPInfo->GetVariableStorage<xAOD::MissingET*>("MetTST");

        xAOD::IParticleContainer Lep(SG::VIEW_ELEMENTS);

        for (const auto& E : *m_electron_selection->GetSignalElectrons()) Lep.push_back(E);
        for (const auto& M : *m_muon_selection->GetSignalMuons()) Lep.push_back(M);

        bool OppositeSign = (nSignalElectrons() + nSignalMuons() == 2 ? XAMPP::OppositeSign(**(Lep.begin()), **(Lep.begin() + 1)) : false);

        ATH_CHECK(dec_OpLep->Store(OppositeSign));

        if (m_photon_selection->GetBaselinePhotons() == 0) return StatusCode::SUCCESS;

        int Z_Veto = XAMPP::GetZVeto(&Lep, m_photon_selection->GetSignalPhotons(), m_ZMassWindow);

        ConstructInvariantMomenta(Lep, m_ParticleConstructor.operator->(),
                                  [this](const xAOD::IParticle* P) -> bool { return IsSignalLepton(P); });

        CalculateMt(m_electron_selection->GetSignalNoORElectrons(), dec_MET);
        CalculateMt(m_muon_selection->GetSignalNoORMuons(), dec_MET);
        CalculateMt(m_photon_selection->GetBaselinePhotons(), dec_MET);

        ATH_CHECK(dec_ZVeto->Store(Z_Veto));

        if (doTruth()) {
            static XAMPP::ParticleStorage* True_ElecStore = m_XAMPPInfo->GetParticleStorage("truth_elec");
            static XAMPP::ParticleStorage* True_MuonStore = m_XAMPPInfo->GetParticleStorage("truth_muon");
            static XAMPP::ParticleStorage* True_PhotStore = m_XAMPPInfo->GetParticleStorage("truth_phot");
            GetParentPdgId(m_truth_selection->GetTruthBaselineElectrons());
            GetParentPdgId(m_truth_selection->GetTruthBaselinePhotons());
            GetParentPdgId(m_truth_selection->GetTruthBaselineMuons());

            ATH_CHECK(True_ElecStore->Fill(m_truth_selection->GetTruthBaselineElectrons()));
            ATH_CHECK(True_PhotStore->Fill(m_truth_selection->GetTruthBaselinePhotons()));
            ATH_CHECK(True_MuonStore->Fill(m_truth_selection->GetTruthBaselineMuons()));
        }
        return StatusCode::SUCCESS;
    }

    bool SUSYttYAnalysisHelper::IsMEPhoton(const xAOD::Photon* Phot, float MinPt) const {
        static CharAccessor acc_ME("ME");
        return acc_ME(*Phot) && Phot->pt() > MinPt;
    }

    bool SUSYttYAnalysisHelper::IsMEPhoton(const xAOD::TruthParticle* Phot, float MinPt) const {
        if (!Phot || !Phot->isPhoton() || Phot->pt() < MinPt) return false;
        Phot = XAMPP::GetFirstChainLink(Phot);
        int truthType = XAMPP::getParticleTruthType(Phot);
        int truthOrigin = XAMPP::getParticleTruthOrigin(Phot);
        if (truthType == MCTruthPartClassifier::UnknownPhoton)
            return false;
        else if (truthType == MCTruthPartClassifier::BkgPhoton && truthOrigin == MCTruthPartClassifier::UndrPhot)
            return false;
        return (Phot->isPhoton() && Phot->pt() > MinPt && XAMPP::isParticleFromHardProcess(Phot));
    }

    StatusCode SUSYttYAnalysisHelper::initPhotonTree() {
        ATH_CHECK(SUSY4LeptonAnalysisHelper::initPhotonTree());
        XAMPP::ParticleStorage* Photons = m_XAMPPInfo->GetParticleStorage("pho");

        ATH_CHECK(Photons->SaveChar("ME"));
        ATH_CHECK(Photons->SaveFloat("ME_pt"));
        ATH_CHECK(Photons->SaveFloat("MT"));
        return StatusCode::SUCCESS;
    }
    StatusCode SUSYttYAnalysisHelper::initTruthTree() {
        ATH_CHECK(m_XAMPPInfo->BookParticleStorage("truth_elec", true));
        ATH_CHECK(m_XAMPPInfo->BookParticleStorage("truth_muon", true));
        ATH_CHECK(m_XAMPPInfo->BookParticleStorage("truth_phot", true));

        XAMPP::ParticleStorage* EleStore = m_XAMPPInfo->GetParticleStorage("truth_elec");
        XAMPP::ParticleStorage* MuoStore = m_XAMPPInfo->GetParticleStorage("truth_muon");
        XAMPP::ParticleStorage* PhoStore = m_XAMPPInfo->GetParticleStorage("truth_phot");

        ATH_CHECK(EleStore->SaveFloat("charge"));
        ATH_CHECK(MuoStore->SaveFloat("charge"));

        ATH_CHECK(EleStore->SaveInteger("parent_pdgId"));
        ATH_CHECK(MuoStore->SaveInteger("parent_pdgId"));
        ATH_CHECK(PhoStore->SaveInteger("parent_pdgId"));

        ATH_CHECK(EleStore->SaveChar("HardProcess"));
        ATH_CHECK(MuoStore->SaveChar("HardProcess"));
        ATH_CHECK(PhoStore->SaveChar("HardProcess"));

        return StatusCode::SUCCESS;
    }
}  // namespace XAMPP
