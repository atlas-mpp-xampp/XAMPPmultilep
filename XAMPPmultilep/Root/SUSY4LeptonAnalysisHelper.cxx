#include <InDetTrackSelectionTool/IInDetTrackSelectionTool.h>
#include <IsolationSelection/IIsolationCloseByCorrectionTool.h>
#include <IsolationSelection/IIsolationSelectionTool.h>
#include <IsolationSelection/IsolationWP.h>
#include <XAMPPbase/AnalysisUtils.h>
#include <XAMPPbase/EventInfo.h>
#include <XAMPPbase/IElectronSelector.h>
#include <XAMPPbase/IJetSelector.h>
#include <XAMPPbase/IMetSelector.h>
#include <XAMPPbase/IMuonSelector.h>
#include <XAMPPbase/IPhotonSelector.h>
#include <XAMPPbase/ISystematics.h>
#include <XAMPPbase/ITauSelector.h>
#include <XAMPPbase/ITriggerTool.h>
#include <XAMPPbase/ITruthSelector.h>
#include <XAMPPbase/ReconstructedParticles.h>
#include <XAMPPbase/ToolHandleSystematics.h>
#include <XAMPPmultilep/AnalysisUtils.h>
#include <XAMPPmultilep/SUSY4LeptonAnalysisHelper.h>

namespace XAMPP {
    static CharDecorator dec_passLMR("passLMR");
    static CharDecorator acc_passOR("passOR");

    static CharDecorator dec_tauOR("passLTOR");
    static CharAccessor acc_tauOR("passLTOR");
    /// Index of the tau overlapping with the jet
    static IntDecorator dec_overlap_tau("overlapingTauIdx");
    static IntAccessor acc_overlap_tau("overlapingTauIdx");

    /// Index of each tau in the container
    static IntDecorator dec_idx("Index");
    /// Close by container tau link. Set in the GetPreJets method of SUSY4LeptonTauSelector
    static SG::AuxElement::Accessor<TauLink> acc_TauLink("CloseByTau");

    bool SUSY4LeptonAnalysisHelper::AcceptEvent() { return true; }
    SUSY4LeptonAnalysisHelper::SUSY4LeptonAnalysisHelper(const std::string& myname) :

        SUSYAnalysisHelper(myname),
        m_RemoveLowMassLeptons(true),
        m_LowMass_OSPairCut(4000.),
        m_SFOS_UpperEdge(10400.),
        m_SFOS_LowerEdge(8400.),
        m_ApplyIsoCorrect(true),
        m_ApplEventSkimming(true),
        m_JetHt_minPt(40.e3),
        m_ZMassWindow(10.e3),
        m_IsoCorrect(""),
        m_IsoTool(""),
        m_isoWPs(),
        m_isoTester_Elecs(),
        m_isoTester_Muons(),
        m_dec_NLepBase(nullptr),
        m_dec_NLepSignal(nullptr),
        m_dec_NElecBase(nullptr),
        m_dec_NElecSignal(nullptr),
        m_dec_NMuonBase(nullptr),
        m_dec_NMuonSignal(nullptr),
        m_dec_NTauBase(nullptr),
        m_dec_NTauSignal(nullptr),
        m_dec_NJetSignal(nullptr),
        m_dec_NJetBase(nullptr),
        m_dec_NBJets(nullptr),
        m_dec_NPhotSignal(nullptr),
        m_dec_NPhotBase(nullptr),
        m_dec_NDiTauBase(nullptr),
        m_dec_NDiTauSignal(nullptr) {
        declareProperty("CorrectIsolation", m_ApplyIsoCorrect);
        declareProperty("SkimEvents", m_ApplEventSkimming);

        declareProperty("RemoveLowMassLeptons", m_RemoveLowMassLeptons);
        declareProperty("SFOSRemovalLowEdge", m_SFOS_LowerEdge);
        declareProperty("SFOSRemovalHighEdge", m_SFOS_UpperEdge);
        declareProperty("OSRemovalHighEdge", m_LowMass_OSPairCut);
        declareProperty("HtMinJetPt", m_JetHt_minPt);
        declareProperty("ZmassWindow", m_ZMassWindow);
        m_IsoCorrect.declarePropertyFor(this, "IsolationCorrection", "The isolation correction tool");
        declareProperty("IsolationSelection", m_IsoTool);
        declareProperty("IsolationWPTools", m_isoWPs);
    }

    StatusCode SUSY4LeptonAnalysisHelper::initializeAnalysisTools() {
        ATH_CHECK(SUSYAnalysisHelper::initializeAnalysisTools());
        if (m_IsoTool.empty()) m_IsoTool = GetCPTool<CP::IIsolationSelectionTool>("IsolationSelectionTool");
        ATH_CHECK(m_IsoTool.retrieve());
        ATH_CHECK(initializeIsoCorrectionTool());
        ATH_CHECK(m_isoWPs.retrieve());

        return StatusCode::SUCCESS;
    }
    StatusCode SUSY4LeptonAnalysisHelper::initializeIsoCorrectionTool() {
        if (!m_IsoCorrect.isUserConfigured() && m_ApplyIsoCorrect) {
            m_IsoCorrect.setTypeAndName("CP::IsolationCloseByCorrectionTool/4LcloseByCorrectioTool");
            ATH_CHECK(m_IsoCorrect.setProperty("SelectionDecorator", "signal"));
            ATH_CHECK(m_IsoCorrect.setProperty("PassoverlapDecorator", "passLMR"));
            ATH_CHECK(m_IsoCorrect.setProperty("BackupPrefix", "ORIG"));
            ATH_CHECK(m_IsoCorrect.setProperty("IsolationSelectionDecorator", "CORR_isol"));
            ATH_CHECK(m_IsoCorrect.setProperty("IsolationSelectionTool", m_IsoTool));
            ATH_CHECK(m_IsoCorrect.retrieve());
        }
        return StatusCode::SUCCESS;
    }
    SUSY4LeptonAnalysisHelper::~SUSY4LeptonAnalysisHelper() {}
    StatusCode SUSY4LeptonAnalysisHelper::FillObjects(const CP::SystematicSet* systset) {
        if (!m_systematics->AffectsOnlyMET(systset) && m_ApplyIsoCorrect) {
            if (m_IsoCorrect->getCloseByIsoCorrection(m_electron_selection->GetPreElectrons(), m_muon_selection->GetPreMuons()) !=
                CP::CorrectionCode::Ok) {
                return StatusCode::FAILURE;
            }
        }
        ATH_CHECK(SUSYAnalysisHelper::FillObjects(systset));
        return StatusCode::SUCCESS;
    }
    StatusCode SUSY4LeptonAnalysisHelper::RemoveOverlap() {
        if (m_systematics->AffectsOnlyMET(m_systematics->GetCurrent())) return StatusCode::SUCCESS;
        ATH_CHECK(m_susytools->OverlapRemoval(m_electron_selection->GetElectrons(), m_muon_selection->GetMuons(),
                                              m_jet_selection->GetPreJets(), m_photon_selection->GetPrePhotons(),
                                              m_tau_selection->GetPreTaus()));
        if (m_systematics->ProcessObject(XAMPP::SelectionObject::Tau)) {
            if (!m_tau_selection->GetPreTaus()->empty()) {
                for (const auto tau : *m_tau_selection->GetPreTaus()) {
                    dec_idx(*tau) = tau->index();
                    dec_tauOR(*tau) = m_tau_selection->GetTauDecorations()->passBaseline(tau);
                    if (m_tau_selection->GetTauDecorations()->passSignal(tau)) {
                        m_tau_selection->GetTauDecorations()->enterOverlapRemoval.set(tau, 2);
                    } else {
                        m_tau_selection->GetTauDecorations()->enterOverlapRemoval.set(tau, 0);
                    }
                }
            }
            for (const auto& jet : *m_jet_selection->GetPreJets()) {
                dec_tauOR(*jet) = m_jet_selection->GetJetDecorations()->passBaseline(jet);
                const xAOD::TauJet* close_by_tau =
                    acc_TauLink.isAvailable(*jet) && acc_TauLink(*jet).isValid() ? (*acc_TauLink(*jet)) : nullptr;
                dec_overlap_tau(*jet) =
                    close_by_tau && m_tau_selection->GetTauDecorations()->passBaseline(close_by_tau) && Overlaps(close_by_tau, jet, 0.2)
                        ? close_by_tau->index()
                        : -1;
            }
        }
        // Only apply the OR twice in case of the event has actually taus
        if (!m_tau_selection->GetPreTaus()->empty()) {
            // Check the overlap removal again
            ATH_CHECK(m_susytools->OverlapRemoval(m_electron_selection->GetElectrons(), m_muon_selection->GetMuons(),
                                                  m_jet_selection->GetPreJets(), m_photon_selection->GetPrePhotons(),
                                                  m_tau_selection->GetPreTaus()));
            for (const auto& tau : *m_tau_selection->GetPreTaus()) {
                m_tau_selection->GetTauDecorations()->passBaseline.set(tau, acc_tauOR(*tau));
            }
        }
        // Test the isolation WP's for electrons and muons
        for (auto ele : *m_electron_selection->GetPreElectrons()) {
            for (const auto& tester : m_isoTester_Elecs) { (*tester.particleDecoration)(*ele) = tester.isoTool->accept(*ele); }
        }
        for (auto muo : *m_muon_selection->GetPreMuons()) {
            for (const auto& tester : m_isoTester_Muons) { (*tester.particleDecoration)(*muo) = tester.isoTool->accept(*muo); }
        }
        ATH_CHECK(LowMassRemoval(m_electron_selection->GetPreElectrons(), m_muon_selection->GetPreMuons()));
        return StatusCode::SUCCESS;
    }
    StatusCode SUSY4LeptonAnalysisHelper::LowMassRemoval(xAOD::IParticleContainer* elecs, xAOD::IParticleContainer* muons) {
        xAOD::IParticleContainer Lep(SG::VIEW_ELEMENTS);
        for (const auto E : *elecs) {
            dec_passLMR(*E) = acc_passOR(*E);
            Lep.push_back(E);
        }
        for (const auto M : *muons) {
            dec_passLMR(*M) = acc_passOR(*M);
            Lep.push_back(M);
        }
        if (!m_RemoveLowMassLeptons || Lep.size() < 2) return StatusCode::SUCCESS;
        ATH_CHECK(RemoveLowMassLeptons(&Lep, OppositeSign, m_LowMass_OSPairCut));
        ATH_CHECK(RemoveLowMassLeptons(m_electron_selection->GetPreElectrons(), IsSFOS, m_SFOS_UpperEdge, m_SFOS_LowerEdge));
        ATH_CHECK(RemoveLowMassLeptons(m_muon_selection->GetPreMuons(), IsSFOS, m_SFOS_UpperEdge, m_SFOS_LowerEdge));
        return StatusCode::SUCCESS;
    }
    StatusCode SUSY4LeptonAnalysisHelper::initializeEventVariables() {
        // Initialize the particle trees in separate functions
        ATH_CHECK(initLightLeptonTrees());
        ATH_CHECK(initRecoParticleTree());
        ATH_CHECK(initTauTree());
        ATH_CHECK(initJetTree());
        ATH_CHECK(initPhotonTree());

        ATH_CHECK(initializeModuleContainers());
        ATH_CHECK(InitializeMultiplicities());
        ATH_CHECK(InitializeMeffVariable());

        if (doTruth()) ATH_CHECK(initTruthTree());

        // Does the event pass the Z-Veto? Calculated for Signal and Baseline leptons seperately
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("ZVeto"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("Base_ZVeto"));

        // Calculate the Mt (min/max) considering light leptons only
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("Mt_ElMuMin"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("Mt_ElMuMax"));
        // Including taus. Might be different due to the exta neutrino from the tau decay
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("Mt_LepMin"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("Mt_LepMax"));

        ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("Signal_Mll_Z1"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("Signal_Mll_Z2"));

        ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("Base_Mll_Z1"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("Base_Mll_Z2"));
        // Check whether the current tree is nominal or not
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<char>("isNominal", false));
        return StatusCode::SUCCESS;
    }
    StatusCode SUSY4LeptonAnalysisHelper::InitializeMultiplicities() {
        // Write out the particle multiplicity in the events
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("n_BaseElec"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("n_SignalElec"));

        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("n_BaseMuon"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("n_SignalMuon"));

        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("n_BaseLep"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("n_SignalLep"));

        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("n_BaseTau", m_systematics->ProcessObject(XAMPP::SelectionObject::Tau)));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("n_SignalTau", m_systematics->ProcessObject(XAMPP::SelectionObject::Tau)));

        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("n_BaseDiTau", m_systematics->ProcessObject(XAMPP::SelectionObject::DiTau)));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("n_SignalDiTau", m_systematics->ProcessObject(XAMPP::SelectionObject::DiTau)));

        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("n_SignalJets"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("n_BaseJets"));
        // Apend b-jets only if they are needed
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("n_BJets", m_systematics->ProcessObject(XAMPP::SelectionObject::BTag)));

        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("n_BasePhot", m_systematics->ProcessObject(XAMPP::SelectionObject::Photon)));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("n_SignalPhot", m_systematics->ProcessObject(XAMPP::SelectionObject::Photon)));

        m_dec_NLepBase = m_XAMPPInfo->GetVariableStorage<int>("n_BaseLep");
        m_dec_NLepSignal = m_XAMPPInfo->GetVariableStorage<int>("n_SignalLep");

        m_dec_NElecBase = m_XAMPPInfo->GetVariableStorage<int>("n_BaseElec");
        m_dec_NElecSignal = m_XAMPPInfo->GetVariableStorage<int>("n_SignalElec");

        m_dec_NMuonBase = m_XAMPPInfo->GetVariableStorage<int>("n_BaseMuon");
        m_dec_NMuonSignal = m_XAMPPInfo->GetVariableStorage<int>("n_SignalMuon");

        m_dec_NTauBase = m_XAMPPInfo->GetVariableStorage<int>("n_BaseTau");
        m_dec_NTauSignal = m_XAMPPInfo->GetVariableStorage<int>("n_SignalTau");

        m_dec_NDiTauBase = m_XAMPPInfo->GetVariableStorage<int>("n_BaseDiTau");
        m_dec_NDiTauSignal = m_XAMPPInfo->GetVariableStorage<int>("n_SignalDiTau");

        m_dec_NJetSignal = m_XAMPPInfo->GetVariableStorage<int>("n_SignalJets");
        m_dec_NJetBase = m_XAMPPInfo->GetVariableStorage<int>("n_BaseJets");

        m_dec_NPhotSignal = m_XAMPPInfo->GetVariableStorage<int>("n_SignalPhot");
        m_dec_NPhotBase = m_XAMPPInfo->GetVariableStorage<int>("n_BasePhot");

        m_dec_NBJets = m_XAMPPInfo->GetVariableStorage<int>("n_BJets");

        return StatusCode::SUCCESS;
    }

    unsigned int SUSY4LeptonAnalysisHelper::nSignalElectrons() const { return m_dec_NElecSignal->GetValue(); }
    unsigned int SUSY4LeptonAnalysisHelper::nSignalMuons() const { return m_dec_NMuonSignal->GetValue(); }
    unsigned int SUSY4LeptonAnalysisHelper::nBaselineElectrons() const { return m_dec_NElecBase->GetValue(); }
    unsigned int SUSY4LeptonAnalysisHelper::nBaselineMuons() const { return m_dec_NMuonBase->GetValue(); }
    unsigned int SUSY4LeptonAnalysisHelper::nSignalDiTaus() const { return m_dec_NDiTauSignal->GetValue(); }
    unsigned int SUSY4LeptonAnalysisHelper::nSignalTaus() const { return m_dec_NTauSignal->GetValue(); }
    unsigned int SUSY4LeptonAnalysisHelper::nSignalJets() const { return m_dec_NJetSignal->GetValue(); }
    unsigned int SUSY4LeptonAnalysisHelper::nBaselineDiTaus() const { return m_dec_NDiTauBase->GetValue(); }
    unsigned int SUSY4LeptonAnalysisHelper::nBaselineTaus() const { return m_dec_NTauBase->GetValue(); }
    unsigned int SUSY4LeptonAnalysisHelper::nBaselineJets() const { return m_dec_NJetBase->GetValue(); }
    StatusCode SUSY4LeptonAnalysisHelper::FillMultiplicities() {
        int NElecSig = m_electron_selection->GetSignalElectrons()->size();
        int NLElecBase = m_electron_selection->GetBaselineElectrons()->size();
        ATH_CHECK(m_dec_NElecBase->Store(NLElecBase));
        ATH_CHECK(m_dec_NElecSignal->Store(NElecSig));

        int NMuonSig = m_muon_selection->GetSignalMuons()->size();
        int NMuonBase = m_muon_selection->GetBaselineMuons()->size();

        ATH_CHECK(m_dec_NMuonBase->Store(NMuonBase));
        ATH_CHECK(m_dec_NMuonSignal->Store(NMuonSig));

        ATH_CHECK(m_dec_NLepBase->Store(NLElecBase + NMuonBase));
        ATH_CHECK(m_dec_NLepSignal->Store(NElecSig + NMuonSig));

        int NTauBase = m_tau_selection->GetBaselineTaus()->size();
        int NTauSig = m_tau_selection->GetSignalTaus()->size();
        ATH_CHECK(m_dec_NTauBase->Store(NTauBase));
        ATH_CHECK(m_dec_NTauSignal->Store(NTauSig));

        int NDiTauBase = 0;  // m_ditau_selection->GetBaselineDiTaus()->size();
        int NDiTauSig = 0;   // m_ditau_selection->GetSignalDiTaus()->size();
        ATH_CHECK(m_dec_NDiTauBase->Store(NDiTauBase));
        ATH_CHECK(m_dec_NDiTauSignal->Store(NDiTauSig));

        int NJetsSignal = m_jet_selection->GetSignalJets()->size();
        int NJetsBase = m_jet_selection->GetBaselineJets()->size();
        int NbJets = m_jet_selection->GetBJets()->size();

        ATH_CHECK(m_dec_NJetSignal->Store(NJetsSignal));
        ATH_CHECK(m_dec_NJetBase->Store(NJetsBase));
        ATH_CHECK(m_dec_NBJets->Store(NbJets));

        int NPhotBase = m_photon_selection->GetBaselinePhotons()->size();
        int NPhotSignal = m_photon_selection->GetSignalPhotons()->size();
        ATH_CHECK(m_dec_NPhotSignal->Store(NPhotSignal));
        ATH_CHECK(m_dec_NPhotBase->Store(NPhotBase));
        return StatusCode::SUCCESS;
    }
    StatusCode SUSY4LeptonAnalysisHelper::CalculateMeffVariable() {
        static XAMPP::Storage<xAOD::MissingET*>* dec_MET = m_XAMPPInfo->GetVariableStorage<xAOD::MissingET*>("MetTST");

        static XAMPP::Storage<float>* dec_SignalHtLep = m_XAMPPInfo->GetVariableStorage<float>("Signal_HtLep");
        static XAMPP::Storage<float>* dec_SignalHtJet = m_XAMPPInfo->GetVariableStorage<float>("Signal_HtJet");
        static XAMPP::Storage<float>* dec_SignalHtPhot = m_XAMPPInfo->GetVariableStorage<float>("Signal_HtPhot");
        static XAMPP::Storage<float>* dec_SignalMeff = m_XAMPPInfo->GetVariableStorage<float>("Signal_Meff");

        static XAMPP::Storage<float>* dec_BaseHtLep = m_XAMPPInfo->GetVariableStorage<float>("Base_HtLep");
        static XAMPP::Storage<float>* dec_BaseHtJet = m_XAMPPInfo->GetVariableStorage<float>("Base_HtJet");
        static XAMPP::Storage<float>* dec_BaseHtPhot = m_XAMPPInfo->GetVariableStorage<float>("Base_HtPhot");
        static XAMPP::Storage<float>* dec_BaseMeff = m_XAMPPInfo->GetVariableStorage<float>("Base_Meff");

        float Met = dec_MET->GetValue()->met();
        // calculate  the Ht  of the baseline objects first
        float BaseElecHt = CalculateHt(m_electron_selection->GetBaselineElectrons());
        float BaseMuonHt = CalculateHt(m_muon_selection->GetBaselineMuons());
        float BaseTauHt = CalculateHt(m_tau_selection->GetBaselineTaus());
        float BaseHtLep = BaseElecHt + BaseMuonHt + BaseTauHt;

        /// Calculate the baseline jet ht as the signal jet ht excluding
        /// all candidates potentially overlapping with loose-taus.
        float BaseJetHt = 0.;
        bool overlapping_tau = false;
        for (auto jet : *m_jet_selection->GetSignalJets()) {
            if (jet->pt() < m_JetHt_minPt) continue;
            if (acc_overlap_tau(*jet) > -1) {
                overlapping_tau = true;
                continue;
            }
            BaseJetHt += jet->pt();
        }
        float BasePhoHt = CalculateHt(m_photon_selection->GetBaselinePhotons());

        ATH_CHECK(dec_BaseHtJet->Store(BaseJetHt));
        ATH_CHECK(dec_BaseHtPhot->Store(BasePhoHt));
        ATH_CHECK(dec_BaseHtLep->Store(BaseHtLep));
        ATH_CHECK(dec_BaseMeff->Store(Met + BasePhoHt + BaseJetHt + BaseHtLep));

        float SignalElecHt =
            nSignalElectrons() != nBaselineElectrons() ? CalculateHt(m_electron_selection->GetSignalElectrons()) : BaseElecHt;
        float SignalMuonHt = nSignalMuons() != nBaselineMuons() ? CalculateHt(m_muon_selection->GetSignalMuons()) : BaseMuonHt;
        float SignalTauHt = nSignalTaus() != nBaselineTaus() ? CalculateHt(m_tau_selection->GetSignalTaus()) : BaseTauHt;
        float SignalHtLep = SignalElecHt + SignalMuonHt + SignalTauHt;
        float SignalJetHt = overlapping_tau ? CalculateHt(m_jet_selection->GetSignalJets(), m_JetHt_minPt) : BaseJetHt;
        float SignalPhoHt = m_dec_NPhotBase->GetValue() != m_dec_NPhotSignal->GetValue()
                                ? CalculateHt(m_photon_selection->GetBaselinePhotons())
                                : BasePhoHt;

        ATH_CHECK(dec_SignalHtJet->Store(SignalJetHt));
        ATH_CHECK(dec_SignalHtPhot->Store(SignalPhoHt));
        ATH_CHECK(dec_SignalHtLep->Store(SignalHtLep));
        ATH_CHECK(dec_SignalMeff->Store(Met + SignalPhoHt + SignalJetHt + SignalHtLep));

        return StatusCode::SUCCESS;
    }
    StatusCode SUSY4LeptonAnalysisHelper::InitializeMeffVariable() {
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("Signal_HtJet"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("Signal_HtLep"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("Signal_HtPhot", m_systematics->ProcessObject(XAMPP::SelectionObject::Photon)));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("Signal_Meff"));
        // Meff and HT using baseline leptons
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("Base_HtJet"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("Base_HtLep"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("Base_HtPhot", m_systematics->ProcessObject(XAMPP::SelectionObject::Photon)));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("Base_Meff"));
        return StatusCode::SUCCESS;
    }

    StatusCode SUSY4LeptonAnalysisHelper::ComputeEventVariables() {
        ATH_CHECK(FillMultiplicities());
        ATH_CHECK(fillModuleContainers());

        if (!isData() && doTruth()) {
            static XAMPP::ParticleStorage* TruthElecStore = m_XAMPPInfo->GetParticleStorage("Truth_Elec");
            static XAMPP::ParticleStorage* TruthMuonStore = m_XAMPPInfo->GetParticleStorage("Truth_Muon");
            static XAMPP::ParticleStorage* TruthTauStore = m_XAMPPInfo->GetParticleStorage("Truth_Tau");

            GetParentPdgId(m_truth_selection->GetTruthBaselineElectrons());
            GetParentPdgId(m_truth_selection->GetTruthBaselineMuons());
            GetParentPdgId(m_truth_selection->GetTruthBaselineTaus());

            ATH_CHECK(TruthElecStore->Fill(m_truth_selection->GetTruthBaselineElectrons()));
            ATH_CHECK(TruthMuonStore->Fill(m_truth_selection->GetTruthBaselineMuons()));
            ATH_CHECK(TruthTauStore->Fill(m_truth_selection->GetTruthBaselineTaus()));
        }

        static XAMPP::ParticleStorage* ElecStore = m_XAMPPInfo->GetParticleStorage("elec");
        static XAMPP::ParticleStorage* MuonStore = m_XAMPPInfo->GetParticleStorage("muon");
        static XAMPP::ParticleStorage* TauStore = m_XAMPPInfo->GetParticleStorage("tau");
        static XAMPP::ParticleStorage* JetStore = m_XAMPPInfo->GetParticleStorage("jet");

        ATH_MSG_DEBUG("Save the particles");
        ATH_CHECK(TauStore->Fill(m_tau_selection->GetPreTaus()));
        ATH_CHECK(ElecStore->Fill(m_electron_selection->GetPreElectrons()));
        ATH_CHECK(MuonStore->Fill(m_muon_selection->GetPreMuons()));
        ATH_CHECK(JetStore->Fill(m_jet_selection->GetSignalNoORJets()));

        if (m_ApplEventSkimming && nBaselineElectrons() + nBaselineTaus() + nBaselineMuons() < 4) return StatusCode::SUCCESS;
        ATH_CHECK(CalculateMeffVariable());
        static XAMPP::Storage<char>* dec_Nominal = m_XAMPPInfo->GetVariableStorage<char>("isNominal");
        ATH_CHECK(dec_Nominal->Store(m_systematics->GetNominal() == m_XAMPPInfo->GetSystematic()));
        static XAMPP::ParticleStorage* RecoCandStore = m_XAMPPInfo->GetParticleStorage("RecoCandidates");

        static XAMPP::Storage<float>* dec_MtLepMin = m_XAMPPInfo->GetVariableStorage<float>("Mt_LepMin");
        static XAMPP::Storage<float>* dec_MtLepMax = m_XAMPPInfo->GetVariableStorage<float>("Mt_LepMax");

        static XAMPP::Storage<float>* dec_MtLightLepMin = m_XAMPPInfo->GetVariableStorage<float>("Mt_ElMuMin");
        static XAMPP::Storage<float>* dec_MtLightLepMax = m_XAMPPInfo->GetVariableStorage<float>("Mt_ElMuMax");

        static XAMPP::Storage<xAOD::MissingET*>* dec_MET = m_XAMPPInfo->GetVariableStorage<xAOD::MissingET*>("MetTST");

        // Calculate the Mt for each particle individually
        ATH_MSG_DEBUG("Calculate the Mt separately for each object");
        CalculateMt(m_electron_selection->GetPreElectrons(), dec_MET);
        CalculateMt(m_muon_selection->GetPreMuons(), dec_MET);
        CalculateMt(m_tau_selection->GetPreTaus(), dec_MET);

        xAOD::IParticleContainer Lep(SG::VIEW_ELEMENTS);
        for (const auto E : *m_electron_selection->GetBaselineElectrons()) Lep.push_back(E);
        for (const auto M : *m_muon_selection->GetBaselineMuons()) Lep.push_back(M);

        // Do the Z-veto  check only with electrons and muons
        ATH_CHECK(PassZVeto(Lep));

        for (const auto T : *m_tau_selection->GetBaselineTaus()) Lep.push_back(T);

        float Light_MtMin(1.e16), Lep_MtMin(1.e16), Light_MtMax(-1.), Lep_MtMax(-1.);
        static FloatAccessor acc_MT("MT");

        for (const auto& L : Lep) {
            if (!IsSignalLepton(L)) continue;
            if (L->type() != xAOD::Type::ObjectType::Tau) {
                if (Light_MtMin > acc_MT(*L)) Light_MtMin = acc_MT(*L);
                if (Light_MtMax < acc_MT(*L)) Light_MtMax = acc_MT(*L);
            }
            if (Lep_MtMin > acc_MT(*L)) Lep_MtMin = acc_MT(*L);
            if (Lep_MtMax < acc_MT(*L)) Lep_MtMax = acc_MT(*L);
        }
        ATH_CHECK(dec_MtLightLepMin->Store(Light_MtMin));
        ATH_CHECK(dec_MtLightLepMax->Store(Light_MtMax));
        ATH_CHECK(dec_MtLepMin->Store(Lep_MtMin));
        ATH_CHECK(dec_MtLepMax->Store(Lep_MtMax));

        ATH_CHECK(RecoCandStore->Fill(m_ParticleConstructor->GetSubContainer("InvariantMomenta")));
        ATH_MSG_DEBUG("The event is finished. Take off");

        return StatusCode::SUCCESS;
    }

    StatusCode SUSY4LeptonAnalysisHelper::initLightLeptonTrees() {
        ATH_CHECK(m_XAMPPInfo->BookParticleStorage("elec"));
        ATH_CHECK(m_XAMPPInfo->BookParticleStorage("muon"));

        std::vector<std::string> LepCharVar{"signal", m_electron_selection->GetElectronDecorations()->passBaseline.getDecorationString(),
                                            "isol"};

        if (m_ApplyIsoCorrect) LepCharVar.push_back("CORR_isol");
        std::vector<std::string> LepFloatVar{"charge", "z0sinTheta", "d0sig", "MT"};
        std::vector<std::string> LepIntVar;
        std::vector<std::string> ElecTruthVar;
        if (!isData()) {
            LepIntVar.push_back("IFFClassType");
            LepIntVar.push_back("truthOrigin");
            LepIntVar.push_back("truthType");
            ElecTruthVar.push_back("firstEgMotherTruthType");
            ElecTruthVar.push_back("firstEgMotherTruthOrigin");
            ElecTruthVar.push_back("firstEgMotherPdgId");
        }
        XAMPP::ParticleStorage* ElecStore = m_XAMPPInfo->GetParticleStorage("elec");
        XAMPP::ParticleStorage* MuonStore = m_XAMPPInfo->GetParticleStorage("muon");

        ElecStore->pipeVariableToAllTrees(
            StringVector{m_electron_selection->GetElectronDecorations()->passBaseline.getDecorationString(), "CORR_isol"});
        MuonStore->pipeVariableToAllTrees(
            StringVector{m_muon_selection->GetMuonDecorations()->passBaseline.getDecorationString(), "CORR_isol"});

        ATH_CHECK(m_XAMPPInfo->createSystematicGroup("ElectronGroup", SelectionObject::Electron));
        ATH_CHECK(m_XAMPPInfo->createSystematicGroup("MuonGroup", SelectionObject::Muon));
        ATH_CHECK(ElecStore->setSystematicGroup("ElectronGroup"));
        ATH_CHECK(MuonStore->setSystematicGroup("MuonGroup"));

        ATH_CHECK(m_triggers->SaveObjectMatching(ElecStore, xAOD::Type::ObjectType::Electron));
        ATH_CHECK(m_triggers->SaveObjectMatching(MuonStore, xAOD::Type::ObjectType::Muon));

        ATH_CHECK(ElecStore->SaveVariable<float>(LepFloatVar));
        ATH_CHECK(MuonStore->SaveVariable<float>(LepFloatVar));

        ATH_CHECK(ElecStore->SaveVariable<char>("QFlip"));
        ATH_CHECK(ElecStore->SaveVariable<char>(LepCharVar));
        ATH_CHECK(MuonStore->SaveVariable<char>(LepCharVar));

        ATH_CHECK(ElecStore->SaveVariable<int>(LepIntVar));
        ATH_CHECK(MuonStore->SaveVariable<int>(LepIntVar));

        ATH_CHECK(ElecStore->SaveVariable<int>(ElecTruthVar));

        for (auto& Cone : getIsolationCones(m_IsoTool->getMuonWPs())) {
            ATH_CHECK(MuonStore->SaveVariable<float>(Cone));
            MuonStore->pipeVariableToAllTrees(Cone);
            if (m_ApplyIsoCorrect) ATH_CHECK(MuonStore->SaveVariable<float>(Cone + "_ORIG"));
        }
        for (auto& Cone : getIsolationCones(m_IsoTool->getElectronWPs())) {
            ATH_CHECK(ElecStore->SaveVariable<float>(Cone));
            ElecStore->pipeVariableToAllTrees(Cone);
            if (m_ApplyIsoCorrect) ATH_CHECK(ElecStore->SaveVariable<float>(Cone + "_ORIG"));
        }

        for (const auto& iso_tool : m_isoWPs) {
            for (const auto& WP : iso_tool->getMuonWPs()) {
                ATH_MSG_INFO("Muon working point " << WP->name());
                m_isoTester_Muons.push_back(IsolationTester(WP->name(), iso_tool));
                ATH_CHECK(MuonStore->SaveVariable<char>("isol_" + WP->name()));
            }
            for (const auto& WP : iso_tool->getElectronWPs()) {
                ATH_MSG_INFO("Electron working point " << WP->name());
                m_isoTester_Elecs.push_back(IsolationTester(WP->name(), iso_tool));
                ATH_CHECK(ElecStore->SaveVariable<char>("isol_" + WP->name()));
            }
        }
        return StatusCode::SUCCESS;
    }
    StatusCode SUSY4LeptonAnalysisHelper::initTauTree() {
        ATH_CHECK(m_XAMPPInfo->BookParticleStorage("tau"));
        XAMPP::ParticleStorage* TauStore = m_XAMPPInfo->GetParticleStorage("tau");
        StringVector FloatVars{"charge",      "MT",   "Width", "BDTEleScoreSigTrans",  //"BDTJetScoreSigTrans",
                               "RNNJetScore", "jetPt"};
        StringVector CharVars{
            m_tau_selection->GetTauDecorations()->passBaseline.getDecorationString(), "signal",  //"passRNNID"
        };
        StringVector IntVars{"NTrks", "NTrksJet", "Index"};
        if (!isData()) {
            StringVector truthInt{"truthOrigin", "truthType", "PartonTruthLabelID", "ConeTruthLabelID"};
            IntVars.insert(IntVars.end(), truthInt.begin(), truthInt.end());
        }
        ATH_CHECK(TauStore->SaveVariable<float>(FloatVars));
        ATH_CHECK(TauStore->SaveVariable<char>(CharVars));
        ATH_CHECK(TauStore->SaveVariable<int>(IntVars));
        ATH_CHECK(m_XAMPPInfo->createSystematicGroup("TauGroup", SelectionObject::Tau));
        ATH_CHECK(TauStore->setSystematicGroup("TauGroup"));
        TauStore->pipeVariableToAllTrees(m_tau_selection->GetTauDecorations()->passBaseline.getDecorationString());
        return StatusCode::SUCCESS;
    }
    StatusCode SUSY4LeptonAnalysisHelper::initJetTree() {
        ATH_CHECK(m_XAMPPInfo->BookParticleStorage("jet"));
        XAMPP::ParticleStorage* JetStore = m_XAMPPInfo->GetParticleStorage("jet");
        ATH_CHECK(m_XAMPPInfo->createSystematicGroup("JetGroup", SelectionObject::Jet));
        ATH_CHECK(JetStore->setSystematicGroup("JetGroup"));
        StringVector char_vars{"passOR", "bjet"};

        JetStore->pipeVariableToAllTrees(StringVector{"passOR", "overlapingTauIdx"});

        ATH_CHECK(JetStore->SaveVariable<char>(char_vars));
        if (m_systematics->ProcessObject(XAMPP::SelectionObject::Tau)) { ATH_CHECK(JetStore->SaveVariable<int>("overlapingTauIdx")); }
        ATH_CHECK(JetStore->SaveVariable<float>("Jvt"));
        ATH_CHECK(JetStore->SaveVariable<int>("NTrks"));
        if (!isData()) { ATH_CHECK(JetStore->SaveVariable<int>("HadronConeExclTruthLabelID")); }

        return StatusCode::SUCCESS;
    }
    StatusCode SUSY4LeptonAnalysisHelper::initPhotonTree() {
        if (!m_systematics->ProcessObject(XAMPP::SelectionObject::Photon)) return true;
        ATH_CHECK(m_XAMPPInfo->BookParticleStorage("pho", true));
        XAMPP::ParticleStorage* Photons = m_XAMPPInfo->GetParticleStorage("pho");
        if (!isData()) {
            ATH_CHECK(Photons->SaveVariable<int>("truthOrigin"));
            ATH_CHECK(Photons->SaveVariable<int>("truthType"));
        }
        ATH_CHECK(Photons->SaveVariable<char>("signal"));
        ATH_CHECK(Photons->SaveVariable<char>("isol"));

        ATH_CHECK(Photons->SaveVariable<float>("ptcone20"));
        ATH_CHECK(Photons->SaveVariable<float>("topoetcone20"));

        return StatusCode::SUCCESS;
    }
    StatusCode SUSY4LeptonAnalysisHelper::initTruthTree() {
        if (isData()) return StatusCode::SUCCESS;

        ATH_CHECK(m_XAMPPInfo->BookCommonParticleStorage("Truth_Elec"));
        ATH_CHECK(m_XAMPPInfo->BookCommonParticleStorage("Truth_Muon"));
        ATH_CHECK(m_XAMPPInfo->BookCommonParticleStorage("Truth_Tau"));

        XAMPP::ParticleStorage* TruthElec = m_XAMPPInfo->GetParticleStorage("Truth_Elec");
        XAMPP::ParticleStorage* TruthMuon = m_XAMPPInfo->GetParticleStorage("Truth_Muon");
        XAMPP::ParticleStorage* TruthTau = m_XAMPPInfo->GetParticleStorage("Truth_Tau");

        ATH_CHECK(TruthElec->SaveVariable<float>("charge"));
        ATH_CHECK(TruthMuon->SaveVariable<float>("charge"));
        ATH_CHECK(TruthTau->SaveVariable<float>("charge"));

        ATH_CHECK(TruthElec->SaveVariable<int>("parent_pdgId"));
        ATH_CHECK(TruthMuon->SaveVariable<int>("parent_pdgId"));
        ATH_CHECK(TruthTau->SaveVariable<int>("parent_pdgId"));

        ATH_CHECK(TruthTau->SaveVariable<char>("isHadronic"));
        ATH_CHECK(TruthTau->SaveVariable<float>("InitPt"));

        return StatusCode::SUCCESS;
    }

    StatusCode SUSY4LeptonAnalysisHelper::initRecoParticleTree() {
        ATH_CHECK(m_XAMPPInfo->BookParticleStorage("RecoCandidates", true));
        XAMPP::ParticleStorage* RecoCandStore = m_XAMPPInfo->GetParticleStorage("RecoCandidates");
        StringVector IntVars{"pdgId", "nConstituents"};
        StringVector CharVars{"SameFlavour", "signal"};
        ATH_CHECK(RecoCandStore->SaveVariable<int>(IntVars));
        ATH_CHECK(RecoCandStore->SaveVariable<char>(CharVars));
        ATH_CHECK(RecoCandStore->SaveVariable<float>("charge"));
        ATH_CHECK(RecoCandStore->SaveVariable<unsigned int>("containerMask"));

        return StatusCode::SUCCESS;
    }
    StatusCode SUSY4LeptonAnalysisHelper::PassZVeto(xAOD::IParticleContainer& Leptons) {
        static XAMPP::Storage<int>* dec_ZVeto = m_XAMPPInfo->GetVariableStorage<int>("ZVeto");
        static XAMPP::Storage<int>* dec_BaseZVeto = m_XAMPPInfo->GetVariableStorage<int>("Base_ZVeto");

        static XAMPP::Storage<float>* dec_Mll_Z1 = m_XAMPPInfo->GetVariableStorage<float>("Signal_Mll_Z1");
        static XAMPP::Storage<float>* dec_Mll_Z2 = m_XAMPPInfo->GetVariableStorage<float>("Signal_Mll_Z2");

        static XAMPP::Storage<float>* dec_Base_Mll_Z1 = m_XAMPPInfo->GetVariableStorage<float>("Base_Mll_Z1");
        static XAMPP::Storage<float>* dec_Base_Mll_Z2 = m_XAMPPInfo->GetVariableStorage<float>("Base_Mll_Z2");

        int ZVeto(XAMPP::ZVeto::Pass), Base_ZVeto(XAMPP::ZVeto::Pass);
        const xAOD::Particle* Mll_Z1 = nullptr;
        const xAOD::Particle* Mll_Z2 = nullptr;
        const xAOD::Particle* Base_Mll_Z1 = nullptr;
        const xAOD::Particle* Base_Mll_Z2 = nullptr;

        // Obtain the two and four lepton invariant masses, if there any
        ATH_MSG_DEBUG("Calculate invariant lepton momenta out of " << Leptons.size() << " particles");

        xAOD::IParticleContainer Signals(SG::VIEW_ELEMENTS);
        for (const auto L : Leptons) {
            if (IsSignalLepton(L)) { Signals.push_back(L); }
        }
        int NBaseLep = Leptons.size();
        int NSignalLep = Signals.size();
        Base_ZVeto = GetZVeto(Leptons, m_ZMassWindow);
        // If nothing has changed between baseline and signal why evaluate the thing again
        ZVeto = NBaseLep == NSignalLep ? Base_ZVeto : GetZVeto(Signals, m_ZMassWindow);

        ATH_CHECK(m_ParticleConstructor->CreateSubContainer("InvariantMomenta"));
        ConstructInvariantMomenta(Leptons, m_ParticleConstructor.get(),
                                  [this](const xAOD::IParticle* P) -> bool { return IsSignalLepton(P); });
        m_ParticleConstructor->DetachSubContainer();

        float BestDiZmass(1.e16), BestDiZmassBase(1.e16);
        xAOD::ParticleContainer* InvLepMom = m_ParticleConstructor->GetSubContainer("InvariantMomenta");
        ATH_MSG_DEBUG("Constructed in total " << InvLepMom->size() << " particles");

        xAOD::ParticleContainer::const_iterator Itr_Begin = InvLepMom->begin();
        xAOD::ParticleContainer::const_iterator Itr_End = InvLepMom->end();
        for (xAOD::ParticleContainer::const_iterator Itr = Itr_Begin; Itr != Itr_End; ++Itr) {
            const xAOD::Particle* ZCand = (*Itr);
            if (!isZCandidate(ZCand)) { continue; }
            float DeltaM_Z = std::abs(ZCand->m() - XAMPP::Z_MASS);
            // Impossible to build  up two Z candidates
            if (NBaseLep < 4) {
                if (!Base_Mll_Z1 || std::abs(Base_Mll_Z1->m() - XAMPP::Z_MASS) > DeltaM_Z) { Base_Mll_Z1 = ZCand; }
                if (IsSignalLepton(ZCand) && (!Mll_Z1 || fabs(Mll_Z1->m() - XAMPP::Z_MASS) > DeltaM_Z)) { Mll_Z1 = ZCand; }
                continue;
            }
            // Check it for the signal leptons as well
            else if (NSignalLep < 4 && IsSignalLepton(ZCand) && (!Mll_Z1 || fabs(Mll_Z1->m() - XAMPP::Z_MASS) > DeltaM_Z)) {
                Mll_Z1 = ZCand;
            }
            // Check if we can find another pair
            bool FoundSecondPair = false;
            // ZVeto is determined -> Go to the ZZ seleciton
            for (xAOD::ParticleContainer::const_iterator Itr1 = Itr_Begin; Itr1 != Itr; ++Itr1) {
                const xAOD::Particle* ZCand1 = (*Itr1);
                // Reject everything uneeded
                if (!isZCandidate(ZCand1) || HasCommonConstructingParticles(ZCand, ZCand1)) { continue; }
                FoundSecondPair = true;
                float DeltaM_Z2 = fabs(ZCand1->m() - XAMPP::Z_MASS);
                if (DeltaM_Z + DeltaM_Z2 < BestDiZmassBase) {
                    Base_Mll_Z1 = DeltaM_Z <= DeltaM_Z2 ? ZCand : ZCand1;
                    Base_Mll_Z2 = DeltaM_Z > DeltaM_Z2 ? ZCand : ZCand1;
                    BestDiZmassBase = DeltaM_Z + DeltaM_Z2;
                }
                if (IsSignalLepton(ZCand) && IsSignalLepton(ZCand1) && (DeltaM_Z + DeltaM_Z2) < BestDiZmass) {
                    Mll_Z1 = DeltaM_Z <= DeltaM_Z2 ? ZCand : ZCand1;
                    Mll_Z2 = DeltaM_Z > DeltaM_Z2 ? ZCand : ZCand1;
                    BestDiZmass = DeltaM_Z + DeltaM_Z2;
                }
            }
            if (!FoundSecondPair) {
                if (!Base_Mll_Z1 || DeltaM_Z < std::abs(Base_Mll_Z1->m() - XAMPP::Z_MASS)) { Base_Mll_Z1 = ZCand; }
                if (IsSignalLepton(ZCand) && (!Mll_Z1 || DeltaM_Z < std::abs(Mll_Z1->m() - XAMPP::Z_MASS))) { Mll_Z1 = ZCand; }
            }
        }
        ATH_CHECK(dec_ZVeto->Store(ZVeto));
        ATH_CHECK(dec_BaseZVeto->Store(Base_ZVeto));

        ATH_CHECK(dec_Mll_Z1->Store(Mll_Z1 ? Mll_Z1->m() : -1.e3));
        ATH_CHECK(dec_Mll_Z2->Store(Mll_Z2 ? Mll_Z2->m() : -1.e3));

        ATH_CHECK(dec_Base_Mll_Z1->Store(Base_Mll_Z1 ? Base_Mll_Z1->m() : -1.e3));
        ATH_CHECK(dec_Base_Mll_Z2->Store(Base_Mll_Z2 ? Base_Mll_Z2->m() : -1.e3));

        return StatusCode::SUCCESS;
    }
    bool SUSY4LeptonAnalysisHelper::IsSignalLepton(const xAOD::IParticle* P) const {
        if (P->type() == xAOD::Type::ObjectType::Electron) {
            return m_electron_selection->GetElectronDecorations()->passSignal(*P) &&
                   m_electron_selection->GetElectronDecorations()->passIsolation(*P);
        } else if (P->type() == xAOD::Type::ObjectType::Muon) {
            return m_muon_selection->GetMuonDecorations()->passSignal(*P) && m_muon_selection->GetMuonDecorations()->passIsolation(*P);
        } else if (P->type() == xAOD::Type::ObjectType::Tau) {
            return m_tau_selection->GetTauDecorations()->passSignal(*P);
        } else if (P->type() == xAOD::Type::ObjectType::Jet) {
            return m_jet_selection->GetJetDecorations()->passSignal(*P);
        } else if (P->type() == xAOD::Type::ObjectType::Photon) {
            return m_photon_selection->GetPhotonDecorations()->passSignal();
        }
        static CharAccessor acc_signal("signal");
        if (acc_signal.isAvailable(*P)) return acc_signal(*P);
        return false;
    }
    StatusCode SUSY4LeptonAnalysisHelper::initializeModuleContainers() {
        const StringVector container_names{"baseline_elec", "baseline_muon", "baseline_tau", "signal_elec", "signal_muon", "signal_tau"};
        for (const auto& container : container_names) { ATH_CHECK(m_XAMPPInfo->BookParticleStorage(container, false, false, false)); }
        return StatusCode::SUCCESS;
    }
    StatusCode SUSY4LeptonAnalysisHelper::fillModuleContainers() {
        static XAMPP::ParticleStorage* baseline_elec = m_XAMPPInfo->GetParticleStorage("baseline_elec");
        static XAMPP::ParticleStorage* baseline_muon = m_XAMPPInfo->GetParticleStorage("baseline_muon");
        static XAMPP::ParticleStorage* baseline_tau = m_XAMPPInfo->GetParticleStorage("baseline_tau");

        static XAMPP::ParticleStorage* signal_elec = m_XAMPPInfo->GetParticleStorage("signal_elec");
        static XAMPP::ParticleStorage* signal_muon = m_XAMPPInfo->GetParticleStorage("signal_muon");
        static XAMPP::ParticleStorage* signal_tau = m_XAMPPInfo->GetParticleStorage("signal_tau");

        ATH_CHECK(baseline_elec->Fill(m_electron_selection->GetBaselineElectrons()));
        ATH_CHECK(baseline_muon->Fill(m_muon_selection->GetBaselineMuons()));
        ATH_CHECK(baseline_tau->Fill(m_tau_selection->GetBaselineTaus()));

        ATH_CHECK(signal_elec->Fill(m_electron_selection->GetSignalElectrons()));
        ATH_CHECK(signal_muon->Fill(m_muon_selection->GetSignalMuons()));
        ATH_CHECK(signal_tau->Fill(m_tau_selection->GetSignalTaus()));

        return StatusCode::SUCCESS;
    }

}  // namespace XAMPP
