#include <IsolationSelection/IsolationWP.h>
#include <TRandom3.h>
#include <XAMPPbase/ReconstructedParticles.h>
#include <XAMPPmultilep/AnalysisUtils.h>
#include <xAODPrimitives/IsolationHelpers.h>

namespace XAMPP {

    static IntAccessor acc_NLep("nConstituents");
    static CharAccessor acc_SF("SameFlavour");

    TVector3 GetRestBoost(const xAOD::IParticle& Part) { return GetRestBoost(&Part); }
    TVector3 GetRestBoost(const xAOD::IParticle* Part) { return GetRestBoost(Part->p4()); }
    TVector3 GetRestBoost(TLorentzVector Vec) { return Vec.BoostVector(); }

    void CalculateMt(xAOD::IParticleContainer& Particles, XAMPP::Storage<xAOD::MissingET*>* dec_MET) { CalculateMt(&Particles, dec_MET); }
    void CalculateMt(const xAOD::IParticleContainer* Particles, XAMPP::Storage<xAOD::MissingET*>* dec_MET) {
        static FloatDecorator dec_MT("MT");
        for (const auto& P : *Particles) dec_MT(*P) = ComputeMt(P, dec_MET);
    }

    float InvariantMass(const xAOD::IParticleContainer* pc) { return CombinedVector(pc).M(); }

    float InvariantMass(const xAOD::IParticleContainer* pc, std::initializer_list<long unsigned int> entries) {
        return CombinedVector(pc, entries).M();
    }

    float CombinedM(const xAOD::IParticleContainer* pc, std::initializer_list<long unsigned int> entries) {
        return CombinedVector(pc, entries).M();
    }

    float CombinedPt(const xAOD::IParticleContainer* pc, std::initializer_list<long unsigned int> entries) {
        return CombinedVector(pc, entries).Pt();
    }

    float CombinedPhi(const xAOD::IParticleContainer* pc, std::initializer_list<long unsigned int> entries) {
        return CombinedVector(pc, entries).Phi();
    }

    float CombinedEta(const xAOD::IParticleContainer* pc, std::initializer_list<long unsigned int> entries) {
        return CombinedVector(pc, entries).Eta();
    }

    float CombinedE(const xAOD::IParticleContainer* pc, std::initializer_list<long unsigned int> entries) {
        return CombinedVector(pc, entries).E();
    }

    TLorentzVector CombinedVector(const xAOD::IParticleContainer* pc) {
        TLorentzVector v(0, 0, 0, 0);
        for (const auto& ipc : *pc) v += ipc->p4();
        return v;
    }

    TLorentzVector CombinedVector(const xAOD::IParticleContainer* pc, std::initializer_list<long unsigned int> entries) {
        TLorentzVector v(0, 0, 0, 0);
        for (const auto i : entries)
            if (i < pc->size())
                v += (*pc)[i]->p4();
            else
                Warning("CombinedVector", "Element %lu is exceeding the size of container %lu", i, pc->size());
        return v;
    }

    float ChargeProduct(const xAOD::IParticleContainer* pc, std::initializer_list<long unsigned int> entries) {
        float Q = 1.f;
        for (const auto i : entries)
            if (i < pc->size())
                Q *= Charge((*pc)[i]);
            else
                Warning("ChargeProduct", "Element %lu is exceeding the size of container %lu", i, pc->size());
        return Q;
    }

    float DeltaR(const xAOD::IParticleContainer* pc, long unsigned int i, long unsigned int j) {
        if (i < pc->size() && j < pc->size())
            return (*pc)[i]->p4().DeltaR((*pc)[j]->p4());
        else
            Warning("DeltaR", "Requested elements %lu or %lu are out of the container range %lu", i, j, pc->size());

        return -11;
    }

    float DeltaEta(const xAOD::IParticleContainer* pc, long unsigned int i, long unsigned int j) {
        if (i < pc->size() && j < pc->size())
            return std::fabs((*pc)[i]->p4().Eta() - (*pc)[j]->p4().Eta());
        else
            Warning("DeltaEta", "Requested elements %lu or %lu are out of the container range %lu", i, j, pc->size());

        return -11;
    }

    float DeltaPhi(const xAOD::IParticleContainer* pc, long unsigned int i, long unsigned int j) {
        if (i < pc->size() && j < pc->size())
            return (*pc)[i]->p4().DeltaPhi((*pc)[j]->p4());
        else
            Warning("DeltaPhi", "Requested elements %lu or %lu are out of the container range %lu", i, j, pc->size());

        return -11;
    }

    float DeltaPhiNorm(const xAOD::IParticleContainer* pc, long unsigned int i, long unsigned int j) {
        return std::fabs(DeltaPhi(pc, i, j)) / TMath::Pi();
    }
    xAOD::Particle* ConstructNeutrino(xAOD::MissingET* Met, float Pz, XAMPP::IReconstructedParticles* Constructor) {
        xAOD::Particle* N = Constructor->CreateEmptyParticle();
        float E = TMath::Sqrt(TMath::Power(Met->met(), 2) + Pz * Pz);
        N->setPxPyPzE(Met->mpx(), Met->mpy(), Pz, E);
        N->setCharge(0.);
        N->setPdgId(12);
        return N;
    }
    void FillWMomentum(const xAOD::IParticle* Lep, xAOD::Particle* Neut, xAOD::Particle* WCand,
                       std::function<bool(const xAOD::IParticle*)> signalFunc) {
        static IntDecorator dec_NLep("nConstituents");
        dec_NLep(*Neut) = 1;
        AddFourMomenta(Lep, Neut, WCand, signalFunc);
        WCand->setPdgId(24);
        WCand->setCharge(Charge(Lep));
    }
    bool LeptonicWReconstruction(const xAOD::IParticle* Lep, XAMPP::Storage<XAMPPmet>* dec_MET, XAMPP::IReconstructedParticles* Constructor,
                                 std::function<bool(const xAOD::IParticle*)> signalFunc, unsigned int lep_idx) {
        xAOD::MissingET* MET = dec_MET->GetValue();
        static CharDecorator dec_IsPos("UsesPosSolution");
        static UIntDecorator dec_ContMask("containerMask");

        // Try to solve the equation system for Pz of the neutrino first using the constrain of the W Mass
        // M_{W}^{2} = m_{L}^{2} + 2(E_{l}E_{nu} - P^{l}_{T}P^{nu}_{T} cos(dPhi) - p_{z}^{nu}p_{z}^{l})
        TRandom3 Rnd(Lep->pt() + MET->met());
        // To avoid sharp line smear the W mass using a BreitWigner distribution
        float W = Rnd.BreitWigner(XAMPP::W_MASS, XAMPP::W_WIDTH);

        // Constant to make the live easier
        float dPhi = xAOD::P4Helpers::deltaPhi(Lep, MET);
        float A = (TMath::Power(W, 2) - TMath::Power(Lep->m(), 2) + 2 * Lep->pt() * MET->met() * TMath::Cos(dPhi)) / (2 * Lep->e());
        float LepBetaZ = Lep->p4().Pz() / Lep->e();

        // Put the equation in form B + Cx + Dx^{2} = 0
        float B = TMath::Power(A, 2) - TMath::Power(MET->met(), 2);
        float C = 2 * A * LepBetaZ;
        float D = TMath::Power(LepBetaZ, 2) - 1.;
        // C^{2} - 4BD > 0
        float QuadDisc = TMath::Power(C, 2) - 4 * B * D;
        if (QuadDisc < 0.) return false;
        // The D == 0 -> Lepton travels along the beam direction -> Nearly impossible to detect this
        if (D == 0.)
            return false;
        else if (QuadDisc > 0) {
            float NuPz1 = (-C + TMath::Sqrt(QuadDisc)) / (2 * D);
            float NuPz2 = (-C - TMath::Sqrt(QuadDisc)) / (2 * D);
            xAOD::Particle* Neut = ConstructNeutrino(MET, NuPz1, Constructor);
            xAOD::Particle* Neut1 = ConstructNeutrino(MET, NuPz2, Constructor);

            dec_ContMask(*Neut) = 1 << lep_idx;
            dec_ContMask(*Neut1) = 1 << lep_idx;

            xAOD::Particle* WCand = Constructor->CreateEmptyParticle();
            FillWMomentum(Lep, Neut, WCand, signalFunc);
            dec_IsPos(*WCand) = (Neut->e() > Neut1->e());
            dec_ContMask(*WCand) = 1 << lep_idx;

            xAOD::Particle* WCand1 = Constructor->CreateEmptyParticle();
            FillWMomentum(Lep, Neut1, WCand1, signalFunc);
            dec_IsPos(*WCand1) = (Neut->e() < Neut1->e());
            dec_ContMask(*WCand1) = 1 << lep_idx;

        } else {
            float NuPz = -C / 2 * D;
            xAOD::Particle* Neut = ConstructNeutrino(MET, NuPz, Constructor);
            dec_ContMask(*Neut) = 1 << lep_idx;
            xAOD::Particle* WCand = Constructor->CreateEmptyParticle();
            FillWMomentum(Lep, Neut, WCand, signalFunc);
            dec_IsPos(*WCand) = true;
            dec_ContMask(*WCand) = 1 << lep_idx;
        }
        return true;
    }
    bool LeptonicWReconstruction(xAOD::IParticleContainer* Leptons, XAMPP::Storage<xAOD::MissingET*>* dec_MET,
                                 XAMPP::IReconstructedParticles* Constructor, std::function<bool(const xAOD::IParticle*)> signalFunc) {
        bool Constructed = false;
        unsigned int pos = 0;
        for (auto Lep : *Leptons) {
            Constructed |= LeptonicWReconstruction(Lep, dec_MET, Constructor, signalFunc, pos);
            ++pos;
        }
        return Constructed;
    }
    bool LeptonicWReconstruction(xAOD::IParticleContainer& Leptons, XAMPP::Storage<xAOD::MissingET*>* dec_MET,
                                 XAMPP::IReconstructedParticles* Constructor, std::function<bool(const xAOD::IParticle*)> signalFunc) {
        return LeptonicWReconstruction(&Leptons, dec_MET, Constructor, signalFunc);
    }

    bool LeptonicWReconstruction(xAOD::IParticleContainer& Leptons, XAMPP::Storage<xAOD::MissingET*>* dec_MET,
                                 asg::AnaToolHandle<XAMPP::IReconstructedParticles>& Constructor,
                                 std::function<bool(const xAOD::IParticle*)> signalFunc) {
        return LeptonicWReconstruction(Leptons, dec_MET, Constructor.get(), signalFunc);
    }
    bool LeptonicWReconstruction(xAOD::IParticleContainer* Leptons, XAMPP::Storage<xAOD::MissingET*>* dec_MET,
                                 asg::AnaToolHandle<XAMPP::IReconstructedParticles>& Constructor,
                                 std::function<bool(const xAOD::IParticle*)> signalFunc) {
        return LeptonicWReconstruction(Leptons, dec_MET, Constructor.get(), signalFunc);
    }
    xAOD::Particle* ConstructNeutrino(xAOD::MissingET* Met, float Pz, asg::AnaToolHandle<XAMPP::IReconstructedParticles>& Constructor) {
        return ConstructNeutrino(Met, Pz, Constructor.get());
    }
    void GetParentPdgId(const xAOD::TruthParticleContainer* Container) {
        static SG::AuxElement::Decorator<int> dec_ParentPdgId("parent_pdgId");
        for (const auto Particle : *Container) {
            unsigned int PdgId = -999999;
            const xAOD::TruthParticle* Part = GetFirstChainLink(Particle);
            for (size_t p = 0; p < Part->nParents(); ++p) {
                if (!Part->parent(p)) continue;
                PdgId = Part->parent(p)->pdgId();
            }
            dec_ParentPdgId(*Particle) = PdgId;
        }
    }
    bool isZCandidate(const xAOD::Particle* ZCand) {
        return (ZCand->pdgId() % 15 != 0 && acc_NLep(*ZCand) == 2 && Charge(ZCand) == 0 && acc_SF(*ZCand));
    }
    int GetParentPdgId(const xAOD::TruthParticle* In) {
        unsigned int PdgId = -999999;
        const xAOD::TruthParticle* Part = GetFirstChainLink(In);
        for (size_t p = 0; p < Part->nParents(); ++p) {
            if (!Part->parent(p)) continue;
            PdgId = Part->parent(p)->pdgId();
        }
        return PdgId;
    }

    void GetHadronicChildren(const xAOD::TruthParticle* Boson, xAOD::TruthParticleContainer& ContToPush) {
        if (!isEWboson(Boson)) return;
        static IntDecorator dec_ParentPdgId("parent_pdgId");

        for (size_t c = 0; c < Boson->nChildren(); ++c) {
            // Yeah it is not nice to that, but hopefully no one will get the idea to change propertiers of the object
            // He'll experience the consequence quite fast
            xAOD::TruthParticle* Child = const_cast<xAOD::TruthParticle*>(Boson->child(c));
            if (!Child || !Child->isQuark()) continue;
            ContToPush.push_back(Child);
            dec_ParentPdgId(*Child) = Boson->pdgId();
        }
    }
    std::vector<std::string> getIsolationCones(const std::vector<CP::IsolationWP*>& WPs) {
        std::vector<std::string> coneNames;
        for (const auto& W : WPs) {
            for (auto& C : W->conditions()) {
                std::string cone = std::string(xAOD::Iso::toString(C->type()));
                if (!IsInVector(cone, coneNames)) coneNames.push_back(cone);
            }
        }
        return coneNames;
    }

    int GetLLE_RPVCoupling(const xAOD::TruthParticle* LSP) {
        LLE_Decay State = GetFinalRPVState(LSP);
        if (State == LLE_Decay::Unknown)
            return 0;  // Warning("GetLLE_RPVCoupling", "Could not determine the LLE RPV coupling");
        else if (State == LLE_Decay::ElElmu || State == LLE_Decay::ElMuel)
            return 121;
        else if (State == LLE_Decay::ElMumu || State == LLE_Decay::MuMuel)
            return 122;
        else if (State == LLE_Decay::ElElta || State == LLE_Decay::ElTael)
            return 113;
        else if (State == LLE_Decay::TaTael || State == LLE_Decay::ElTata)
            return 133;
        else if (State == LLE_Decay::MuMuta || State == LLE_Decay::MuTamu)
            return 232;
        else if (State == LLE_Decay::MuTata || State == LLE_Decay::TaTamu)
            return 233;
        else if (State == LLE_Decay::ElTamu)
            return 123 * 231;
        else if (State == LLE_Decay::MuTael)
            return 123 * 132;
        else if (State == LLE_Decay::ElMuta)
            return 132 * 231;
        return 0;
    }

    LLE_Decay GetFinalRPVState(const xAOD::TruthParticle* LSP) {
        unsigned int NumEl = 0;
        unsigned int NumElNu = 0;
        unsigned int NumMu = 0;
        unsigned int NumMuNu = 0;
        unsigned int NumTau = 0;
        unsigned int NumTauNu = 0;
        if (LSP == nullptr || LSP->nChildren() < 3)
            return LLE_Decay::Unknown;
        else {
            for (size_t c = 0; c < LSP->nChildren(); ++c) {
                if (LSP->child(c)->isElectron() == true)
                    ++NumEl;
                else if (LSP->child(c)->absPdgId() == 12)
                    ++NumElNu;
                else if (LSP->child(c)->isMuon() == true)
                    ++NumMu;
                else if (LSP->child(c)->absPdgId() == 14)
                    ++NumMuNu;
                else if (LSP->child(c)->isTau() == true)
                    ++NumTau;
                else if (LSP->child(c)->absPdgId() == 16)
                    ++NumTauNu;
            }
        }
        if (NumEl == 2 && NumElNu == 0 && NumMu == 0 && NumMuNu == 1 && NumTau == 0 && NumTauNu == 0)
            return LLE_Decay::ElElmu;  // 121
        else if (NumEl == 1 && NumElNu == 1 && NumMu == 1 && NumMuNu == 0 && NumTau == 0 && NumTauNu == 0)
            return LLE_Decay::ElMuel;  // 121
        else if (NumEl == 1 && NumElNu == 0 && NumMu == 1 && NumMuNu == 1 && NumTau == 0 && NumTauNu == 0)
            return LLE_Decay::ElMumu;  // 122
        else if (NumEl == 0 && NumElNu == 1 && NumMu == 2 && NumMuNu == 0 && NumTau == 0 && NumTauNu == 0)
            return LLE_Decay::MuMuel;  // 122
        else if (NumEl == 1 && NumElNu == 0 && NumMu == 0 && NumMuNu == 1 && NumTau == 1 && NumTauNu == 0)
            return LLE_Decay::ElTamu;  // 123 OR 231
        else if (NumEl == 0 && NumElNu == 1 && NumMu == 1 && NumMuNu == 0 && NumTau == 1 && NumTauNu == 0)
            return LLE_Decay::MuTael;  // 123 OR 132
        else if (NumEl == 2 && NumElNu == 0 && NumMu == 0 && NumMuNu == 0 && NumTau == 0 && NumTauNu == 1)
            return LLE_Decay::ElElta;  // 113
        else if (NumEl == 1 && NumElNu == 1 && NumMu == 0 && NumMuNu == 0 && NumTau == 1 && NumTauNu == 0)
            return LLE_Decay::ElTael;  // 113
        else if (NumEl == 1 && NumElNu == 0 && NumMu == 1 && NumMuNu == 0 && NumTau == 0 && NumTauNu == 1)
            return LLE_Decay::ElMuta;  // 132 OR 231
        else if (NumEl == 0 && NumElNu == 1 && NumMu == 0 && NumMuNu == 0 && NumTau == 2 && NumTauNu == 0)
            return LLE_Decay::TaTael;  // 133
        else if (NumEl == 1 && NumElNu == 0 && NumMu == 0 && NumMuNu == 0 && NumTau == 1 && NumTauNu == 1)
            return LLE_Decay::ElTata;  // 133
        else if (NumEl == 0 && NumElNu == 0 && NumMu == 2 && NumMuNu == 0 && NumTau == 0 && NumTauNu == 1)
            return LLE_Decay::MuMuta;  // 232
        else if (NumEl == 0 && NumElNu == 0 && NumMu == 1 && NumMuNu == 1 && NumTau == 1 && NumTauNu == 0)
            return LLE_Decay::MuTamu;  // 232
        else if (NumEl == 0 && NumElNu == 0 && NumMu == 1 && NumMuNu == 0 && NumTau == 1 && NumTauNu == 1)
            return LLE_Decay::MuTata;  // 233
        else if (NumEl == 0 && NumElNu == 0 && NumMu == 0 && NumMuNu == 1 && NumTau == 2 && NumTauNu == 0)
            return LLE_Decay::TaTamu;  // 233
        return LLE_Decay::Unknown;
    }

}  // namespace XAMPP
