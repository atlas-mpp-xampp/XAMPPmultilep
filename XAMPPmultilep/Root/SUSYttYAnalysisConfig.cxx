#include <XAMPPbase/AnalysisUtils.h>
#include <XAMPPmultilep/SUSYttYAnalysisConfig.h>

namespace XAMPP {
    SUSYttYAnalysisConfig::SUSYttYAnalysisConfig(std::string Analysis) : AnalysisConfig(Analysis) {}

    StatusCode SUSYttYAnalysisConfig::initializeCustomCuts() {
        CutFlow ttYSelection("2Lepton");  // Lets give our cutflow a proper name.. Be creative

        Cut* ttYCut1 = NewCut("SampleOverlap", Cut::CutType::CutChar, false);
        if (!ttYCut1->initialize("PassY_Sel", "=1")) return StatusCode::FAILURE;
        ttYSelection.push_back(ttYCut1);

        Cut* TrigCut = NewCut("TrigMatching", Cut::CutType::CutChar, true);
        if (!TrigCut->initialize("TrigMatching", "=1")) return StatusCode::FAILURE;
        ttYSelection.push_back(TrigCut);
        // Select events with 1 or 2  leptons
        Cut* OneLep = NewCut("N_{l}>0", Cut::CutType::CutInt, true);
        if (!OneLep->initialize("n_SignalLep", ">0")) return StatusCode::FAILURE;
        ttYSelection.push_back(OneLep);
        Cut* Lep = NewCut("N_{l}=2", Cut::CutType::CutInt, true);
        if (!Lep->initialize("n_SignalLep", "=2")) return StatusCode::FAILURE;
        ttYSelection.push_back(Lep);
        Cut* OsCut = NewCut("l^{+}l^{-}", Cut::CutType::CutChar, true);
        if (!OsCut->initialize("OS_Lep", "=1")) return StatusCode::FAILURE;
        ttYSelection.push_back(OsCut);
        Cut* nBasePhot = NewCut("N_{#gamma}^{Baseline}#geq1", Cut::CutType::CutInt, true);
        if (!nBasePhot->initialize("n_BasePhot", ">0")) return StatusCode::FAILURE;
        ttYSelection.push_back(nBasePhot);

        Cut* nPhot = NewCut("N_{#gamma}#geq1", Cut::CutType::CutInt, false);
        if (!nPhot->initialize("n_SignalPhot", ">0")) return StatusCode::FAILURE;
        ttYSelection.push_back(nPhot);

        Cut* nJets = NewCut("N_{jets}#geq2", Cut::CutType::CutInt, false);
        if (!nJets->initialize("n_SignalJets", ">1")) return StatusCode::FAILURE;
        ttYSelection.push_back(nJets);
        Cut* nbJets = NewCut("N_{Btags}#geq1", Cut::CutType::CutInt, false);
        if (!nbJets->initialize("n_BJets", ">0")) return StatusCode::FAILURE;
        ttYSelection.push_back(nbJets);
        Cut* nbJets1 = NewCut("N_{Btags}#geq2", Cut::CutType::CutInt, false);
        if (!nbJets1->initialize("n_BJets", ">1")) return StatusCode::FAILURE;
        ttYSelection.push_back(nbJets1);
        Cut* ZVetoCut = NewCut("ZVeto", Cut::CutType::CutInt, false);
        if (!ZVetoCut->initialize(m_XAMPPInfo->GetVariableStorage<int>("PassZVeto"), ZVeto::Pass, Cut::Relation::Equal))
            return StatusCode::FAILURE;
        ttYSelection.push_back(ZVetoCut);

        ATH_CHECK(AddToCutFlows(ttYSelection));

        CutFlow OneLepton("1Lepton");
        OneLepton.push_back(ttYCut1);
        OneLepton.push_back(TrigCut);
        OneLepton.push_back(OneLep);
        // Be satisfied if the photon trigger has fired in this region
        Cut* Ph_Trigger = NewCut("#gamma-Trigger", Cut::CutType::CutChar, true);
        if (!Ph_Trigger->initialize("TrigHLT_g120_loose", "=1")) return StatusCode::FAILURE;
        OneLepton.push_back(Ph_Trigger->combine(TrigCut, Cut::Combine::OR));

        Cut* OneLepOnly = NewCut("N_{l}=1", Cut::CutType::CutInt, true);
        if (!OneLepOnly->initialize("n_SignalLep", "=1")) return StatusCode::FAILURE;
        OneLepton.push_back(OneLepOnly);
        OneLepton.push_back(nBasePhot);
        OneLepton.push_back(nPhot);
        OneLepton.push_back(nJets);
        nJets = NewCut("N_{jets}#geq3", Cut::CutType::CutInt, false);
        if (!nJets->initialize("n_SignalJets", ">2")) return StatusCode::FAILURE;
        OneLepton.push_back(nJets);
        nJets = NewCut("N_{jets}#geq4", Cut::CutType::CutInt, false);
        if (!nJets->initialize("n_SignalJets", ">3")) return StatusCode::FAILURE;
        OneLepton.push_back(nJets);
        OneLepton.push_back(nbJets);
        OneLepton.push_back(nbJets1);
        ATH_CHECK(AddToCutFlows(OneLepton));
        return StatusCode::SUCCESS;
    }
}  // namespace XAMPP
