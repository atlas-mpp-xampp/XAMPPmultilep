#include <XAMPPbase/AnalysisUtils.h>
#include <XAMPPbase/ReconstructedParticles.h>
#include <XAMPPmultilep/AnalysisUtils.h>
#include <XAMPPmultilep/SUSYllqqAnalysisHelper.h>

namespace XAMPP {
    static IntAccessor acc_NLep("NLep");

    SUSYllqqAnalysisHelper::SUSYllqqAnalysisHelper(const std::string& myname) : SUSY4LeptonAnalysisHelper(myname) {}
    SUSYllqqAnalysisHelper::~SUSYllqqAnalysisHelper() {}
    StatusCode SUSYllqqAnalysisHelper::RemoveOverlap() {
        ATH_CHECK(SUSY4LeptonAnalysisHelper::RemoveOverlap());
        //        ATH_CHECK(XAMPP::RemoveOverlap(m_jet_selection->GetCustomJets("PreSelFatJet10"),
        //        m_electron_selection->GetPreElectrons(), 1.));
        //        ATH_CHECK(XAMPP::RemoveOverlap(m_jet_selection->GetCustomJets("PreSelFatJet10"), m_muon_selection->GetPreMuons(), 1.));

        return StatusCode::SUCCESS;
    }
    StatusCode SUSYllqqAnalysisHelper::initializeEventVariables() {
        ATH_CHECK(initLightLeptonTrees());
        ATH_CHECK(initRecoParticleTree());
        ATH_CHECK(InitializeMultiplicities());

        ATH_CHECK(initJetTree());
        if (doTruth()) ATH_CHECK(initTruthTree());

        ATH_CHECK(m_XAMPPInfo->NewEventVariable<char>("OS_Lep", false));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("Probe_MT"));

        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("ZVeto"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("Base_ZVeto", false));

        ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("Mll_Z1"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("Mll_Z2", false));

        ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("Base_Mll_Z1", false));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("Base_Mll_Z2", false));

        return StatusCode::SUCCESS;
    }
    StatusCode SUSYllqqAnalysisHelper::ComputeEventVariables() {
        ATH_CHECK(FillMultiplicities());

        static XAMPP::Storage<char>* dec_OpLep = m_XAMPPInfo->GetVariableStorage<char>("OS_Lep");
        static XAMPP::Storage<int>* dec_ZVeto = m_XAMPPInfo->GetVariableStorage<int>("ZVeto");
        static XAMPP::Storage<float>* dec_ProbeMT = m_XAMPPInfo->GetVariableStorage<float>("Probe_MT");
        static XAMPP::Storage<xAOD::MissingET*>* dec_MET = m_XAMPPInfo->GetVariableStorage<xAOD::MissingET*>("MetTST");

        xAOD::IParticleContainer Lep(SG::VIEW_ELEMENTS);
        for (const auto& E : *m_electron_selection->GetSignalElectrons()) Lep.push_back(E);
        for (const auto& M : *m_muon_selection->GetSignalMuons()) Lep.push_back(M);

        unsigned int nSignalLep = nSignalElectrons() + nSignalMuons();
        bool OppositeSign = (nSignalLep == 2 ? XAMPP::OppositeSign(**(Lep.begin()), **(Lep.begin() + 1)) : false);
        ATH_CHECK(dec_OpLep->Store(OppositeSign));

        float ProbeMt = -1.e9;
        ATH_CHECK(PassZVeto(Lep));
        if (dec_ZVeto->GetValue() != XAMPP::ZVeto::Pass && nSignalLep > 2) {
            xAOD::ParticleContainer* InvLepMom = m_ParticleConstructor->GetSubContainer("LepInv");
            const xAOD::Particle* BestZ = nullptr;
            // Finding the best Z candidate in the event
            for (const auto ZCand : *InvLepMom) {
                if (!isZCandidate(ZCand)) { continue; }
                float DeltaM_Z = fabs(ZCand->m() - XAMPP::Z_MASS);
                // Impossible to build  up two Z candidates
                if (!BestZ || fabs(BestZ->m() - Z_MASS) > DeltaM_Z) { BestZ = ZCand; }
            }
            static SG::AuxElement::Accessor<const xAOD::IParticle*> acc_Ptr1("BuildPtr1");
            static SG::AuxElement::Accessor<const xAOD::IParticle*> acc_Ptr2("BuildPtr2");
            for (const auto& SecZ : *InvLepMom) {
                if (nSignalLep == 4 && isZCandidate(SecZ) && !HasCommonConstructingParticles(BestZ, SecZ)) {
                    ProbeMt = SecZ->p4().Mt();
                    break;
                } else if (nSignalLep == 3 && acc_NLep(*SecZ) == 3) {
                    if (acc_Ptr1(*SecZ) == BestZ) {
                        LeptonicWReconstruction(
                            acc_Ptr2(*SecZ), dec_MET, m_ParticleConstructor.get(),
                            [this](const xAOD::IParticle* P) -> bool { return IsSignalLepton(P); }, 0);
                        ProbeMt = ComputeMt(acc_Ptr2(*SecZ), dec_MET);
                        break;
                    } else if (acc_Ptr2(*SecZ) == BestZ) {
                        LeptonicWReconstruction(
                            acc_Ptr1(*SecZ), dec_MET, m_ParticleConstructor.get(),
                            [this](const xAOD::IParticle* P) -> bool { return IsSignalLepton(P); }, 0);
                        ProbeMt = ComputeMt(acc_Ptr1(*SecZ), dec_MET);
                        break;
                    }
                }
            }
        }
        ATH_CHECK(dec_ProbeMT->Store(ProbeMt));
        CalculateMt(m_electron_selection->GetBaselineElectrons(), dec_MET);
        CalculateMt(m_muon_selection->GetBaselineMuons(), dec_MET);
        CalculateMt(m_jet_selection->GetBaselineJets(), dec_MET);
        // Obtain the truth particles from the Z boson decay

        if (!isData()) {
            xAOD::TruthParticleContainer Z_Quarks(SG::VIEW_ELEMENTS);
            for (const auto initial : *m_truth_selection->GetTruthPrimaryParticles()) { GetHadronicChildren(initial, Z_Quarks); }
            static IntDecorator dec_pdgId("pdgId");
            static IntDecorator dec_parentpdgId("parent_pdgId");
            static IntAccessor acc_parentpdgId("parent_pdgId");

            for (auto jet : *m_jet_selection->GetBaselineJets()) {
                const xAOD::TruthParticle* NearQuark =
                    dynamic_cast<const xAOD::TruthParticle*>(GetClosestParticle(&Z_Quarks, jet, true, m_jet_selection->GetBaselineJets()));

                float dR = NearQuark ? xAOD::P4Helpers::deltaR(NearQuark, jet) : -1.;
                //                std::cout<<"dR: "<<dR<<std::endl;
                if (NearQuark) {
                    const xAOD::TruthParticle* QuarkChain = GetLastChainLink(NearQuark);
                    // Search for the
                    for (size_t c = 0; c < QuarkChain->nChildren(); ++c) {
                        const xAOD::TruthParticle* Child = QuarkChain->child(c);
                        if (Child && dR > xAOD::P4Helpers::deltaR(Child, jet)) dR = xAOD::P4Helpers::deltaR(Child, jet);
                        //                        PromptParticle(Child);
                    }
                }
                // Only accept a Near quark matching within 0.4 to the jet
                if (dR > 0.4) {
                    ATH_MSG_DEBUG(" The jet with pt: " << jet->pt() / 1.e3 << " GeV, eta: " << jet->eta() << ", phi: " << jet->phi()
                                                       << " does not overlap with quark pt: " << NearQuark->pt() / 1.e3
                                                       << " GeV, eta: " << NearQuark->eta() << " phi: " << NearQuark->phi() << " pdgId: "
                                                       << NearQuark->pdgId() << " parent pdgId: " << acc_parentpdgId(*NearQuark)
                                                       << ". Delta R between both: " << dR);
                    NearQuark = nullptr;
                }
                dec_pdgId(*jet) = NearQuark ? NearQuark->pdgId() : 1.e8;
                dec_parentpdgId(*jet) = NearQuark ? acc_parentpdgId(*NearQuark) : 1.e8;
            }
        }
        ATH_CHECK(FillParticleTrees());
        return StatusCode::SUCCESS;
    }
    StatusCode SUSYllqqAnalysisHelper::initJetTree() {
        ATH_CHECK(SUSY4LeptonAnalysisHelper::initJetTree());
        //        ATH_CHECK(m_XAMPPInfo->BookParticleStorage("FatJet10", true));
        //        XAMPP::ParticleStorage* FatJet10Store = m_XAMPPInfo->GetParticleStorage("FatJet10");
        //        ATH_CHECK(FatJet10Store->SaveInteger("Ztag"));
        //        ATH_CHECK(FatJet10Store->SaveInteger("Wtag"));

        XAMPP::ParticleStorage* JetStore = m_XAMPPInfo->GetParticleStorage("jet");
        ATH_CHECK(JetStore->SaveFloat("charge"));
        for (unsigned int k = 2; k < 10; ++k) { ATH_CHECK(JetStore->SaveFloat(Form("Q0%i", k))); }
        //        ATH_CHECK(JetStore->SaveVariable<std::vector<float>>("TrackPts"));
        ATH_CHECK(JetStore->SaveVariable<std::vector<float>>("TrackFracs"));
        ATH_CHECK(JetStore->SaveVariable<std::vector<int>>("TrackQs"));

        ATH_CHECK(JetStore->SaveFloat("EMFrac"));
        ATH_CHECK(JetStore->SaveFloat("HECFrac"));
        ATH_CHECK(JetStore->SaveFloat("Width"));
        ATH_CHECK(JetStore->SaveFloat("LeadingClusterPt"));
        if (!isData()) {
            ATH_CHECK(JetStore->SaveInteger("PartonTruthLabelID"));
            ATH_CHECK(JetStore->SaveInteger("ConeTruthLabelID"));
            // This pdgId is only meaningful with the quark originates from a W/Z boson decay
            ATH_CHECK(JetStore->SaveInteger("pdgId"));
            ATH_CHECK(JetStore->SaveInteger("parent_pdgId"));
        }
        return StatusCode::SUCCESS;
    }
    StatusCode SUSYllqqAnalysisHelper::FillParticleTrees() {
        static XAMPP::ParticleStorage* ElecStore = m_XAMPPInfo->GetParticleStorage("elec");
        static XAMPP::ParticleStorage* MuonStore = m_XAMPPInfo->GetParticleStorage("muon");
        static XAMPP::ParticleStorage* JetStore = m_XAMPPInfo->GetParticleStorage("jet");
        static XAMPP::ParticleStorage* RecoCandStore = m_XAMPPInfo->GetParticleStorage("RecoCandidates");
        ATH_CHECK(ElecStore->Fill(m_electron_selection->GetBaselineElectrons()));
        ATH_CHECK(MuonStore->Fill(m_muon_selection->GetBaselineMuons()));
        ATH_CHECK(JetStore->Fill(m_jet_selection->GetBaselineJets()));
        ATH_CHECK(RecoCandStore->Fill(m_ParticleConstructor->GetContainer()));
        if (doTruth()) {
            static XAMPP::ParticleStorage* BosonStore = m_XAMPPInfo->GetParticleStorage("Bosons");
            static XAMPP::ParticleStorage* Tru_EleStore = m_XAMPPInfo->GetParticleStorage("Truth_elec");
            static XAMPP::ParticleStorage* Tru_MuoStore = m_XAMPPInfo->GetParticleStorage("Truth_muon");
            static XAMPP::ParticleStorage* Tru_NeuStore = m_XAMPPInfo->GetParticleStorage("Truth_neutrino");

            GetParentPdgId(m_truth_selection->GetTruthBaselineElectrons());
            GetParentPdgId(m_truth_selection->GetTruthBaselineMuons());

            ATH_CHECK(BosonStore->Fill(m_truth_selection->GetTruthPrimaryParticles()));
            ATH_CHECK(Tru_EleStore->Fill(m_truth_selection->GetTruthBaselineElectrons()));
            ATH_CHECK(Tru_MuoStore->Fill(m_truth_selection->GetTruthBaselineMuons()));
            ATH_CHECK(Tru_NeuStore->Fill(m_truth_selection->GetTruthNeutrinos()));
        }
        return StatusCode::SUCCESS;
    }
    StatusCode SUSYllqqAnalysisHelper::initTruthTree() {
        ATH_CHECK(m_XAMPPInfo->BookParticleStorage("Bosons", true));
        ATH_CHECK(m_XAMPPInfo->BookParticleStorage("Truth_elec", true));
        ATH_CHECK(m_XAMPPInfo->BookParticleStorage("Truth_muon", true));
        ATH_CHECK(m_XAMPPInfo->BookParticleStorage("Truth_neutrino", true));

        XAMPP::ParticleStorage* BosonStore = m_XAMPPInfo->GetParticleStorage("Bosons");
        ATH_CHECK(BosonStore->SaveInteger("pdgId"));
        ATH_CHECK(BosonStore->SaveFloat("charge"));

        XAMPP::ParticleStorage* EleStore = m_XAMPPInfo->GetParticleStorage("Truth_elec");
        ATH_CHECK(EleStore->SaveFloat("charge"));
        ATH_CHECK(EleStore->SaveChar("HardProcess"));
        ATH_CHECK(EleStore->SaveInteger("parent_pdgId"));

        XAMPP::ParticleStorage* MuoStore = m_XAMPPInfo->GetParticleStorage("Truth_muon");
        ATH_CHECK(MuoStore->SaveFloat("charge"));
        ATH_CHECK(MuoStore->SaveChar("HardProcess"));
        ATH_CHECK(MuoStore->SaveInteger("parent_pdgId"));

        return StatusCode::SUCCESS;
    }
}  // namespace XAMPP
