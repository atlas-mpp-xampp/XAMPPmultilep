#include <MCTruthClassifier/MCTruthClassifier.h>
#include <MCTruthClassifier/MCTruthClassifierDefs.h>
#include <XAMPPbase/AnalysisUtils.h>
#include <XAMPPbase/ReconstructedParticles.h>
#include <XAMPPmultilep/AnalysisUtils.h>
#include <XAMPPmultilep/SUSY4LeptonScaleFactorAnalysisHelper.h>

namespace XAMPP {
    SUSY4LeptonScaleFactorAnalysisHelper::SUSY4LeptonScaleFactorAnalysisHelper(const std::string& myname) :
        SUSY4LeptonAnalysisHelper(myname) {}
    SUSY4LeptonScaleFactorAnalysisHelper::~SUSY4LeptonScaleFactorAnalysisHelper() {}
    StatusCode SUSY4LeptonScaleFactorAnalysisHelper::initializeEventVariables() {
        // Initialize the particle trees
        ATH_CHECK(initLightLeptonTrees());
        ATH_CHECK(initTauTree());
        ATH_CHECK(InitializeMultiplicities());

        ATH_CHECK(InitializeMeffVariable());

        // ttbar analysis
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<char>("SS_BaseLep", false));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<char>("DFOS_SignalLep", false));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<char>("SS_SFSig", false));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<char>("SS3rdBase_SFSig", false));

        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("ZVeto"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("Base_ZVeto"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("Signal_Mll_Z1"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("Signal_Mll_Z2"));

        ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("Base_Mll_Z1"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("Base_Mll_Z2"));

        ATH_CHECK(m_XAMPPInfo->NewEventVariable<char>("Lep_W", false));
        ATH_CHECK(initJetTree());
        ATH_CHECK(initRecoParticleTree());

        ATH_CHECK(m_XAMPPInfo->BookParticleStorage("phot"));
        XAMPP::ParticleStorage* store_phot = m_XAMPPInfo->GetParticleStorage("phot");
        if (!isData()) {
            ATH_CHECK(store_phot->SaveVariable<int>("truthOrigin"));
            ATH_CHECK(store_phot->SaveVariable<int>("truthType"));
        }
        ATH_CHECK(store_phot->SaveVariable<char>("isol"));
        ATH_CHECK(store_phot->SaveVariable<char>("signal"));

        return StatusCode::SUCCESS;
    }

    StatusCode SUSY4LeptonScaleFactorAnalysisHelper::ComputeEventVariables() {
        ATH_CHECK(FillMultiplicities());
        ATH_CHECK(CalculateMeffVariable());
        /*! particle stores */
        static XAMPP::ParticleStorage* store_muons = m_XAMPPInfo->GetParticleStorage("muon");
        static XAMPP::ParticleStorage* store_recocand = m_XAMPPInfo->GetParticleStorage("RecoCandidates");
        static XAMPP::ParticleStorage* store_taus = m_XAMPPInfo->GetParticleStorage("tau");
        static XAMPP::ParticleStorage* store_elec = m_XAMPPInfo->GetParticleStorage("elec");
        static XAMPP::ParticleStorage* store_phot = m_XAMPPInfo->GetParticleStorage("phot");
        static XAMPP::ParticleStorage* store_jet = m_XAMPPInfo->GetParticleStorage("jet");

        static XAMPP::Storage<char>* dec_SS_BaseLep = m_XAMPPInfo->GetVariableStorage<char>("SS_BaseLep");
        static XAMPP::Storage<char>* dec_SS_SFSig = m_XAMPPInfo->GetVariableStorage<char>("DFOS_SignalLep");
        static XAMPP::Storage<char>* dec_SS3rdBase_SFSig = m_XAMPPInfo->GetVariableStorage<char>("SS3rdBase_SFSig");

        static XAMPP::Storage<xAOD::MissingET*>* dec_MET = m_XAMPPInfo->GetVariableStorage<xAOD::MissingET*>("MetTST");

        xAOD::IParticleContainer BaseLep(SG::VIEW_ELEMENTS);
        xAOD::IParticleContainer SignalLep(SG::VIEW_ELEMENTS);
        for (const auto& E : *m_electron_selection->GetBaselineElectrons()) BaseLep.push_back(E);
        for (const auto& M : *m_muon_selection->GetBaselineMuons()) BaseLep.push_back(M);
        ATH_CHECK(PassZVeto(BaseLep));
        for (const auto& M : *m_muon_selection->GetSignalMuons()) SignalLep.push_back(M);
        for (const auto& E : *m_electron_selection->GetSignalElectrons()) SignalLep.push_back(E);

        BaseLep.sort(XAMPP::ptsorter);
        SignalLep.sort(XAMPP::ptsorter);
        CalculateMt(m_electron_selection->GetPreElectrons(), dec_MET);
        CalculateMt(m_muon_selection->GetPreMuons(), dec_MET);
        CalculateMt(m_tau_selection->GetPreTaus(), dec_MET);
        // build up the Z+jets analysis

        bool SameSignBase = nBaselineElectrons() + nBaselineMuons() == 2 ? !OppositeSign(*BaseLep.begin(), *(BaseLep.begin() + 1)) : false;
        ATH_CHECK(dec_SS_BaseLep->Store(SameSignBase));

        bool SF_Signal = (nSignalElectrons() + nSignalMuons() >= 2) && IsDFOS(SignalLep[0], SignalLep[1]);
        ATH_CHECK(dec_SS_SFSig->Store(SF_Signal));
        bool SS3rdBase_SFSig = false;
        if (nBaselineMuons() == 2) {
            SS3rdBase_SFSig = XAMPP::ChargeProduct(m_muon_selection->GetBaselineMuons(), {0, 1}) > 0;
        } else if (nBaselineElectrons() == 2) {
            SS3rdBase_SFSig = XAMPP::ChargeProduct(m_electron_selection->GetBaselineElectrons(), {0, 1}) > 0;
        }
        ATH_CHECK(dec_SS3rdBase_SFSig->Store(SS3rdBase_SFSig));

        // fill m_ParticleConstructor for the ttbar analysis
        ATH_CHECK(LeptonicTopReconstruction());

        // fill particle with pre-defined vars
        ATH_CHECK(store_taus->Fill(m_tau_selection->GetPreTaus()));
        ATH_CHECK(store_elec->Fill(m_electron_selection->GetPreElectrons()));
        ATH_CHECK(store_muons->Fill(m_muon_selection->GetPreMuons()));
        ATH_CHECK(store_recocand->Fill(m_ParticleConstructor->GetContainer()));
        ATH_CHECK(store_phot->Fill(m_photon_selection->GetPrePhotons()));
        ATH_CHECK(store_jet->Fill(m_jet_selection->GetSignalNoORJets()));
        return StatusCode::SUCCESS;
    }

    // Try to reconstruct the top with a fake lepton in events with same sign pairs
    StatusCode SUSY4LeptonScaleFactorAnalysisHelper::LeptonicTopReconstruction() {
        static XAMPP::Storage<char>* dec_HasW = m_XAMPPInfo->GetVariableStorage<char>("Lep_W");
        static XAMPP::Storage<XAMPPmet>* dec_MET = m_XAMPPInfo->GetVariableStorage<XAMPPmet>("MetTST");
        static CharDecorator dec_WTagged("WTagged");

        xAOD::IParticleContainer Leptons(SG::VIEW_ELEMENTS);
        for (const auto& M : *m_muon_selection->GetSignalMuons()) Leptons.push_back(M);
        for (const auto& E : *m_electron_selection->GetSignalElectrons()) Leptons.push_back(E);
        Leptons.sort(XAMPP::ptsorter);

        ATH_CHECK(dec_HasW->Store(LeptonicWReconstruction(Leptons, dec_MET, m_ParticleConstructor.get(),
                                                          [this](const xAOD::IParticle* P) -> bool { return IsSignalLepton(P); })));
        // Make sure that the decorator is available for the ntuples
        if (nBaselineElectrons() && !nSignalElectrons()) {
            const xAOD::Electron* el = *m_electron_selection->GetBaselineElectrons()->begin();
            dec_WTagged(*el) = false;
        }
        if (nBaselineMuons() && !nSignalMuons()) {
            const xAOD::Muon* mu = *m_muon_selection->GetBaselineMuons()->begin();
            dec_WTagged(*mu) = false;
        }

        if (!dec_HasW->GetValue()) return StatusCode::SUCCESS;

        xAOD::ParticleContainer* Cont = m_ParticleConstructor->GetContainer();
        xAOD::ParticleContainer WCand(SG::VIEW_ELEMENTS);
        for (const auto& P : *Cont)
            if (fabs(P->pdgId()) == 24) WCand.push_back(P);
        // Now try top reconstruction
        for (const auto& bjet : *m_jet_selection->GetBJets()) {
            const xAOD::IParticle* NearW = GetClosestParticle(&WCand, bjet);
            xAOD::Particle* Top = m_ParticleConstructor->CreateEmptyParticle();
            AddFourMomenta(bjet, NearW, Top, [this](const xAOD::IParticle* P) -> bool { return IsSignalLepton(P); }

            );
            Top->setPdgId(Charge(NearW) * 6);
            Top->setCharge(Charge(NearW) * 2. / 3.);
        }
        return StatusCode::SUCCESS;
    }
}  // namespace XAMPP
