#include <XAMPPbase/EventInfo.h>
#include <XAMPPmultilep/SUSY4LeptonElectronSelector.h>

#include <fstream>
#include <iostream>
#include <sstream>

#include "AsgAnalysisInterfaces/IEfficiencyScaleFactorTool.h"
#include "ElectronPhotonSelectorTools/AsgElectronChargeIDSelectorTool.h"

namespace XAMPP {
    static CharDecorator dec_isolsig("IsoSignal");
    static CharDecorator dec_QFliped("QFlip");
    static CharAccessor acc_signal("signal");

    SUSY4LeptonElectronSelector::SUSY4LeptonElectronSelector(const std::string& myname) :
        SUSYElectronSelector(myname),
        m_NonIsolElectrons(nullptr),
        m_FakeElectrons(nullptr),
        m_chargeSelector("EleChargeFlipTagger"),
        m_chargeFlipEff("EleChargeFlipSFTool"),
        m_RunChargeFlip(true),
        m_ApplyQFlipSF(true),
        m_charge_flip_SF() {
        declareProperty("ChargeFlipTagger", m_chargeSelector);
        declareProperty("ElectronChargeFlipSFTool", m_chargeFlipEff);
        declareProperty("ApplyQFlipSF", m_ApplyQFlipSF);
        declareProperty("TagQFlip", m_RunChargeFlip);
    }
    SUSY4LeptonElectronSelector::~SUSY4LeptonElectronSelector() {}
    StatusCode SUSY4LeptonElectronSelector::initialize() {
        if (isInitialized()) return StatusCode::SUCCESS;

        ATH_CHECK(SUSYElectronSelector::initialize());
        // https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/ElectronChargeFlipTaggerTool
        if (m_RunChargeFlip) {
            if (!m_chargeSelector.isSet()) m_chargeSelector = GetCPTool<IAsgElectronLikelihoodTool>("ElectronChargeIDSelectorTool");
            if (!m_chargeFlipEff.isSet()) m_chargeFlipEff = GetCPTool<CP::IEfficiencyScaleFactorTool>("ElectronChargeEffCorrectionTool");
            ATH_CHECK(m_chargeSelector.retrieve());
            ATH_CHECK(m_chargeFlipEff.retrieve());
            for (const auto& sys : m_systematics->GetWeightSystematics(ObjectType())) {
                if (!XAMPP::ToolIsAffectedBySystematic(m_chargeFlipEff, sys)) continue;
            }
        }
        return StatusCode::SUCCESS;
    }
    StatusCode SUSY4LeptonElectronSelector::FillElectrons(const CP::SystematicSet& systset) {
        ATH_CHECK(SUSYElectronSelector::FillElectrons(systset));
        ATH_CHECK(ViewElementsContainer("nonIsoSig", m_NonIsolElectrons));
        ATH_CHECK(ViewElementsContainer("fake", m_FakeElectrons));
        // Signal electrons have been filled by the SUSYElectronSelector::FillElectrons() method
        for (const auto& ielec : *GetPreElectrons()) {
            dec_QFliped(*ielec) = m_RunChargeFlip && !m_chargeSelector->accept(ielec);
            dec_isolsig(*ielec) = PassSignal(*ielec);
            if (acc_signal(*ielec)) m_NonIsolElectrons->push_back(ielec);
            if (!PassSignal(*ielec)) m_FakeElectrons->push_back(ielec);
        }
        ATH_MSG_DEBUG("Number of all electrons: " << GetElectrons()->size());
        ATH_MSG_DEBUG("Number of preselected electrons: " << GetPreElectrons()->size());
        ATH_MSG_DEBUG("Number of selected baseline electrons: " << GetBaselineElectrons()->size());
        ATH_MSG_DEBUG("Number of selected non isolated signal electrons: " << m_NonIsolElectrons->size());
        ATH_MSG_DEBUG("Number of selected signal electrons: " << GetSignalElectrons()->size());

        return StatusCode::SUCCESS;
    }
    StatusCode SUSY4LeptonElectronSelector::SaveScaleFactor() {
        ATH_CHECK(SUSYElectronSelector::SaveScaleFactor());
        const CP::SystematicSet* kineSet = m_XAMPPInfo->GetSystematic();
        for (auto& ScaleFactors : m_charge_flip_SF) {
            if (kineSet != m_systematics->GetNominal() && ScaleFactors->systematic() != m_systematics->GetNominal()) continue;
            ATH_CHECK(m_systematics->setSystematic(ScaleFactors->systematic()));
            for (auto electron : *GetSignalElectrons()) {
                // bool is_Signal = !m_writeBaselineSF || PassSignal(*electron);
                ATH_CHECK(ScaleFactors->saveSF(*electron, true));
            }
            ATH_CHECK(ScaleFactors->applySF());
        }
        return StatusCode::SUCCESS;
    }

    //#####################################################################
    //                      ChargeFlipElectronWeight
    //#####################################################################
    ChargeFlipElectronWeight::ChargeFlipElectronWeight(ToolHandle<CP::IEfficiencyScaleFactorTool>& SFTool) :
        ElectronWeightDecorator(), m_SFTool(SFTool) {}

    ChargeFlipElectronWeight::~ChargeFlipElectronWeight() {}

    StatusCode ChargeFlipElectronWeight::calculateSF(const xAOD::Electron& Electron, double& SF) {
        if (m_SFTool->getEfficiencyScaleFactor(Electron, SF) == CP::CorrectionCode::Error) { return StatusCode::FAILURE; }

        return StatusCode::SUCCESS;
    }

}  // namespace XAMPP
