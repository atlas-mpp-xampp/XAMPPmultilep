#include <XAMPPbase/AnalysisUtils.h>
#include <XAMPPmultilep/SUSY4LeptonScaleFactorAnalysisConfig.h>

namespace XAMPP {
    SUSY4LeptonScaleFactorAnalysisConfig::SUSY4LeptonScaleFactorAnalysisConfig(std::string Analysis) : AnalysisConfig(Analysis) {}

    StatusCode SUSY4LeptonScaleFactorAnalysisConfig::initializeCustomCuts() {
        Cut* TrigMatch = NewCut("TrigMatching", Cut::CutType::CutChar, true);
        if (!TrigMatch->initialize("TrigMatching", "=1")) return StatusCode::FAILURE;
        Cut* OneBaseLep = NewCut("N^{baseline}_{Lep}>=1", Cut::CutType::CutInt, true);
        if (!OneBaseLep->initialize("n_BaseLep", ">=1")) return StatusCode::FAILURE;

        Cut* OneSignaLep = NewCut("N_{Lep}^{signal}>=1", Cut::CutType::CutInt, false);
        if (!OneSignaLep->initialize("n_SignalLep", ">0")) return StatusCode::FAILURE;

        Cut* MinTwoBaseLep = NewCut("N^{baseline}_{Lep}>=2", Cut::CutType::CutInt, true);
        if (!MinTwoBaseLep->initialize("n_BaseLep", ">=2")) return StatusCode::FAILURE;

        Cut* TwoBaseLep = NewCut("N^{baseline}_{Lep}=2", Cut::CutType::CutInt, true);
        if (!TwoBaseLep->initialize("n_BaseLep", "=2")) return StatusCode::FAILURE;

        Cut* TwoSignalLep = NewCut("N^{signal}_{Lep}>=2", Cut::CutType::CutInt, true);
        if (!TwoSignalLep->initialize("n_SignalLep", ">=2")) return StatusCode::FAILURE;

        Cut* ThreeBaseLep = NewCut("N^{baseline}_{Lep}=3", Cut::CutType::CutInt, true);
        if (!ThreeBaseLep->initialize("n_BaseLep", "=3")) return StatusCode::FAILURE;

        Cut* Mu2Base = NewCut("N_{#mu}^{baseline} #geq 2", Cut::CutType::CutInt, true);
        if (!Mu2Base->initialize("n_BaseMuon", ">1")) return StatusCode::FAILURE;

        Cut* Mu2Signal = NewCut("N_{#mu}^{signal} #geq 2", Cut::CutType::CutInt, true);
        if (!Mu2Signal->initialize("n_SignalMuon", ">1")) return StatusCode::FAILURE;

        Cut* Tau1Base = NewCut("N_{#tau}^{base} #geq 1", Cut::CutType::CutInt, true);
        if (!Tau1Base->initialize("n_BaseTau", ">0")) return StatusCode::FAILURE;
        Cut* Dump = ThreeBaseLep->combine(Tau1Base, Cut::Combine::OR);

        Cut* SS_SFSig = NewCut("SS^{3rd baseline}_{SF signal}", Cut::CutType::CutChar, true);
        if (!SS_SFSig->initialize("SS3rdBase_SFSig", "=1")) return StatusCode::FAILURE;

        Cut* ThreeLepSS_SF = SS_SFSig->combine(ThreeBaseLep, Cut::Combine::AND);
        Cut* TauOr3Lep = ThreeLepSS_SF->combine(Tau1Base, Cut::Combine::OR);

        Cut* OFFlavour = NewCut("DFOS^{signal} = 1", Cut::CutType::CutChar, true);
        if (!OFFlavour->initialize("DFOS_SignalLep", "=1")) return StatusCode::FAILURE;

        Cut* El2Base = NewCut("N_{e}^{baseline} #geq 2", Cut::CutType::CutInt, true);
        if (!El2Base->initialize("n_BaseElec", ">1")) return StatusCode::FAILURE;
        Cut* El2Signal = NewCut("N_{e}^{signal} #geq 2", Cut::CutType::CutInt, true);
        if (!El2Signal->initialize("n_SignalElec", ">1")) return StatusCode::FAILURE;

        Cut* Wcand = NewCut("N_{W#pm} = 1", Cut::CutType::CutChar, false);
        if (!Wcand->initialize("Lep_W", "=1")) return StatusCode::FAILURE;
        Cut* SS_BaseLep = NewCut("q_{lep}^{0} #times q_{lep}^{1} > 0", Cut::CutType::CutChar, true);
        if (!SS_BaseLep->initialize("SS_BaseLep", ">0")) return StatusCode::FAILURE;
        // Zmumu
        CutFlow Zmumu("Zmumu");
        Zmumu.push_back(TrigMatch);
        Zmumu.push_back(Mu2Base);
        Zmumu.push_back(Mu2Signal);
        Zmumu.push_back(Dump);

        ATH_CHECK(AddToCutFlows(Zmumu));

        CutFlow TTbarCR("TTCR");
        TTbarCR.push_back(OneBaseLep);
        TTbarCR.push_back(OneSignaLep);

        TTbarCR.push_back(MinTwoBaseLep);
        TTbarCR.push_back(TwoSignalLep);
        TTbarCR.push_back(OFFlavour);
        TTbarCR.push_back(TauOr3Lep);
        ATH_CHECK(AddToCutFlows(TTbarCR));
        //
        CutFlow Zee("Zee");
        Zee.push_back(TrigMatch);
        Zee.push_back(El2Base);
        Zee.push_back(El2Signal);
        Zee.push_back(Dump);
        ATH_CHECK(AddToCutFlows(Zee));
        //
        CutFlow Wjets("Wjets");
        Wjets.push_back(TrigMatch);
        Wjets.push_back(OneBaseLep);
        Wjets.push_back(OneSignaLep);
        Wjets.push_back(TwoBaseLep);
        Wjets.push_back(SS_BaseLep);
        Wjets.push_back(Wcand);
        Cut* Jet2Signal = NewCut("N_{jet}^{signal} #geq 2", Cut::CutType::CutInt, false);
        if (!Jet2Signal->initialize("n_SignalJets", ">1")) return StatusCode::FAILURE;
        Wjets.push_back(Jet2Signal);

        ATH_CHECK(AddToCutFlows(Wjets));
        if (!isData()) {
            CutFlow FF_Calculation("OneBaselineLep");
            FF_Calculation.push_back(OneBaseLep);
            ATH_CHECK(AddToCutFlows(FF_Calculation));
        }
        return StatusCode::SUCCESS;
    }
}  // namespace XAMPP
