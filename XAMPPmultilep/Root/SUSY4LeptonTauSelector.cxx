#include <PathResolver/PathResolver.h>
#include <SUSYTools/SUSYObjDef_xAOD.h>
#include <TRandom.h>
#include <TauAnalysisTools/TauSelectionTool.h>
#include <TauAnalysisTools/TauTruthMatchingTool.h>
#include <XAMPPbase/IJetSelector.h>
#include <XAMPPbase/IMetSelector.h>
#include <XAMPPmultilep/SUSY4LeptonTauSelector.h>

#include <fstream>
#include <iostream>
#include <sstream>

namespace XAMPP {
    SUSY4LeptonTauSelector::SUSY4LeptonTauSelector(const std::string& myname) :
        SUSYTauSelector(myname), m_jet_selection("SUSYJetSelector") {
        declareProperty("JetSelectionTool", m_jet_selection);
    }
    SUSY4LeptonTauSelector::~SUSY4LeptonTauSelector() {}
    StatusCode SUSY4LeptonTauSelector::initialize() {
        if (isInitialized()) { return StatusCode::SUCCESS; }
        ATH_CHECK(SUSYTauSelector::initialize());
        ATH_CHECK(m_jet_selection.retrieve());
        return StatusCode::SUCCESS;
    }
    StatusCode SUSY4LeptonTauSelector::InitialFill(const CP::SystematicSet& systset) {
        ATH_CHECK(SUSYTauSelector::InitialFill(systset));
        static FloatAccessor acc_width("Width");
        static FloatDecorator dec_width("Width");
        static FloatDecorator dec_jetPt("jetPt");
        static IntDecorator dec_NTrksJet("NTrksJet");
        static SG::AuxElement::Decorator<TauLink> dec_TauLink("CloseByTau");

        for (const auto& itau : *GetPreTaus()) {
            //###################################
            //  Save additional information     #
            //###################################
            const xAOD::Jet* NearJet = dynamic_cast<const xAOD::Jet*>(GetClosestParticle(m_jet_selection->GetPreJets(), itau));
            float Width = -1., jetPt = -1.;
            unsigned int n_Trks = -1;
            if (NearJet) {
                std::vector<int> nTrkVec;
                NearJet->getAttribute(xAOD::JetAttribute::NumTrkPt1000, nTrkVec);
                n_Trks = !nTrkVec.empty() ? nTrkVec.at(m_XAMPPInfo->GetPrimaryVertex()->index()) : 0;
                if (acc_width.isAvailable(*NearJet)) Width = acc_width(*NearJet);
                jetPt = NearJet->pt();
                dec_TauLink(*NearJet) = GetLink(*itau);
            }
            dec_width(*itau) = Width;
            dec_NTrksJet(*itau) = n_Trks;
            dec_jetPt(*itau) = jetPt;
        }

        return StatusCode::SUCCESS;
    }
}  // namespace XAMPP
