#include <XAMPPbase/AnalysisUtils.h>
#include <XAMPPbase/EventInfo.h>
#include <XAMPPbase/ReconstructedParticles.h>
#include <XAMPPmultilep/AnalysisUtils.h>
#include <XAMPPmultilep/SUSY4LeptonTruthSelector.h>

#include <fstream>
#include <iostream>
#include <sstream>

#include "FourMomUtils/xAODP4Helpers.h"

namespace XAMPP {
    static SG::AuxElement::ConstAccessor<const xAOD::IParticle*> acc_Ptr1("BuildPtr1");
    static SG::AuxElement::ConstAccessor<const xAOD::IParticle*> acc_Ptr2("BuildPtr2");

    SUSY4LeptonTruthSelector::SUSY4LeptonTruthSelector(const std::string& myname) :
        SUSYTruthSelector(myname),
        m_Dec_LLE(nullptr),
        m_Dec_GGM100(nullptr),
        m_Dec_GGM90(nullptr),
        m_Dec_GGM80(nullptr),
        m_Dec_GGM75(nullptr),
        m_Dec_GGM70(nullptr),
        m_Dec_GGM60(nullptr),
        m_Dec_GGM40(nullptr),
        m_Dec_GGM30(nullptr),
        m_Dec_GGM25(nullptr),
        m_Dec_GGM20(nullptr),
        m_Dec_GGM10(nullptr),
        m_Dec_GGM0(nullptr),
        m_ParticleConstructor("ParticleConstructor"),
        m_ggm_dsids() {
        m_ParticleConstructor.declarePropertyFor(this, "ParticleConstructor", "The XAMPP particle constuctor");
        declareProperty("GGMDSIDs", m_ggm_dsids);
    }
    SUSY4LeptonTruthSelector::~SUSY4LeptonTruthSelector() {}
    StatusCode SUSY4LeptonTruthSelector::LoadContainers() {
        ATH_CHECK(SUSYTruthSelector::LoadContainers());
        int State = GetInitialState();
        double GGM_BR100(1.), GGM_BR90(1.), GGM_BR80(1.), GGM_BR75(1.), GGM_BR70(1.), GGM_BR60(1.), GGM_BR40(1.), GGM_BR30(1.),
            GGM_BR25(1.), GGM_BR20(1.), GGM_BR10(1.), GGM_BR0(1.), LLE_Weight(1.);
        int mcChannel = m_XAMPPInfo->mcChannelNumber();

        bool is_ggm = IsInVector(mcChannel, m_ggm_dsids);
        if (State != 0) {
            unsigned int nZ(0), nH(0);
            const xAOD::TruthParticleContainer* truth_particles = nullptr;
            ATH_CHECK(evtStore()->retrieve(truth_particles, isTRUTH3() ? BSMKey() : "TruthParticles"));
            for (const auto& P : *truth_particles) {
                // for the list of GGM samples c.f.
                // https://gitlab.cern.ch/atlas-mpp-xampp/XAMPPmultilep/blob/master/XAMPPmultilep/data/SampleLists/mc16_13TeV/signals.txt
                // Exclude the dsids 396090 & 396091 since they correspond to m = 95 && m = 110 which were generated with 100%
                if (is_ggm) {
                    // For the GGM samples cound the number of children in Z and H
                    if (P->pdgId() == 1000022) {
                        for (unsigned int c = 0; c < P->nChildren(); ++c) {
                            if (P->child(c)->isHiggs())
                                ++nH;
                            else if (P->child(c)->isZ())
                                ++nZ;
                        }
                    }
                } else if (P->pdgId() == 1000022) {
                    const xAOD::TruthParticle* LSP = GetLastChainLink(P);
                    if (!IsSame(LSP, P)) continue;
                    LLE_Decay Decay = GetFinalRPVState(LSP);
                    if (Decay == LLE_Decay::ElElmu || Decay == LLE_Decay::MuMuel)
                        LLE_Weight *= 0.75;
                    else if (Decay == LLE_Decay::ElMumu || Decay == LLE_Decay::ElMuel)
                        LLE_Weight *= 1.5;
                    else if (Decay == LLE_Decay::TaTamu || Decay == LLE_Decay::TaTael)
                        LLE_Weight *= 1.5;
                    else if (Decay == LLE_Decay::MuTata || Decay == LLE_Decay::ElTata)
                        LLE_Weight *= 0.75;
                }
            }
            if (nZ == 2 && nH == 0) {
                GGM_BR100 = 4;
                GGM_BR90 = 3.24;
                GGM_BR80 = 2.56;
                GGM_BR75 = 2.25;
                GGM_BR70 = 1.96;
                GGM_BR60 = 1.44;
                GGM_BR40 = 0.64;
                GGM_BR30 = 0.36;
                GGM_BR25 = 0.25;
                GGM_BR20 = 0.16;
                GGM_BR10 = 0.04;
                GGM_BR0 = 0;
            } else if (nZ == 1 && nH == 1) {
                GGM_BR100 = 0;
                GGM_BR90 = 0.36;
                GGM_BR80 = 0.64;
                GGM_BR75 = 0.75;
                GGM_BR70 = 0.84;
                GGM_BR60 = 0.96;
                GGM_BR40 = 0.96;
                GGM_BR30 = 0.84;
                GGM_BR25 = 0.75;
                GGM_BR20 = 0.64;
                GGM_BR10 = 0.36;
                GGM_BR0 = 0;

            } else if (nZ == 0 && nH == 2) {
                GGM_BR100 = 0;
                GGM_BR90 = 0.04;
                GGM_BR80 = 0.16;
                GGM_BR75 = 0.25;
                GGM_BR70 = 0.36;
                GGM_BR60 = 0.64;
                GGM_BR40 = 1.44;
                GGM_BR30 = 1.96;
                GGM_BR25 = 2.25;
                GGM_BR20 = 2.56;
                GGM_BR10 = 3.24;
                GGM_BR0 = 4;
            }
        }

        ATH_CHECK(m_Dec_GGM100->ConstStore(GGM_BR100));
        ATH_CHECK(m_Dec_GGM90->ConstStore(GGM_BR90));
        ATH_CHECK(m_Dec_GGM80->ConstStore(GGM_BR80));
        ATH_CHECK(m_Dec_GGM75->ConstStore(GGM_BR75));
        ATH_CHECK(m_Dec_GGM70->ConstStore(GGM_BR70));
        ATH_CHECK(m_Dec_GGM60->ConstStore(GGM_BR60));
        ATH_CHECK(m_Dec_GGM40->ConstStore(GGM_BR40));
        ATH_CHECK(m_Dec_GGM30->ConstStore(GGM_BR30));
        ATH_CHECK(m_Dec_GGM25->ConstStore(GGM_BR25));
        ATH_CHECK(m_Dec_GGM20->ConstStore(GGM_BR20));
        ATH_CHECK(m_Dec_GGM10->ConstStore(GGM_BR10));
        ATH_CHECK(m_Dec_GGM0->ConstStore(GGM_BR0));
        ATH_CHECK(m_Dec_LLE->ConstStore(LLE_Weight));

        return StatusCode::SUCCESS;
    }
    StatusCode SUSY4LeptonTruthSelector::initialize() {
        if (isInitialized()) { return StatusCode::SUCCESS; }
        std::sort(m_ggm_dsids.begin(), m_ggm_dsids.end());
        ATH_CHECK(SUSYTruthSelector::initialize());
        ATH_CHECK(m_XAMPPInfo->NewCommonEventVariable<double>("LLEWeight"));
        // BR reweighting of the GGM samples
        ATH_CHECK(m_XAMPPInfo->NewCommonEventVariable<double>("GGM100Weight"));
        ATH_CHECK(m_XAMPPInfo->NewCommonEventVariable<double>("GGM90Weight"));
        ATH_CHECK(m_XAMPPInfo->NewCommonEventVariable<double>("GGM80Weight"));

        ATH_CHECK(m_XAMPPInfo->NewCommonEventVariable<double>("GGM75Weight"));
        ATH_CHECK(m_XAMPPInfo->NewCommonEventVariable<double>("GGM70Weight"));
        ATH_CHECK(m_XAMPPInfo->NewCommonEventVariable<double>("GGM60Weight"));
        ATH_CHECK(m_XAMPPInfo->NewCommonEventVariable<double>("GGM40Weight"));
        ATH_CHECK(m_XAMPPInfo->NewCommonEventVariable<double>("GGM30Weight"));
        ATH_CHECK(m_XAMPPInfo->NewCommonEventVariable<double>("GGM25Weight"));
        ATH_CHECK(m_XAMPPInfo->NewCommonEventVariable<double>("GGM20Weight"));
        ATH_CHECK(m_XAMPPInfo->NewCommonEventVariable<double>("GGM10Weight"));
        ATH_CHECK(m_XAMPPInfo->NewCommonEventVariable<double>("GGM0Weight"));

        ATH_CHECK(m_XAMPPInfo->NewCommonEventVariable<XAMPPmet>("MetTruth_Int"));     // TruthMET only in the nominal
        ATH_CHECK(m_XAMPPInfo->NewCommonEventVariable<XAMPPmet>("MetTruth_IntOut"));  // TruthMET only in the nominal

        m_Dec_LLE = m_XAMPPInfo->GetVariableStorage<double>("LLEWeight");

        m_Dec_GGM25 = m_XAMPPInfo->GetVariableStorage<double>("GGM25Weight");
        m_Dec_GGM75 = m_XAMPPInfo->GetVariableStorage<double>("GGM75Weight");
        m_Dec_GGM100 = m_XAMPPInfo->GetVariableStorage<double>("GGM100Weight");
        m_Dec_GGM0 = m_XAMPPInfo->GetVariableStorage<double>("GGM0Weight");
        m_Dec_GGM90 = m_XAMPPInfo->GetVariableStorage<double>("GGM90Weight");
        m_Dec_GGM80 = m_XAMPPInfo->GetVariableStorage<double>("GGM80Weight");
        m_Dec_GGM70 = m_XAMPPInfo->GetVariableStorage<double>("GGM70Weight");
        m_Dec_GGM60 = m_XAMPPInfo->GetVariableStorage<double>("GGM60Weight");
        m_Dec_GGM40 = m_XAMPPInfo->GetVariableStorage<double>("GGM40Weight");
        m_Dec_GGM30 = m_XAMPPInfo->GetVariableStorage<double>("GGM30Weight");
        m_Dec_GGM20 = m_XAMPPInfo->GetVariableStorage<double>("GGM20Weight");
        m_Dec_GGM10 = m_XAMPPInfo->GetVariableStorage<double>("GGM10Weight");

        ATH_CHECK(m_ParticleConstructor.retrieve());

        return StatusCode::SUCCESS;
    }
    void SUSY4LeptonTruthSelector::InitDecorators(xAOD::TruthParticle* T, bool Pass) {
        SUSYTruthSelector::InitDecorators(T, Pass);
        if (!Pass) return;
        static CharDecorator dec_HardProc("HardProcess");
        dec_HardProc(*T) = (T->isChLepton() || T->isPhoton()) && XAMPP::isParticleFromHardProcess(T);
    }
    StatusCode SUSY4LeptonTruthSelector::InitialFill(const CP::SystematicSet& systset) {
        ATH_CHECK(SUSYTruthSelector::InitialFill(systset));
        ATH_CHECK(m_ParticleConstructor->PrepareContainer(&systset));

        xAOD::MissingETContainer* container = nullptr;
        if (CreateMetContainerLink("MET_Truth", container) == LinkStatus::Failed) return StatusCode::FAILURE;

        static XAMPP::Storage<XAMPPmet>* dec_IntMet = m_XAMPPInfo->GetVariableStorage<XAMPPmet>("MetTruth_Int");
        xAOD::MissingET* IntMet = XAMPP::GetMET_obj("Int", container);
        ATH_CHECK(dec_IntMet->Store(IntMet));

        static XAMPP::Storage<XAMPPmet>* dec_IntMetOut = m_XAMPPInfo->GetVariableStorage<XAMPPmet>("MetTruth_IntOut");
        xAOD::MissingET* IntMetOut = XAMPP::GetMET_obj("IntOut", container);
        ATH_CHECK(dec_IntMetOut->Store(IntMetOut));
        ATH_CHECK(GetTruthDiTaus(*GetTruthBaselineTaus()));
        return StatusCode::SUCCESS;
    }

    StatusCode SUSY4LeptonTruthSelector::GetTruthDiTaus(xAOD::IParticleContainer& TruthTaus) {
        if (!ProcessObject(XAMPP::SelectionObject::DiTau)) {
            ATH_MSG_DEBUG("Di tau selecction in the main analysis is disabled...");
            return StatusCode::SUCCESS;
        }
        ATH_CHECK(m_ParticleConstructor->CreateSubContainer("TruthDiTaus"));
        ConstructInvariantMomenta(
            TruthTaus, m_ParticleConstructor.get(), [this](const xAOD::IParticle* P) -> bool { return PassSignal(*P); }, 2);
        m_ParticleConstructor->DetachSubContainer();

        static FloatDecorator dec_DiTauWidth("Width");
        static IntDecorator dec_ParPdgId1("parent1_pdgId");
        static IntDecorator dec_ParPdgId2("parent2_pdgId");
        static IntDecorator dec_SameParent("same_parent");
        static IntDecorator dec_N_HadronicDecays("N_HadronicDecays");
        static CharAccessor acc_TauDecay("isHadronic");
        static CharDecorator dec_MatchedToRecoDiTau("MatchedToRecoDiTau");
        static CharDecorator dec_MatchedToHighBDTRecoDiTau("MatchedToHighBDTRecoDiTau");

        for (auto TruthDiTau : *m_ParticleConstructor->GetSubContainer("TruthDiTaus")) {
            // Remove the TruthDiTau from the general reconstructed particles
            m_ParticleConstructor->WellDefined(TruthDiTau, false);
            dec_MatchedToRecoDiTau(*TruthDiTau) = false;
            dec_MatchedToHighBDTRecoDiTau(*TruthDiTau) = false;
            const xAOD::TruthParticle* Particle1 = dynamic_cast<const xAOD::TruthParticle*>(acc_Ptr1(*TruthDiTau));
            const xAOD::TruthParticle* Particle2 = dynamic_cast<const xAOD::TruthParticle*>(acc_Ptr2(*TruthDiTau));
            dec_DiTauWidth(*TruthDiTau) = xAOD::P4Helpers::deltaR(Particle1, Particle2, false);
            dec_ParPdgId1(*TruthDiTau) = GetParentPdgId(Particle1);
            dec_ParPdgId2(*TruthDiTau) = GetParentPdgId(Particle2);
            int N_Hadronic = 0;
            if (acc_TauDecay(*Particle1) == true) ++N_Hadronic;
            if (acc_TauDecay(*Particle2) == true) ++N_Hadronic;
            dec_N_HadronicDecays(*TruthDiTau) = N_Hadronic;

            const xAOD::TruthParticle* Parent1 = nullptr;
            const xAOD::TruthParticle* Part1 = GetFirstChainLink(Particle1);
            for (size_t p = 0; p < Part1->nParents(); ++p) {
                if (!Part1->parent(p)) continue;
                Parent1 = Part1->parent(p);
            }
            const xAOD::TruthParticle* Parent2 = nullptr;
            const xAOD::TruthParticle* Part2 = GetFirstChainLink(Particle2);
            for (size_t p = 0; p < Part2->nParents(); ++p) {
                if (!Part2->parent(p)) continue;
                Parent2 = Part2->parent(p);
            }

            if (Parent1 && Parent2) {
                dec_SameParent(*TruthDiTau) = IsSame(Parent1, Parent2);
            } else {
                std::cout << "SUSY4LeptonTruthSelector: something went wrong, while getting DiTauParentParticles" << std::endl;
                dec_SameParent(*TruthDiTau) = false;
            }
        }
        return StatusCode::SUCCESS;
    }
    bool SUSY4LeptonTruthSelector::PassBaseline(const xAOD::IParticle& Particle) const {
        if (!SUSYTruthSelector::PassBaseline(Particle)) return false;
        unsigned int pdgId = abs(TypeToPdgId(Particle));
        if (pdgId == 11 || pdgId == 13) {
            static CharAccessor acc_passLMR("passLMR");
            if (!acc_passLMR.isAvailable(Particle)) {
                ATH_MSG_WARNING("Low mass decoration not found");
                PromptParticle(Particle);
                return false;
            }
            return acc_passLMR(Particle);
        }
        return true;
    }
}  // namespace XAMPP
