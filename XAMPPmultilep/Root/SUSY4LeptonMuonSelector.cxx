#include <XAMPPbase/EventInfo.h>
#include <XAMPPmultilep/SUSY4LeptonMuonSelector.h>

#include <fstream>
#include <iostream>
#include <sstream>

namespace XAMPP {
    static CharDecorator dec_isolsig("IsoSignal");
    static CharAccessor acc_signal("signal");

    SUSY4LeptonMuonSelector::SUSY4LeptonMuonSelector(const std::string& myname) :
        SUSYMuonSelector(myname), m_NonIsolMuons(nullptr), m_FakeMuons(nullptr) {}
    StatusCode SUSY4LeptonMuonSelector::initialize() {
        ATH_CHECK(SUSYMuonSelector::initialize());

        return StatusCode::SUCCESS;
    }

    SUSY4LeptonMuonSelector::~SUSY4LeptonMuonSelector() {}
    StatusCode SUSY4LeptonMuonSelector::FillMuons(const CP::SystematicSet& systset) {
        ATH_CHECK(SUSYMuonSelector::FillMuons(systset));
        ATH_CHECK(ViewElementsContainer("nonIsoSig", m_NonIsolMuons));
        ATH_CHECK(ViewElementsContainer("fake", m_FakeMuons));

        for (const auto& imuon : *GetPreMuons()) {
            if (!PassBaseline(*imuon)) continue;
            dec_isolsig(*imuon) = PassSignal(*imuon);
            if (acc_signal(*imuon))
                m_NonIsolMuons->push_back(imuon);
            else
                continue;
            if (!PassSignal(*imuon)) m_FakeMuons->push_back(imuon);
        }
        ATH_MSG_DEBUG("Number of all Muons: " << GetMuons()->size());
        ATH_MSG_DEBUG("Number of preselected Muons: " << GetPreMuons()->size());
        ATH_MSG_DEBUG("Number of selected baseline Muons: " << GetBaselineMuons()->size());
        ATH_MSG_DEBUG("Number of selected non isolated signal Muons: " << m_NonIsolMuons->size());
        ATH_MSG_DEBUG("Number of selected signal Muons: " << GetSignalMuons()->size());

        return StatusCode::SUCCESS;
    }
}  // namespace XAMPP
