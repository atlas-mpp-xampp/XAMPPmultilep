#include <XAMPPmultilep/SUSY4LeptonRJbuilder.h>
namespace XAMPP {

    static IntAccessor acc_NLep("nConstituents");
    static CharAccessor acc_SF("SameFlavour");

    SUSY4LepJigSawBuilder::SUSY4LepJigSawBuilder(const std::string& name) :
        BaseAnalysisModule(name),
        m_met(nullptr),
        m_electrons(nullptr),
        m_muons(nullptr),
        m_jets(nullptr),
        m_JigSaws(nullptr),
        m_electronContainer("elec"),
        m_muonContainer("muon"),
        m_diLeptonContainer("InvariantMomenta"),
        m_jetContainer(""),
        m_metContainer("MetTST") {
        declareProperty("ElectronContainer", m_electronContainer);
        declareProperty("MuonContainer", m_muonContainer);
        declareProperty("JetContainer", m_jetContainer);
        declareProperty("DiLeptonContainer", m_diLeptonContainer);
        declareProperty("MissingET", m_metContainer);
    }
    double SUSY4LepJigSawBuilder::metric(const xAOD::Particle* P, const xAOD::Particle* P1) const {
        if (P == nullptr || P1 == nullptr) return FLT_MAX;
        return mass2(P) + mass2(P1);
    }
    double SUSY4LepJigSawBuilder::mass2(const xAOD::Particle* P) const {
        unsigned int C = fabs(Charge(P));
        float M2 = pow(P->m(), 2);
        if (C > 0) M2 = M2 * pow(C, 5);
        if (!acc_SF(*P)) M2 *= 2;
        return M2;
    }
    void SUSY4LepJigSawBuilder::saveInLabFrame(ReconstructionFrame& Frame, int T) {
        xAOD::Particle* new_part = m_ParticleConstructor->CreateEmptyParticle();
        new_part->setP4(Frame.GetFourVector(*m_lab_frame));
        new_part->setPdgId(T);
    }

    StatusCode SUSY4LepJigSawBuilder::fill() {
        xAOD::IParticleContainer* electrons = m_electrons->Container();
        xAOD::IParticleContainer* muons = m_muons->Container();
        xAOD::IParticleContainer* jets = m_jets->Container();

        // Create the Reco candidate container
        ATH_CHECK(m_ParticleConstructor->CreateSubContainer("RJ_Particles"));
        ATH_CHECK(m_JigSaws->Fill(m_ParticleConstructor->GetSubContainer("RJ_Particles")));
        // skip all events where we have to few leptons
        unsigned int nLep = electrons->size() + muons->size();
        if (nLep < 4) {
            ATH_MSG_DEBUG("We do not have enough electrons and muons in the event");
            return StatusCode::SUCCESS;
        }
        xAOD::ParticleContainer* InvLepMom = m_ParticleConstructor->GetSubContainer(m_diLeptonContainer);
        if (InvLepMom == nullptr) { ATH_MSG_ERROR("Could not retrieve " << m_diLeptonContainer << " from the Reconstructed particles"); }
        m_lab_frame->ClearEvent();

        xAOD::ParticleContainer DiLeptons(SG::VIEW_ELEMENTS);
        DiLeptons.reserve(nLep * (nLep - 1));
        // Filter the dilepton pairs
        for (auto Lep : *InvLepMom) {
            if (acc_NLep(*Lep) == 2) DiLeptons.push_back(Lep);
        }
        // Sort the di lepton pairs
        DiLeptons.sort([](const xAOD::Particle* P1, const xAOD::Particle* P2) {
            // Sort the neutral particles in the first row
            unsigned int C1(fabs(Charge(P1))), C2(fabs(Charge(P2)));
            if (C2 != C1) return C1 < C2;
            // Then prefer same flavour pairs before different flavour flavour pairs
            if (acc_SF(*P1) != acc_SF(*P2)) return acc_SF(*P1) == 1;
            return P1->m() < P2->m();
        });

        // No catch the best two candidates
        xAOD::Particle* BestZ = nullptr;
        xAOD::Particle* SBestZ = nullptr;
        xAOD::ParticleContainer::iterator begin = DiLeptons.begin();
        xAOD::ParticleContainer::iterator end = DiLeptons.end();
        for (xAOD::ParticleContainer::iterator L = begin; L != end; ++L) {
            xAOD::Particle* Z1 = (*L);
            for (xAOD::ParticleContainer::iterator L1 = L + 1; L1 != end; ++L1) {
                xAOD::Particle* Z2 = (*L1);
                if (HasCommonConstructingParticles(Z1, Z2)) continue;
                if (metric(Z1, Z2) < metric(BestZ, SBestZ)) {
                    BestZ = Z1;
                    SBestZ = Z2;
                }
            }
        }
        if (BestZ == nullptr || SBestZ == nullptr) {
            ATH_MSG_DEBUG("Somehow no di lepton candidate could be found");
            return StatusCode::SUCCESS;
        }
        xAOD::IParticleContainer Visible(SG::VIEW_ELEMENTS);
        Visible.push_back(BestZ);
        Visible.push_back(SBestZ);
        // Pipe the 5-th and 6-th lepton to the Visibles
        for (auto el : *electrons) {
            if (isConstituent(el, BestZ)) continue;
            if (isConstituent(el, SBestZ)) continue;
            Visible.push_back(el);
        }
        for (auto mu : *muons) {
            if (isConstituent(mu, BestZ)) continue;
            if (isConstituent(mu, SBestZ)) continue;
            Visible.push_back(mu);
        }
        for (auto jet : *jets) Visible.push_back(jet);

        std::vector<VisibleRecoFrame_Ptr> Visible_Part = {m_Za_2L, m_Zb_2L, m_Q1b_2Q, m_Q2b_2Q};
        unsigned int N(Visible_Part.size()), k(0);

        for (auto V : Visible) {
            if (k >= N) break;
            Visible_Part.at(k)->SetLabFrameFourVector(V->p4());
            Visible_Part.at(k)->SetCharge(TypeToPdgId(V));
            ++k;
        }
        for (; k < N; ++k) { Visible_Part.at(k)->SetLabFrameFourVector(TLorentzVector()); }

        // Assign the missing-transverse eneergy
        xAOD::MissingET* met = m_met->GetValue();
        TVector3 met_TLV;
        met_TLV.SetPtEtaPhi(met->met(), 0, met->phi());
        m_InvisGroup->SetLabFrameThreeVector(met_TLV);

        if (!m_lab_frame->AnalyzeEvent()) {
            ATH_MSG_DEBUG("Event analysis did not work out");
            return StatusCode::SUCCESS;
        };
        saveInLabFrame(*m_N2a_GZ, JigSawFrame::N2a);
        saveInLabFrame(*m_Ga, JigSawFrame::Ga);

        saveInLabFrame(*m_N2b_GZ, JigSawFrame::N1b);
        saveInLabFrame(*m_Gb, JigSawFrame::Gb);
        saveInLabFrame(*m_C1b_N2W, JigSawFrame::C1b);
        saveInLabFrame(*m_Wb_qq, JigSawFrame::Wb);

        return StatusCode::SUCCESS;

        //  std::cout << "Lep-A1: M: " << m_L1a_2L->GetFourVector(*m_lab_frame).M() / 1.e3 << " [GeV]  pt: " <<
        //  m_L1a_2L->GetFourVector(*m_lab_frame).Pt() / 1.e3 << " [GeV]  eta: " << m_L1a_2L->GetFourVector(*m_lab_frame).Eta() << " phi:  "
        //  << m_L1a_2L->GetFourVector(*m_lab_frame).Phi() << "   Q: " << m_L1a_2L->GetCharge().GetNumerator() << std::endl;
        // std::cout << "Lep-A2: M: " << m_L2a_2L->GetFourVector(*m_lab_frame).M() / 1.e3 << " [GeV]  pt: " <<
        // m_L2a_2L->GetFourVector(*m_lab_frame).Pt() / 1.e3 << " [GeV]  eta: " << m_L2a_2L->GetFourVector(*m_lab_frame).Eta() << " phi:  "
        // << m_L2a_2L->GetFourVector(*m_lab_frame).Phi() << "   Q: " << m_L2a_2L->GetCharge().GetNumerator() << std::endl << std::endl;

        std::cout << "Za: M:" << m_Za_2L->GetFourVector(*m_lab_frame).M() / 1.e3
                  << " [GeV] pt: " << m_Za_2L->GetFourVector(*m_lab_frame).Pt() / 1.e3
                  << " [GeV]  eta: " << m_Za_2L->GetFourVector(*m_lab_frame).Eta()
                  << " phi:  " << m_Za_2L->GetFourVector(*m_lab_frame).Phi() << "   Q: " << m_Za_2L->GetCharge().GetNumerator() << std::endl
                  << std::endl;
        std::cout << "N2a: M:" << m_N2a_GZ->GetFourVector(*m_lab_frame).M() / 1.e3
                  << " [GeV] pt: " << m_N2a_GZ->GetFourVector(*m_lab_frame).Pt() / 1.e3
                  << " [GeV]  eta: " << m_N2a_GZ->GetFourVector(*m_lab_frame).Eta()
                  << " phi:  " << m_N2a_GZ->GetFourVector(*m_lab_frame).Phi() << std::endl
                  << std::endl;

        // std::cout << "Lep-B1: M: " << m_L1b_2L->GetFourVector(*m_lab_frame).M() / 1.e3 << " [GeV]  pt: " <<
        // m_L1b_2L->GetFourVector(*m_lab_frame).Pt() / 1.e3 << " [GeV]  eta: " << m_L1b_2L->GetFourVector(*m_lab_frame).Eta() << " phi:  "
        // << m_L1b_2L->GetFourVector(*m_lab_frame).Phi()<< "   Q: " << m_L1b_2L->GetCharge().GetNumerator()  << std::endl; std::cout <<
        // "Lep-B2: M: " << m_L2b_2L->GetFourVector(*m_lab_frame).M() / 1.e3 << " [GeV]  pt: " << m_L2b_2L->GetFourVector(*m_lab_frame).Pt()
        // / 1.e3 << " [GeV]  eta: " << m_L2b_2L->GetFourVector(*m_lab_frame).Eta() << " phi:  " <<
        // m_L2b_2L->GetFourVector(*m_lab_frame).Phi()<< "   Q: " << m_L2b_2L->GetCharge().GetNumerator()  << std::endl << std::endl;
        std::cout << "Zb: M:" << m_Zb_2L->GetFourVector(*m_lab_frame).M() / 1.e3
                  << " [GeV] pt: " << m_Zb_2L->GetFourVector(*m_lab_frame).Pt() / 1.e3
                  << " [GeV]  eta: " << m_Zb_2L->GetFourVector(*m_lab_frame).Eta()
                  << " phi:  " << m_Zb_2L->GetFourVector(*m_lab_frame).Phi() << "   Q: " << m_Zb_2L->GetCharge().GetNumerator() << std::endl
                  << std::endl;
        std::cout << "N2b: M:" << m_N2b_GZ->GetFourVector(*m_lab_frame).M() / 1.e3
                  << " [GeV] pt: " << m_N2b_GZ->GetFourVector(*m_lab_frame).Pt() / 1.e3
                  << " [GeV]  eta: " << m_N2b_GZ->GetFourVector(*m_lab_frame).Eta()
                  << " phi:  " << m_N2b_GZ->GetFourVector(*m_lab_frame).Phi() << std::endl
                  << std::endl;

        std::cout << "Qb1: M: " << m_Q1b_2Q->GetFourVector(*m_lab_frame).M() / 1.e3
                  << " [GeV] pt: " << m_Q1b_2Q->GetFourVector(*m_lab_frame).Pt() / 1.e3
                  << " [GeV]  eta: " << m_Q1b_2Q->GetFourVector(*m_lab_frame).Eta()
                  << " phi:  " << m_Q1b_2Q->GetFourVector(*m_lab_frame).Phi() << std::endl;
        std::cout << "Qb2: M: " << m_Q2b_2Q->GetFourVector(*m_lab_frame).M() / 1.e3
                  << " [GeV] pt: " << m_Q2b_2Q->GetFourVector(*m_lab_frame).Pt() / 1.e3
                  << " [GeV]  eta: " << m_Q2b_2Q->GetFourVector(*m_lab_frame).Eta()
                  << " phi:  " << m_Q2b_2Q->GetFourVector(*m_lab_frame).Phi() << std::endl;

        std::cout << "C1b: M:" << m_C1b_N2W->GetFourVector(*m_lab_frame).M() / 1.e3
                  << " [GeV] pt: " << m_C1b_N2W->GetFourVector(*m_lab_frame).Pt() / 1.e3
                  << " [GeV]  eta: " << m_C1b_N2W->GetFourVector(*m_lab_frame).Eta()
                  << " phi:  " << m_N2b_GZ->GetFourVector(*m_lab_frame).Phi() << std::endl
                  << std::endl;

        return StatusCode::SUCCESS;
    }
    StatusCode SUSY4LepJigSawBuilder::bookVariables() {
        ATH_CHECK(retrieve(m_electrons, m_electronContainer));
        ATH_CHECK(retrieve(m_muons, m_muonContainer));
        ATH_CHECK(retrieve(m_jets, m_jetContainer));
        ATH_CHECK(retrieve(m_electrons, m_electronContainer));

        m_met = m_XAMPPInfo->GetVariableStorage<xAOD::MissingET*>(m_metContainer);

        ATH_CHECK(bookParticleStore("JigSawParticles", m_JigSaws, true));
        ATH_CHECK(m_JigSaws->SaveVariable<int>("pdgId"));
        return StatusCode::SUCCESS;
    }
    StatusCode SUSY4LepJigSawBuilder::initialize() {
        ATH_CHECK(BaseAnalysisModule::initialize());

        // Frame of the Z->ll system leg - a
        m_Za_2L = VisibleRecoFrame_Ptr(new VisibleRecoFrame("Za_2L", "Z_{a}#to l^{+}l^{i}"));
        // Frame of the Z->ll system leg - b
        m_Zb_2L = VisibleRecoFrame_Ptr(new VisibleRecoFrame("Zb_2L", "Z_{b}#to l^{+}l^{i}"));

        // Frame of the N2-> ZG system leg - a
        m_N2a_GZ = RecoFrame_Ptr(new DecayRecoFrame("N2a_ZG", "#tilde{#chi}^{1}_{0}#to Z#tilde{G}"));
        m_Ga = RecoFrame_Ptr(new InvisibleRecoFrame("G_a", "#tilde{G}_{a}"));
        m_N2a_GZ->AddChildFrame(*m_Ga);
        m_N2a_GZ->AddChildFrame(*m_Za_2L);

        // Frame of the N2-> ZG system leg - b
        m_N2b_GZ = RecoFrame_Ptr(new DecayRecoFrame("N2a_ZG", "#tilde{#chi}^{1}_{0}#to Z#tilde{G}"));

        m_Gb = RecoFrame_Ptr(new InvisibleRecoFrame("G_b", "#tilde{G}_{b}"));
        m_N2b_GZ->AddChildFrame(*m_Gb);
        m_N2b_GZ->AddChildFrame(*m_Zb_2L);

        // Frame of the W->qq system -- leg b only
        m_Wb_qq = RecoFrame_Ptr(new DecayRecoFrame("W_b", "W_{b}#to qq"));
        m_Q1b_2Q = VisibleRecoFrame_Ptr(new VisibleRecoFrame("Q1b", "q_{1b}"));
        m_Q2b_2Q = VisibleRecoFrame_Ptr(new VisibleRecoFrame("Q2b", "q_{2b}"));
        m_Wb_qq->AddChildFrame(*m_Q1b_2Q);
        m_Wb_qq->AddChildFrame(*m_Q2b_2Q);

        // Frame of the C1->N2W system -- leg b only
        m_C1b_N2W = RecoFrame_Ptr(new DecayRecoFrame("C1_b", "#tilde{#chi}^{#pm}_{1}#to W#tilde{#chi}^{0}_{1}"));
        m_C1b_N2W->AddChildFrame(*m_Wb_qq);
        m_C1b_N2W->AddChildFrame(*m_N2b_GZ);

        // Frame of the C1N2 system
        m_C1N1N2_frame = RecoFrame_Ptr(new RestFrames::DecayRecoFrame("C1N2-GGM", "#tilde{#chi}^{#pm}_{1} #tilde{#chi}^{0}_{2}"));
        m_C1N1N2_frame->AddChildFrame(*m_C1b_N2W);
        m_C1N1N2_frame->AddChildFrame(*m_N2a_GZ);

        // Define the lab-frame of the system
        m_lab_frame = std::shared_ptr<RestFrames::LabRecoFrame>(new RestFrames::LabRecoFrame("LabFrame", "LabFrame"));
        m_lab_frame->SetChildFrame(*m_C1N1N2_frame);
        if (!m_lab_frame->InitializeTree()) return StatusCode::FAILURE;

        ATH_MSG_INFO("Decay tree successfully built");
        m_InvisGroup = InvisibleGroup_Ptr(new InvisibleGroup("Invisible", "#tilde{G} - jigsaws"));
        m_InvisGroup->AddFrame(*m_Ga);
        m_InvisGroup->AddFrame(*m_Gb);
        ATH_MSG_INFO("Invisible group built");

        // The invariant mass of the invisible system can be choosen
        // to be the smallest Lorentz invariant function of the visible Masses
        m_inivisblemass = Jigsaw_Ptr(new SetMassInvJigsaw("X1_Mass", "Set M_{#tilde{G}, #tilde{G}} to minimum"));

        // The rapidity of the visible system can be choosen to be equal to the rapidity of the visible
        // particles
        SetRapidityInvJigsaw* rapidity = new SetRapidityInvJigsaw("Rapidity", "Equal rapidity");
        m_rapidity = Jigsaw_Ptr(rapidity);
        rapidity->AddVisibleFrames(m_C1N1N2_frame->GetListVisibleFrames());

        ContraBoostInvJigsaw* contraboost = new ContraBoostInvJigsaw("C1N1N2_contra", "Contra boost invariant");
        m_contraboost = Jigsaw_Ptr(contraboost);

        contraboost->AddVisibleFrames(m_N2a_GZ->GetListVisibleFrames(), 0);
        contraboost->AddVisibleFrames(m_C1b_N2W->GetListVisibleFrames(), 1);
        contraboost->AddInvisibleFrames(m_N2a_GZ->GetListInvisibleFrames(), 0);
        contraboost->AddInvisibleFrames(m_C1b_N2W->GetListInvisibleFrames(), 1);

        MinMassDiffInvJigsaw* minmassdiff = new MinMassDiffInvJigsaw("minMass", "#Delta m_{1} - m_{2}", 2);

        minmassdiff->AddVisibleFrames(m_N2a_GZ->GetListVisibleFrames(), 0);
        minmassdiff->AddVisibleFrames(m_C1b_N2W->GetListVisibleFrames(), 1);
        minmassdiff->AddInvisibleFrames(m_N2a_GZ->GetListInvisibleFrames(), 0);
        minmassdiff->AddInvisibleFrames(m_C1b_N2W->GetListInvisibleFrames(), 1);

        m_min_deltaM = Jigsaw_Ptr(minmassdiff);

        // MinMassDiffInvJigsaw

        m_InvisGroup->AddJigsaw(*m_inivisblemass);
        m_InvisGroup->AddJigsaw(*m_rapidity);
        m_InvisGroup->AddJigsaw(*m_contraboost);
        m_InvisGroup->AddJigsaw(*m_min_deltaM);

        // Define which leptons can  be interchanged between the two legs
        // m_LepComb = CombGrp_Ptr(new CombinatoricGroup("Lep combinatoric", "Z_{i}#to #it{l}_{i1}#it{l}_{i2}"));
        // m_LepComb->AddFrame(*m_Za_2L);
        // m_LepComb->AddFrame(*m_Zb_2L);
        // Apparently we need to constrain the number of elements to 1 otherwise the di-zmass freaks out
        // m_LepComb->SetNElementsForFrame(*m_Za_2L, 1);
        // m_LepComb->SetNElementsForFrame(*m_Zb_2L, 1);

        // MinMassesCombJigsaw* MinMll_R = new MinMassesCombJigsaw("MinMll_R", "min Mll");
        // m_LepResolver = Jigsaw_Ptr(MinMll_R);
        // m_LepComb->AddJigsaw(*m_LepResolver);
        // MinMll_R->AddFrame(*m_Za_2L, 0);
        // MinMll_R->AddFrame(*m_Zb_2L, 1);

        // MinMll_R->AddCombFrame(*m_L1a_2L, 0);
        // MinMll_R->AddCombFrame(*m_L1b_2L, 1);
        // MinMll_R->AddObjectFrames((*m_L1a_2L) + (*m_L2a_2L), 0);
        // MinMll_R->AddObjectFrames((*m_L1b_2L) + (*m_L2b_2L), 1);

        // m_LepResolver
        if (!m_lab_frame->InitializeAnalysis()) return StatusCode::FAILURE;

        //        Jigsaw_Ptr m_rapidity;

        //        // Invisible Groups
        //        InvisibleGroup INV("INV","#tilde{#chi}_{1}^{ 0} Jigsaws");
        //        INV.AddFrames(X1a+X1b);
        //
        //        // Set di-LSP mass to minimum Lorentz-invariant expression
        //        SetMassInvJigsaw X1_mass("X1_mass","Set M_{#tilde{#chi}_{1}^{ 0} #tilde{#chi}_{1}^{ 0}} to minimum");
        //        INV.AddJigsaw(X1_mass);
        //
        //        // Set di-LSP rapidity to that of visible particles
        //        SetRapidityInvJigsaw X1_eta("X1_eta","#eta_{#tilde{#chi}_{1}^{ 0} #tilde{#chi}_{1}^{ 0}} = #eta_{2#gamma+2#it{l}}");
        //        INV.AddJigsaw(X1_eta);
        //        X1_eta.AddVisibleFrames(X2X2.GetListVisibleFrames());
        return StatusCode::SUCCESS;
    }

}  // namespace XAMPP
