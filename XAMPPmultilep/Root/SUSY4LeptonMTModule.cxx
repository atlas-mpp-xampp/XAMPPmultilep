#include <XAMPPmultilep/AnalysisUtils.h>
#include <XAMPPmultilep/SUSY4LeptonMTModule.h>

namespace XAMPP {
    static IntAccessor acc_NLep("nConstituents");
    static CharAccessor acc_SF("SameFlavour");
    SUSY4LepMTModule::SUSY4LepMTModule(const std::string& name) :
        BaseAnalysisModule(name),
        m_met_container("MetTST"),
        m_electron_container(),
        m_muon_container(),
        m_tau_container(),
        m_met(nullptr),
        m_electrons(nullptr),
        m_muons(nullptr),
        m_taus(nullptr),
        m_dec_MT2(nullptr),
        m_signal_only(false) {
        declareProperty("MissingET", m_met_container);
        declareProperty("ElectronContainer", m_electron_container);
        declareProperty("MuonContainer", m_muon_container);
        declareProperty("TauContainer", m_tau_container);
        declareProperty("SignalOnly", m_signal_only);
    }
    StatusCode SUSY4LepMTModule::fill() {
        xAOD::IParticleContainer* tau = m_taus->Container();
        xAOD::IParticleContainer* ele = m_electrons->Container();
        xAOD::IParticleContainer* muo = m_muons->Container();

        if (tau->size() + ele->size() + muo->size() < 4) {
            ATH_CHECK(fillDefaultValues());
            return StatusCode::SUCCESS;
        }
        xAOD::ParticleContainer* di_lep = m_ParticleConstructor->GetSubContainer("InvariantMomenta");
        if (di_lep == nullptr) return StatusCode::FAILURE;
        ATH_CHECK(m_ParticleConstructor->CreateSubContainer(full_name("TauMomenta")));
        ConstructInvariantMomenta(
            tau, m_ParticleConstructor.get(),
            [](const xAOD::IParticle* P) -> bool {
                static CharAccessor acc_signal("signal");
                if (acc_signal.isAvailable(*P)) return acc_signal(*P);
                return false;
            },
            2);
        m_ParticleConstructor->DetachSubContainer();
        const xAOD::Particle* Best_Lep_Z = nullptr;
        const xAOD::Particle* Sec_Lep_Z = nullptr;
        const xAOD::Particle* Best_Tau_Z = nullptr;
        static CharAccessor acc_signal("signal");
        for (xAOD::ParticleContainer::const_iterator Z1 = di_lep->begin(); Z1 != di_lep->end(); ++Z1) {
            const xAOD::Particle* ZCand = (*Z1);
            if (!isZCandidate(ZCand) || (m_signal_only && !acc_signal(*ZCand))) { continue; }
            if (!Best_Lep_Z) Best_Lep_Z = ZCand;
            bool found_sec = false;
            float d_M1 = std::fabs(ZCand->m() - XAMPP::Z_MASS);
            for (xAOD::ParticleContainer::const_iterator Z2 = di_lep->begin(); Z1 != Z2; ++Z2) {
                const xAOD::Particle* ZCand1 = (*Z2);
                if (!isZCandidate(ZCand1) || HasCommonConstructingParticles(ZCand, ZCand1) || (m_signal_only && !acc_signal(*ZCand1))) {
                    continue;
                }
                found_sec = true;
                float d_M2 = std::fabs(ZCand1->m() - XAMPP::Z_MASS);
                if (!Sec_Lep_Z || std::fabs(Best_Lep_Z->m() - XAMPP::Z_MASS) + std::fabs(Sec_Lep_Z->m() - XAMPP::Z_MASS) > d_M1 + d_M2) {
                    if (d_M1 < d_M2) {
                        Best_Lep_Z = ZCand;
                        Sec_Lep_Z = ZCand1;
                    } else {
                        Best_Lep_Z = ZCand1;
                        Sec_Lep_Z = ZCand;
                    }
                }
            }
            if (!found_sec && !Sec_Lep_Z) {
                for (const auto tau_Z : *m_ParticleConstructor->GetSubContainer(full_name("TauMomenta"))) {
                    if (tau_Z->charge() != 0 || (m_signal_only && !acc_signal(*tau_Z))) continue;
                    float d_M2 = std::fabs(tau_Z->m() - XAMPP::Z_MASS);
                    found_sec = true;
                    if (!Best_Tau_Z ||
                        std::fabs(Best_Lep_Z->m() - XAMPP::Z_MASS) + std::fabs(Best_Tau_Z->m() - XAMPP::Z_MASS) > d_M1 + d_M2) {
                        Best_Tau_Z = tau_Z;
                        Best_Lep_Z = ZCand;
                    }
                }
            }
            if (!found_sec) {
                if (std::fabs(Best_Lep_Z->m() - XAMPP::Z_MASS) > d_M1) Best_Lep_Z = ZCand;
            }
        }
        float MT2 = FLT_MAX;
        if (Sec_Lep_Z) {
            MT2 = CalculateMT2(Best_Lep_Z, Sec_Lep_Z, m_met);
        } else if (Best_Tau_Z) {
            MT2 = CalculateMT2(Best_Lep_Z, Best_Tau_Z, m_met);
        }
        ATH_CHECK(m_dec_MT2->Store(MT2));
        return StatusCode::SUCCESS;
    }

    StatusCode SUSY4LepMTModule::bookVariables() {
        m_met = m_XAMPPInfo->GetVariableStorage<xAOD::MissingET*>(m_met_container);
        if (m_met == nullptr) return StatusCode::FAILURE;
        ATH_CHECK(retrieve(m_electrons, m_electron_container));
        ATH_CHECK(retrieve(m_muons, m_muon_container));
        ATH_CHECK(retrieve(m_taus, m_tau_container));
        ATH_CHECK(newVariable("MT2", FLT_MAX, m_dec_MT2));
        return StatusCode::SUCCESS;
    }
}  // namespace XAMPP
