import ROOT, os, sys


def hist1DtoStrList(h):
    strList = []
    xAxis = h.GetXaxis()
    for xBin in range(0, h.GetNbinsX() + 2):
        strList.append("{:5.1f}, {:5.1f}: {:10.5f} {:10.5f} {:10.5f}\n".format(xAxis.GetBinLowEdge(xBin), xAxis.GetBinUpEdge(xBin),
                                                                               h.GetBinContent(xBin), h.GetBinErrorUp(xBin),
                                                                               h.GetBinErrorLow(xBin)))
    return strList


def hist2DtoStrList(h):
    xAxis = h.GetXaxis()
    yAxis = h.GetYaxis()
    strList = []
    for xBin in range(0, h.GetNbinsX() + 2):
        for yBin in range(0, h.GetNbinsY() + 2):
            strList.append("({:6.1f}, {:6.1f}) ({:6.1f}, {:6.1f}): {:10.5f} {:10.5f} {:10.5f}".format(
                xAxis.GetBinLowEdge(xBin), xAxis.GetBinUpEdge(xBin), yAxis.GetBinLowEdge(yBin), yAxis.GetBinUpEdge(yBin),
                h.GetBinContent(xBin, yBin), h.GetBinErrorUp(xBin, yBin), h.GetBinErrorLow(xBin, yBin)))
    return strList


def histToStrList(h, hLongName=""):
    strList = []
    if not isinstance(h, ROOT.TH1):
        print("{} is not a histogram".format(h.GetName()))
        return []
    elif isinstance(h, ROOT.TH3):
        print("{} is a TH3. Not supported.".format(h.GetName()))
        return []

    strList.append("{}\n".format(hLongName if hLongName != "" else h.GetName()))
    if isinstance(h, ROOT.TH2):
        strList += hist2DtoStrList(h)
    else:
        strList += hist1DtoStrList(h)
    strList.append('------------------------------------------------\n')
    return strList


def getHistoNamesFromDir(directory, dirName=""):
    hList = []
    for key in directory.GetListOfKeys():
        item = directory.Get(key.GetName())
        if isinstance(item, ROOT.TH1):
            hList.append(os.path.join(dirName, key.GetName()))
        elif isinstance(item, ROOT.TDirectoryFile):
            hList += getHistoNamesFromDir(item, os.path.join(dirName, item.GetName()))
    return hList


def histFromFileToStrList(inFileName):
    inFile = ROOT.TFile(inFileName)
    if not inFile.IsOpen():
        print("Could not open input ROOT file {}".format(inFileName))
        sys.exit(1)

    histograms = {}
    for hName in getHistoNamesFromDir(inFile):
        histograms[hName] = inFile.Get(hName)

    strList = []
    for hName in sorted(histograms.keys()):
        for line in histToStrList(histograms[hName], hName):
            strList.append(line)
    return strList
