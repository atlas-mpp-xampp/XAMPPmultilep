#!/usr/bin/python

import os
import ROOT
import numpy as np


class Event:
    def __init__(self, event, inSubRegion=False, inSubSubRegion=False):
        self.inSubRegion = inSubRegion
        self.inSubSubRegion = inSubSubRegion
        self.EventNumber = event.EventNumber
        self.runNumber = event.runNumber
        self.Met = event.Met
        self.Meff = event.Meff
        self.NJets = event.NJets
        self.NBJets = event.NBJets
        self.Mll_Z1 = event.Mll_Z1
        self.Mll_Z2 = event.Mll_Z2
        self.SigTau_pt = list(event.SigTau_pt)
        self.SigLep_pt = list(event.SigLep_pt)
        self.SigEle_pt = list(event.SigEle_pt)
        self.SigEle_q = list(event.SigEle_q)
        self.SigMuo_pt = list(event.SigMuo_pt)
        self.SigMuo_q = list(event.SigMuo_q)
        self.SigJet_pt = list(event.SigJet_pt)
        self.SigJet_bjet = [True if v else False for v in event.SigJet_bjet]


'''
Variables we need
- met 
- meff
- njets
- nbjets
- lepton pt, flavor
- m(SFOS) 1, m(SFOS) 2
- jet1 pt, jet2 pt
'''


def latexPrinter(events, outFile, subRegion="", subSubRegion="", hasBJets=True, tableOnly=True):
    nTau = 0
    if len(events[0].SigLep_pt) >= 4:
        pass
    elif len(events[0].SigLep_pt) == 3:
        nTau = 1
    elif len(events[0].SigLep_pt) == 2:
        nTau = 2

    variableTitles = []
    variableTitles = ["Run number", "Event number"]
    if subRegion != "":
        variableTitles += [subRegion]
    if subSubRegion != "":
        variableTitles += [subSubRegion]
    variableTitles += [
        "\\met",
        "\\meff",
        "$m_\\text{SFOS}$ pair 1",
    ]
    if nTau == 0:
        variableTitles += [
            "$m_\\text{SFOS}$ pair 2",
        ]
    variableTitles += [
        "N jets",
    ]
    if hasBJets:
        variableTitles += ["N $b-$jets"]

    for ii in range(1, 5 - nTau):
        variableTitles += ["$\\ell{}$ flavor".format(ii)]
        variableTitles += ["$\\pT^{{\\ell{}}}$".format(ii)]
    if "5L" in outFile:
        for ii in range(5, 7):
            variableTitles += ["$\\ell{}$ flavor".format(ii)]
            variableTitles += ["$\\pT^{{\\ell{}}}$".format(ii)]
    for ii in range(1, nTau + 1):
        variableTitles += ["$\\pT^{{\\tau{}}}$".format(ii)]
    for ii in range(1, 3):
        if hasBJets:
            variableTitles += ["j{} is $b-$jet".format(ii)]
        variableTitles += ["$\\pT^{{j{}}}$".format(ii)]

    with open(outFile, "w") as f:
        if not tableOnly:
            pass
        f.write("\\begin{sidewaystable}\n")
        f.write("\\begin{adjustbox}{width =0.99\\textwidth}\n")
        f.write("\\begin{tabular}{" + " ".join(["c" for ii in range(len(variableTitles))]) + "}\n")
        f.write("\\hline\n")
        f.write(" & ".join(variableTitles) + " \\\\\n")
        f.write("\\hline\\hline\n")
        for event in events:
            f.write("{:d} & {:d} & ".format(event.runNumber, event.EventNumber))
            if subRegion != "":
                f.write("* &" if event.inSubRegion else "&")
            if subSubRegion != "":
                f.write("* &" if event.inSubSubRegion else "&")
            f.write(" & ".join(["{:8.2f}".format(var) for var in [event.Met, event.Meff]]))
            if event.Mll_Z1 > 0:
                f.write(" & {:8.2f}".format(event.Mll_Z1))
            else:
                f.write(" &  ")
            if nTau == 0:
                if event.Mll_Z2 > 0:
                    f.write(" & {:8.2f}".format(event.Mll_Z2))
                else:
                    f.write(" &  ")
            f.write(" & {:8d}".format(event.NJets))
            if hasBJets:
                f.write(" & {:8d}".format(event.NBJets))
            for ii in range(0, 4 - nTau):
                flavor = ""
                for q, pT in zip(event.SigEle_q, event.SigEle_pt):
                    if abs(pT - event.SigLep_pt[ii]) < 1e-6:
                        flavor = "$e^{{{}}}$".format("+" if q > 0 else "-")
                for q, pT in zip(event.SigMuo_q, event.SigMuo_pt):
                    if abs(pT - event.SigLep_pt[ii]) < 1e-6:
                        flavor = "$\\mu^{{{}}}$".format("+" if q > 0 else "-")
                f.write(" & {} & {:8.2f}".format(flavor, event.SigLep_pt[ii]))
            if "5L" in outFile:
                for ii in range(4, 6):
                    # Skip 6th lepton if it doesn't exist
                    if ii == 5 and len(event.SigLep_pt) <= 5:
                        f.write(" & & ")
                        continue
                    flavor = ""
                    for q, pT in zip(event.SigEle_q, event.SigEle_pt):
                        if abs(pT - event.SigLep_pt[ii]) < 1e-6:
                            flavor = "$e^{{{}}}$".format("+" if q > 0 else "-")
                    for q, pT in zip(event.SigMuo_q, event.SigMuo_pt):
                        if abs(pT - event.SigLep_pt[ii]) < 1e-6:
                            flavor = "$\\mu^{{{}}}$".format("+" if q > 0 else "-")
                    f.write(" & {} & {:8.2f}".format(flavor, event.SigLep_pt[ii]))
            for ii in range(0, nTau):
                f.write(" & {:8.2f}".format(event.SigTau_pt[ii]))
            for ii in range(0, 2):
                if len(event.SigJet_pt) > ii:
                    if hasBJets:
                        f.write(" & {} & {:8.2f}".format(("yes" if event.SigJet_bjet[ii] else "no"), event.SigJet_pt[ii]))
                    else:
                        f.write(" & {:8.2f}".format(event.SigJet_pt[ii]))
                else:
                    if hasBJets:
                        f.write(" &    &        ")
                    else:
                        f.write(" &      ")

            f.write("\\\\\n")
        f.write("\\hline\n")
        f.write("\\end{tabular}\n\n")
        f.write("\\end{adjustbox}\n")
        f.write("\\caption{{Event printouts of signal region {}. ".format(outFile.split('.')[0]))
        if subSubRegion != "":
            f.write("Events in the sub-regions {} and {} are indicated by * in the appropriate columns. ".format(subRegion, subSubRegion))
        elif subRegion != "":
            f.write("Events in the sub-region {} are indicated by * in the appropriate column. ".format(subRegion))
        f.write("The variables \\met, \\meff, $m_\\text{SFOS}$, \\pT are printed in units of GeV and are omitted when not available.}\n")
        f.write("\\label{{table:printout-{}}}\n".format(outFile.split('.')[0]))
        f.write("\\end{sidewaystable}\n")
    return


def main():
    basedir = "/data/atlas/atlasdata/gallardo/4L/Histograms/0701-5L-trees/data"

    tc = ROOT.TChain("debugTree_Nominal")
    tc.Add(os.path.join(basedir, "*.root"))

    SR0A = []
    SR0D = []
    SR0G = []
    SR1A = []
    SR1C = []
    SR2A = []
    SR2C = []
    SR5L = []

    for event in tc:
        if event.IsSR0A:
            SR0A.append(Event(event, event.IsSR0B))
        if event.IsSR0G:
            SR0G.append(Event(event))

        if event.IsSR1A:
            SR1A.append(Event(event, event.IsSR1B))
        if event.IsSR1C:
            SR1C.append(Event(event))

        if event.IsSR2A:
            SR2A.append(Event(event, event.IsSR2B))
        if event.IsSR2C:
            SR2C.append(Event(event))

        if event.IsSR0D:
            SR0D.append(Event(event, event.IsSR0E, event.IsSR0F))

        if event.Is5L:
            SR5L.append(Event(event))

    latexPrinter(SR0A, "SR0A.tex", subRegion="SR0B", hasBJets=False)
    latexPrinter(SR0G, "SR0G.tex")

    latexPrinter(SR1A, "SR1A.tex", subRegion="SR1B", hasBJets=False)
    latexPrinter(SR2A, "SR2A.tex", subRegion="SR2B", hasBJets=False)

    latexPrinter(SR1C, "SR1C.tex")
    latexPrinter(SR2C, "SR2C.tex")

    latexPrinter(SR0D, "SR0D.tex", subRegion="SR0E", subSubRegion="SR0F")
    latexPrinter(SR5L, "SR5L.tex")


if __name__ == "__main__":
    main()
