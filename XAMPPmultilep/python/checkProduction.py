#! /usr/bin/env python
from ClusterSubmission.Utils import ReadListFromFile, ResolvePath, WriteList, id_generator
from ClusterSubmission.PandaBookKeeper import getArgumentParser, interesting_grid_jobs, get_broken_jobs, write_broken_log
from ClusterSubmission.RequestToGroupDisk import initiateReplication
from ClusterSubmission.ListDisk import RUCIO_ACCOUNT, RUCIO_RSE
from XAMPPmultilep.SubmitToGrid import getAnalyses

import logging, os
logging.basicConfig(format='%(levelname)s : %(message)s', level=logging.INFO)


def setupRetryParser():
    ds_dir = ResolvePath("XAMPPmultilep/SampleLists/mc16_13TeV/")
    parser = getArgumentParser()
    #parser.set_defaults(select_status=["finished", "broken", "exhausted", "failed", "done", "running", "submitting"])
    parser.set_defaults(sync_db=True)
    parser.set_defaults(automatic_retry=True)
    parser.add_argument("--destRSE", help="Replicate the data to these sites", default=[RUCIO_RSE], nargs="+")
    parser.add_argument("--replicate_ds", help="Replicate output datasets", default=False, action="store_true")

    parser.add_argument("--resubmit_broken", help="Automatically resubmit the broken jobs", default=False, action="store_true")
    parser.add_argument('--stream',
                        help='What analysis stream should be assigned',
                        choices=[ana for ana in getAnalyses().iterkeys()],
                        required=True)
    parser.add_argument("--production", help="Name of the production", required=True)
    parser.add_argument('--productionRole', help='specify optionally a production role you have e.g. perf-muons', default='')
    parser.add_argument('--noSyst', help='No systematics will be submitted', action="store_true", default=False)
    parser.add_argument('--in_samples',
                        help="which samples have been used to make the production",
                        default=["%s/%s" % (ds_dir, item) for item in os.listdir(ds_dir)],
                        nargs="+")
    return parser


if __name__ == '__main__':
    options = setupRetryParser().parse_args()
    options.job_name_pattern = ["XAMPP.%s.%s." % (options.stream, options.production)]
    options.select_scopes = ["user.%s" % (RUCIO_ACCOUNT) if len(options.productionRole) == 0 else "group.%s" % (options.productionRole)]
    grid_jobs = interesting_grid_jobs(options)
    broken = get_broken_jobs(grid_jobs)

    to_replicate = []
    for job in grid_jobs:
        ### Reduce the submitted grid jobs
        ### All running/done jobs can be distributed to the sites
        if job in broken: continue
        to_replicate += ["%s_XAMPP" % (job.jobName[:len(job.jobName) if job.jobName.find("/") == -1 else job.jobName.rfind("/")])]

    ### Replicate the output datasets
    if options.replicate_ds:
        for rse in options.destRSE:
            initiateReplication(ListOfDataSets=to_replicate, Rucio=options.rucio, RSE=rse)
    if len(broken) > 0: write_broken_log(options, broken)
    if not options.resubmit_broken: exit(0)
    to_resubmit = []
    for job in broken:
        job_name = job.jobName
        if job_name.endswith("/"): job_name = job_name[:-1]
        physical_sample = job_name[job_name.find(".", job_name.find(options.production)) + 1:]
        to_resubmit += [physical_sample]
    submit_cmd = "python %s --stream %s --production %s --sampleList %s %s %s --DuplicateTask --physicalSamples %s" % (
        ResolvePath("XAMPPmultilep/python/SubmitToGrid.py"),
        options.stream,
        options.production,
        " ".join(options.in_samples),
        "" if len(options.productionRole) == 0 else "--productionRole %s" % (options.productionRole),
        "" if not options.noSyst else "--noSyst",
        " ".join(to_resubmit),
    )

    logging.info("Going to resubmit the %d broken jobs which are very likely due to failure" % (len(broken)))
    os.system(submit_cmd)
