from ClusterSubmission.Utils import ResolvePath, CreateDirectory, ReadListFromFile, ExecuteCommands, WriteList, setupBatchSubmitArgParser
from XAMPPbase.UpdateCI_CutFlow import setupCIparser, run_athena_cmds, evaluate_cut_flows
from XAMPPmultilep.SubmitToGrid import getAnalyses
import os, argparse


def setup4L_CIparser():
    parser = setupCIparser()
    parser.add_argument("--analysis",
                        help="About whch analysis are we talking about",
                        choices=[a for a in getAnalyses().iterkeys()],
                        required=True)
    parser.set_defaults(EOSpath="root://eoshome.cern.ch//eos/user/x/xampp/ci/4lepton/")
    parser.set_defaults(ciDir=ResolvePath("XAMPPmultilep/test/"))

    parser.set_defaults(noSyst=True)
    return parser


if __name__ == '__main__':
    options = setup4L_CIparser().parse_args()

    Proccessed_Smp = run_athena_cmds(options, job_options=getAnalyses()[options.analysis])
    evaluate_cut_flows(options, Proccessed_Smp, analysis=options.analysis)

    ### Clean up what's left from the previous directories
    CleanCmd = "rm -rf %s" % (options.TEMPdir)
    print "INFO: Clean up the working directory %s" % (CleanCmd)
    #os.system(CleanCmd)
