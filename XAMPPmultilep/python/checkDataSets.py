#! /usr/bin/env python
from ClusterSubmission.PeriodRunConverter import GetPeriodRunConverter
from XAMPPmultilep.SubmitToGrid import getLogicalDataSets
from ClusterSubmission.Utils import WriteList, ReadListFromFile, ResolvePath

if __name__ == '__main__':

    Existing_Datasets = ReadListFromFile(ResolvePath("XAMPPmultilep/SampleLists/mc16_13TeV/backgrounds.txt")) + ReadListFromFile(
        ResolvePath("XAMPPmultilep/SampleLists/mc16_13TeV/signals.txt"))
    Existing_DSIDS = [int(L.split(".")[1]) for L in Existing_Datasets]

    DSIDs = []
    for logical, samples in getLogicalDataSets().iteritems():
        DSIDs += [s for s in samples if s not in Existing_DSIDS]

    print DSIDs
    WriteList(["mc16_13TeV.%d" % (d) for d in DSIDs], "XAMPPmultilep/data/SampleLists/mc16_13TeV/toAdd.txt")
