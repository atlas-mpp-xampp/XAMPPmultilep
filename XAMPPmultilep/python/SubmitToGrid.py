#! /usr/bin/env python
import os, argparse, commands, sys, logging
from ClusterSubmission.Utils import ReadListFromFile, ResolvePath, CheckPandaSetup, ClearFromDuplicates
from ClusterSubmission.PeriodRunConverter import GetPeriodRunConverter

rangeID = lambda start, end: range(start, end + 1)


def getLogicalDataSets():
    return {
        #Z+jets -  EW is missing
        "MG5Py8_Ztautau_Np": rangeID(361510, 361514),
        "MG5Py8_Zmumu_HT": rangeID(363123, 363146),
        "MG5Py8_Zee_HT": rangeID(363147, 363170),
        "PowHegPy8_DYee": rangeID(301000, 301018),  # + [361106, 361664, 361665],
        "PowHegPy8_Zee": [361106, 361664, 361665],
        "PowHegPy8_DYmumu": rangeID(301020, 301038),  # + [361107, 361666, 361667],
        "PowHegPy8_Zmumu": [361107, 361666, 361667],
        "PowHegPy8_DYtautau": rangeID(301040, 301058),  # + [361108, 361668, 361669],
        "PowHegPy8_Ztautau": [361108, 361668, 361669],
        "Sherpa221_Zmumu": rangeID(364198, 364203) + rangeID(364100, 364113),  # + [344441],
        "Sherpa221_Zee": rangeID(364204, 364209) + rangeID(364114, 364127),  # + [344442],
        "Sherpa221_Ztautau": rangeID(364210, 364215) + rangeID(364128, 364141),  # + [344443],
        ### Low mll Z+jets
        "Sherpa221_Zee_LowMll": [364280, 364358, 364361],
        "Sherpa221_Zmumu_LowMll": [364281, 364359, 364362],
        "Sherpa221_Ztautau_LowMll": [364282, 364360, 364363],

        #ZGamma + jets"
        "Sherpa221_ZYee": rangeID(364500, 364504),
        "Sherpa221_ZYmumu": rangeID(364505, 364509),
        "Sherpa221_ZYtautau": rangeID(364510, 364514),
        "PowHegPy8_Wenu": rangeID(301060, 301098) + [361100, 361103],
        "PowHegPy8_Wmunu": rangeID(301100, 301138) + [361101, 361104],
        "PowHegPy8_Wtaunu": rangeID(301140, 301178) + [361102, 361105],
        "Sherpa221_Wmunu": rangeID(364156, 364169),  #+ [344438],
        "Sherpa221_Wenu": rangeID(364170, 364183),  #+ [344439],
        "Sherpa221_Wtaunu": rangeID(364184, 364197),  #+ [344440],

        #Top
        "PowhegPy_top": rangeID(410011, 410014) + rangeID(410025, 410026) + rangeID(410644, 410649),
        "PowHegPy8_ttbar_incl": rangeID(410470, 410471),
        "PowHegPy8_ttbar_diL": [410472],
        "PowHegPy8_ttbar_3L": [343637],
        "MG5Py8_ttZ": rangeID(410111, 410116),
        "MG5Py8_ttW": rangeID(410066, 410068),
        #"MG5Py8_ttWZ": [407321],
        # "MG5Py8_ttWW": [410081],
        "MG5Py8_ttXX": [410081, 407321] + rangeID(500460, 500462),
        "Sherpa_ttV": [410142, 410144],
        "aMCatNLOPy8_ttZ": rangeID(410218, 410220) + rangeID(410276, 410278) + rangeID(410156, 410157),
        "Sherpa221_ttZ": [413022],
        "Sherpa221_ttZ_multileg": [413023],
        "aMcAtNloPy8_tHjb_4fl": [346414],
        "aMcAtNloPy8_tWH": [346511],
        ### Systematic samples w.r.t. underlying event tune
        "aMCatNLOPy8_ttZ_A14VarUp": [410370, 410372, 410374],
        "aMCatNLOPy8_ttZ_A14VarDn": [410371, 410373, 410375],
        "aMCatNLOPy8_ttW": [410155],
        "MG5Py8_tZ_3l": [410550],
        "MG5Py8_tZ_noAllHad": [410560],
        "MG5Py8_4t": [410080, 304014],
        "Sherpa_221_ttZ": [413022],
        ## ttGamma
        "aMCAtNLO_ttY": [407320],
        "MG5Py8_ttY_incl": [410082],
        "MG5Py8_ttY_4080": [410083],
        "MG5Py8_ttY_80": [410084],

        #Multibosons VV and VVV
        "PowHegPy8_VV": rangeID(361600, 361610),
        "Sherpa221_VV": rangeID(363355, 363360) + rangeID(363489, 363494),  # + rangeID(361070, 361073),
        "Sherpa221_VVV": rangeID(407311, 407315) + rangeID(364283, 364285),
        "Sherpa222_VV": rangeID(364253, 364255) + [364250, 364288, 364289],
        "Sherpa222_VV_CKKW15": [364256, 364275, 364297, 366074, 366079],
        "Sherpa222_VV_CKKW30": [364257, 364276, 364298, 366078, 366073],
        "Sherpa222_VV_QSF025": [364258, 364277, 364300, 366072, 366077],
        "Sherpa222_VV_QSF4": [364259, 364278, 364301, 366071, 366076],
        "Sherpa222_VV_CSSKIN": [364260, 364279, 364299, 366075, 366080],
        "Sherpa222_ZZm4L": [364251, 364252],
        "Sherpa222_VVV": rangeID(363507, 363509) + rangeID(364242, 364249),
        "Sherpa222_VV3L": [364261],
        "Sherpa222_ggZZ": [345705, 345706],
        "Sherpa222_ggZZnoH": [345708, 345709],

        ### This one is the inclusive sample for the Higgs decay but sliced into the ttbar decays
        "aMcAtNloPy8_ttH": rangeID(343365, 343367),
        "PowHegPy8_WH": rangeID(345104, 345105) + [341964],  # rangeID(341421, 341436) [WH->qq4L; WH->2L 2L2q; overlap with VVV]
        "PowHegPy8_ZH": [341947],  # + [345103] # + rangeID(341449, 341462) [ZH->qq4L, ZH->2L 2L2q, ZH->2L 4L; overlaps with VVV]
        "PowHegPy8_ZHtautau": [345217],
        "PowHegPy8_ggH": [345060],
        "PowHegPy8_ttH": [346343, 346344, 346345],
        # "PowHegPy8_VBFH": [341488], # overlaps with 364283
        "aMcAtNlo_tWZ": [410408],

        ### Signal samples

        ### GGM model
        "GGMHinoZh50_130": [395244],
        "GGMHinoZh50_150": [395245],
        "GGMHinoZh50_200": [395246],
        "GGMHinoZh50_250": [395247],
        "GGMHinoZh50_300": [395248],
        "GGMHinoZh50_325": [395249],
        "GGMHinoZh50_350": [395250],
        "GGMHinoZh50_375": [395251],
        "GGMHinoZh50_400": [395252],
        "GGMHinoZh50_425": [395253],
        "GGMHinoZh50_450": [395254],
        "GGMHinoZh50_475": [395255],
        "GGMHinoZh50_500": [395256],
        "GGMHinoZh50_525": [395257],
        "GGMHinoZh50_550": [395258],
        "GGMHinoZh50_95": [396090],
        "GGMHinoZh50_110": [396091],
        "GGMHinoZh50_600": [397943],
        "GGMHinoZh50_650": [397944],
        "GGMHinoZh50_700": [397945],
        "GGMHinoZh50_750": [397946],
        "GGMHinoZh50_800": [397947],

        ### Exotic bilepton model
        "BiLepton_Ypp331_M750": [311686],
        "BiLepton_Ypp331_M850": [311687],
        "BiLepton_Ypp331_M850": [311687],
        "BiLepton_Ypp331_M950": [311688],
        "BiLepton_Ypp331_M1050": [311689],

        #### Wino model
        "Wino_1300_10_LLE12k": [397503],
        "Wino_1300_100_LLE12k": [397504],
        "Wino_1300_1290_LLE12k": [397505],
        "Wino_1500_10_LLE12k": [397506],
        "Wino_1500_100_LLE12k": [397507],
        "Wino_1500_1490_LLE12k": [397508],
        "Wino_1700_10_LLE12k": [397509],
        "Wino_1700_50_LLE12k": [397510],
        "Wino_1700_200_LLE12k": [397511],
        "Wino_1700_800_LLE12k": [397512],
        "Wino_1700_1600_LLE12k": [397513],
        "Wino_1700_1690_LLE12k": [397514],
        "Wino_1800_10_LLE12k": [397515],
        "Wino_1800_50_LLE12k": [397516],
        "Wino_1800_200_LLE12k": [397517],
        "Wino_1800_900_LLE12k": [397518],
        "Wino_1800_1700_LLE12k": [397519],
        "Wino_1800_1790_LLE12k": [397520],
        "Wino_1900_10_LLE12k": [397521],
        "Wino_1900_50_LLE12k": [397522],
        "Wino_1900_200_LLE12k": [397523],
        "Wino_1900_1000_LLE12k": [397524],
        "Wino_1900_1800_LLE12k": [397525],
        "Wino_1900_1890_LLE12k": [397526],
        "Wino_800_10_LLEi33": [397527],
        "Wino_800_50_LLEi33": [397528],
        "Wino_800_100_LLEi33": [397529],
        "Wino_800_790_LLEi33": [397530],
        "Wino_1000_10_LLEi33": [397531],
        "Wino_1000_50_LLEi33": [397532],
        "Wino_1000_100_LLEi33": [397533],
        "Wino_1000_990_LLEi33": [397534],
        "Wino_1100_100_LLEi33": [397535],
        "Wino_1100_500_LLEi33": [397536],
        "Wino_1100_1000_LLEi33": [397537],
        "Wino_1100_1090_LLEi33": [397538],
        "Wino_1200_100_LLEi33": [397539],
        "Wino_1200_600_LLEi33": [397540],
        "Wino_1200_1100_LLEi33": [397541],
        "Wino_1200_1190_LLEi33": [397542],
        "Wino_1300_100_LLEi33": [397543],
        "Wino_1300_700_LLEi33": [397544],
        "Wino_1300_1200_LLEi33": [397545],
        "Wino_1300_1290_LLEi33": [397546],
        'Wino_1300_200_LLE12k': [449832],
        'Wino_1300_800_LLE12k': [449833],
        'Wino_1500_200_LLE12k': [449834],
        'Wino_1500_800_LLE12k': [449835],
        'Wino_1500_1200_LLE12k': [449836],
        'Wino_1700_1200_LLE12k': [449837],
        'Wino_1800_1200_LLE12k': [449838],
        'Wino_1000_500_LLEi33': [449839],
        'Wino_1000_700_LLEi33': [449840],
        'Wino_1100_700_LLEi33': [449841],
        'Wino_1200_500_LLEi33': [449842],
        'Wino_1200_700_LLEi33': [449843],
        'Wino_1200_1000_LLEi33': [449844],
        'Wino_1300_500_LLEi33': [449845],
        'Wino_1300_1000_LLEi33': [449846],
        'Wino_1400_100_LLEi33': [449847],
        'Wino_1400_500_LLEi33': [449848],
        'Wino_1400_700_LLEi33': [449849],
        'Wino_1400_1000_LLEi33': [449850],
        'Wino_1400_1390_LLEi33': [449851],
        ### Gluino model
        "GG_1600_10_LLEi33": [397586],
        "GG_1600_1590_LLEi33": [397589],
        "GG_1600_50_LLEi33": [397587],
        "GG_1600_800_LLEi33": [397588],
        "GG_1900_1000_LLEi33": [397594],
        "GG_1900_100_LLEi33": [397592],
        "GG_1900_10_LLEi33": [397590],
        "GG_1900_1890_LLEi33": [397595],
        "GG_1900_500_LLEi33": [397593],
        "GG_1900_50_LLEi33": [397591],
        "GG_2000_1000_LLEi33": [397597],
        "GG_2000_100_LLEi33": [397596],
        "GG_2000_1500_LLEi33": [397598],
        "GG_2000_1990_LLEi33": [397599],
        "GG_2100_1000_LLEi33": [397601],
        "GG_2100_100_LLEi33": [397600],
        "GG_2100_1500_LLEi33": [397602],
        "GG_2100_2090_LLEi33": [397603],
        "GG_2200_100_LLEi33": [397604],
        "GG_2200_10_LLE12k": [397553],
        "GG_2200_1100_LLEi33": [397605],
        "GG_2200_1600_LLEi33": [397606],
        "GG_2200_200_LLE12k": [397555],
        "GG_2200_2190_LLE12k": [397556],
        "GG_2200_2190_LLEi33": [397607],
        "GG_2200_50_LLE12k": [397554],
        "GG_2400_1000_LLE12k": [397560],
        "GG_2400_10_LLE12k": [397557],
        "GG_2400_1500_LLE12k": [397561],
        "GG_2400_2000_LLE12k": [397562],
        "GG_2400_200_LLE12k": [397559],
        "GG_2400_2390_LLE12k": [397563],
        "GG_2400_50_LLE12k": [397558],
        "GG_2500_1000_LLE12k": [397567],
        "GG_2500_10_LLE12k": [397564],
        "GG_2500_1500_LLE12k": [397568],
        "GG_2500_2000_LLE12k": [397569],
        "GG_2500_200_LLE12k": [397566],
        "GG_2500_2490_LLE12k": [397570],
        "GG_2500_50_LLE12k": [397565],
        "GG_2600_1000_LLE12k": [397574],
        "GG_2600_10_LLE12k": [397571],
        "GG_2600_1500_LLE12k": [397575],
        "GG_2600_2000_LLE12k": [397576],
        "GG_2600_200_LLE12k": [397573],
        "GG_2600_2590_LLE12k": [397577],
        "GG_2600_50_LLE12k": [397572],
        "GG_2700_1000_LLE12k": [397579],
        "GG_2700_1500_LLE12k": [397580],
        "GG_2700_200_LLE12k": [397578],
        "GG_2700_2690_LLE12k": [397581],
        "GG_2800_1000_LLE12k": [397583],
        "GG_2800_2000_LLE12k": [397584],
        "GG_2800_200_LLE12k": [397582],
        "GG_2800_2790_LLE12k": [397585],
        'GG_1400_100_LLEi33': [404292],
        'GG_1400_10_LLEi33': [404290],
        'GG_1400_1390_LLEi33': [404295],
        'GG_1400_500_LLEi33': [404293],
        'GG_1400_50_LLEi33': [404291],
        'GG_1400_900_LLEi33': [404294],
        'GG_1600_100_LLEi33': [449883],
        'GG_1600_1500_LLEi33': [449885],
        'GG_1600_500_LLEi33': [449884],
        'GG_1800_1000_LLEi33': [449888],
        'GG_1800_100_LLEi33': [449886],
        'GG_1800_1500_LLEi33': [449889],
        'GG_1800_1790_LLEi33': [449890],
        'GG_1800_500_LLEi33': [449887],
        'GG_1900_1500_LLEi33': [449891],
        'GG_2000_1000_LLE12k': [449877],
        'GG_2000_100_LLE12k': [449876],
        'GG_2000_1500_LLE12k': [449878],
        'GG_2000_1990_LLE12k': [449879],
        'GG_2100_500_LLEi33': [449893],
        'GG_2200_1000_LLE12k': [449880],
        'GG_2200_1500_LLE12k': [449881],
        'GG_2200_1500_LLEi33': [449894],
        'GG_2200_2000_LLE12k': [449882],
        'GG_2200_2190_LLEi33': [397607],
        'GG_2200_2190_LLEi33': [449895],

        ### Slepton model
        "LV_900_10_LLE12k": [397813],
        "LV_900_100_LLE12k": [397814],
        "LV_900_890_LLE12k": [397815],
        "LV_1100_10_LLE12k": [397816],
        "LV_1100_50_LLE12k": [397817],
        "LV_1100_200_LLE12k": [397818],
        "LV_1100_600_LLE12k": [397819],
        "LV_1100_1090_LLE12k": [397820],
        "LV_1200_10_LLE12k": [397821],
        "LV_1200_50_LLE12k": [397822],
        "LV_1200_200_LLE12k": [397823],
        "LV_1200_600_LLE12k": [397824],
        "LV_1200_1190_LLE12k": [397825],
        "LV_1300_10_LLE12k": [397826],
        "LV_1300_50_LLE12k": [397827],
        "LV_1300_200_LLE12k": [397828],
        "LV_1300_700_LLE12k": [397829],
        "LV_1300_1290_LLE12k": [397830],
        "LV_700_10_LLEi33": [397831],
        "LV_700_50_LLEi33": [397832],
        "LV_700_100_LLEi33": [397833],
        "LV_700_600_LLEi33": [397834],
        "LV_700_690_LLEi33": [397835],
        "LV_800_100_LLEi33": [397836],
        "LV_800_400_LLEi33": [397837],
        "LV_800_700_LLEi33": [397838],
        "LV_800_790_LLEi33": [397839],
        "LV_900_100_LLEi33": [397840],
        "LV_900_500_LLEi33": [397841],
        "LV_900_800_LLEi33": [397842],
        "LV_900_890_LLEi33": [397843],
        "LV_1000_100_LLEi33": [397844],
        "LV_1000_500_LLEi33": [397845],
        "LV_1000_900_LLEi33": [397846],
        "LV_1000_990_LLEi33": [397847],
        'LV_1000_50_LLE12k': [449852],
        'LV_1000_200_LLE12k': [449854],
        'LV_1000_400_LLE12k': [449855],
        'LV_1000_600_LLE12k': [449856],
        'LV_1100_800_LLE12k': [449860],
        'LV_1200_400_LLE12k': [449861],
        'LV_1200_800_LLE12k': [449862],
        'LV_1300_400_LLE12k': [449863],
        'LV_1300_800_LLE12k': [449864],
        'LV_800_300_LLEi33': [449865],
        'LV_800_600_LLEi33': [449866],
        'LV_1000_300_LLEi33': [449869],
        'LV_1100_100_LLEi33': [449871],
        'LV_1100_1090_LLEi33': [449875],
        'GG_2200_2190_LLEi33': [397607],
        'Wino_1300_directLLE12k': [449826],
        'Wino_1600_directLLE12k': [449827],
        'Wino_1800_directLLE12k': [449828],
        'LV_700_directLLE12k': [449829],
        'LV_900_directLLE12k': [449830],
        'LV_600_directLLEi33': [449831],
        'LV_1000_100_LLE12k': [449853],
        'LV_1000_800_LLE12k': [449857],
        'LV_1000_990_LLE12k': [449858],
        'LV_1100_400_LLE12k': [449859],
        'LV_900_300_LLEi33': [449867],
        'LV_900_700_LLEi33': [449868],
        'LV_1000_800_LLEi33': [449870],
        'LV_1100_300_LLEi33': [449872],
        'LV_1100_600_LLEi33': [449873],
        'LV_1100_800_LLEi33': [449874],
        'GG_2000_500_LLEi33': [449892],

        ### Samples for the combination efforts
        'C1pN2_WZ_100p0_95p0_2L2MET75_MadSpin': [377407],
        'C1pN2_WZ_100p0_75p0_2L2MET75_MadSpin': [377410],
        'C1N2_WZ_500p0_100p0_3L_2L7': [392202],
        'C1N2_WZ_500p0_100p0_2L2J_2L7': [392302],
        'C1N2_Wh_hgg_200p0_0p0_all': [394865],
        'C1N2_Wh_hgg_300p0_100p0_all': [394870],
        'C1N2N1_GGMHinoZh50_500_4LT4': [395256],
        'SlepSlep_direct_200p0_170p0_2L2MET75': [395944],
        'SlepSlep_direct_200p0_150p0_2L8': [395946],
        'SlepSlep_direct_200p0_140p0_2L8': [395947],
        'SlepSlep_direct_300p0_290p0_2L2MET75': [395968],
        'SlepSlep_direct_300p0_280p0_2L8': [395969],
        'C1N2_Stau_500p0_200p0_2L1T': [396468],
        'C1N2_Wh_hall_200p0_0p0_1T': [396597],
        'C1N2_Wh_hall_200p0_0p0_2L7': [396644],
        'C1N2_Wh_hall_300p0_100p0_2L7': [396668],
        'C1N2_Wh_hbb_300p0_100p0_lep': [396715],
        'C1N2_Wh_hbb_900p0_200p0_lep': [396804],
        'C1C1_WW_375p0_75p0_1L': [396881],
        'C1C1_WW_500p0_0p0_1L': [396892],
        'C1C1_WW_375p0_75p0_2L8_MadSpin': [396970],
        'C1N2_Wh_hbb_900p0_200p0_had': [397065],
        'C1N2N1_GGMHinoZh50_500_2L3': [397220],
        'C1N2N1_GGMHinoZh50_500_noFilter': [397231],
        'C1N2_WZ_500_0': [397737],
    }


def GetDSID(DS):
    DSName = DS[DS.find(":") + 1:]
    #Assume the structure: mc16_13TeV.410025.S18.LepHad.V04_XAMPP
    if not DSName.startswith("mc"):
        print DSName
        return -1
    dsid = int(DSName[DSName.find(".") + 1:DSName.find(".", DSName.find(".") + 1)])
    return dsid


def GetRunNumber(DSName):
    if DS.find(":") > -1:
        SubmittingAcc = DS[:DS.find(":")]
        DSName = DS[DS.find(SubmittingAcc, DS.find(":") + 1) + len(SubmittingAcc) + 1:]
    else:
        DSName = DS

    #Assume the structure data16_13TeV.<DSID>.<Blah>
    if not DSName.startswith("data"): return -1
    try:
        dsid = int(DSName[DSName.find(".") + 1:DSName.find(".", DSName.find(".") + 1)])
    except:
        return -1
    return dsid


def GetYearAndPeriod(DSName):
    #Assume: data15_13TeV.periodH.physics_Main.PhysCont.XAMPPstau.v01.grp15_v01_p3213_XAMPP

    if not DSName.startswith("data"): return -1
    period = [elem for elem in DSName.split(".") if elem.startswith('period')][-1].strip('period')
    year = [elem for elem in DSName.split(".") if elem.startswith('data')][-1].replace('data', "").replace("_13TeV", "")
    if not period or not year:
        return "", ""
    return year, period


def GetLogicalDataSetName(DS):
    SubmittingAcc = DS[:DS.find(":")]
    DSName = DS if DS.find(":") == -1 else DS[DS.find(":") + 1:]
    ID = GetDSID(DSName)
    if ID != -1:
        for Logical, RunRanges in getLogicalDataSets().iteritems():
            if ID in RunRanges: return Logical
    else:
        RunNumber = GetRunNumber(DSName)
        if RunNumber != -1:
            year, period = GetPeriodRunConverter().GetPeriodFromRun(RunNumber)
            if year and period: return "data%i_period%s" % (year, period)
        else:
            year, period = GetYearAndPeriod(DSName)
            if year and period: return "data%s_period%s" % (year, period)
    return DSName


def getAnalyses():
    return {
        "SUSY4L": "XAMPPmultilep/run4LeptonAnalysis.py",
        "4LepSF": "XAMPPmultilep/run4LeptonSFAnalysis.py",
        "Truth4L": "XAMPPmultilep/runTruth4L.py",
        "ttGamma": "XAMPPmultilep/run_ttY.py",
    }


def setupGridSubmitArgParser():
    parser = argparse.ArgumentParser(
        description=
        'This script submits the analysis code to the grid. For more help type \"python XAMPPmultilep/python/SubmitToGrid.py -h\"',
        prog='SubmitToGrid',
        conflict_handler='resolve',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--noSyst', help='No systematics will be submitted', action="store_true", default=False)
    parser.add_argument('--stream',
                        help='What analysis stream should be assigned',
                        choices=[ana for ana in getAnalyses().iterkeys()],
                        required=True)
    parser.add_argument("--production", help="Name of the production", required=True)
    parser.add_argument("--sampleList", help="List of datasets to submit the production on", required=True, nargs='+')
    parser.add_argument('--productionRole', help='specify optionally a production role you have e.g. perf-muons', default='')
    parser.add_argument('--DuplicateTask',
                        help='You can create another job with the same output dataset...',
                        action='store_true',
                        default=False)

    parser.add_argument(
        "--physicalSamples",
        help="Only submit datasets which belong to this list of physical samples",
        nargs='+',
        default=[],
    )
    parser.add_argument("--logicalSamplesOnly",
                        help="Omit to submit on samples which are not part of the logical sample lists",
                        default=False,
                        action='store_true')
    parser.add_argument("--newTarBall", help="Make a new TarBall with the first submit command", default=False, action='store_true')
    parser.add_argument("--completeCampaigns",
                        help="Only sumbits samples which have mc16a, mc16d and mc16e in",
                        default=False,
                        action='store_true')
    parser.add_argument('--noMergeJob', help='Do not merge the files on the grid', action='store_true', default=False)

    return parser


if __name__ == '__main__':
    RunOptions = setupGridSubmitArgParser().parse_args()
    CheckPandaSetup()
    ### Do not use the AODs since they are place holders in the list
    All_DS = []
    for Smp in RunOptions.sampleList:
        All_DS += [F for F in ReadListFromFile(Smp) if F.find("DAOD") != -1]
    All_DS = ClearFromDuplicates(All_DS)
    All_DS.sort()
    ### Build the dictionary
    Samples_ToSubmit = {}

    for DS in All_DS:
        Logical_Name = GetLogicalDataSetName(DS)
        if RunOptions.logicalSamplesOnly and Logical_Name not in getLogicalDataSets().iterkeys() and Logical_Name.find("data") == -1:
            continue
        if len(RunOptions.physicalSamples) > 0 and Logical_Name not in RunOptions.physicalSamples:
            continue

        try:
            Samples_ToSubmit[Logical_Name] += [DS]
        except:
            Samples_ToSubmit[Logical_Name] = [DS]

    SubmitScript = ResolvePath("XAMPPbase/python/SubmitToGrid.py")
    remove_taball = RunOptions.newTarBall
    Skipped_DS = []

    for Sample, DataSetsToSubmit in Samples_ToSubmit.iteritems():
        incomplete_campaign = False
        if RunOptions.completeCampaigns:
            dsids = [GetDSID(s) for s in DataSetsToSubmit]
            for dsid in dsids:
                if dsids.count(dsid) % 3 != 0:
                    incomplete_campaign = True
                    break

        if incomplete_campaign:
            Skipped_DS += DataSetsToSubmit
            continue
        Cmd = "python %s  --jobOptions %s --inputDS %s --outDS XAMPP.%s.%s.%s %s %s %s --noAmiCheck %s %s %s" % (
            SubmitScript, getAnalyses()[RunOptions.stream], ",".join(DataSetsToSubmit), RunOptions.stream, RunOptions.production, Sample,
            "--noSyst" if RunOptions.noSyst else "", "" if len(RunOptions.productionRole) == 0 else "--productionRole %s" %
            (RunOptions.productionRole), "--nFilesPerJob 1" if "data" not in Sample and not RunOptions.noSyst else "--nGBPerJob 5",
            "" if not RunOptions.DuplicateTask else " --DuplicateTask ", "" if not remove_taball else " --newTarBall",
            "--noMergeJob" if RunOptions.noMergeJob else "")
        os.system(Cmd)
        remove_taball = False

    for Smp in RunOptions.sampleList:
        Skipped_DS += [F for F in ReadListFromFile(Smp) if F.find("DAOD") == -1]
    if len(Skipped_DS) > 0:
        print "WARNING: The following datasets were not submitted since they are no DAODs"
        for S in Skipped_DS:
            print "    --- %s" % (S)
