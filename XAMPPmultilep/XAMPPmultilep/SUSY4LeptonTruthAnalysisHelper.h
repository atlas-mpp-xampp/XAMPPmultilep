#ifndef XAMPPmultilep_SUSY4TruthLeptonAnalysisHelper_H
#define XAMPPmultilep_SUSY4TruthLeptonAnalysisHelper_H
#include <XAMPPbase/SUSYTruthAnalysisHelper.h>

#include <fstream>
#include <iostream>
#include <sstream>
namespace XAMPP {
    class SUSY4LeptonTruthAnalysisHelper : public SUSYTruthAnalysisHelper {
    public:
        // Create a proper constructor for Athena
        ASG_TOOL_CLASS(SUSY4LeptonTruthAnalysisHelper, XAMPP::IAnalysisHelper)

        SUSY4LeptonTruthAnalysisHelper(const std::string& myname);
        virtual ~SUSY4LeptonTruthAnalysisHelper();
        virtual StatusCode RemoveOverlap();

    protected:
        virtual StatusCode initializeObjectTools();

        virtual bool SUSYdsid(int) const { return true; }
        virtual StatusCode initializeEventVariables();
        virtual StatusCode initializeParticleStores();
        virtual StatusCode ComputeEventVariables();
        StatusCode PassZVeto(xAOD::IParticleContainer& Leptons);
        bool IsSignalLepton(const xAOD::IParticle* P) const;

    private:
        bool m_applyDFOSVeto;
        bool m_ApplyIsoCorrect;
        float m_lowMassDiLepCut;
        float m_JetHt_minPt;
        float m_ZMassWindow;
        bool m_useNuForInvP4;
    };
}  // namespace XAMPP
#endif
