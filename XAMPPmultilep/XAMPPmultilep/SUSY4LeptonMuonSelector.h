#ifndef SUSY4LeptonMuonSelector_H
#define SUSY4LeptonMuonSelector_H
#include <XAMPPbase/SUSYMuonSelector.h>

namespace XAMPP {
    class SUSY4LeptonMuonSelector : public SUSYMuonSelector {
    public:
        // Create a proper constructor for Athena
        ASG_TOOL_CLASS(SUSY4LeptonMuonSelector, XAMPP::IMuonSelector)

        SUSY4LeptonMuonSelector(const std::string& myname);
        virtual ~SUSY4LeptonMuonSelector();

        StatusCode FillMuons(const CP::SystematicSet& systset) override;
        StatusCode initialize() override;

    protected:
        xAOD::MuonContainer* m_NonIsolMuons;
        xAOD::MuonContainer* m_FakeMuons;
    };
}  // namespace XAMPP
#endif
