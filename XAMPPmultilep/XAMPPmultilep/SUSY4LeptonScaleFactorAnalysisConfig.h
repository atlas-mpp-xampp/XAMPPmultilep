#ifndef XAMPPmultilep_SUSY4LeptonScaleFactorAnalysisConfig_h
#define XAMPPmultilep_SUSY4LeptonScaleFactorAnalysisConfig_h

#include <XAMPPbase/AnalysisConfig.h>

namespace XAMPP {
    class SUSY4LeptonScaleFactorAnalysisConfig : public AnalysisConfig {
    public:
        // Create a proper constructor for Athena
        ASG_TOOL_CLASS(SUSY4LeptonScaleFactorAnalysisConfig, XAMPP::IAnalysisConfig)

        SUSY4LeptonScaleFactorAnalysisConfig(std::string Analysis = "SUSY4LeptonScaleFactor");
        virtual ~SUSY4LeptonScaleFactorAnalysisConfig() {}

    protected:
        virtual StatusCode initializeCustomCuts();
    };
}  // namespace XAMPP
#endif
