#ifndef SUSY4LeptonMTModule_H
#define SUSY4LeptonMTModule_H

// Framework includes
#include <XAMPPbase/BaseAnalysisModule.h>
#include <XAMPPbase/EventInfo.h>
#include <XAMPPmultilep/AnalysisUtils.h>

#include <fstream>
#include <iostream>
#include <map>
#include <memory>
#include <sstream>
#include <vector>
namespace XAMPP {
    class SUSY4LepMTModule : public BaseAnalysisModule {
    public:
        SUSY4LepMTModule(const std::string& name);
        ASG_TOOL_CLASS(SUSY4LepMTModule, XAMPP::IAnalysisModule)

        virtual StatusCode fill();
        virtual StatusCode bookVariables();

    private:
        std::string m_met_container;

        std::string m_electron_container;
        std::string m_muon_container;
        std::string m_tau_container;

        XAMPP::Storage<XAMPPmet>* m_met;

        XAMPP::ParticleStorage* m_electrons;
        XAMPP::ParticleStorage* m_muons;
        XAMPP::ParticleStorage* m_taus;

        XAMPP::Storage<float>* m_dec_MT2;
        bool m_signal_only;
    };
}  // namespace XAMPP
#endif
