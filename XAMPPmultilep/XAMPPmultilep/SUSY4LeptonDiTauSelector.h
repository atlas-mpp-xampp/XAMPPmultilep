#ifdef SUSY4LeptonDiTauSelector_H
#define SUSY4LeptonDiTauSelector_H

#include <XAMPPmultilep/SUSY4LeptonTauSelector.h>
#include <xAODTau/DiTauJetContainer.h>

//#include <tauRecTools/IDiTauDiscriminantTool.h>
//#include <tauRecTools/IDiTauIDVarCalculator.h>

// namespace tauRecTools {
//    class IDiTauDiscriminantTool;
//    class IDiTauIDVarCalculator;
//}

namespace XAMPP {
    class SUSY4LeptonAnalysisHelper;
    class ITruthSelector;
    class IReconstructedParticles;

    class SUSY4LeptonDiTauSelector : public SUSY4LeptonTauSelector {
    public:
        // Create a proper constructor for Athena
        ASG_TOOL_CLASS(SUSY4LeptonDiTauSelector, XAMPP::ITauSelector)

        SUSY4LeptonDiTauSelector(const std::string& myname);
        virtual ~SUSY4LeptonDiTauSelector();
        virtual StatusCode initialize();
        virtual StatusCode FillTaus(const CP::SystematicSet& systset);
        virtual StatusCode InitialFill(const CP::SystematicSet& systset);

    protected:
        virtual bool PassPreSelection(const xAOD::IParticle& P) const;
        virtual bool PassSignal(const xAOD::IParticle& P) const;

        virtual StatusCode PerformTruthMatching(const xAOD::DiTauJet* itau);

        StatusCode PipeSubJetsIntoContainer(const xAOD::DiTauJet* DiTau, xAOD::JetContainer*& SubJetContainer);
        unsigned int NSubJets(const xAOD::DiTauJet* DiTau) const;
        unsigned int GetNMatches(xAOD::IParticleContainer* SubJetContainer, xAOD::IParticleContainer* TruthContainer, float dR) const;
        unsigned int GetNMatchesToFatJet(const xAOD::DiTauJet* DiTau, xAOD::IParticleContainer* TruthContainer, float dR) const;

        ToolHandle<ITruthSelector> m_truth_selection;

        //            asg::AnaToolHandle<tauRecTools::IDiTauDiscriminantTool> m_DiscrTool;
        //            asg::AnaToolHandle<tauRecTools::IDiTauIDVarCalculator> m_IDVarCalculator;

        asg::AnaToolHandle<XAMPP::IReconstructedParticles> m_ParticleConstructor;
    };
}  // namespace XAMPP

#endif
