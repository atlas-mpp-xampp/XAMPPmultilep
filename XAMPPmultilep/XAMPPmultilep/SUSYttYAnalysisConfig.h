#ifndef SUSYttYAnalysisConfig_h
#define SUSYttYAnalysisConfig_h
#include <XAMPPbase/AnalysisConfig.h>

namespace XAMPP {
    class SUSYttYAnalysisConfig : public AnalysisConfig {
    public:
        ASG_TOOL_CLASS(SUSYttYAnalysisConfig, XAMPP::IAnalysisConfig)

        SUSYttYAnalysisConfig(std::string Analysis = "SUSYttY");
        virtual ~SUSYttYAnalysisConfig() {}

    protected:
        virtual StatusCode initializeCustomCuts();
    };
}  // namespace XAMPP
#endif
