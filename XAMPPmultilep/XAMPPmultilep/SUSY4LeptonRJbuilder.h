#ifndef SUSY4LeptonRJbuilder_H
#define SUSY4LeptonRJbuilder_H

// Framework includes
#include <AsgTools/AnaToolHandle.h>
#include <AsgTools/ToolHandle.h>
#include <XAMPPbase/BaseAnalysisModule.h>
#include <XAMPPbase/ISystematics.h>
#include <XAMPPmultilep/AnalysisUtils.h>

#include <fstream>
#include <iostream>
#include <map>
#include <memory>
#include <sstream>
#include <vector>

#include "RestFrames/RestFrames.hh"
using namespace RestFrames;

typedef std::shared_ptr<ReconstructionFrame> RecoFrame_Ptr;
typedef std::shared_ptr<VisibleRecoFrame> VisibleRecoFrame_Ptr;
typedef std::shared_ptr<InvisibleGroup> InvisibleGroup_Ptr;
typedef std::shared_ptr<Jigsaw> Jigsaw_Ptr;
typedef std::shared_ptr<CombinatoricGroup> CombGrp_Ptr;
namespace XAMPP {
    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    //  Recusrive Jigsaw reconstruction, described in https://arxiv.org/abs/1705.10733
    //  might be suitable for the GGM part of the analysis. This tool implements the RJ
    //  for the C1N2N1 - GGM model following the example provided by abhishek.sharma@adelaide.edu.au.
    //          N2->ZG-> 2LG (a)
    //   C1N2 /
    //        \   1
    //         WN1->ZG->2LG (b)
    //
    class SUSY4LepJigSawBuilder : public BaseAnalysisModule {
    public:
        SUSY4LepJigSawBuilder(const std::string& name);

        ASG_TOOL_CLASS(SUSY4LepJigSawBuilder, XAMPP::IAnalysisModule)

        virtual StatusCode fill();
        virtual StatusCode initialize();
        virtual StatusCode bookVariables();
        enum JigSawFrame { N2a = 1, Ga = 2, C1b = -1, Wb = -2, N1b = -3, Gb = -4 };

    protected:
        virtual double metric(const xAOD::Particle* P, const xAOD::Particle* P2) const;

    private:
        double mass2(const xAOD::Particle* P) const;
        void saveInLabFrame(ReconstructionFrame& Frame, int FrameType);

        XAMPP::Storage<XAMPPmet>* m_met;

        // The input containers....
        XAMPP::ParticleStorage* m_electrons;
        XAMPP::ParticleStorage* m_muons;
        XAMPP::ParticleStorage* m_jets;
        // The output storage
        XAMPP::ParticleStorage* m_JigSaws;

        std::string m_electronContainer;
        std::string m_muonContainer;
        std::string m_diLeptonContainer;
        std::string m_jetContainer;
        std::string m_metContainer;

        // Frames of the drawn particle tree upstairs
        VisibleRecoFrame_Ptr m_Za_2L;
        VisibleRecoFrame_Ptr m_Zb_2L;

        RecoFrame_Ptr m_N2a_GZ;
        RecoFrame_Ptr m_N2b_GZ;

        RecoFrame_Ptr m_Ga;
        RecoFrame_Ptr m_Gb;

        RecoFrame_Ptr m_Wb_qq;

        VisibleRecoFrame_Ptr m_Q1b_2Q;
        VisibleRecoFrame_Ptr m_Q2b_2Q;

        RecoFrame_Ptr m_C1b_N2W;

        RecoFrame_Ptr m_C1N1N2_frame;
        std::shared_ptr<LabRecoFrame> m_lab_frame;

        InvisibleGroup_Ptr m_InvisGroup;
        // General JigSaws to impose on the entire decay tree
        Jigsaw_Ptr m_inivisblemass;
        Jigsaw_Ptr m_contraboost;
        Jigsaw_Ptr m_rapidity;
        Jigsaw_Ptr m_min_deltaM;

        // Combinatoric JigSaw to rsolve the 4 leptons in the decay tree
        CombGrp_Ptr m_LepComb;
        Jigsaw_Ptr m_LepResolver;
    };
}  // namespace XAMPP
#endif
