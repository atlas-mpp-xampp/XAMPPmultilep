#ifndef XAMPPMULTILEP_XAMPPMULTILEPDICT_H
#define XAMPPMULTILEP_XAMPPMULTILEPDICT_H

#include <XAMPPmultilep/SUSY4LeptonAnalysisConfig.h>
#include <XAMPPmultilep/SUSY4LeptonAnalysisHelper.h>
#include <XAMPPmultilep/SUSY4LeptonDiTauSelector.h>
#include <XAMPPmultilep/SUSY4LeptonElectronSelector.h>
#include <XAMPPmultilep/SUSY4LeptonMTModule.h>
#include <XAMPPmultilep/SUSY4LeptonMuonSelector.h>
#include <XAMPPmultilep/SUSY4LeptonRJbuilder.h>
#include <XAMPPmultilep/SUSY4LeptonScaleFactorAnalysisConfig.h>
#include <XAMPPmultilep/SUSY4LeptonScaleFactorAnalysisHelper.h>
#include <XAMPPmultilep/SUSY4LeptonTauSelector.h>
#include <XAMPPmultilep/SUSY4LeptonTruthAnalysisHelper.h>
#include <XAMPPmultilep/SUSY4LeptonTruthSelector.h>
#include <XAMPPmultilep/SUSYllqqAnalysisConfig.h>
#include <XAMPPmultilep/SUSYllqqAnalysisHelper.h>
#include <XAMPPmultilep/SUSYllqqJetSelector.h>
#include <XAMPPmultilep/SUSYttYAnalysisConfig.h>
#include <XAMPPmultilep/SUSYttYAnalysisHelper.h>
#include <XAMPPmultilep/SUSYttYPhotonSelector.h>

#endif
