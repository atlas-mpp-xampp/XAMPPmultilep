#ifndef SUSY4TruthLeptonTruthSelector_H
#define SUSY4TruthLeptonTruthSelector_H

#include <XAMPPbase/SUSYTruthSelector.h>

#include <fstream>
#include <iostream>
#include <sstream>
namespace XAMPP {
    class IReconstructedParticles;
    class SUSY4LeptonTruthSelector : public SUSYTruthSelector {
    public:
        // Create a proper constructor for Athena
        ASG_TOOL_CLASS(SUSY4LeptonTruthSelector, XAMPP::ITruthSelector)

        SUSY4LeptonTruthSelector(const std::string& myname);
        virtual ~SUSY4LeptonTruthSelector();
        virtual StatusCode initialize();
        virtual StatusCode LoadContainers();
        virtual StatusCode InitialFill(const CP::SystematicSet& systset);

    protected:
        using SUSYTruthSelector::InitDecorators;
        virtual void InitDecorators(xAOD::TruthParticle* T, bool Pass);
        virtual bool PassBaseline(const xAOD::IParticle& Particle) const;

        StatusCode GetTruthDiTaus(xAOD::IParticleContainer& TruthTaus);

        XAMPP::Storage<double>* m_Dec_LLE;

        XAMPP::Storage<double>* m_Dec_GGM100;
        XAMPP::Storage<double>* m_Dec_GGM90;
        XAMPP::Storage<double>* m_Dec_GGM80;
        XAMPP::Storage<double>* m_Dec_GGM75;
        XAMPP::Storage<double>* m_Dec_GGM70;
        XAMPP::Storage<double>* m_Dec_GGM60;
        XAMPP::Storage<double>* m_Dec_GGM40;
        XAMPP::Storage<double>* m_Dec_GGM30;
        XAMPP::Storage<double>* m_Dec_GGM25;
        XAMPP::Storage<double>* m_Dec_GGM20;
        XAMPP::Storage<double>* m_Dec_GGM10;
        XAMPP::Storage<double>* m_Dec_GGM0;

        asg::AnaToolHandle<XAMPP::IReconstructedParticles> m_ParticleConstructor;

    private:
        std::vector<int> m_ggm_dsids;
    };
}  // namespace XAMPP
#endif
