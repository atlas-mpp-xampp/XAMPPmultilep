#ifndef SUSY4LeptonElectronSelector_H
#define SUSY4LeptonElectronSelector_H
#include <XAMPPbase/SUSYElectronSelector.h>

namespace CP {
    class IEfficiencyScaleFactorTool;
}
class IAsgElectronLikelihoodTool;

namespace XAMPP {
    class SUSY4LeptonElectronSelector : public SUSYElectronSelector {
    public:
        // Create a proper constructor for Athena
        ASG_TOOL_CLASS(SUSY4LeptonElectronSelector, XAMPP::IElectronSelector)

        SUSY4LeptonElectronSelector(const std::string& myname);
        virtual ~SUSY4LeptonElectronSelector();

        virtual StatusCode FillElectrons(const CP::SystematicSet& systset);
        virtual StatusCode initialize();
        virtual StatusCode SaveScaleFactor();

    protected:
        xAOD::ElectronContainer* m_NonIsolElectrons;
        xAOD::ElectronContainer* m_FakeElectrons;
        ToolHandle<IAsgElectronLikelihoodTool> m_chargeSelector;
        ToolHandle<CP::IEfficiencyScaleFactorTool> m_chargeFlipEff;

        bool m_RunChargeFlip;
        bool m_ApplyQFlipSF;
        std::vector<ElectronWeightHandler_Ptr> m_charge_flip_SF;
    };

    class ChargeFlipElectronWeight : public ElectronWeightDecorator {
    public:
        ChargeFlipElectronWeight(ToolHandle<CP::IEfficiencyScaleFactorTool>& SFTool);
        virtual ~ChargeFlipElectronWeight();

    protected:
        virtual StatusCode calculateSF(const xAOD::Electron& Electron, double& SF);

    private:
        ToolHandle<CP::IEfficiencyScaleFactorTool> m_SFTool;
    };
}  // namespace XAMPP
#endif
