#ifndef SUSY4LeptonAnalysisHelper_H
#define SUSY4LeptonAnalysisHelper_H

#include <AsgTools/AnaToolHandle.h>
#include <AsgTools/ToolHandle.h>
#include <IsolationSelection/IIsolationSelectionTool.h>
#include <XAMPPbase/SUSYAnalysisHelper.h>

namespace InDet {
    class IInDetTrackSelectionTool;
}
namespace CP {
    class IIsolationCloseByCorrectionTool;
}

namespace XAMPP {
    class SUSY4LeptonAnalysisHelper : public SUSYAnalysisHelper {
    public:
        // Create a proper constructor for Athena
        ASG_TOOL_CLASS(SUSY4LeptonAnalysisHelper, XAMPP::IAnalysisHelper)

        SUSY4LeptonAnalysisHelper(const std::string& myname);
        virtual ~SUSY4LeptonAnalysisHelper();

        StatusCode FillObjects(const CP::SystematicSet* systset) override;
        StatusCode RemoveOverlap() override;
        bool AcceptEvent() override;

    protected:
        virtual StatusCode initLightLeptonTrees();
        virtual StatusCode initRecoParticleTree();
        virtual StatusCode initTauTree();
        virtual StatusCode initJetTree();
        virtual StatusCode initPhotonTree();
        virtual StatusCode initTruthTree();

        /// Create instances of ParticleStorages not to be written out
        // to the final trees but rather to be used as pipelines for AnalysisModules
        virtual StatusCode initializeModuleContainers();
        virtual StatusCode fillModuleContainers();

        virtual StatusCode initializeAnalysisTools() override;

        virtual StatusCode initializeIsoCorrectionTool();

        StatusCode LowMassRemoval(xAOD::IParticleContainer* elecs, xAOD::IParticleContainer* muons);

        virtual StatusCode initializeEventVariables() override;
        virtual StatusCode ComputeEventVariables() override;

        // Stanard methods to call in order to store the
        // Multiplicities  of the objects
        StatusCode InitializeMultiplicities();
        StatusCode FillMultiplicities();

        StatusCode CalculateMeffVariable();
        StatusCode InitializeMeffVariable();

        StatusCode PassZVeto(xAOD::IParticleContainer& Leptons);

        unsigned int nSignalElectrons() const;
        unsigned int nSignalMuons() const;
        unsigned int nSignalDiTaus() const;
        unsigned int nSignalTaus() const;
        unsigned int nSignalJets() const;

        unsigned int nBaselineElectrons() const;
        unsigned int nBaselineMuons() const;
        unsigned int nBaselineDiTaus() const;
        unsigned int nBaselineTaus() const;
        unsigned int nBaselineJets() const;
        bool IsSignalLepton(const xAOD::IParticle* P) const;

    private:
        bool m_RemoveLowMassLeptons;
        float m_LowMass_OSPairCut;
        float m_SFOS_UpperEdge;
        float m_SFOS_LowerEdge;
        bool m_ApplyIsoCorrect;
        bool m_ApplEventSkimming;

    protected:
        float m_JetHt_minPt;
        float m_ZMassWindow;

    private:
        // asg::AnaToolHandle<NearLep::IIsoCorrection> m_IsoCorrect;
        asg::AnaToolHandle<CP::IIsolationCloseByCorrectionTool> m_IsoCorrect;
        ToolHandle<CP::IIsolationSelectionTool> m_IsoTool;
        ToolHandleArray<CP::IIsolationSelectionTool> m_isoWPs;

        struct IsolationTester {
            SelectionDecorator particleDecoration;
            std::string name;
            ToolHandle<CP::IIsolationSelectionTool> isoTool;
            IsolationTester(const std::string& n, const ToolHandle<CP::IIsolationSelectionTool>& tool) {
                particleDecoration = std::make_unique<CharDecorator>("isol_" + n);
                name = n;
                isoTool = tool;
            }
        };
        std::vector<IsolationTester> m_isoTester_Elecs;
        std::vector<IsolationTester> m_isoTester_Muons;

        XAMPP::Storage<int>* m_dec_NLepBase;
        XAMPP::Storage<int>* m_dec_NLepSignal;

        XAMPP::Storage<int>* m_dec_NElecBase;
        XAMPP::Storage<int>* m_dec_NElecSignal;

        XAMPP::Storage<int>* m_dec_NMuonBase;
        XAMPP::Storage<int>* m_dec_NMuonSignal;

        XAMPP::Storage<int>* m_dec_NTauBase;
        XAMPP::Storage<int>* m_dec_NTauSignal;

        XAMPP::Storage<int>* m_dec_NJetSignal;
        XAMPP::Storage<int>* m_dec_NJetBase;
        XAMPP::Storage<int>* m_dec_NBJets;

        XAMPP::Storage<int>* m_dec_NPhotSignal;
        XAMPP::Storage<int>* m_dec_NPhotBase;

        XAMPP::Storage<int>* m_dec_NDiTauBase;
        XAMPP::Storage<int>* m_dec_NDiTauSignal;
    };
}  // namespace XAMPP
#endif
