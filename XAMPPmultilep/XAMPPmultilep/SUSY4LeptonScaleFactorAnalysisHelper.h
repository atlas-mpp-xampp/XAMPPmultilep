#ifndef XAMPPmultilep_SUSY4LeptonScaleFactorAnalysisHelper_H
#define XAMPPmultilep_SUSY4LeptonScaleFactorAnalysisHelper_H

#include <XAMPPmultilep/SUSY4LeptonAnalysisHelper.h>

namespace XAMPP {

    class SUSY4LeptonScaleFactorAnalysisHelper : public SUSY4LeptonAnalysisHelper {
    public:
        // Create a proper constructor for Athena
        ASG_TOOL_CLASS(SUSY4LeptonScaleFactorAnalysisHelper, XAMPP::IAnalysisHelper)

        SUSY4LeptonScaleFactorAnalysisHelper(const std::string& myname);
        virtual ~SUSY4LeptonScaleFactorAnalysisHelper();
        // virtual StatusCode RemoveOverlap();

    protected:
        virtual StatusCode initializeEventVariables();

        auto initializeDiParticleVars(const std::string& prefix) -> StatusCode;

        virtual StatusCode ComputeEventVariables();

    private:
        StatusCode LeptonicTopReconstruction();
    };
}  // namespace XAMPP
#endif
