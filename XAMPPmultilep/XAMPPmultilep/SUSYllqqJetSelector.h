#ifndef XAMPPmultilep_SUSYllqqJetSelector_H
#define XAMPPmultilep_SUSYllqqJetSelector_H

#include <InDetTrackSelectionTool/IInDetTrackSelectionTool.h>
#include <XAMPPbase/SUSYJetSelector.h>

class IBTaggingEfficiencyTool;
class IBTaggingSelectionTool;
namespace XAMPP {
    class IReconstructedParticles;
    class SUSYllqqJetSelector : public SUSYJetSelector {
    public:
        ASG_TOOL_CLASS(SUSYllqqJetSelector, XAMPP::IJetSelector)

        SUSYllqqJetSelector(const std::string& myname);
        virtual ~SUSYllqqJetSelector();

        virtual StatusCode LoadContainers();
        virtual StatusCode initialize();
        virtual StatusCode InitialFill(const CP::SystematicSet& systset);
        virtual StatusCode FillJets(const CP::SystematicSet& systset);

        virtual StatusCode SaveScaleFactor();

    private:
        // The charge of a jet can be calculated as the pt-weighted sum of the track charges
        // https://arxiv.org/pdf/1209.2421.pdf
        // https://cds.cern.ch/record/2255823/files/ATL-PHYS-PROC-2017-017.pdf
        float CalculateCharge(const xAOD::Jet* jet, float Kappa) const;

        float CalculatePtWeightedCharge(const xAOD::Jet* jet, float Kappa = 1., unsigned int MaxTrk = -1) const;
        float CalculateAngularWeightedCharge(const xAOD::Jet* jet, float Kappa = 1., unsigned int MaxTrk = -1) const;
        float CalculateEnergyWeightedCharge(const xAOD::Jet* jet, float Kappa = 1., unsigned int MaxTrk = -1) const;

        xAOD::JetContainer* m_PreTrkJets02;
        std::string m_TrkJet02Key;

        xAOD::JetContainer* m_PreFatJets10;
        std::string m_FatJetKey10;
        unsigned int m_JetChargeToUse;
        asg::AnaToolHandle<InDet::IInDetTrackSelectionTool> m_TrkSelTool;
        asg::AnaToolHandle<XAMPP::IReconstructedParticles> m_ParticleConstructor;
        void SaveTrackFraction(xAOD::Jet* J, xAOD::TrackParticleContainer& Container, unsigned int Trk);

        ToolHandle<IBTaggingEfficiencyTool> m_BtagSFTool_WP77;
        ToolHandle<IBTaggingSelectionTool> m_BtagSelTool_WP77;

        std::map<const CP::SystematicSet*, std::shared_ptr<JetWeightDecorator>> m_BtagSFs;

        std::string m_BTagWP77_decoration;
        std::shared_ptr<JetDecorations> m_jet_wp77_decors;
    };
}  // namespace XAMPP
#endif
