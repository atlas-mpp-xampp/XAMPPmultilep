#ifndef SUSY4LeptonAnalysisConfig_h
#define SUSY4LeptonAnalysisConfig_h
#include <XAMPPbase/AnalysisConfig.h>

namespace XAMPP {
    class SUSY4LeptonAnalysisConfig : public AnalysisConfig {
    public:
        ASG_TOOL_CLASS(SUSY4LeptonAnalysisConfig, XAMPP::IAnalysisConfig)

        SUSY4LeptonAnalysisConfig(const std::string& Analysis);
        virtual ~SUSY4LeptonAnalysisConfig() = default;

    protected:
        virtual StatusCode initializeCustomCuts();
        bool m_SkimEvents;
        bool m_TriggerSkimming;
    };
    class SUSY4LepTruthAnalysisConfig : public TruthAnalysisConfig {
    public:
        ASG_TOOL_CLASS(SUSY4LepTruthAnalysisConfig, XAMPP::IAnalysisConfig)

        SUSY4LepTruthAnalysisConfig(const std::string& Analysis);
        virtual ~SUSY4LepTruthAnalysisConfig() = default;

    protected:
        virtual StatusCode initializeCustomCuts();
        bool m_skim;
    };
}  // namespace XAMPP
#endif
