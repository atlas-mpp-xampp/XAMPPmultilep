#ifndef SUSYttYAnalysisHelper_H
#define SUSYttYAnalysisHelper_H

#include <XAMPPmultilep/SUSY4LeptonAnalysisHelper.h>

namespace XAMPP {
    class SUSYttYAnalysisHelper : public SUSY4LeptonAnalysisHelper {
    public:
        ASG_TOOL_CLASS(SUSYttYAnalysisHelper, XAMPP::IAnalysisHelper)

        SUSYttYAnalysisHelper(const std::string& myname);
        virtual ~SUSYttYAnalysisHelper();
        virtual StatusCode initialize();
        virtual unsigned int finalState();

    protected:
        virtual StatusCode initializeEventVariables();
        virtual StatusCode ComputeEventVariables();
        virtual bool AcceptEvent();
        bool IsMEPhoton(const xAOD::Photon* Phot, float MinPt = 15.e3) const;
        bool IsMEPhoton(const xAOD::TruthParticle* Phot, float MinPt = 15.e3) const;

        virtual StatusCode initPhotonTree();
        virtual StatusCode initTruthTree();

    private:
        float m_MEphotonPtCut;
        std::vector<int> m_MEPhotonSamples;
        std::vector<int> m_FSRPhotonSamples;

        bool m_SkimMECut;
    };
}  // namespace XAMPP
#endif
