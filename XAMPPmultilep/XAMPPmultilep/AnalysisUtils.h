#ifndef XAMPPmultilep_AnalysisUtils_H
#define XAMPPmultilep_AnalysisUtils_H

#include <TLorentzVector.h>
#include <XAMPPbase/AnalysisUtils.h>
namespace CP {
    class IsolationWP;
}

namespace XAMPP {

    enum LLE_Decay {
        Unknown = 0,
        ElElmu,
        ElMuel,  // 121
        ElMumu,
        MuMuel,  // 122
        ElTamu,
        MuTael,  // 123
        ElElta,
        ElTael,  // 131
        ElMuta,  // MuTael //132
        ElTata,
        TaTael,  // 133
        // ElMuta,ElTamu, //231
        MuMuta,
        MuTamu,  // 232
        MuTata,
        TaTamu  // 233
    };

    /*! boost*/
    TVector3 GetRestBoost(const xAOD::IParticle& Part);
    TVector3 GetRestBoost(const xAOD::IParticle* Part);
    TVector3 GetRestBoost(TLorentzVector Vec);

    int GetParentPdgId(const xAOD::TruthParticle* P);

    /*! invariant mass */
    float InvariantMass(const xAOD::IParticleContainer* pc);
    float InvariantMass(const xAOD::IParticleContainer* pc, std::initializer_list<long unsigned int> entries);
    float CombinedM(const xAOD::IParticleContainer* pc, std::initializer_list<long unsigned int> entries);

    /*! eta */
    float CombinedEta(const xAOD::IParticleContainer* pc, std::initializer_list<long unsigned int> entries);

    /*! phi */
    float CombinedPhi(const xAOD::IParticleContainer* pc, std::initializer_list<long unsigned int> entries);

    /*! energy */
    float CombinedE(const xAOD::IParticleContainer* pc, std::initializer_list<long unsigned int> entries);

    /*! pt */
    float CombinedPt(const xAOD::IParticleContainer* pc, std::initializer_list<long unsigned int> entries);

    /*! combined p4 */
    TLorentzVector CombinedVector(const xAOD::IParticleContainer* pc);
    TLorentzVector CombinedVector(const xAOD::IParticleContainer* pc, std::initializer_list<long unsigned int> entries);

    /*! charge */
    float ChargeProduct(const xAOD::IParticleContainer* pc, std::initializer_list<long unsigned int> entries);

    /*! distances */
    float DeltaR(const xAOD::IParticleContainer* pc, long unsigned int i, long unsigned int j);
    float DeltaEta(const xAOD::IParticleContainer* pc, long unsigned int i, long unsigned int j);
    float DeltaPhi(const xAOD::IParticleContainer* pc, long unsigned int i, long unsigned int j);
    float DeltaPhiNorm(const xAOD::IParticleContainer* pc, long unsigned int i, long unsigned int j);

    /*! W reconstruction */
    bool LeptonicWReconstruction(xAOD::IParticleContainer& Leptons, XAMPP::Storage<xAOD::MissingET*>* dec_MET,
                                 XAMPP::IReconstructedParticles* Constructor, std::function<bool(const xAOD::IParticle*)> signalFunc);
    bool LeptonicWReconstruction(xAOD::IParticleContainer* Leptons, XAMPP::Storage<xAOD::MissingET*>* dec_MET,
                                 XAMPP::IReconstructedParticles* Constructor, std::function<bool(const xAOD::IParticle*)> signalFunc);

    bool LeptonicWReconstruction(const xAOD::IParticle* Lep, XAMPP::Storage<xAOD::MissingET*>* dec_MET,
                                 XAMPP::IReconstructedParticles* Constructor, std::function<bool(const xAOD::IParticle*)> signalFunc,
                                 unsigned int pos_in_container);

    xAOD::Particle* ConstructNeutrino(xAOD::MissingET* Met, float Pz, XAMPP::IReconstructedParticles* Constructor);

    xAOD::Particle* ConstructNeutrino(xAOD::MissingET* Met, float Pz, asg::AnaToolHandle<XAMPP::IReconstructedParticles>& Constructor,
                                      std::function<bool(const xAOD::IParticle*)> signalFunc);

    bool LeptonicWReconstruction(xAOD::IParticleContainer& Leptons, XAMPP::Storage<xAOD::MissingET*>* dec_MET,
                                 asg::AnaToolHandle<XAMPP::IReconstructedParticles>& Constructor,
                                 std::function<bool(const xAOD::IParticle*)> signalFunc);
    bool LeptonicWReconstruction(xAOD::IParticleContainer* Leptons, XAMPP::Storage<xAOD::MissingET*>* dec_MET,
                                 asg::AnaToolHandle<XAMPP::IReconstructedParticles>& Constructor,
                                 std::function<bool(const xAOD::IParticle*)> signalFunc);

    void FillWMomentum(const xAOD::IParticle* Lep, xAOD::Particle& Neut, xAOD::Particle* WCand,
                       std::function<bool(const xAOD::IParticle*)> signalFunc);

    void GetParentPdgId(const xAOD::TruthParticleContainer* Container);
    bool isZCandidate(const xAOD::Particle* ZCand);

    void GetHadronicChildren(const xAOD::TruthParticle* Boson, xAOD::TruthParticleContainer& ContToPush);

    std::vector<std::string> getIsolationCones(const std::vector<CP::IsolationWP*>& WPs);

    // RPV-LLE Decay Functions
    LLE_Decay GetFinalRPVState(const xAOD::TruthParticle* LSP);
    int GetLLE_RPVCoupling(const xAOD::TruthParticle* LSP);

}  // namespace XAMPP
#endif
