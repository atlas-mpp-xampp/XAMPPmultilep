#ifndef SUSYttYPhotonSelector_H
#define SUSYttYPhotonSelector_H

#include <XAMPPbase/SUSYPhotonSelector.h>
namespace XAMPP {
    class SUSYttYPhotonSelector : public SUSYPhotonSelector {
    public:
        ASG_TOOL_CLASS(SUSYttYPhotonSelector, XAMPP::IPhotonSelector)

        SUSYttYPhotonSelector(std::string myname);
        virtual ~SUSYttYPhotonSelector();

        virtual StatusCode FillPhotons(const CP::SystematicSet& systset);

    protected:
        void IsMEPhoton(xAOD::Photon* Ph);
    };
}  // namespace XAMPP
#endif
