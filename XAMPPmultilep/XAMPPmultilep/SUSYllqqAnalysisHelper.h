#ifndef XAMPPmultilep_SUSYLLqqYAnalysisHelper_H
#define XAMPPmultilep_SUSYLLqqYAnalysisHelper_H

#include <XAMPPmultilep/SUSY4LeptonAnalysisHelper.h>

namespace XAMPP {
    class SUSYllqqAnalysisHelper : public SUSY4LeptonAnalysisHelper {
    public:
        // Create a proper constructor for Athena
        ASG_TOOL_CLASS(SUSYllqqAnalysisHelper, XAMPP::IAnalysisHelper)

        SUSYllqqAnalysisHelper(const std::string& myname);
        virtual ~SUSYllqqAnalysisHelper();
        virtual StatusCode RemoveOverlap();

    protected:
        virtual StatusCode initializeEventVariables();
        virtual StatusCode ComputeEventVariables();
        virtual StatusCode initJetTree();
        virtual StatusCode initTruthTree();

    private:
        StatusCode FillParticleTrees();
    };
}  // namespace XAMPP
#endif
