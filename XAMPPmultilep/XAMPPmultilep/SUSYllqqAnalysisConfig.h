#ifndef XAMPPmultilep_SUSYllqqYAnalysisConfig_h
#define XAMPPmultilep_SUSYllqqYAnalysisConfig_h

#include <XAMPPbase/AnalysisConfig.h>
namespace XAMPP {
    class SUSYllqqAnalysisConfig : public AnalysisConfig {
    public:
        // Create a proper constructor for Athena
        ASG_TOOL_CLASS(SUSYllqqAnalysisConfig, XAMPP::IAnalysisConfig)

        SUSYllqqAnalysisConfig(std::string Analysis = "LLqq");
        virtual ~SUSYllqqAnalysisConfig() {}

    protected:
        virtual StatusCode initializeCustomCuts();
    };
}  // namespace XAMPP
#endif
