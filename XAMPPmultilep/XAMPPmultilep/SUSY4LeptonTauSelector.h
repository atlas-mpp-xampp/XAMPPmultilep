#ifndef SUSY4LeptonTauSelector_H
#define SUSY4LeptonTauSelector_H

#include <XAMPPbase/SUSYTauSelector.h>
namespace TauAnalysisTools {
    class ITauSelectionTool;
}
namespace XAMPP {
    class IJetSelector;
    class SUSY4LeptonTauSelector : public SUSYTauSelector {
    public:
        // Create a proper constructor for Athena
        ASG_TOOL_CLASS(SUSY4LeptonTauSelector, XAMPP::ITauSelector)

        SUSY4LeptonTauSelector(const std::string& myname);
        virtual ~SUSY4LeptonTauSelector();
        virtual StatusCode initialize();

        virtual StatusCode InitialFill(const CP::SystematicSet& systset);

    protected:
        ToolHandle<XAMPP::IJetSelector> m_jet_selection;
    };
}  // namespace XAMPP
#endif
